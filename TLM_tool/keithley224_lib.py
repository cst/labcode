import pyvisa

class keithley224(object):
    """
    Control Class for the Keithley 224
    """
    def __init__(self,address="GPIB0::15::INSTR"):
        """
        Parameters
        ----------
        address: The GPIBO address is to be found at the I/O monitor 

        Atrributes
        ---------
        rm: pyvisa resource manager to connect to instrument(s)
        inst : an object to control the keithley 224

        Help
        ---
        1) Set instrument timeout to NONE to prevent disconnections
        2) Use control_ren for with the following codes for remote operation
        
        VI_GPIB_REN_DEASSERT         = 0
        VI_GPIB_REN_ASSERT           = 1
        VI_GPIB_REN_DEASSERT_GTL     = 2
        VI_GPIB_REN_ASSERT_ADDRESS   = 3
        VI_GPIB_REN_ASSERT_LLO       = 4
        VI_GPIB_REN_ASSERT_ADDRESS_LLO = 5
        VI_GPIB_REN_ADDRESS_GTL      = 6
        
        """
        self.address = address
        self.rm = pyvisa.ResourceManager()
        self.inst = self.rm.open_resource(address)
        self.inst.timeout = None
        self.inst.control_ren(5) #local lockout blocks the tool
        
        self.inst.clear() #reset the device
        self.operate = False
        self.current = 0
        self.voltage = 3
        self.time = 50e-3

    def display(self,number):
        """
        Change the front display panel
        
        Parameters
        ---------
        number : 0-2
    
        Help
        ----
        D0 = Source
        D1 = Voltage limit
        D2 = Dwell time
        """
        self.inst.write(f"D{number}X")

    
    def set_operation(self,number):
        """
        Use this function for standby or operate mode
        
        Parameters
        ----------
        Number:0(Standby) or 1(Operate)
        """
        self.inst.write(f"F{number}X")
    
    def set_range(self,number):
        """
        Sets the tool range
        
        Parameters
        ---------
        number:0 and 5-9
        
        Ranges:
        R0 = Auto Range (Force Most Significant number)
        R5 = Full Scale: 20uA 2.0E-5
        R6 = Full Scale: 200uA 2.0E-4
        R7 = Full Scale: 2mA 2.0E-3
        R8 = Full Scale: 20mA 2.0E-2
        R9 = Full Scale : 101mA 1.01E-1
        """
        self.inst.write(f"R{number}X")

    def set_current(self,value):
        """
        Sets the Current Source Output Value

        Parameters:
        value: float 
        min -101mA
        max = 101mA
        """

        sci_value = "{:.4E}".format(value) #according to tool manual for current
        self.inst.write(f"I{sci_value}X")
        self.current = value

    def set_voltage_limit(self,value):
        """
        Sets the Voltage Limit(Compliance)

        Parameters
        ---------
        value: float 
        min: 1V
        max : 105V
        """
        sci_value=  "{:.2E}".format(value) #manual format for voltage
        self.inst.write(f"V{sci_value}X")
        self.voltage = value

    def set_time(self,value):
        """
        Sets the Dwell Time
        
        Parameters
        ----------
        value: float
        limits: 50msec to 999msec (1msec steps)
        """
        sci_value = "{:.3E}".format(value) # manual format for dwell time
        self.inst.write(f"W{sci_value}X")
        self.time = value

    def __del__(self):
        """
        Destructor: Disconnect Computer from Instrument
        """
        self.inst.control_ren(0) # Return to local Mode see codes above
        self.inst.close()
        
    def read_parameters(self):
        """
        Sends the setted data back to the software (Without prefix) current-voltage-wait_time
        """
        str_response=self.inst.query("G1X")
        str_response= str_response.strip()
        values = str_response.split(',')
        
        I = float(values[0])
        V = float(values[1])
        W = float(values[2])

        return I,V,W

    def change_operation_mode(self):
        """
        Changes the operation mode
        Standby -> On
        On -> Standby
        """
        if self.operate == True:
            self.set_operation(0)
            self.operate = False
        else:
            self.set_operation(1)
            self.operate = True
        
    
    
        
     




    
        
        
        
    
import matplotlib.pyplot as plt
import numpy as np
import time
from datetime import datetime

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox
import copy

import pandas as pd

#Get dataframe from results
def get_dataframe_from_results(dictionary):
    # creating a shallow copy
    dictionary_copy = copy.copy(dictionary)
    for old_key in dictionary_copy.keys():
        if old_key[0]=='I':
            new_key = old_key+"/A"
        else: #V
            new_key = old_key + "/V"
        dictionary[new_key] = dictionary.pop(old_key)
   
    df = pd.DataFrame(dictionary)
    return df
def number_of_points(dict):
    try:
        diff = dict['stop'].value - dict['start'].value
        ratio = abs(diff/dict['step'].value)
        points = int(ratio+1)
    
    except ZeroDivisionError:
        points = 1

    #the programm crashed because for secondary step we had no problem setting start = stop and then it was dividing by zero
    
    if points>128:
        points = 128
    return points

def check_values(dictionary,function):
    valid = True

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    if function =='primary':
        if abs(dictionary['step'].value) > abs(dictionary['stop'].value-dictionary['start'].value) or dictionary['step'].value==0:#invalid parameter setting 
            valid = False
            tkinter.messagebox.showerror(message="Invalid parameter setting!")

        if dictionary['start'].value<dictionary['step'].value and dictionary['step'].value<0: #change polarity
            dictionary['step'].value =(-1)*dictionary['step'].value

        elif dictionary['start'].value>dictionary['stop'].value and dictionary['step'].value>0:
            dictionary['step'].value = (-1)*dictionary['step'].value

        else:
            pass
    
    if function == 'secondary':
        if dictionary['start'].value == dictionary['stop'].value:
            pass
        elif abs(dictionary['step'].value) > abs(dictionary['stop'].value-dictionary['start'].value) or dictionary['step'].value==0:#invalid parameter setting 
            valid = False
            tkinter.messagebox.showerror(message="Invalid parameter setting!")
        if dictionary['start'].value<dictionary['step'].value and dictionary['step'].value<0: #change polarity
            dictionary['step'].value =(-1)*dictionary['step'].value

        elif dictionary['start'].value>dictionary['stop'].value and dictionary['step'].value>0:
            dictionary['step'].value = (-1)*dictionary['step'].value

    if function == 'synchronous':
        pass
    
    if valid == True:
        #check compliance
        comp = dictionary['comp'].value
        start = dictionary['start'].value
        stop = dictionary['stop'].value

        if abs(comp)*max(abs(start),abs(stop))>2:
            dictionary["comp"].value=np.sign(comp)*2/max(abs(start),abs(stop))
        
    root.destroy()
    return valid 

def add_widgets_to_list(source_dictionary,target_list):
    for widget in source_dictionary.values():
        target_list.append(widget)

def change_state(widgets_list):
    for widget in widgets_list:
        widget.disabled = not widget.disabled

def enable_widgets(widgets_list):
    for widget in widgets_list:
        widget.disabled = False

def disable_widgets(widgets_list):
    for widget in widgets_list:
        widget.disabled = True

def information_box(information):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #display meaagebox
    tkinter.messagebox.showinfo(message=information)
    root.destroy()

def error_box(information):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #display meaagebox
    tkinter.messagebox.showerror(message=information)
    root.destroy()

#normalization factor to is for both normalizations 10**6/width mA/mm = uA/um = 10**(-6)A/um (returned from the tool)
def normalization_factor(width):
    factor = 10**6/width
    return factor


def save_as_ini(default_filename):
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".ini", filetypes=[("Ini files","*.ini")],title = "save as ini",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".ini") == False:
        #open again filedialog with error message box
        answer=tk.messagebox.askyesno(message='Do you want to cancel the ini file Save?')
        if answer == True:
            raise Exception("Ini File Operation aborted!")
        else:
            file = filedialog.asksaveasfilename(defaultextension=".ini", filetypes=[("Ini files","*.ini")],title = "save as ini",initialfile =default_filename)
    root.destroy()
    return file

def load_ini():
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    
    file = filedialog.askopenfilename(filetypes=[("Ini files","*.ini")],title ='Select ini file')
    while file.endswith(".ini") == False:
        #open again filedialog with error message box
        answer=tk.messagebox.askyesno(message='Do you want to cancel the ini file load?')
        if answer == True:
            raise Exception("Ini File Operation aborted!")
        else:
            file = filedialog.askopenfilename(filetypes=[("Ini files","*.ini")],title = "Select ini file")
    root.destroy()
    return file

# function to return ratio and offset for synchronous sweep measurement
def calculate_line(VTG,VBG):
    ratio = (VBG['stop'].value-VBG['start'].value)/(VTG['stop'].value-VTG['start'].value)
    offset = VBG['start'].value-ratio*VTG['start'].value    
    return ratio,offset



#check if smu configuration has a double value
def check_configuration(smu_map:dict):
    #convert the dictionaries values to a list
    
    map_list = []
    for element in smu_map.values():
        map_list.append(element.value)

    #remove the duplicates by using a set
    map_set = set(map_list)

    if len(map_set)!= len(map_list): #we have duplicates
        raise Exception("You cannot assign a smu to multiple contacts")



# The function for graph in the tool
def graph_tool(params,device):
    device.delete_axis("X")
    device.delete_axis("Y1")
    device.delete_axis("Y2")
    device.del_user_functions()

    device.clear_error_stack()

    # How to define user functions correctly and not multiple times
    plot_list = [params["PLOT"]["x"],params["PLOT"]["y1"],params["PLOT"]["y2"]]
    plot_set= set(plot_list)

    
    #define the no values
    for element in plot_set:
        if element != "None":
            if element.endswith("mm"): #only I values
                #define the respective user function
                device.user_function(element,"mA/mm",f"{element[:-2]}*{normalization_factor(params['SAMPLE']['width'])}") #max 3
    
            #define the absolute(A to indicate absolute) always 3 (max 6!)
            if element.startswith('I') and element.endswith('mm'):
                device.user_function('A'+element,"mA/mm",f"ABS({element})")
            elif element.startswith('I') and element.endswith('mm')== False: 
                device.user_function('A'+element,"A",f"ABS({element})")  
             

    # Now send the parameters in the tool
    device.display_variable('X',params["PLOT"]["x"])
    device.error_occured()
    device.axis_scale('X','LIN')
    device.error_occured()
    device.display_variable_min_max('X','MIN',params["PLOT"]["x_min"])
    device.error_occured()
    device.display_variable_min_max('X','MAX',params["PLOT"]["x_max"])
    device.error_occured()


    if params["PLOT"]["y1_scale"]=='LOG':
        device.display_variable('Y1',"A"+params["PLOT"]["y1"])
    else:
        device.display_variable('Y1',params["PLOT"]["y1"])
    device.error_occured()
    device.axis_scale('Y1',params["PLOT"]["y1_scale"])
    device.error_occured()
    device.display_variable_min_max('Y1','MIN',params["PLOT"]["y1_min"])
    device.error_occured()
    device.display_variable_min_max('Y1','MAX',params["PLOT"]["y1_max"])
    device.error_occured()

    if params["PLOT"]["y2"]!= "None":
        if params["PLOT"]["y2_scale"]=='LOG':
            device.display_variable('Y2',"A"+params["PLOT"]["y2"])
        else:
            device.display_variable('Y2',params["PLOT"]["y2"])
        device.error_occured()
        device.axis_scale('Y2',params["PLOT"]["y2_scale"])
        device.error_occured()
        device.display_variable_min_max('Y2','MIN',params["PLOT"]["y2_min"])
        device.display_variable_min_max('Y2','MAX',params["PLOT"]["y2_max"])
        device.error_occured()



#plot software functions
def values_to_plot(params,device):
    #plotted variables as they are named in the tool
    #return the plotted data for easier configuration
    plotted_variables = {'X':device.get_axis_variable('X'),'Y1': device.get_axis_variable('Y1')}

    if params["PLOT"]["y2"]!= "None":
        plotted_variables['Y2']= device.get_axis_variable('Y2')

    plot_values ={}
    for axis,variable in plotted_variables.items():
        plot_values.setdefault(axis,np.array(device.return_values(variable)))

    return plot_values

def set_axes_labels(plot_list):
    axes_labels=[]
    
    # define the axes labels similarly to the user functions
    for element in plot_list:
        if element != "None": #only the last one
            if element.startswith("V"):
                label = f"{element} (V)"
            elif element.startswith('I') and element.endswith('mm'):
                label = f"{element[:-2]} (uA/um)"
            else: # regular I
                 label = f"{element} (A)"
            axes_labels.append(label)
    
    return axes_labels

    
    

    

    

   
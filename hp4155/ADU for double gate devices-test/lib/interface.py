import ipywidgets as widgets
from ipywidgets import GridspecLayout,Layout
from IPython.display import clear_output
import sys
import os

width = "50%"
height = 'auto'
style = {'description_width': 'initial'}
floatbox_width = "80%"

def header(name,integration):
    style = {'description_width': 'initial'}
    options_integration=["SHORt","MEDium","LONG"]

    check=widgets.Checkbox(
        description = name,
        value = True,
        indent = False
    )
    integration= widgets.Dropdown(
        options=options_integration,
        value=integration,description='Integration Time',
        style =style,
        layout=Layout(height='auto', width="30%")
    )

    select =widgets.Dropdown(
        options = ['VTG','VBG',"BOTH"],
        description = 'Sweeping Gates:',
        value ='BOTH',
        style=  {'description_width': 'initial'}
    )
    
    return check, integration ,select


def primary(name,start,step,stop,comp):
    primary_grid = GridspecLayout(4,4)
    primary_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))
    primary_grid[:,3].style.font_weight = 'bold'


    #first line
    primary_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    primary_grid[0,1]=widgets.Label("Step(V)",layout=Layout(height='auto', width='auto'))
    primary_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    primary_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))
    primary_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width=floatbox_width))
    primary_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))

    #third line 
    primary_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    primary_grid[2,1] =widgets.Label("Power Compliance(W)(0=OFF)",layout=Layout(height='auto', width='auto'))#mind the gap
    primary_grid[2,2] =widgets.Label("Hysterisis",layout=Layout(height='auto', width='auto'))#mind the gap

    #fourth line
    primary_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width=floatbox_width))
    primary_grid[3,1]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width=floatbox_width))#mind the gap
    primary_grid[3,2]=widgets.Dropdown(options=['SINGle','DOUBle'],value='SINGle',layout=Layout(height='auto', width=floatbox_width))#mind the gap


    parameters = {
        'start': primary_grid[1,0],
        'step': primary_grid[1,1],
        'stop': primary_grid[1,2],
        'comp': primary_grid[3,0],
        'hyst':primary_grid[3,2],
        'pcomp':primary_grid[3,1]
    }
    return primary_grid,parameters

def secondary(name,start,step,stop,comp):
    secondary_grid = GridspecLayout(4,4)
    secondary_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))
    secondary_grid[:,3].style.font_weight = 'bold'

    #first line
    secondary_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,1]=widgets.Label("Step(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    secondary_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))

    #third line 
    secondary_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    secondary_grid[2,2] =widgets.Label("Power Compliance(W)(0=OFF)",layout=Layout(height='auto', width='auto'))#mind the gap

    #fourth line
    secondary_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[3,2]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width=floatbox_width))#mind the gap

    parameters = {
        'start': secondary_grid[1,0],
        'step': secondary_grid[1,1],
        'stop':secondary_grid[1,2],
        'comp':secondary_grid[3,0],
        'pcomp':secondary_grid[3,2]
    }
    

    return secondary_grid,parameters


def information_box_new():
    width = '90%'
    sample_information=widgets.Label("Sample Information",layout=Layout(height=height, width='50%'))
    sample_information.style.font_weight='bold'
    information_grid=GridspecLayout(3,2)
    
    for i in range(3):
        for j in range(2):
            if i ==2 and j == 1:
                information_grid[i,j]=widgets.Checkbox(value = True,indent = False)
            elif i == 2 and j == 0:                
                information_grid[i,j]=widgets.BoundedFloatText(
                    value=100,
                    min=1e-3,
                    max=sys.float_info.max,step=1,
                    layout=Layout(height=height, width=width)
                )
            else:
                information_grid[i,j]=widgets.Text(layout=Layout(height=height, width=width))

    information_grid[0,0].description = "Processing-Nr:"
    information_grid[1,0].description = "Sample:"
    information_grid[2,0].description = "Device Width(um):"
    information_grid[0,1].description = "Field(XYY):"
    information_grid[1,1].description = "Device:"
    information_grid[2,1].description ='Save Plots'

    for i in range(3):
        for j in range(2):
            information_grid[i,j].style = style

    

    config = widgets.Label("SMU Configuration",layout=Layout(height='auto', width='auto'))

    top_gate = widgets.Dropdown(options=[1,2,3,4],value=1,layout=Layout(height='auto', width='auto'),description = 'Top Gate:',style = style)
    drain = widgets.Dropdown(options=[1,2,3,4],value=2,layout=Layout(height='auto', width='auto'),description = 'Drain:',style = style)
    back_gate = widgets.Dropdown(options=[1,2,3,4],value=3,layout=Layout(height='auto', width='auto'),description = 'Back Gate:',style = style)
    source = widgets.Dropdown(options=[1,2,3,4],value=4,layout=Layout(height='auto', width='auto'),description = 'Source(Ground):',style = style)


    vbox2 = widgets.VBox([config,top_gate,drain,back_gate,source])
    vbox1=widgets.VBox([sample_information,information_grid])
    display(widgets.HBox([vbox1,vbox2]))

    information = {
        'processing_number': information_grid[0,0],
        'sample' : information_grid[1,0],
        'field': information_grid[0,1],
        'device':information_grid[1,1],    
        'width': information_grid[2,0],
        'save_fig':information_grid[2,1]
    }
    
    smu_assign ={
        'TG':top_gate,
        'D':drain,
        'S':source,
        'BG':back_gate
    }
    
    return information,smu_assign
        
def synchronous(name,start,step,stop,comp):
    synchronous_grid = GridspecLayout(4,4)
    synchronous_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))
    synchronous_grid[:,3].style.font_weight = 'bold'


    #first line
    synchronous_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    synchronous_grid[0,1]=widgets.Label("Step(V)(Only 1 Gate)",layout=Layout(height='auto', width='auto'),style = style)
    synchronous_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    synchronous_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))
    synchronous_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width=floatbox_width))
    synchronous_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))

    #third line 
    synchronous_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    synchronous_grid[2,1] =widgets.Label("Power Compliance(W)(0=OFF)",layout=Layout(height='auto', width='auto'),style = style)#mind the gap
    synchronous_grid[2,2] =widgets.Label("Hysterisis(Only 1 gate)",layout=Layout(height='auto', width='auto'),style = style)#mind the gap

    #fourth line
    synchronous_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width=floatbox_width))
    synchronous_grid[3,1]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width=floatbox_width))#mind the gap
    synchronous_grid[3,2]=widgets.Dropdown(options=['SINGle','DOUBle'],value='SINGle',layout=Layout(height='auto', width=floatbox_width))#mind the gap


    parameters = {
        'start': synchronous_grid[1,0],
        'stop': synchronous_grid[1,2],
        'comp': synchronous_grid[3,0],
        'pcomp':synchronous_grid[3,1],
        'step': synchronous_grid[1,1],
        'hyst': synchronous_grid[3,2]
    }
    return synchronous_grid,parameters

def additional_secondary(name,start,step,stop,comp):
    secondary_grid = GridspecLayout(4,4)
    secondary_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))
    secondary_grid[:,3].style.font_weight = 'bold'

    #first line
    secondary_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,1]=widgets.Label("Step(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    secondary_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))

    #third line 
    secondary_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    secondary_grid[2,2] =widgets.Label("Power Compliance(W)(0=OFF)(Only 1 Gate)",layout=Layout(height='auto', width='auto'),style = style)#mind the gap

    #fourth line
    secondary_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[3,2]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width=floatbox_width))#mind the gap

    parameters = {
        'start': secondary_grid[1,0],
        'step': secondary_grid[1,1],
        'stop':secondary_grid[1,2],
        'comp':secondary_grid[3,0],
        'pcomp':secondary_grid[3,2]
    }
    return secondary_grid,parameters


def plot_config(meas): #meas = 1,2,3 for transfer,output,gatediode 
    config_grid = GridspecLayout(6,4)

    if meas== 3:
        options_x= ['VTG','VBG']
    else :
        options_x = ['VTG','VBG','VDS']
    
    if meas ==3:
        options = ['ITG','IBG','ITGmm','IBGmm','None']
    else:
        options = ['ITG','IBG','ID','ITGmm','IBGmm','IDmm','None']

    # define the default values (y2 is always empty) taken from the interface
    if meas == 1:# Transfer
        x_name = 'VTG'
        x_scale = 'LIN'
        x_min = -5
        x_max = 5
        y1_name = 'IDmm'
        y1_scale = 'LOG'
        y1_min = 0
        y1_max = 100
    
    elif meas == 2: # outptut
        x_name = 'VDS'
        x_scale = 'LIN'
        x_min = 0
        x_max = 5
        y1_name = 'IDmm'
        y1_scale = 'LIN'
        y1_min = -100
        y1_max = 100

    else: # == 3 gatediode
        x_name = 'VTG'
        x_scale = 'LIN'
        x_min = -5
        x_max = 5
        y1_name = 'ITGmm'
        y1_scale = 'LOG'
        y1_min = 0
        y1_max = 10

    
    config_grid[0,:]=widgets.Label('Send Plotting configurations to the tool',layout=Layout(height='auto', width='auto'))
    #first line headers
    config_grid[1,1]= widgets.Label('X',layout=Layout(height='auto', width='auto'))
    config_grid[1,2]= widgets.Label('Y1',layout=Layout(height='auto', width='auto'))
    config_grid[1,3]= widgets.Label('Y2',layout=Layout(height='auto', width='auto'))

    #first column 
    config_grid[2,0] = widgets.Label('NAME',layout=Layout(height='auto', width='auto'))
    config_grid[3,0] = widgets.Label('SCALE',layout=Layout(height='auto', width='auto'))
    config_grid[4,0] = widgets.Label('MIN',layout=Layout(height='auto', width='auto'))
    config_grid[5,0] = widgets.Label('MAX',layout=Layout(height='auto', width='auto'))

    #iterate through the second line (NAME)
    must_options = [x for x in options if x!='None']
    config_grid[2,1]=widgets.Dropdown(layout=Layout(height='auto', width='auto'),options = options_x,value = x_name)
    config_grid[2,2]=widgets.Dropdown(layout=Layout(height='auto', width='auto'),options = must_options,value = y1_name)
    config_grid[2,3]=widgets.Dropdown(layout=Layout(height='auto', width='auto'),options = options,value = "None")

    #Iterate through the third line (scale) 
    options_scale = ['LIN','LOG']
    config_grid[3,1] = widgets.Dropdown(value = x_scale,options = options_scale, layout=Layout(height='auto', width='auto'),disabled = True)
    config_grid[3,2] = widgets.Dropdown(value = y1_scale,options = options_scale, layout=Layout(height='auto', width='auto'))
    config_grid[3,3] = widgets.Dropdown(value = 'LIN',options = options_scale, layout=Layout(height='auto', width='auto'))

    #iterate throuh the last 2 lines(min-max)
   
    config_grid[4,1] = widgets.FloatText(value = x_min,layout=Layout(height='auto', width='auto')) #min
    config_grid[4,2] = widgets.FloatText(value = y1_min,layout=Layout(height='auto', width='auto')) #min
    config_grid[4,3] = widgets.FloatText(value = 0,layout=Layout(height='auto', width='auto')) #min

    config_grid[5,1]=widgets.FloatText(value = x_max,layout=Layout(height='auto', width='auto')) #max X-axis
    config_grid[5,2]=widgets.FloatText(value = y1_max,layout=Layout(height='auto', width='auto')) #max Y1-axis
    config_grid[5,3]=widgets.FloatText(value = 0,layout=Layout(height='auto', width='auto')) #max Y2-axis

    config_dict = {
        "x":config_grid[2,1],
        "y1":config_grid[2,2],
        "y2":config_grid[2,3],
        #"x_scale":config_grid[3,1],
        "y1_scale":config_grid[3,2],
        "y2_scale":config_grid[3,3],
        "x_min":config_grid[4,1],
        "y1_min":config_grid[4,2],
        "y2_min":config_grid[4,3],
        "x_max":config_grid[5,1],
        "y1_max":config_grid[5,2],
        "y2_max":config_grid[5,3],
    }

    return config_grid,config_dict
    

    
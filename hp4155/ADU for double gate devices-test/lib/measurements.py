# New measurements file for ADU

# The new measurements will have the smus configuration as a parameter and written from the interface

import sys
sys.path.insert(0, '..') #append parent directory

import hp4155a
from help import *
from decimal import Decimal

import os

def Setup(device):
    device.reset()

    #setup sweep measurement mode
    device.measurement_mode('SWE')

    #disable all irrelevant units
    device.disable_not_smu()

# Transfer only VTG 
def Transfer_VTG(device,params):
    # calculate normalization factor
    
    norm = normalization_factor(params["SAMPLE"]["width"])
    points = params["VAR2"]["points"]
    try:
        device.setup_smu(params["MAP"]['TG'],params["SMU_T"])
        device.setup_smu(params["MAP"]['D'],params["SMU_D"])
        device.setup_smu(params["MAP"]['BG'],params["SMU_B"])
        device.setup_smu(params["MAP"]['S'],params["SMU_S"])
    
        device.setup_var1(params["VAR1"])
        device.setup_var2(params["VAR2"])
    
        device.integration_time(params["INTEGRATION"])
    
        variables_list =["VTG","ITG","VDS","ID"]
        device.variables_to_save(variables_list)
    
        try:
            plotted_variables = graph_tool(params,device)
        except Exception as e:
            error_box(e)
            return
    
    
        device.single_measurement()
        while device.operation_completed()==False:
            pass
    
        device.autoscaling()
        device.error_occured()
    
        values = dict([(variable,device.return_values(variable)) for variable in variables_list])
        df = get_dataframe_from_results(values)
    except Exception as e:
        error_box(e)
        return

    # Append the normalized current 
    df["IDmm/uA/um"]= (df["ID/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["ITGmm/uA/um"]= (df["ITG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

    # Save the results
    default_filename = f"{params['SAMPLE']['sample']}_{params['SAMPLE']['field']}_{params['SAMPLE']['device']}_TOP_GATE_U.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Transfer Curve at {date}"+"\n")
        f.write(f"Series:{params['SAMPLE']['processing_number']}"+"\n")
        f.write(f"Sample:{params['SAMPLE']['sample']}"+"\n")
        f.write(f"Field:{params['SAMPLE']['field']}"+"\n")
        f.write(f"Device:{params['SAMPLE']['device']}"+"\n")
        f.write(f"Device Width/um:{params['SAMPLE']['width']}"+"\n")
        f.write("Sweeping Gate:VTG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VTG from {params['VAR1']['start']}V to {params['VAR1']['stop']}V with step {params['VAR1']['step']}V"+"\n")
        f.write(f"VDS from {params['VAR2']['start']}V to {params['VAR2']['stop']}V with step {params['VAR2']['step']}V"+"\n")
        
        if params['VAR1']['pcomp']==0:
            f.write(f"Top Gate Current Compliance/A:{params['VAR1']['comp']}"+"\n")
        else:
            f.write(f"Top Gate Power Compliance/A:{params['VAR1']['pcomp']}"+"\n")

        if params['VAR2']['pcomp'] == 0:
            f.write(f"Drain Current Compliance/A:{params['VAR1']['comp']}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{params['VAR1']['pcomp']}"+"\n")
        f.write(f"Integration Time:{params['INTEGRATION']}"+"\n")
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')

    plot_values = values_to_plot(params,device)

    # Plot user specified results
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    plot_list = [params["PLOT"]["x"],params["PLOT"]["y1"],params["PLOT"]["y2"]]
    scale_list =['LIN',params["PLOT"]["y1_scale"],params["PLOT"]["y2_scale"]]
    axes_labels = set_axes_labels(plot_list)

    if scale_list[1]=='LOG':
        ax1.set_yscale('log')

    #now set the labels
    ax1.set_xlabel(axes_labels[0])
    ax1.set_ylabel(axes_labels[1])

    x = np.split(plot_values['X'],points)
    y1 = np.split(plot_values['Y1'],points)
    labels =np.mean(np.array_split(df["VDS/V"],points),axis = 1) # VDS values for labels
    
    for i in range(points):
        ax1.plot(x[i],y1[i],label = f"VDS:{round(labels[i],3)} (V)")
    
    # Adding title
    fig.suptitle('Transfer Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')

    display(fig)
    #save plot if checked
    if params["SAMPLE"]['save_fig'] == True:
        filename= os.path.splitext(file)[0]
        fig.savefig(filename+'_Y1.png')
    
    #add the second axis if applicable
    if plot_list[2]!= "None":
        fig,ax2=plt.subplots(figsize=(10,6),layout='constrained')
        y2 = np.split(plot_values['Y2'],points)
        
        if scale_list[2]=='LOG':
            ax2.set_yscale('log')

        ax2.set_xlabel(axes_labels[0])
         
        ax2.set_ylabel(axes_labels[2])
        for i in range(points):
            ax2.plot(x[i],y2[i],label = f"VDS:{round(labels[i],3)} (V)")
        
        # Adding title
        fig.suptitle('Transfer Curve', fontweight ="bold")
        fig.legend(loc='outside right upper')
    
        display(fig)
        #save plot if checked
        if params["SAMPLE"]['save_fig'] == True:
            filename= os.path.splitext(file)[0]
            fig.savefig(filename+'_Y2.png')


# Transfer only VBG
def Transfer_VBG(device,params):
    try:
        device.setup_smu(params["MAP"]['TG'],params["SMU_T"])
        device.setup_smu(params["MAP"]['D'],params["SMU_D"])
        device.setup_smu(params["MAP"]['BG'],params["SMU_B"])
        device.setup_smu(params["MAP"]['S'],params["SMU_S"])
      
    
        device.setup_var1(params["VAR1"])
        device.setup_var2(params["VAR2"])
    
        device.integration_time(params["INTEGRATION"])
       
    
        variables_list =["VBG","IBG","VDS","ID"]
        device.variables_to_save(variables_list)
        try:
            plotted_variables = graph_tool(params,device)
        except Exception as e:
            error_box(e)
            return
    
        device.single_measurement()
        while device.operation_completed()==False:
            pass
        device.autoscaling()
        device.error_occured()
    
        values = dict([(variable,device.return_values(variable)) for variable in variables_list])
        df = get_dataframe_from_results(values)
    
    except Exception as e:
        error_box(e)
        return
    # calculate normalization factor
    norm = normalization_factor(params["SAMPLE"]["width"])
    points = params["VAR2"]["points"]

    # Append the normalized current 
    df["IDmm/uA/um"]= (df["ID/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["IBGmm/uA/um"]= (df["IBG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

     # Save the results
    default_filename = f"{params['SAMPLE']['sample']}_{params['SAMPLE']['field']}_{params['SAMPLE']['device']}_BACK_GATE_U.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Transfer Curve at {date}"+"\n")
        f.write(f"Series:{params['SAMPLE']['processing_number']}"+"\n")
        f.write(f"Sample:{params['SAMPLE']['sample']}"+"\n")
        f.write(f"Field:{params['SAMPLE']['field']}"+"\n")
        f.write(f"Device:{params['SAMPLE']['device']}"+"\n")
        f.write(f"Device Width/um:{params['SAMPLE']['width']}"+"\n")
        f.write("Sweeping Gate:VBG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VBG from {params['VAR1']['start']}V to {params['VAR1']['stop']}V with step {params['VAR1']['step']}V"+"\n")
        f.write(f"VDS from {params['VAR2']['start']}V to {params['VAR2']['stop']}V with step {params['VAR2']['step']}V"+"\n")
        
        #calculate the values
        if params['VAR1']['pcomp']==0:
            f.write(f"Back Gate Current Compliance/A:{params['VAR1']['comp']}"+"\n")
        else:
            f.write(f"Back Gate Power Compliance/A:{params['VAR1']['pcomp']}"+"\n")
            
        if params['VAR2']['pcomp'] == 0:
            f.write(f"Drain Current Compliance/A:{params['VAR2']['comp']}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{params['VAR2']['pcomp']}"+"\n")
        
    
        f.write(f"Integration Time:{params['INTEGRATION']}"+"\n")
         
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')

    plot_values = values_to_plot(params,device)

    # Plot user specified results
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    plot_list = [params["PLOT"]["x"],params["PLOT"]["y1"],params["PLOT"]["y2"]]
    scale_list =['LIN',params["PLOT"]["y1_scale"],params["PLOT"]["y2_scale"]]
    axes_labels = set_axes_labels(plot_list)

    if scale_list[1]=='LOG':
        ax1.set_yscale('log')

    #now set the labels
    ax1.set_xlabel(axes_labels[0])
    ax1.set_ylabel(axes_labels[1])

    x = np.split(plot_values['X'],points)
    y1 = np.split(plot_values['Y1'],points)
    labels =np.mean(np.array_split(df["VDS/V"],points),axis = 1) # VDS values for labels
    
    for i in range(points):
        ax1.plot(x[i],y1[i],label = f"VDS:{round(labels[i],3)} (V)")
    
    # Adding title
    fig.suptitle('Transfer Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')

    display(fig)
    #save plot if checked
    if params["SAMPLE"]['save_fig'] == True:
        filename= os.path.splitext(file)[0]
        fig.savefig(filename+'_Y1.png')
    
    #add the second axis if applicable
    if plot_list[2]!= "None":
        fig,ax2=plt.subplots(figsize=(10,6),layout='constrained')
        y2 = np.split(plot_values['Y2'],points)
        
        if scale_list[2]=='LOG':
            ax2.set_yscale('log')

        ax2.set_xlabel(axes_labels[0])
         
        ax2.set_ylabel(axes_labels[2])
        for i in range(points):
            ax2.plot(x[i],y2[i],label = f"VDS:{round(labels[i],3)} (V)")
        
        # Adding title
        fig.suptitle('Transfer Curve', fontweight ="bold")
        fig.legend(loc='outside right upper')
    
        display(fig)
        #save plot if checked
        if params["SAMPLE"]['save_fig'] == True:
            filename= os.path.splitext(file)[0]
            fig.savefig(filename+'_Y2.png')

    
def Transfer_BOTH(device,params):
    try:
        smu_t = device.smu_dict()
        smu_t.update(vname = 'VTG',iname='ITG',mode = 'V',func='VAR1')
       
        smu_b = device.smu_dict()
        smu_b.update(vname = 'VBG',iname='IBG',mode = 'V',func = 'VARD')
    
        device.setup_smu(params["MAP"]['TG'],params["SMU_T"])
        device.setup_smu(params["MAP"]['D'],params["SMU_D"])
        device.setup_smu(params["MAP"]['BG'],params["SMU_B"])
        device.setup_smu(params["MAP"]['S'],params["SMU_S"])
      
    
        device.setup_var1(params["VAR1"])
        device.setup_var2(params["VAR2"])
        device.setup_vard(params["VARD"])
    
        device.integration_time(params["INTEGRATION"])
    
        variables_list =["VBG","IBG","VDS","ID","VTG","ITG"]
        device.variables_to_save(variables_list)
    
        try:
            plotted_variables = graph_tool(params,device)
        except Exception as e:
            error_box(e)
            return
    
    
        device.single_measurement()
        while device.operation_completed()==False:
            pass
    
        device.autoscaling()
        device.error_occured()
    
        values = dict([(variable,device.return_values(variable)) for variable in variables_list])
        df = get_dataframe_from_results(values)
    except Exception as e:
        error_box(e)
        return

    # calculate normalization factor
    norm = normalization_factor(params["SAMPLE"]["width"])
    points = params["VAR2"]["points"]

    # Append the normalized current 
    df["IDmm/uA/um"]= (df["ID/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["IBGmm/uA/um"]= (df["IBG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["ITGmm/uA/um"]= (df["ITG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')


    # Save the results
    default_filename = f"{params['SAMPLE']['sample']}_{params['SAMPLE']['field']}_{params['SAMPLE']['device']}_BOTH_GATES_U.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()
    

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Transfer Curve at {date}"+"\n")
        f.write(f"Series:{params['SAMPLE']['processing_number']}"+"\n")
        f.write(f"Sample:{params['SAMPLE']['sample']}"+"\n")
        f.write(f"Field:{params['SAMPLE']['field']}"+"\n")
        f.write(f"Device:{params['SAMPLE']['device']}"+"\n")
        f.write(f"Device Width/um:{params['SAMPLE']['width']}"+"\n")
        f.write("Sweeping Gates:VBG,VTG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VBG from {params['VARD']['start']}V to {params['VARD']['stop']}V with step {params['VARD']['step']}V"+"\n")
        f.write(f"VTG from {params['VAR1']['start']}V to {params['VAR1']['stop']}V with step {params['VAR1']['step']}V"+"\n")
        f.write(f"VDS from {params['VAR2']['start']}V to {params['VAR2']['stop']}V with step {params['VAR2']['step']}V"+"\n")
        
        #calculate thes
        if params['VARD']['pcomp']==0:
            f.write(f"Back Gate Current Compliance/A:{params['VARD']['comp']}"+"\n")
        else:
            f.write(f"Back Gate Power Compliance/A:{params['VARD']['pcomp']}"+"\n")

        if params['VAR1']['pcomp']==0:
            f.write(f"Top Gate Current Compliance/A:{params['VAR1']['comp']}"+"\n")
        else:
            f.write(f"Top Gate Power Compliance/A:{params['VAR1']['pcomp']}"+"\n")

        if params['VAR2']['pcomp'] == 0:
            f.write(f"Drain Current Compliance/A:{params['VAR2']['comp']}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{params['VAR2']['pcomp']}"+"\n")

        f.write(f"Integration Time:{params['INTEGRATION']}"+"\n")
         
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')
    plot_values = values_to_plot(params,device)

    # Plot user specified results
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    plot_list = [params["PLOT"]["x"],params["PLOT"]["y1"],params["PLOT"]["y2"]]
    scale_list =['LIN',params["PLOT"]["y1_scale"],params["PLOT"]["y2_scale"]]
    axes_labels = set_axes_labels(plot_list)

    if scale_list[1]=='LOG':
        ax1.set_yscale('log')

    #now set the labels
    ax1.set_xlabel(axes_labels[0])
    ax1.set_ylabel(axes_labels[1])

    x = np.split(plot_values['X'],points)
    y1 = np.split(plot_values['Y1'],points)
    labels =np.mean(np.array_split(df["VDS/V"],points),axis = 1) # VDS values for labels
    
    for i in range(points):
        ax1.plot(x[i],y1[i],label = f"VDS:{round(labels[i],3)} (V)")
    
    # Adding title
    fig.suptitle('Transfer Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')

    display(fig)
    #save plot if checked
    if params["SAMPLE"]['save_fig'] == True:
        filename= os.path.splitext(file)[0]
        fig.savefig(filename+'_Y1.png')
    
    #add the second axis if applicable
    if plot_list[2]!= "None":
        fig,ax2=plt.subplots(figsize=(10,6),layout='constrained')
        y2 = np.split(plot_values['Y2'],points)
        
        if scale_list[2]=='LOG':
            ax2.set_yscale('log')

        ax2.set_xlabel(axes_labels[0])
         
        ax2.set_ylabel(axes_labels[2])
        for i in range(points):
            ax2.plot(x[i],y2[i],label = f"VDS:{round(labels[i],3)} (V)")
        
        # Adding title
        fig.suptitle('Transfer Curve', fontweight ="bold")
        fig.legend(loc='outside right upper')
    
        display(fig)
        #save plot if checked
        if params["SAMPLE"]['save_fig'] == True:
            filename= os.path.splitext(file)[0]
            fig.savefig(filename+'Y2.png')

# Output with VTG
def Output_VTG(device,params):
    try:
        device.setup_smu(params['MAP']['TG'],params["SMU_T"])
        device.setup_smu(params['MAP']['D'],params["SMU_D"])
        device.setup_smu(params['MAP']['BG'],params["SMU_B"])
        device.setup_smu(params['MAP']['S'],params["SMU_S"])
        
        device.setup_var1(params["VAR1"])
        device.setup_var2(params["VAR2"])
    
        device.integration_time(params["INTEGRATION"])
    
        variables_list = ['VDS','ID','VTG','ITG']
        device.variables_to_save(variables_list)
    
        try:
            plotted_variables = graph_tool(params,device)
        except Exception as e:
            error_box(e)
            return
    
        device.single_measurement()
        
        while device.operation_completed()==False:
            pass
    
        device.autoscaling()
        device.error_occured()
    
        values = dict([(variable,device.return_values(variable)) for variable in variables_list])
        df = get_dataframe_from_results(values)
    except Exception as e:
        error_box(e)
        return

    
    # plot results
    # calculate normalization factor
    norm = normalization_factor(params['SAMPLE']["width"])
    points = params["VAR2"]["points"]

    # Append the normalized current 
    df["IDmm/uA/um"]= (df["ID/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["ITGmm/uA/um"]= (df["ITG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

    # Save the results
    default_filename = f"{params['SAMPLE']['sample']}_{params['SAMPLE']['field']}_{params['SAMPLE']['device']}_TOP_GATE_A.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Output Curve at {date}"+"\n")
        f.write(f"Series:{params['SAMPLE']['processing_number']}"+"\n")
        f.write(f"Sample:{params['SAMPLE']['sample']}"+"\n")
        f.write(f"Field:{params['SAMPLE']['field']}"+"\n")
        f.write(f"Device:{params['SAMPLE']['device']}"+"\n")
        f.write(f"Device Width/um:{params['SAMPLE']['width']}"+"\n")
        f.write("Sweeping Gate:VTG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VTG from {params['VAR2']['start']}V to {params['VAR2']['stop']}V with step {params['VAR2']['step']}V"+"\n")
        f.write(f"VDS from {params['VAR1']['start']}V to {params['VAR1']['stop']}V with step {params['VAR1']['step']}V"+"\n")
        
        #calculate the values
        if params['VAR2']['pcomp']==0:
            f.write(f"Top Gate Current Compliance/A:{params['VAR2']['comp']}"+"\n")
        else:
            f.write(f"Top Gate Power Compliance/A:{params['VAR2']['pcomp']}"+"\n")

        if params['VAR1']['pcomp'] == 0:
            f.write(f"Drain Current Compliance/A:{params['VAR1']['comp']}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{params['VAR1']['pcomp']}"+"\n")

        f.write(f"Integration Time:{params['INTEGRATION']}"+"\n")
         
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')

    plot_values = values_to_plot(params,device)

    # Plot user specified results
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    plot_list = [params["PLOT"]["x"],params["PLOT"]["y1"],params["PLOT"]["y2"]]
    scale_list =['LIN',params["PLOT"]["y1_scale"],params["PLOT"]["y2_scale"]]
    axes_labels = set_axes_labels(plot_list)

    if scale_list[1]=='LOG':
        ax1.set_yscale('log')

    #now set the labels
    ax1.set_xlabel(axes_labels[0])
    ax1.set_ylabel(axes_labels[1])

    x = np.split(plot_values['X'],points)
    y1 = np.split(plot_values['Y1'],points)
    labels =np.mean(np.array_split(df["VTG/V"],points),axis = 1) # VDS values for labels
    
    for i in range(points):
        ax1.plot(x[i],y1[i],label = f"VTG:{round(labels[i],3)} (V)")
    
    # Adding title
    fig.suptitle('Output Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')

    display(fig)
    #save plot if checked
    if params["SAMPLE"]['save_fig'] == True:
        filename= os.path.splitext(file)[0]
        fig.savefig(filename+'_Y1.png')
    
    #add the second axis if applicable
    if plot_list[2]!= "None":
        fig,ax2=plt.subplots(figsize=(10,6),layout='constrained')
        y2 = np.split(plot_values['Y2'],points)
        
        if scale_list[2]=='LOG':
            ax2.set_yscale('log')

        ax2.set_xlabel(axes_labels[0])
         
        ax2.set_ylabel(axes_labels[2])
        for i in range(points):
            ax2.plot(x[i],y2[i],label = f"VTG:{round(labels[i],3)} (V)")
        
        # Adding title
        fig.suptitle('Output Curve', fontweight ="bold")
        fig.legend(loc='outside right upper')
    
        display(fig)
        #save plot if checked
        if params["SAMPLE"]['save_fig'] == True:
            filename= os.path.splitext(file)[0]
            fig.savefig(filename+'Y2.png')
    

#Output VBG
def Output_VBG(device,params):
    try:
        device.setup_smu(params["MAP"]['TG'],params["SMU_T"])
        device.setup_smu(params["MAP"]['D'],params["SMU_D"])
        device.setup_smu(params["MAP"]['BG'],params["SMU_B"])
        device.setup_smu(params["MAP"]['S'],params["SMU_S"])
    
    
        device.setup_var1(params["VAR1"])
        device.setup_var2(params["VAR2"])
    
        device.integration_time(params["INTEGRATION"])
    
        variables_list = ['VDS','ID','VBG','IBG']
        device.variables_to_save(variables_list)
         
        try:
            plotted_variables = graph_tool(params,device)
        except Exception as e:
            error_box(e)
            return
    
    
        device.single_measurement()
        
        while device.operation_completed()==False:
            pass
        
        device.autoscaling()
        device.error_occured()
        
        values = dict([(variable,device.return_values(variable)) for variable in variables_list])
        df = get_dataframe_from_results(values)
    except Exception as e:
        error_box(e)
        return

    # plot results

    # calculate normalization factor
    norm = normalization_factor(params["SAMPLE"]["width"])
    points = params["VAR2"]["points"]

    # Append the normalized current 
    df["IDmm/uA/um"]= (df["ID/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["IBGmm/uA/um"]= (df["IBG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

    # Save the results
    default_filename = f"{params['SAMPLE']['sample']}_{params['SAMPLE']['field']}_{params['SAMPLE']['device']}_BACK_GATE_A.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Output Curve at {date}"+"\n")
        f.write(f"Series:{params['SAMPLE']['processing_number']}"+"\n")
        f.write(f"Sample:{params['SAMPLE']['sample']}"+"\n")
        f.write(f"Field:{params['SAMPLE']['field']}"+"\n")
        f.write(f"Device:{params['SAMPLE']['device']}"+"\n")
        f.write(f"Device Width/um:{params['SAMPLE']['width']}"+"\n")
        f.write("Sweeping Gate:VBG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VBG from {params['VAR2']['start']}V to {params['VAR2']['stop']}V with step {params['VAR2']['step']}V"+"\n")
        f.write(f"VDS from {params['VAR1']['start']}V to {params['VAR1']['stop']}V with step {params['VAR1']['step']}V"+"\n")
        
        #calculate the values
        if params['VAR2']['pcomp']==0:
            f.write(f"Back Gate Current Compliance/A:{params['VAR2']['comp']}"+"\n")
        else:
            f.write(f"Back Gate Power Compliance/A:{params['VAR2']['pcomp']}"+"\n")

        if params['VAR1']['pcomp'] == 0:
            f.write(f"Drain Current Compliance/A:{params['VAR1']['comp']}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{params['VAR1']['pcomp']}"+"\n")

        f.write(f"Integration Time:{params['INTEGRATION']}"+"\n")
         
        f.write("\nResults\n")
    df.to_csv(file,sep=" ",mode='a')

    plot_values = values_to_plot(params,device)

    # Plot user specified results
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    plot_list = [params["PLOT"]["x"],params["PLOT"]["y1"],params["PLOT"]["y2"]]
    scale_list =['LIN',params["PLOT"]["y1_scale"],params["PLOT"]["y2_scale"]]
    axes_labels = set_axes_labels(plot_list)

    if scale_list[1]=='LOG':
        ax1.set_yscale('log')

    #now set the labels
    ax1.set_xlabel(axes_labels[0])
    ax1.set_ylabel(axes_labels[1])

    x = np.split(plot_values['X'],points)
    y1 = np.split(plot_values['Y1'],points)
    labels =np.mean(np.array_split(df["VBG/V"],points),axis = 1) # VDS values for labels
    
    for i in range(points):
        ax1.plot(x[i],y1[i],label = f"VBG:{round(labels[i],3)} (V)")
    
    # Adding title
    fig.suptitle('Output Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')

    display(fig)
    #save plot if checked
    if params["SAMPLE"]['save_fig'] == True:
        filename= os.path.splitext(file)[0]
        fig.savefig(filename+'_Y1.png')
    
    #add the second axis if applicable
    if plot_list[2]!= "None":
        fig,ax2=plt.subplots(figsize=(10,6),layout='constrained')
        y2 = np.split(plot_values['Y2'],points)
        
        if scale_list[2]=='LOG':
            ax2.set_yscale('log')

        ax2.set_xlabel(axes_labels[0])
         
        ax2.set_ylabel(axes_labels[2])
        for i in range(points):
            ax2.plot(x[i],y2[i],label = f"VBG:{round(labels[i],3)} (V)")
        
        # Adding title
        fig.suptitle('Output Curve', fontweight ="bold")
        fig.legend(loc='outside right upper')
    
        display(fig)
        #save plot if checked
        if params["SAMPLE"]['save_fig'] == True:
            filename= os.path.splitext(file)[0]
            fig.savefig(filename+'Y2.png')
    
# Output both
def Output_BOTH(device,params):
    try:
        device.setup_smu(params["MAP"]['TG'],params["SMU_T"])
        device.setup_smu(params["MAP"]['D'],params["SMU_D"])
        device.setup_smu(params["MAP"]['BG'],params["SMU_B"])
        device.setup_smu(params["MAP"]['S'],params["SMU_S"])
    
    
        device.setup_var1(params["VAR1"])
        device.setup_var2(params["VAR2"])
        
    
        device.integration_time(params["INTEGRATION"])
        try:
            plotted_variables = graph_tool(params,device)
        except Exception as e:
            error_box(e)
            return
    
        
    
        variables_list = ['VDS','ID','VBG','IBG','VTG','ITG']
        device.variables_to_save(variables_list)
    
        for i , value in enumerate(params["VAR3"]['values']):
            cons = device.cons_smu_dict()
            cons.update(comp = params['VAR3']['comp'],value = value)
            device.setup_cons_smu(params['MAP']['BG'],cons)
    
            if i == 0:
                device.single_measurement()
            else:
                device.append_measurement()
        
            while device.operation_completed()==False:
                pass
            device.autoscaling()
            device.error_occured()
    
        values = dict([(variable,device.return_values(variable)) for variable in variables_list])
        df = get_dataframe_from_results(values)
    except Exception as e:
        error_box(e)
        return

    # plot results
    points = params["VAR2"]['points']*params["VAR3"]["points"] # number of curves

    # calculate normalization factor
    norm = normalization_factor(params["SAMPLE"]["width"])

    # Append the normalized current 
    df["IDmm/uA/um"]= (df["ID/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["IBGmm/uA/um"]= (df["IBG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["ITGmm/uA/um"]= (df["ITG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

      # Save the results
    default_filename = f"{params['SAMPLE']['sample']}_{params['SAMPLE']['field']}_{params['SAMPLE']['device']}_BOTH_GATES_A.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Output Curve at {date}"+"\n")
        f.write(f"Series:{params['SAMPLE']['processing_number']}"+"\n")
        f.write(f"Sample:{params['SAMPLE']['sample']}"+"\n")
        f.write(f"Field:{params['SAMPLE']['field']}"+"\n")
        f.write(f"Device:{params['SAMPLE']['device']}"+"\n")
        f.write(f"Device Width/um:{params['SAMPLE']['width']}"+"\n")
        f.write("Sweeping Gates:VBG,VTG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VBG from {params['VAR3']['start']}V to {params['VAR3']['stop']}V with step {params['VAR3']['step']}V"+"\n")
        f.write(f"VTG from {params['VAR2']['start']}V to {params['VAR2']['stop']}V with step {params['VAR2']['step']}V"+"\n")
        f.write(f"VDS from {params['VAR1']['start']}V to {params['VAR1']['stop']}V with step {params['VAR1']['step']}V"+"\n")
      
        
        #calculate the values
        f.write(f"Back Gate Current Compliance/A:{params['VAR3']['comp']}"+"\n")
        
        if params['VAR2']['pcomp']==0:
            f.write(f"Top Gate Current Compliance/A:{params['VAR2']['comp']}"+"\n")
        else:
            f.write(f"Top Gate Power Compliance/A:{params['VAR2']['pcomp']}"+"\n")


        if params['VAR1']['pcomp'] == 0:
            f.write(f"Drain Current Compliance/A:{params['VAR1']['comp']}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{params['VAR1']['pcomp']}"+"\n")

        f.write(f"Integration Time:{params['INTEGRATION']}"+"\n")
         
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')

    plot_values = values_to_plot(params,device)

    # Plot user specified results
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    plot_list = [params["PLOT"]["x"],params["PLOT"]["y1"],params["PLOT"]["y2"]]
    scale_list =['LIN',params["PLOT"]["y1_scale"],params["PLOT"]["y2_scale"]]
    axes_labels = set_axes_labels(plot_list)

    if scale_list[1]=='LOG':
        ax1.set_yscale('log')

    #now set the labels
    ax1.set_xlabel(axes_labels[0])
    ax1.set_ylabel(axes_labels[1])

    x = np.split(plot_values['X'],points)
    y1 = np.split(plot_values['Y1'],points)
    labels_VTG =np.mean(np.array_split(df["VTG/V"],points),axis = 1) # VTG values for labels
    labels_VBG = np.mean(np.array_split(df["VBG/V"],points),axis = 1) # VBG values for labels
    
    for i in range(points):
        ax1.plot(x[i],y1[i],label = f"VTG:{round(labels_VTG[i],3)} (V) , VBG:{round(labels_VBG[i],3)} (V)")
    
    # Adding title
    fig.suptitle('Output Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')

    display(fig)
    #save plot if checked
    if params["SAMPLE"]['save_fig'] == True:
        filename= os.path.splitext(file)[0]
        fig.savefig(filename+'_Y1.png')
    
    #add the second axis if applicable
    if plot_list[2]!= "None":
        fig,ax2=plt.subplots(figsize=(10,6),layout='constrained')
        y2 = np.split(plot_values['Y2'],points)
        
        if scale_list[2]=='LOG':
            ax2.set_yscale('log')

        ax2.set_xlabel(axes_labels[0])
         
        ax2.set_ylabel(axes_labels[2])
        for i in range(points):
            ax2.plot(x[i],y2[i],label = f"VTG:{round(labels_VTG[i],3)} (V) , VBG:{round(labels_VBG[i],3)} (V)")
        
        # Adding title
        fig.suptitle('Output Curve', fontweight ="bold")
        fig.legend(loc='outside right upper')
    
        display(fig)
        #save plot if checked
        if params["SAMPLE"]['save_fig'] == True:
            filename= os.path.splitext(file)[0]
            fig.savefig(filename+'_Y2.png')
  

def Gatediode_VTG(device,params):
    try:
        device.setup_smu(params["MAP"]['TG'],params["SMU_T"])
        device.smu_disable(params["MAP"]['BG'])
        device.setup_smu(params["MAP"]['S'],params["SMU_S"])
    
        device.setup_var1(params["VAR1"])
    
        device.integration_time(params["INTEGRATION"])
    
        variables_list = ['VTG','ITG']
        device.variables_to_save(variables_list)
        try:
            plotted_variables = graph_tool(params,device)
        except Exception as e:
            error_box(e)
            return
    
    
        device.single_measurement()
        
        while device.operation_completed()==False:
            pass
        device.autoscaling()
        device.error_occured()
        
        values = dict([(variable,device.return_values(variable)) for variable in variables_list])
        df = get_dataframe_from_results(values)
    except Exception as e:
        error_box(e)
        return

    # calculate normalization factor
    norm = normalization_factor(params['SAMPLE']["width"])

    # Append the normalized current 
    df["ITGmm/uA/um"]= (df["ITG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

     # Save the results
    default_filename = f"{params['SAMPLE']['sample']}_{params['SAMPLE']['field']}_{params['SAMPLE']['device']}_TOP_GATE_D.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Gatediode Curve at {date}"+"\n")
        f.write(f"Series:{params['SAMPLE']['processing_number']}"+"\n")
        f.write(f"Sample:{params['SAMPLE']['sample']}"+"\n")
        f.write(f"Field:{params['SAMPLE']['field']}"+"\n")
        f.write(f"Device:{params['SAMPLE']['device']}"+"\n")
        f.write(f"Device Width/um:{params['SAMPLE']['width']}"+"\n")
        f.write("Sweeping Gate:VTG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VTG from {params['VAR1']['start']}V to {params['VAR1']['stop']}V with step {params['VAR1']['step']}V"+"\n")
        
        #calculate the values
        if params['VAR1']['pcomp']==0:
            f.write(f"Top Gate Current Compliance/A:{params['VAR1']['comp']}"+"\n")
        else:
            f.write(f"Top Gate Power Compliance/A:{params['VAR1']['pcomp'].value}"+"\n")

        f.write(f"Integration Time:{params['INTEGRATION']}"+"\n")
        
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')

    plot_values = values_to_plot(params,device) 
    # Plot user specified results
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    plot_list = [params["PLOT"]["x"],params["PLOT"]["y1"],params["PLOT"]["y2"]]
    scale_list =['LIN',params["PLOT"]["y1_scale"],params["PLOT"]["y2_scale"]]
    axes_labels =set_axes_labels(plot_list)

    if scale_list[1]=='LOG':
        ax1.set_yscale('log')


    #now set the labels
    ax1.set_xlabel(axes_labels[0])
    ax1.set_ylabel(axes_labels[1])

    x = plot_values['X']
    y1 = plot_values['Y1']
    
    ax1.plot(x,y1)
    fig.suptitle('Gatediode Curve', fontweight ="bold")
    display(fig)

    if params["SAMPLE"]["save_fig"] == True:
        filename= os.path.splitext(file)[0]
        fig.savefig(filename+'_Y1.png')


    #add the second axis if applicable
    if plot_list[2]!= "None":
        fig,ax2=plt.subplots(figsize=(10,6),layout='constrained')
        y2 = plot_values['Y2']
        
        if scale_list[2]=='LOG':
            ax2.set_yscale('log')
        
        ax2.set_xlabel(axes_labels[0])
        ax2.set_ylabel(axes_labels[2])
        ax2.plot(x,y2)

        fig.suptitle('Gatediode Curve', fontweight ="bold")
        display(fig)

        if params["SAMPLE"]["save_fig"] == True:
            filename= os.path.splitext(file)[0]
            fig.savefig(filename+'_Y2.png')

   
def Gatediode_VBG(device,params):
    try:
        device.setup_smu(params["MAP"]['BG'],params["SMU_B"])
        device.smu_disable(params["MAP"]['TG'])
        device.setup_smu(params["MAP"]['S'],params["SMU_S"])
    
        device.setup_var1(params["VAR1"])
    
        device.integration_time(params["INTEGRATION"])
    
        variables_list = ['VBG','IBG']
        device.variables_to_save(variables_list)
         
        try:
            plotted_variables = graph_tool(params,device)
        except Exception as e:
            error_box(e)
            return
    
    
        device.single_measurement()
        while device.operation_completed()==False:
            pass
    
        device.autoscaling()
        device.error_occured()
    
        values = dict([(variable,device.return_values(variable)) for variable in variables_list])
        df = get_dataframe_from_results(values)
    except Exception as e:
        error_box(e)
        return

    # plot results

    # calculate normalization factor
    norm = normalization_factor(params["SAMPLE"]["width"])

    # Append the normalized current 
    df["IBGmm/uA/um"]= (df["IBG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

    # Save the results
    default_filename = f"{params['SAMPLE']['sample']}_{params['SAMPLE']['field']}_{params['SAMPLE']['device']}_BACK_GATE_D.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Gatediode Curve at {date}"+"\n")
        f.write(f"Series:{params['SAMPLE']['processing_number']}"+"\n")
        f.write(f"Sample:{params['SAMPLE']['sample']}"+"\n")
        f.write(f"Field:{params['SAMPLE']['field']}"+"\n")
        f.write(f"Device:{params['SAMPLE']['device']}"+"\n")
        f.write(f"Device Width/um:{params['SAMPLE']['width']}"+"\n")
        f.write("Sweeping Gate:VBG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VBG from {params['VAR1']['start']}V to {params['VAR1']['stop']}V with step {params['VAR1']['step']}V"+"\n")
        
        #calculate the values
        if params['VAR1']['pcomp']==0:
            f.write(f"Back Gate Current Compliance/A:{params['VAR1']['comp']}"+"\n")
        else:
            f.write(f"Back Gate Power Compliance/A:{params['VAR1']['pcomp']}"+"\n")

        f.write(f"Integration Time:{params['INTEGRATION']}"+"\n")
         
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')

    plot_values = values_to_plot(params,device) 
    # Plot user specified results
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    plot_list = [params["PLOT"]["x"],params["PLOT"]["y1"],params["PLOT"]["y2"]]
    scale_list =['LIN',params["PLOT"]["y1_scale"],params["PLOT"]["y2_scale"]]
    axes_labels =set_axes_labels(plot_list)

    if scale_list[1]=='LOG':
        ax1.set_yscale('log')


    #now set the labels
    ax1.set_xlabel(axes_labels[0])
    ax1.set_ylabel(axes_labels[1])

    x = plot_values['X']
    y1 = plot_values['Y1']
    
    ax1.plot(x,y1)
    fig.suptitle('Gatediode Curve', fontweight ="bold")
    display(fig)

    if params["SAMPLE"]["save_fig"] == True:
        filename= os.path.splitext(file)[0]
        fig.savefig(filename+'_Y1.png')


    #add the second axis if applicable
    if plot_list[2]!= "None":
        fig,ax2=plt.subplots(figsize=(10,6),layout='constrained')
        y2 = plot_values['Y2']
        
        if scale_list[2]=='LOG':
            ax2.set_yscale('log')
        
        ax2.set_xlabel(axes_labels[0])
        ax2.set_ylabel(axes_labels[2])
        ax2.plot(x,y2)

        fig.suptitle('Gatediode Curve', fontweight ="bold")
        display(fig)

        if params["SAMPLE"]["save_fig"] == True:
            filename= os.path.splitext(file)[0]
            fig.savefig(filename+'_Y2.png')
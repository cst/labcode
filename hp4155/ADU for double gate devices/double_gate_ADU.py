import sys
sys.path.insert(0, './lib')
sys.path.insert(0, '..') #append parent directory


from interface import *
from help import *
from measurements import *
import configparser
    
# Create the grids
#create the information grid
style = {'description_width': 'initial'}
sample = information_box_new()


###end of sampling information#######################################

# move checkboxes outside of the tabs
transfer_check,integration_transfer,transfer_gates = header('Transfer Curve',"MEDium")
output_check,integration_output,output_gates = header('Output Curve',"SHORt")
gatediode_check,integration_gatediode,_=header('Gatediode',"MEDium")

checkboxes = widgets.HBox([transfer_check,output_check,gatediode_check])
display(checkboxes)

#transfer
Vds_transfer_widgets,Vds_transfer = secondary('VDS',0.05,0.95,1,1e-2)
Vtg_transfer_widgets,Vtg_transfer = primary('VTG',-5,0.01,5,1e-3)
Vbg_transfer_widgets,Vbg_transfer = synchronous('VBG',-15,0.1,15,1e-3)
replot_transfer_widgets,replot_transfer = replot()
transfer_box = widgets.VBox([integration_transfer,transfer_gates,Vds_transfer_widgets,Vtg_transfer_widgets,Vbg_transfer_widgets,replot_transfer_widgets])


#output
Vds_output_widgets,Vds_output = primary('VDS',0,0.01,5,1e-2)
Vtg_output_widgets,Vtg_output = secondary('VTG',-5,2,5,1e-3)
Vbg_output_widgets,Vbg_output = additional_secondary('VBG',-15,5,15,1e-3)
replot_output_widgets,replot_output = replot()

output_box = widgets.VBox([integration_output,output_gates,Vds_output_widgets,Vtg_output_widgets,Vbg_output_widgets,replot_output_widgets])


#GateDiodde
terminal = widgets.Dropdown(
    options = ['VTG','VBG'],
    description = 'Selected Gate:',
    value ='VTG',
    style=  {'description_width': 'initial'}
)
Vg_gatediode_widgets,Vg_gatediode=primary('VG',-5,0.05,5,1e-3)
replot_gatediode_widgets,replot_gatediode = replot()
gatediode_box = widgets.VBox([integration_gatediode,terminal,Vg_gatediode_widgets,replot_gatediode_widgets])


#the tab widget
children = [transfer_box,output_box,gatediode_box]
titles = ["Transfer","Output","Gatediode"]
tab = widgets.Tab()
tab.children = children
tab.titles = titles

display(tab)

button = widgets.Button(description ='Start Measurement')
output = widgets.Output()

export_ini_button = widgets.Button(description = 'Export as ini')
import_ini_button = widgets.Button(description='Import from ini')
replot_button = widgets.Button(description = 'Replot',disabled = True)

all_widgets =[transfer_gates,output_gates,button,transfer_check,integration_transfer,output_check,integration_output,gatediode_check,integration_gatediode,terminal,export_ini_button,import_ini_button]

add_widgets_to_list(sample,all_widgets)
add_widgets_to_list(Vds_transfer,all_widgets)
add_widgets_to_list(Vtg_transfer,all_widgets)
add_widgets_to_list(Vbg_transfer,all_widgets)
add_widgets_to_list(Vds_output,all_widgets)
add_widgets_to_list(Vtg_output,all_widgets)
add_widgets_to_list(Vbg_output,all_widgets)
add_widgets_to_list(Vg_gatediode,all_widgets)

add_widgets_to_list(replot_transfer,all_widgets)
add_widgets_to_list(replot_output,all_widgets)
add_widgets_to_list(replot_gatediode,all_widgets)

line = widgets.HBox([button,import_ini_button,export_ini_button,replot_button])
display(line,output)
device = hp4155a.HP4155a('GPIB0::17::INSTR')

df_transfer =pd.DataFrame()
df_output =pd.DataFrame()
df_gatediode = pd.DataFrame()

points_transfer = 0
points_output = 0
points_gatediode = 0

def on_start_clicked(b):
    with output:
        global df_transfer,df_output,df_gatediode,points_transfer,points_output
        clear_output()
        replot_button.disabled = True
        #disable all widgets
        disable_widgets(all_widgets)

        #reset replotting columns
        replot_transfer['x_variable'].options = []
        replot_transfer['y_variable'].options = []
        replot_output['x_variable'].options= []
        replot_output['y_variable'].options = []
        replot_gatediode['x_variable'].options = []
        replot_gatediode['y_variable'].options = []

        Setup(device) #setup the device
            
        if transfer_check.value == True:
            match transfer_gates.value:
                case 'VTG' if check_values(Vtg_transfer,'primary') and check_values(Vds_transfer,'secondary'):
                    df_transfer,points_transfer = Transfer_VTG(Vtg_transfer,Vds_transfer,integration_transfer.value,sample,device)
                    replot_transfer['x_variable'].options = df_transfer.columns.tolist()
                    replot_transfer['y_variable'].options = df_transfer.columns.tolist()
                    replot_transfer['x_variable'].value = 'VTG/V'
                    replot_transfer['y_variable'].value = 'IDmm/uA/um'
                case 'VBG' if check_values(Vbg_transfer,'primary') and check_values(Vds_transfer,'secondary'):
                    df_transfer,points_transfer = Transfer_VBG(Vbg_transfer,Vds_transfer,integration_transfer.value,sample,device)
                    replot_transfer['x_variable'].options = df_transfer.columns.tolist()
                    replot_transfer['y_variable'].options = df_transfer.columns.tolist()
                    replot_transfer['x_variable'].value = 'VBG/V'
                    replot_transfer['y_variable'].value = 'IDmm/uA/um'
                case 'BOTH' if check_values(Vbg_transfer,'synchronous') and check_values(Vds_transfer,'secondary') and check_values(Vtg_transfer,'primary'):
                    df_transfer,points_transfer = Transfer_BOTH(Vtg_transfer,Vbg_transfer,Vds_transfer,integration_transfer.value,sample,device)
                    replot_transfer['x_variable'].options = df_transfer.columns.tolist()
                    replot_transfer['y_variable'].options = df_transfer.columns.tolist()
                    replot_transfer['x_variable'].value = 'VBG/V'
                    replot_transfer['y_variable'].value = 'IDmm/uA/um'
                case _ :
                    information_box("Transfer Measurement skipped due to invalid parameters")
                    df_transfer = pd.DataFrame()
                    points_transfer = 0
            
        if output_check.value == True:
            match output_gates.value:
                case 'VTG' if check_values(Vds_output,'primary') and check_values(Vtg_output,'secondary'):
                    df_output,points_output = Output_VTG(Vds_output,Vtg_output,integration_output.value,sample,device)
                    replot_output['x_variable'].options = df_output.columns.tolist()
                    replot_output['y_variable'].options = df_output.columns.tolist()
                    replot_output['x_variable'].value = 'VDS/V'
                    replot_output['y_variable'].value = 'IDmm/uA/um'
                case 'VBG' if check_values(Vds_output,'primary') and check_values(Vbg_output,'secondary'):
                    df_output,points_output = Output_VBG(Vds_output,Vbg_output,integration_output.value,sample,device) 
                    replot_output['x_variable'].options = df_output.columns.tolist()
                    replot_output['y_variable'].options = df_output.columns.tolist()
                    replot_output['x_variable'].value = 'VDS/V'
                    replot_output['y_variable'].value = 'IDmm/uA/um'
                case 'BOTH' if check_values(Vds_output,'primary') and check_values(Vtg_output,'secondary') and check_values(Vbg_output,'secondary'):
                    df_output,points_output = Output_BOTH(Vds_output,Vtg_output,Vbg_output,integration_output.value,sample,device)
                    replot_output['x_variable'].options = df_output.columns.tolist()
                    replot_output['y_variable'].options = df_output.columns.tolist()
                    replot_output['x_variable'].value = 'VDS/V'
                    replot_output['y_variable'].value = 'IDmm/uA/um'
                case _ :
                    information_box("Output Measurement skipped due to invalid parameters")
                    df_output = pd.DataFrame()
                    points_output = 0
                    

        if gatediode_check.value == True:
            match terminal.value:
                case 'VTG' if check_values(Vg_gatediode,'primary'):
                    df_gatediode = Gatediode_VTG(Vg_gatediode,integration_gatediode.value,sample,device)
                    replot_gatediode['x_variable'].options = df_gatediode.columns.tolist()
                    replot_gatediode['y_variable'].options = df_gatediode.columns.tolist()
                    replot_gatediode['x_variable'].value = 'VTG/V'
                    replot_gatediode['y_variable'].value = 'ITGmm/uA/um'
                case 'VBG' if check_values(Vg_gatediode,'primary'):
                     df_gatediode = Gatediode_VBG(Vg_gatediode,integration_gatediode.value,sample,device)
                     replot_gatediode['x_variable'].options = df_gatediode.columns.tolist()
                     replot_gatediode['y_variable'].options = df_gatediode.columns.tolist()
                     replot_gatediode['x_variable'].value = 'VBG/V'
                     replot_gatediode['y_variable'].value = 'IBGmm/uA/um'
                case _ :
                    information_box("Gatediode Measurement skipped due to invalid parameters")
                    df_gatediode = pd.DataFrame()
        
        information_box("Measurement finished!")
        enable_widgets(all_widgets)
        replot_button.disabled = False
                      
            
def on_export_ini_clicked(b):
    with output:
        disable_widgets(all_widgets)
        config = configparser.ConfigParser()
        default_filename = 'ADU_double_gate.ini'
        file = save_as_ini(default_filename)
        with open(file,'w') as configfile:
            config.add_section('ALL VALUES ARE IN SI-UNITS!')
            config.add_section('IT IS RECOMMENDED TO CHANGE THE INI FILE FROM THE INTERFACE AND DO NOT CHANGE ANY VALUES MANUALLY')

            # Transfer curve
            config.add_section('Transfer')
            config.set('Transfer','Integration',integration_transfer.value)

            config.add_section("Vtg_transfer")
            for parameter,widget in Vtg_transfer.items():
                config.set('Vtg_transfer',parameter,str(widget.value))

            config.add_section("Vbg_transfer")
            for parameter,widget in Vbg_transfer.items():
                config.set('Vbg_transfer',parameter,str(widget.value))

            config.add_section('Vds_transfer')
            for parameter,widget in Vds_transfer.items():
                config.set('Vds_transfer',parameter,str(widget.value))

            #output
            config.add_section('Output')
            config.set('Output','Integration',integration_output.value)

            config.add_section("Vtg_output")
            for parameter,widget in Vtg_output.items():
                config.set('Vtg_output',parameter,str(widget.value))

            config.add_section("Vbg_output")
            for parameter,widget in Vbg_output.items():
                config.set('Vbg_output',parameter,str(widget.value))
            
            config.add_section('Vds_output')
            for parameter,widget in Vds_output.items():
                config.set('Vds_output',parameter,str(widget.value))

            # Gatediode
            config.add_section('Gatediode')
            config.set('Gatediode','Integration',integration_gatediode.value)

            config.add_section("Vg_gatediode")
            for parameter,widget in Vg_gatediode.items():
                config.set('Vg_gatediode',parameter,str(widget.value))        

            config.write(configfile)
            enable_widgets(all_widgets)


def on_import_ini_clicked(b):
    with output:
        disable_widgets(all_widgets)
        #load values to the interface
        config = configparser.ConfigParser()
        file = load_ini()
        #read the values from each section
        try:
            config.read(file)

            #transfer curve
            integration_transfer.value = config.get('Transfer', "integration")
            for parameter,widget in Vtg_transfer.items():
                widget.value = config.get('Vtg_transfer',parameter)
            for parameter,widget in Vds_transfer.items():
                widget.value = config.get('Vds_transfer',parameter)
            for parameter,widget in Vbg_transfer.items():
                widget.value = config.get('Vbg_transfer',parameter)
            
            #output curve
            integration_output.value = config.get('Output','integration')
            for parameter,widget in Vtg_output.items():
                widget.value = config.get('Vtg_output',parameter)
            for parameter,widget in Vds_output.items():
                widget.value = config.get('Vds_output',parameter)
            for parameter,widget in Vbg_output.items():
                widget.value = config.get('Vbg_output',parameter)
        
            # gatediode
            integration_gatediode.value = config.get('Gatediode','integration')
            for parameter,widget in Vg_gatediode.items():
                widget.value = config.get('Vg_gatediode',parameter)

            information_box("all parameters loaded succesfully")
        except Exception as error:
            if type(error).__name__ =='NoSectionError':
                information_box(f"{error}.Explanation: Section(header) [section] does not exist. Create a new ini file or compare it with functional ini files!")
            elif type(error).__name__=='NoOptionError':
                information_box(f'{error}.Explanation: The variable name before the equal sign is not recognized. Create a new ini file or compare it with functional ini files!')
            elif type(error).__name__ == 'TraitError':
                information_box(f'{error}.Explanation: Invalid Parameter Setting. Check if you set an invalid value!')
            elif type(error).__name__ =="DuplicateOptionError":
                information_box(f"{error}. Explaination: The section contains the setted parameter more than once!")
            else:
                information_box(f"A {type(error).__name__} has occurred. Create A new ini file")
        enable_widgets(all_widgets)

def on_replot_button_clicked(b):
    with output:
        global df_transfer,df_output,df_gatediode,points_transfer,points_output
        clear_output()
        disable_widgets(all_widgets)
        replot_button.disabled = True

        # plot transfer
        replot_results(replot_transfer,df_transfer,points_transfer,'Transfer Results')

        #plot output
        replot_results(replot_output,df_output,points_output,'Output Results')

        # plot gatediode
        replot_results(replot_gatediode,df_gatediode,1,'Gatediode Results')

        enable_widgets(all_widgets)
        replot_button.disabled = False

            

button.on_click(on_start_clicked)
import_ini_button.on_click(on_import_ini_clicked)
export_ini_button.on_click(on_export_ini_clicked)
replot_button.on_click(on_replot_button_clicked)











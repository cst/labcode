import ipywidgets as widgets
from ipywidgets import GridspecLayout,Layout
from IPython.display import clear_output
import sys
import os

width = "50%"
height = 'auto'
style = {'description_width': 'initial'}
floatbox_width = "80%"

def header(name,integration):
    style = {'description_width': 'initial'}
    options_integration=["SHORt","MEDium","LONG"]

    check=widgets.Checkbox(
        description = name,
        value = True,
        indent = False
    )
    integration= widgets.Dropdown(
        options=options_integration,
        value=integration,description='Integration Time',
        style =style,
        layout=Layout(height='auto', width="30%")
    )

    select =widgets.Dropdown(
        options = ['VTG','VBG',"BOTH"],
        description = 'Sweeping Gates:',
        value ='BOTH',
        style=  {'description_width': 'initial'}
    )
    
    return check, integration ,select


def primary(name,start,step,stop,comp):
    primary_grid = GridspecLayout(4,4)
    primary_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))
    primary_grid[:,3].style.font_weight = 'bold'


    #first line
    primary_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    primary_grid[0,1]=widgets.Label("Step(V)",layout=Layout(height='auto', width='auto'))
    primary_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    primary_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))
    primary_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width=floatbox_width))
    primary_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))

    #third line 
    primary_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    primary_grid[2,1] =widgets.Label("Power Compliance(W)(0=OFF)",layout=Layout(height='auto', width='auto'))#mind the gap
    primary_grid[2,2] =widgets.Label("Hysterisis",layout=Layout(height='auto', width='auto'))#mind the gap

    #fourth line
    primary_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width=floatbox_width))
    primary_grid[3,1]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width=floatbox_width))#mind the gap
    primary_grid[3,2]=widgets.Dropdown(options=['SINGle','DOUBle'],value='SINGle',layout=Layout(height='auto', width=floatbox_width))#mind the gap


    parameters = {
        'start': primary_grid[1,0],
        'step': primary_grid[1,1],
        'stop': primary_grid[1,2],
        'comp': primary_grid[3,0],
        'hyst':primary_grid[3,2],
        'pcomp':primary_grid[3,1]
    }
    return primary_grid,parameters

def secondary(name,start,step,stop,comp):
    secondary_grid = GridspecLayout(4,4)
    secondary_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))
    secondary_grid[:,3].style.font_weight = 'bold'

    #first line
    secondary_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,1]=widgets.Label("Step(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    secondary_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))

    #third line 
    secondary_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    secondary_grid[2,2] =widgets.Label("Power Compliance(W)(0=OFF)",layout=Layout(height='auto', width='auto'))#mind the gap

    #fourth line
    secondary_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[3,2]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width=floatbox_width))#mind the gap

    parameters = {
        'start': secondary_grid[1,0],
        'step': secondary_grid[1,1],
        'stop':secondary_grid[1,2],
        'comp':secondary_grid[3,0],
        'pcomp':secondary_grid[3,2]
    }
    

    return secondary_grid,parameters


def information_box_new():
    width = '90%'
    sample_information=widgets.Label("Sample Information",layout=Layout(height=height, width='50%'))
    sample_information.style.font_weight='bold'
    information_grid=GridspecLayout(3,2)
    
    for i in range(3):
        for j in range(2):
            if i ==2 and j == 1:
                continue
            elif i == 2 and j == 0:                
                information_grid[i,j]=widgets.BoundedFloatText(
                    value=100,
                    min=1e-3,
                    max=sys.float_info.max,step=1,
                    layout=Layout(height=height, width=width)
                )
            else:
                information_grid[i,j]=widgets.Text(layout=Layout(height=height, width=width))

    information_grid[0,0].description = "Processing-Nr:"
    information_grid[1,0].description = "Sample:"
    information_grid[2,0].description = "Device Width(um):"
    information_grid[0,1].description = "Field(XYY):"
    information_grid[1,1].description = "Device:"

    for i in range(3):
        for j in range(2):
            if i ==2 and j == 1:
                continue
            information_grid[i,j].style = style

    

    config = widgets.Label("SMU Configuration",layout=Layout(height='auto', width='auto'))
    smu1 = widgets.Label("SMU1:Top Gate",layout=Layout(height='auto', width='auto'))
    smu2 = widgets.Label("SMU2:Drain",layout=Layout(height='auto', width='auto'))
    smu3 = widgets.Label("SMU3:Back Gate",layout=Layout(height='auto', width='auto'))
    smu4 = widgets.Label("SMU4:Source(Ground)",layout=Layout(height='auto', width='auto'))

    config.style.font_weight='bold'


    vbox2 = widgets.VBox([config,smu1,smu2,smu3,smu4])
    vbox1=widgets.VBox([sample_information,information_grid])
    display(widgets.HBox([vbox1,vbox2]))

    information = {
        'processing_number': information_grid[0,0],
        'sample' : information_grid[1,0],
        'field': information_grid[0,1],
        'device':information_grid[1,1],    
        'width': information_grid[2,0]
    }

    return information
        
def synchronous(name,start,step,stop,comp):
    synchronous_grid = GridspecLayout(4,4)
    synchronous_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))
    synchronous_grid[:,3].style.font_weight = 'bold'


    #first line
    synchronous_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    synchronous_grid[0,1]=widgets.Label("Step(V)(Only 1 Gate)",layout=Layout(height='auto', width='auto'),style = style)
    synchronous_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    synchronous_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))
    synchronous_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width=floatbox_width))
    synchronous_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))

    #third line 
    synchronous_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    synchronous_grid[2,1] =widgets.Label("Power Compliance(W)(0=OFF)",layout=Layout(height='auto', width='auto'),style = style)#mind the gap
    synchronous_grid[2,2] =widgets.Label("Hysterisis(Only 1 gate)",layout=Layout(height='auto', width='auto'),style = style)#mind the gap

    #fourth line
    synchronous_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width=floatbox_width))
    synchronous_grid[3,1]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width=floatbox_width))#mind the gap
    synchronous_grid[3,2]=widgets.Dropdown(options=['SINGle','DOUBle'],value='SINGle',layout=Layout(height='auto', width=floatbox_width))#mind the gap


    parameters = {
        'start': synchronous_grid[1,0],
        'stop': synchronous_grid[1,2],
        'comp': synchronous_grid[3,0],
        'pcomp':synchronous_grid[3,1],
        'step': synchronous_grid[1,1],
        'hyst': synchronous_grid[3,2]
    }
    return synchronous_grid,parameters

def additional_secondary(name,start,step,stop,comp):
    secondary_grid = GridspecLayout(4,4)
    secondary_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))
    secondary_grid[:,3].style.font_weight = 'bold'

    #first line
    secondary_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,1]=widgets.Label("Step(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    secondary_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))

    #third line 
    secondary_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    secondary_grid[2,2] =widgets.Label("Power Compliance(W)(0=OFF)(Only 1 Gate)",layout=Layout(height='auto', width='auto'),style = style)#mind the gap

    #fourth line
    secondary_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[3,2]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width=floatbox_width))#mind the gap

    parameters = {
        'start': secondary_grid[1,0],
        'step': secondary_grid[1,1],
        'stop':secondary_grid[1,2],
        'comp':secondary_grid[3,0],
        'pcomp':secondary_grid[3,2]
    }
    return secondary_grid,parameters

def replot():
    replot_grid = GridspecLayout(7,2)

    replot_grid[0,0]= widgets.Checkbox(
        description = "Replot",
        value = False,
        indent = False,
    )

    replot_grid[1,0] = widgets.Label('X-axis',layout=Layout(height='auto', width='auto'))
    replot_grid[1,1] = widgets.Label('Y-axis',layout=Layout(height='auto', width='auto'))

    for i in range(2):
        replot_grid[2,i]= widgets.Dropdown(
            options = [],
            description = "Variable:",
            layout=Layout(height='auto', width=floatbox_width),
        )
        
        replot_grid[3,i] = widgets.FloatText(
            value = None,
            description = 'min:',
            layout=Layout(height='auto', width=floatbox_width),
            
        )

        replot_grid[4,i] = widgets.FloatText(
            value = None,
            description = 'max:',
            layout=Layout(height='auto', width=floatbox_width),
        
        )

        replot_grid[5,i] = widgets.Dropdown(
            description = 'scale:',
            options =['linear','log'],
            value = 'linear',
            layout=Layout(height='auto', width=floatbox_width),
            
        )

        replot_grid[6,i]= widgets.Checkbox(
            description = "Autolimits",
            value = True,
            style = style,
        )
    replot_dict ={
        'x_variable':replot_grid[2,0],
        'x_min':replot_grid[3,0],
        'x_max':replot_grid[4,0],
        'x_scale':replot_grid[5,0],
        'x_auto':replot_grid[6,0],
        'y_variable': replot_grid[2,1],
        'y_min':replot_grid[3,1],
        'y_max':replot_grid[4,1],
        'y_scale':replot_grid[5,1],
        'y_auto':replot_grid[6,1],
        'check': replot_grid[0,0]
        
    }
    return replot_grid, replot_dict
        

    

    
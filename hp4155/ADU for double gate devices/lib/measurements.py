# New measurements file for ADU

# The new measurements will have the smus configuration as a parameter and written from the interface

import sys
sys.path.insert(0, '..') #append parent directory

import hp4155a
from help import *
from decimal import Decimal

def Setup(device):
    device.reset()

    #setup sweep measurement mode
    device.measurement_mode('SWE')

    #disable all irrelevant units
    device.disable_not_smu()

# Transfer only VTG 
def Transfer_VTG(VTG,VDS,integration,sample,device): 
    smu1 = device.smu_dict()
    smu1.update(vname = 'VTG',iname='ITG',mode = 'V',func='VAR1')
    smu2 = device.smu_dict()
    smu2.update(vname='VDS',iname='ID',mode = 'V',func = 'VAR2')
    smu3 = device.smu_dict()
    smu3.update(vname = 'VBG',iname='IBG',mode = 'COMM',func = 'CONS')
    smu4 = device.smu_dict()
    smu4.update(vname ='VS',iname = 'IS',mode = 'COMM',func='CONS')

    device.setup_smu(1,smu1)
    device.setup_smu(2,smu2)
    device.setup_smu(3,smu3)
    device.setup_smu(4,smu4)

    var1 = device.var1_dict()
    var1.update(
        mode=VTG['hyst'].value,
        start=VTG['start'].value,
        stop=VTG['stop'].value,
        step=VTG['step'].value,
        comp =VTG['comp'].value,
        pcomp=VTG['pcomp'].value
    )

    device.setup_var1(var1)

    points = number_of_points(VDS)

    var2=device.var2_dict()
    var2.update(
        start=VDS['start'].value,
        step=VDS['step'].value,
        points=points,
        comp=VDS['comp'].value,
        pcomp=VDS['pcomp'].value
    )

    device.setup_var2(var2)

    device.integration_time(integration)
    device.display_variable('X','VTG')
    device.display_variable('Y1','ID')
    device.display_variable('Y2','ITG')

    variables_list =["VTG","ITG","VDS","ID"]
    device.variables_to_save(variables_list)

    device.single_measurement()
    while device.operation_completed()==False:
        pass

    device.autoscaling()

    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    df = get_dataframe_from_results(values)

    # calculate normalization factor
    norm = normalization_factor(sample["width"].value)

    # Append the normalized current 
    df["IDmm/uA/um"]= (df["ID/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["ITGmm/uA/um"]= (df["ITG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

    # Plot normalized Results VTG-IDmm
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    x = np.array_split(df["VTG/V"],points)
    y = np.array_split(df["IDmm/uA/um"].abs(),points)
    labels =np.mean(np.array_split(df["VDS/V"],points),axis = 1) # VDS values for labels

    ax1.set_xlabel('$V_{TG} (V)$') 
    ax1.set_ylabel('$I_{D} (uA/um)$')
    ax1.set_yscale('log')

    for i in range(points):
        ax1.plot(x[i],y[i],label = f"VDS:{round(labels[i],3)} V")

    # Adding title
    fig.suptitle('Transfer Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')
    #fig.tight_layout()

    display(fig)

    # Save the results
    default_filename = f"{sample['sample'].value}_{sample['field'].value}_{sample['device'].value}_TOP_GATE_U.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Transfer Curve at {date}"+"\n")
        f.write(f"Series:{sample['processing_number'].value}"+"\n")
        f.write(f"Sample:{sample['sample'].value}"+"\n")
        f.write(f"Field:{sample['field'].value}"+"\n")
        f.write(f"Device:{sample['device'].value}"+"\n")
        f.write(f"Device Width/um:{sample['width'].value}"+"\n")
        f.write("Sweeping Gate:VTG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VTG from {VTG['start'].value}V to {VTG['stop'].value}V with step {VTG['step'].value}V"+"\n")
        f.write(f"VDS from {VDS['start'].value}V to {VDS['stop'].value}V with step {VDS['step'].value}V"+"\n")
        
        if VTG['pcomp'].value==0:
            f.write(f"Top Gate Current Compliance/A:{VTG['comp'].value}"+"\n")
        else:
            f.write(f"Top Gate Power Compliance/A:{VTG['pcomp'].value}"+"\n")

        if VDS['pcomp'].value == 0:
            f.write(f"Drain Current Compliance/A:{VDS['comp'].value}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{VDS['pcomp'].value}"+"\n")
        f.write(f'Integration Time:{integration}'+"\n")
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')
    return df,points

# Transfer only VBG
def Transfer_VBG(VBG,VDS,integration,sample,device):
    smu1 = device.smu_dict()
    smu1.update(vname = 'VTG',iname='ITG',mode = 'COMM',func='CONS')
    smu2 = device.smu_dict()
    smu2.update(vname='VDS',iname='ID',mode = 'V',func = 'VAR2')
    smu3 = device.smu_dict()
    smu3.update(vname = 'VBG',iname='IBG',mode = 'V',func = 'VAR1')
    smu4 = device.smu_dict()
    smu4.update(vname ='VS',iname = 'IS',mode = 'COMM',func='CONS')

    device.setup_smu(1,smu1)
    device.setup_smu(2,smu2)
    device.setup_smu(3,smu3)
    device.setup_smu(4,smu4)

    var1 = device.var1_dict()
    var1.update(
        mode=VBG['hyst'].value,
        start=VBG['start'].value,
        stop=VBG['stop'].value,
        step=VBG['step'].value,
        comp =VBG['comp'].value,
        pcomp=VBG['pcomp'].value
    )

    device.setup_var1(var1)

    points = number_of_points(VDS)

    var2=device.var2_dict()
    var2.update(
        start=VDS['start'].value,
        step=VDS['step'].value,
        points=points,
        comp=VDS['comp'].value,
        pcomp=VDS['pcomp'].value
    )

    device.setup_var2(var2)

    device.integration_time(integration)
    device.display_variable('X','VBG')
    device.display_variable('Y1','ID')
    device.display_variable('Y2','IBG')

    variables_list =["VBG","IBG","VDS","ID"]
    device.variables_to_save(variables_list)

    device.single_measurement()
    while device.operation_completed()==False:
        pass
    device.autoscaling()

    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    df = get_dataframe_from_results(values)

    # calculate normalization factor
    norm = normalization_factor(sample["width"].value)

    # Append the normalized current 
    df["IDmm/uA/um"]= (df["ID/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["IBGmm/uA/um"]= (df["IBG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

    # Plot normalized Results VTG-IDmm
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    x = np.array_split(df["VBG/V"],points)
    y = np.array_split(df["IDmm/uA/um"].abs(),points)
    labels =np.mean(np.array_split(df["VDS/V"],points),axis = 1) # VDS values for labels

    ax1.set_xlabel('$V_{BG} (V)$') 
    ax1.set_ylabel('$I_{D} (uA/um)$')
    ax1.set_yscale('log')

    for i in range(points):
        ax1.plot(x[i],y[i],label = f"VDS:{round(labels[i],3)} V")

    # Adding title
    fig.suptitle('Transfer Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')
    #fig.tight_layout()

    display(fig)

    # Save the results
    default_filename = f"{sample['sample'].value}_{sample['field'].value}_{sample['device'].value}_BACK_GATE_U.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Transfer Curve at {date}"+"\n")
        f.write(f"Series:{sample['processing_number'].value}"+"\n")
        f.write(f"Sample:{sample['sample'].value}"+"\n")
        f.write(f"Field:{sample['field'].value}"+"\n")
        f.write(f"Device:{sample['device'].value}"+"\n")
        f.write(f"Device Width/um:{sample['width'].value}"+"\n")
        f.write("Sweeping Gate:VBG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VBG from {VBG['start'].value}V to {VBG['stop'].value}V with step {VBG['step'].value}V"+"\n")
        f.write(f"VDS from {VDS['start'].value}V to {VDS['stop'].value}V with step {VDS['step'].value}V"+"\n")
        
        #calculate the values
        if VBG['pcomp'].value==0:
            f.write(f"Back Gate Current Compliance/A:{VBG['comp'].value}"+"\n")
        else:
            f.write(f"Back Gate Power Compliance/A:{VBG['pcomp'].value}"+"\n")
            
        if VDS['pcomp'].value == 0:
            f.write(f"Drain Current Compliance/A:{VDS['comp'].value}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{VDS['pcomp'].value}"+"\n")
        
    
        f.write(f'Integration Time:{integration}'+"\n")
         
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')
    return df,points

    
def Transfer_BOTH(VTG,VBG,VDS,integration,sample,device):
    smu1 = device.smu_dict()
    smu1.update(vname = 'VTG',iname='ITG',mode = 'V',func='VAR1')
    smu2 = device.smu_dict()
    smu2.update(vname='VDS',iname='ID',mode = 'V',func = 'VAR2')
    smu3 = device.smu_dict()
    smu3.update(vname = 'VBG',iname='IBG',mode = 'V',func = 'VARD')
    smu4 = device.smu_dict()
    smu4.update(vname ='VS',iname = 'IS',mode = 'COMM',func='CONS')

    device.setup_smu(1,smu1)
    device.setup_smu(2,smu2)
    device.setup_smu(3,smu3)
    device.setup_smu(4,smu4)

    var1 = device.var1_dict()
    var1.update(
        mode=VTG['hyst'].value,
        start=VTG['start'].value,
        stop=VTG['stop'].value,
        step=VTG['step'].value,
        comp =VTG['comp'].value,
        pcomp=VTG['pcomp'].value
    )

    device.setup_var1(var1)

    points = number_of_points(VDS)

    var2=device.var2_dict()
    var2.update(
        start=VDS['start'].value,
        step=VDS['step'].value,
        points=points,
        comp=VDS['comp'].value,
        pcomp=VDS['pcomp'].value
    )

    device.setup_var2(var2)

    #calculate parameters for VARD
    ratio,offset = calculate_line(VTG,VBG)

    # update VBG step
    VBG["step"].value = Decimal(str(ratio)) * Decimal(str(VTG["step"].value))

    vard = device.vard_dict()
    vard.update(
        offset = offset,
        ratio = ratio,
        comp = VBG["comp"].value,
        pcomp = VBG["pcomp"].value
    )
    device.setup_vard(vard)

    device.integration_time(integration)
    device.display_variable('X','VTG')
    device.display_variable('Y1','ID')
    device.display_variable('Y2','ITG')

    variables_list =["VBG","IBG","VDS","ID","VTG","ITG"]
    device.variables_to_save(variables_list)

    device.single_measurement()
    while device.operation_completed()==False:
        pass

    device.autoscaling()

    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    df = get_dataframe_from_results(values)

    # calculate normalization factor
    norm = normalization_factor(sample["width"].value)

    # Append the normalized current 
    df["IDmm/uA/um"]= (df["ID/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["IBGmm/uA/um"]= (df["IBG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["ITGmm/uA/um"]= (df["ITG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

    # Plot normalized Results VTG-IDmm
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    x1 = np.array_split(df["VBG/V"],points)
    y = np.array_split(df["IDmm/uA/um"].abs(),points)
    x2 = np.array_split(df["VTG/V"],points)
    labels =np.mean(np.array_split(df["VDS/V"],points),axis = 1) # VDS values for labels

    ax1.set_xlabel('$V_{BG} (V)$') 
    ax1.set_ylabel('$I_{D} (uA/um)$')
    ax1.set_yscale('log')

    for i in range(points):
        ax1.plot(x1[i],y[i],label = f"VDS:{round(labels[i],3)} V")

    # add opposite x axis
    ax2 = ax1.twiny()
    ax2.set_xlabel('$V_{TG} (V)$')

    for i in range(points):
        ax2.plot(x2[i],y[i])
    
    # Adding title
    fig.suptitle('Transfer Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')
    #fig.tight_layout()

    display(fig)

    # Save the results
    default_filename = f"{sample['sample'].value}_{sample['field'].value}_{sample['device'].value}_BOTH_GATES_U.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Transfer Curve at {date}"+"\n")
        f.write(f"Series:{sample['processing_number'].value}"+"\n")
        f.write(f"Sample:{sample['sample'].value}"+"\n")
        f.write(f"Field:{sample['field'].value}"+"\n")
        f.write(f"Device:{sample['device'].value}"+"\n")
        f.write(f"Device Width/um:{sample['width'].value}"+"\n")
        f.write("Sweeping Gates:VBG,VTG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VBG from {VBG['start'].value}V to {VBG['stop'].value}V with step {VBG['step'].value}V"+"\n")
        f.write(f"VTG from {VTG['start'].value}V to {VTG['stop'].value}V with step {VTG['step'].value}V"+"\n")
        f.write(f"VDS from {VDS['start'].value}V to {VDS['stop'].value}V with step {VDS['step'].value}V"+"\n")
        
        #calculate the values
        if VBG['pcomp'].value==0:
            f.write(f"Back Gate Current Compliance/A:{VBG['comp'].value}"+"\n")
        else:
            f.write(f"Back Gate Power Compliance/A:{VBG['pcomp'].value}"+"\n")

        if VTG['pcomp'].value==0:
            f.write(f"Top Gate Current Compliance/A:{VTG['comp'].value}"+"\n")
        else:
            f.write(f"Top Gate Power Compliance/A:{VTG['pcomp'].value}"+"\n")

        if VDS['pcomp'].value == 0:
            f.write(f"Drain Current Compliance/A:{VDS['comp'].value}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{VDS['pcomp'].value}"+"\n")

        f.write(f'Integration Time:{integration}'+"\n")
         
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')
    return df,points

# Output with VTG
def Output_VTG(VDS,VTG,integration,sample,device):
    smu1=device.smu_dict()
    smu1.update(vname ='VTG',iname = 'ITG',mode = 'V',func='VAR2')
    smu2 = device.smu_dict()
    smu2.update(vname='VDS',iname='ID',mode = 'V',func = 'VAR1')
    smu3= device.smu_dict()
    smu3.update(vname='VBG',iname='IBG',mode = 'COMM',func='CONS')
    smu4 = device.smu_dict()
    smu4.update(vname='VS',iname = 'IS',mode = 'COMM',func='CONS')

    device.setup_smu(1,smu1)
    device.setup_smu(2,smu2)
    device.setup_smu(3,smu3)
    device.setup_smu(4,smu4)

    var1 = device.var1_dict()
    var1.update(
        mode=VDS['hyst'].value,
        start=VDS['start'].value,
        stop=VDS['stop'].value,
        step=VDS['step'].value,
        comp =VDS['comp'].value,
        pcomp=VDS['pcomp'].value
    )

    device.setup_var1(var1)

    points = number_of_points(VTG)

    var2=device.var2_dict()
    var2.update(
        start=VTG['start'].value,
        step=VTG['step'].value,
        points=points,
        comp=VTG['comp'].value,
        pcomp=VTG['pcomp'].value
    )
    device.setup_var2(var2)

    device.integration_time(integration)
    device.display_variable('X','VDS')
    device.display_variable('Y1','ID')
    device.display_variable('Y2','ITG')

    variables_list = ['VDS','ID','VTG','ITG']
    device.variables_to_save(variables_list)

    device.single_measurement()
    
    while device.operation_completed()==False:
        pass

    device.autoscaling()

    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    df = get_dataframe_from_results(values)

    # plot results

    # calculate normalization factor
    norm = normalization_factor(sample["width"].value)

    # Append the normalized current 
    df["IDmm/uA/um"]= df["ID/A"]*norm
    df["ITGmm/uA/um"]= df["ITG/A"]*norm

    # Plot normalized Results VTG-IDmm
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    x = np.array_split(df["VDS/V"],points)
    y = np.array_split(df["IDmm/uA/um"],points)
    labels =np.mean(np.array_split(df["VTG/V"],points),axis = 1) # VDS values for labels

    ax1.set_xlabel('$V_{DS} (V)$') 
    ax1.set_ylabel('$I_{D} (uA/um)$')

    for i in range(points):
        ax1.plot(x[i],y[i],label = f"VTG:{round(labels[i],3)} V")

    # Adding title
    fig.suptitle('Output Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')
    #fig.tight_layout()

    display(fig)

    #save Results
    # Save the results
    default_filename = f"{sample['sample'].value}_{sample['field'].value}_{sample['device'].value}_TOP_GATE_A.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Output Curve at {date}"+"\n")
        f.write(f"Series:{sample['processing_number'].value}"+"\n")
        f.write(f"Sample:{sample['sample'].value}"+"\n")
        f.write(f"Field:{sample['field'].value}"+"\n")
        f.write(f"Device:{sample['device'].value}"+"\n")
        f.write(f"Device Width/um:{sample['width'].value}"+"\n")
        f.write("Sweeping Gate:VTG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VTG from {VTG['start'].value}V to {VTG['stop'].value}V with step {VTG['step'].value}V"+"\n")
        f.write(f"VDS from {VDS['start'].value}V to {VDS['stop'].value}V with step {VDS['step'].value}V"+"\n")
        
        #calculate the values
        if VTG['pcomp'].value==0:
            f.write(f"Top Gate Current Compliance/A:{VTG['comp'].value}"+"\n")
        else:
            f.write(f"Top Gate Power Compliance/A:{VTG['pcomp'].value}"+"\n")

        if VDS['pcomp'].value == 0:
            f.write(f"Drain Current Compliance/A:{VDS['comp'].value}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{VDS['pcomp'].value}"+"\n")

        f.write(f'Integration Time:{integration}'+"\n")
         
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')
    return df,points

#Output VBG
def Output_VBG(VDS,VBG,integration,sample,device):
    smu1=device.smu_dict()
    smu1.update(vname ='VTG',iname = 'ITG',mode = 'COMM',func='CONS')
    smu2 = device.smu_dict()
    smu2.update(vname='VDS',iname='ID',mode = 'V',func = 'VAR1')
    smu3= device.smu_dict()
    smu3.update(vname='VBG',iname='IBG',mode = 'V',func = 'VAR2')
    smu4 = device.smu_dict()
    smu4.update(vname='VS',iname = 'IS',mode = 'COMM',func='CONS')

    device.setup_smu(1,smu1)
    device.setup_smu(2,smu2)
    device.setup_smu(3,smu3)
    device.setup_smu(4,smu4)

    var1 = device.var1_dict()
    var1.update(
        mode=VDS['hyst'].value,
        start=VDS['start'].value,
        stop=VDS['stop'].value,
        step=VDS['step'].value,
        comp =VDS['comp'].value,
        pcomp=VDS['pcomp'].value
    )

    device.setup_var1(var1)

    points = number_of_points(VBG)

    var2=device.var2_dict()
    var2.update(
        start=VBG['start'].value,
        step=VBG['step'].value,
        points=points,
        comp=VBG['comp'].value,
        pcomp=VBG['pcomp'].value
    )
    device.setup_var2(var2)

    device.integration_time(integration)
    device.display_variable('X','VDS')
    device.display_variable('Y1','ID')
    device.display_variable('Y2','IBG')

    variables_list = ['VDS','ID','VBG','IBG']
    device.variables_to_save(variables_list)

    device.single_measurement()
    
    while device.operation_completed()==False:
        pass
    
    device.autoscaling()
    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    df = get_dataframe_from_results(values)

    # plot results

    # calculate normalization factor
    norm = normalization_factor(sample["width"].value)

    # Append the normalized current 
    df["IDmm/uA/um"]= (df["ID/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["IBGmm/uA/um"]= (df["IBG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

    # Plot normalized Results VTG-IDmm
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    x = np.array_split(df["VDS/V"],points)
    y = np.array_split(df["IDmm/uA/um"],points)
    labels_VBG =np.mean(np.array_split(df["VBG/V"],points),axis = 1) # VDS values for labels

    ax1.set_xlabel('$V_{DS} (V)$') 
    ax1.set_ylabel('$I_{D} (uA/um)$')

    for i in range(points):
        ax1.plot(x[i],y[i],label = f"VBG:{round(labels_VBG[i],3)} V")

    # Adding title
    fig.suptitle('Output Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')
    #fig.tight_layout()

    display(fig)

    #save Results
    # Save the results
    default_filename = f"{sample['sample'].value}_{sample['field'].value}_{sample['device'].value}_BACK_GATE_A.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Output Curve at {date}"+"\n")
        f.write(f"Series:{sample['processing_number'].value}"+"\n")
        f.write(f"Sample:{sample['sample'].value}"+"\n")
        f.write(f"Field:{sample['field'].value}"+"\n")
        f.write(f"Device:{sample['device'].value}"+"\n")
        f.write(f"Device Width/um:{sample['width'].value}"+"\n")
        f.write("Sweeping Gate:VBG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VBG from {VBG['start'].value}V to {VBG['stop'].value}V with step {VBG['step'].value}V"+"\n")
        f.write(f"VDS from {VDS['start'].value}V to {VDS['stop'].value}V with step {VDS['step'].value}V"+"\n")
        
        #calculate the values
        if VBG['pcomp'].value==0:
            f.write(f"Back Gate Current Compliance/A:{VBG['comp'].value}"+"\n")
        else:
            f.write(f"Back Gate Power Compliance/A:{VBG['pcomp'].value}"+"\n")

        if VDS['pcomp'].value == 0:
            f.write(f"Drain Current Compliance/A:{VDS['comp'].value}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{VDS['pcomp'].value}"+"\n")

        f.write(f'Integration Time:{integration}'+"\n")
         
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')
    return df,points

# Output both
def Output_BOTH(VDS,VTG,VBG,integration,sample,device):
    smu1=device.smu_dict()
    smu1.update(vname ='VTG',iname = 'ITG',mode = 'V',func='VAR2')
    smu2 = device.smu_dict()
    smu2.update(vname='VDS',iname='ID',mode = 'V',func = 'VAR1')
    smu3= device.smu_dict()
    smu3.update(vname='VBG',iname='IBG',mode = 'V',func = 'CONS')
    smu4 = device.smu_dict()
    smu4.update(vname='VS',iname = 'IS',mode = 'COMM',func='CONS')

    device.setup_smu(1,smu1)
    device.setup_smu(2,smu2)
    device.setup_smu(3,smu3)
    device.setup_smu(4,smu4)

    var1 = device.var1_dict()
    var1.update(
        mode=VDS['hyst'].value,
        start=VDS['start'].value,
        stop=VDS['stop'].value,
        step=VDS['step'].value,
        comp =VDS['comp'].value,
        pcomp=VDS['pcomp'].value
    )

    device.setup_var1(var1)

    points_VTG = number_of_points(VTG)

    var2=device.var2_dict()
    var2.update(
        start=VTG['start'].value,
        step=VTG['step'].value,
        points=points_VTG,
        comp=VTG['comp'].value,
        pcomp=VTG['pcomp'].value
    )
    device.setup_var2(var2)
    

    points_VBG = number_of_points(VBG)
    values_VBG = np.linspace(VBG["start"].value,VBG["stop"].value,num = points_VBG,endpoint= True)

    device.integration_time(integration)
    device.display_variable('X','VDS')
    device.display_variable('Y1','ID')
    device.display_variable('Y2','ITG')

    variables_list = ['VDS','ID','VBG','IBG','VTG','ITG']
    device.variables_to_save(variables_list)

    for i , value in enumerate(values_VBG):
        cons = device.cons_smu_dict()
        cons.update(comp = VBG['comp'].value,value = value)
        device.setup_cons_smu(3,cons)

        if i == 0:
            device.single_measurement()
        else:
            device.append_measurement()
    
        while device.operation_completed()==False:
            pass
        device.autoscaling()

    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    df = get_dataframe_from_results(values)

    # plot results
    points = points_VTG*points_VBG # number of curves

    # calculate normalization factor
    norm = normalization_factor(sample["width"].value)

    # Append the normalized current 
    df["IDmm/uA/um"]= (df["ID/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["IBGmm/uA/um"]= (df["IBG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')
    df["ITGmm/uA/um"]= (df["ITG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

    # Plot normalized Results VTG-IDmm
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    x = np.array_split(df["VDS/V"],points)
    y = np.array_split(df["IDmm/uA/um"],points)
    labels_VBG =np.mean(np.array_split(df["VBG/V"],points),axis = 1) # VBG values for labels
    labels_VTG = np.mean(np.array_split(df["VTG/V"],points),axis = 1)

    ax1.set_xlabel('$V_{DS} (V)$') 
    ax1.set_ylabel('$I_{D} (uA/um)$')

    for i in range(points):
        ax1.plot(x[i],y[i],label = f"VBG:{round(labels_VBG[i],3)} V , VTG:{round(labels_VTG[i],3)} V")

    # Adding title
    fig.suptitle('Output Curve', fontweight ="bold")
    fig.legend(loc='outside right upper')
    #fig.tight_layout()

    display(fig)

    #save Results
    # Save the results
    default_filename = f"{sample['sample'].value}_{sample['field'].value}_{sample['device'].value}_BOTH_GATES_A.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Output Curve at {date}"+"\n")
        f.write(f"Series:{sample['processing_number'].value}"+"\n")
        f.write(f"Sample:{sample['sample'].value}"+"\n")
        f.write(f"Field:{sample['field'].value}"+"\n")
        f.write(f"Device:{sample['device'].value}"+"\n")
        f.write(f"Device Width/um:{sample['width'].value}"+"\n")
        f.write("Sweeping Gates:VBG,VTG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VBG from {VBG['start'].value}V to {VBG['stop'].value}V with step {VBG['step'].value}V"+"\n")
        f.write(f"VTG from {VTG['start'].value}V to {VTG['stop'].value}V with step {VTG['step'].value}V"+"\n")
        f.write(f"VDS from {VDS['start'].value}V to {VDS['stop'].value}V with step {VDS['step'].value}V"+"\n")
      
        
        #calculate the values
        f.write(f"Back Gate Current Compliance/A:{VBG['comp'].value}"+"\n")
        
        if VTG['pcomp'].value==0:
            f.write(f"Top Gate Current Compliance/A:{VTG['comp'].value}"+"\n")
        else:
            f.write(f"Top Gate Power Compliance/A:{VTG['pcomp'].value}"+"\n")


        if VDS['pcomp'].value == 0:
            f.write(f"Drain Current Compliance/A:{VDS['comp'].value}"+"\n")
        else:
             f.write(f"Drain Power Compliance/A:{VDS['pcomp'].value}"+"\n")

        f.write(f'Integration Time:{integration}'+"\n")
         
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')
    return df,points


def Gatediode_VTG(VTG,integration,sample,device):
    smu1=device.smu_dict()
    smu1.update(vname ='VTG',iname = 'ITG',mode = 'V',func='VAR1')
    smu4 = device.smu_dict()
    smu4.update(vname='VS',iname = 'IS',mode = 'COMM',func='CONS')

    device.setup_smu(1,smu1)
    device.smu_disable(2)
    device.smu_disable(3)
    device.setup_smu(4,smu4)

    var1 = device.var1_dict()
    var1.update(
        mode=VTG['hyst'].value,
        start=VTG['start'].value,
        stop=VTG['stop'].value,
        step=VTG['step'].value,
        comp =VTG['comp'].value,
        pcomp=VTG['pcomp'].value
    )
    device.setup_var1(var1)

    device.integration_time(integration)

    variables_list = ['VTG','ITG']
    device.variables_to_save(variables_list)
    device.display_variable('X','VTG')
    device.display_variable('Y1','ITG')

    device.single_measurement()
    
    while device.operation_completed()==False:
        pass
    device.autoscaling()

    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    df = get_dataframe_from_results(values)

    # plot results

    # calculate normalization factor
    norm = normalization_factor(sample["width"].value)

    # Append the normalized current 
    df["ITGmm/uA/um"]= (df["ITG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

    # Plot normalized Results VTG-IDmm
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    x = df["VTG/V"]
    y = df["ITGmm/uA/um"].abs()

    ax1.set_xlabel('$V_{TG} (V)$') 
    ax1.set_ylabel('$I_{TG} (uA/um)$')
    ax1.set_yscale('log')

    ax1.plot(x,y)

    # Adding title
    fig.suptitle('Gatediode Curve', fontweight ="bold")
    #fig.tight_layout()

    display(fig)

    #save Results
    # Save the results
    default_filename = f"{sample['sample'].value}_{sample['field'].value}_{sample['device'].value}_TOP_GATE_D.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Gatediode Curve at {date}"+"\n")
        f.write(f"Series:{sample['processing_number'].value}"+"\n")
        f.write(f"Sample:{sample['sample'].value}"+"\n")
        f.write(f"Field:{sample['field'].value}"+"\n")
        f.write(f"Device:{sample['device'].value}"+"\n")
        f.write(f"Device Width/um:{sample['width'].value}"+"\n")
        f.write("Sweeping Gate:VTG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VTG from {VTG['start'].value}V to {VTG['stop'].value}V with step {VTG['step'].value}V"+"\n")
        
        #calculate the values
        if VTG['pcomp'].value==0:
            f.write(f"Top Gate Current Compliance/A:{VTG['comp'].value}"+"\n")
        else:
            f.write(f"Top Gate Power Compliance/A:{VTG['pcomp'].value}"+"\n")

        f.write(f'Integration Time:{integration}'+"\n")
        
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')
    return df

def Gatediode_VBG(VBG,integration,sample,device):
    smu3=device.smu_dict()
    smu3.update(vname ='VBG',iname = 'IBG',mode = 'V',func='VAR1')
    smu4 = device.smu_dict()
    smu4.update(vname='VS',iname = 'IS', mode='COMM',func='CONS')

    device.smu_disable(1)
    device.smu_disable(2)
    device.setup_smu(3,smu3)
    device.setup_smu(4,smu4)

    var1 = device.var1_dict()
    var1.update(
        mode=VBG['hyst'].value,
        start=VBG['start'].value,
        stop=VBG['stop'].value,
        step=VBG['step'].value,
        comp =VBG['comp'].value,
        pcomp=VBG['pcomp'].value
    )
    device.setup_var1(var1)

    device.integration_time(integration)

    variables_list = ['VBG','IBG']
    device.variables_to_save(variables_list)

    
    device.display_variable('X','VBG')
    device.display_variable('Y1','IBG')

    device.single_measurement()
    while device.operation_completed()==False:
        pass

    device.autoscaling()

    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    df = get_dataframe_from_results(values)

    # plot results

    # calculate normalization factor
    norm = normalization_factor(sample["width"].value)

    # Append the normalized current 
    df["IBGmm/uA/um"]= (df["IBG/A"].apply(lambda x: Decimal(str(x))*Decimal(str(norm)))).astype('float')

    # Plot normalized Results VTG-IDmm
    fig,ax1= plt.subplots(figsize=(10,6),layout='constrained')

    x = df["VBG/V"]
    y = df["IBGmm/uA/um"].abs()

    ax1.set_xlabel('$V_{BG} (V)$') 
    ax1.set_ylabel('$I_{BG} (uA/um)$')
    ax1.set_yscale('log')

    ax1.plot(x,y)

    # Adding title
    fig.suptitle('Gatediode Curve', fontweight ="bold")
    #fig.tight_layout()

    display(fig)

    #save Results
    # Save the results
    default_filename = f"{sample['sample'].value}_{sample['field'].value}_{sample['device'].value}_BACK_GATE_D.txt"

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =default_filename)

    root.destroy()

    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Gatediode Curve at {date}"+"\n")
        f.write(f"Series:{sample['processing_number'].value}"+"\n")
        f.write(f"Sample:{sample['sample'].value}"+"\n")
        f.write(f"Field:{sample['field'].value}"+"\n")
        f.write(f"Device:{sample['device'].value}"+"\n")
        f.write(f"Device Width/um:{sample['width'].value}"+"\n")
        f.write("Sweeping Gate:VBG"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VBG from {VBG['start'].value}V to {VBG['stop'].value}V with step {VBG['step'].value}V"+"\n")
        
        #calculate the values
        if VBG['pcomp'].value==0:
            f.write(f"Back Gate Current Compliance/A:{VBG['comp'].value}"+"\n")
        else:
            f.write(f"Back Gate Power Compliance/A:{VBG['pcomp'].value}"+"\n")

        f.write(f'Integration Time:{integration}'+"\n")
         
        f.write("\nResults\n")

    df.to_csv(file,sep=" ",mode='a')
    return df    
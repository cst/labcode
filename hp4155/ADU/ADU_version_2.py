import sys
sys.path.insert(0, './lib')
sys.path.insert(0, '..') #append parent directory


from interface_version_2 import *
from help import *
from measurements_new import *
import configparser


# Create the grids
#create the information grid
style = {'description_width': 'initial'}
sample = information_box_new()

###end of sampling information#######################################
test_contacts=widgets.Checkbox(description = "Test of the Contacts",value = True,indent = False)
#display(test_contacts)
#print()

# move checkboxes outside of the tabs
transfer_check,integration_transfer = header('Transfer Curve')
output_check,integration_output = header('Output Curve')
gatediode_check,integration_gatediode=header('Gatediode')

checkboxes = widgets.HBox([test_contacts,transfer_check,output_check,gatediode_check])
display(checkboxes)
print()


#transfer
Vds_transfer_widgets,Vds_transfer = secondary('Vds',0.1,0.45,1,0.1)
Vgs_transfer_widgets,Vgs_transfer = primary('Vgs',2,-0.05,-8,0.01)
transfer_box = widgets.VBox([integration_transfer,Vds_transfer_widgets,Vgs_transfer_widgets])


#output
Vds_output_widgets,Vds_output = primary('Vds',0,0.1,15,0.1)
Vgs_output_widgets,Vgs_output = secondary('Vgs',2,-1,-8,0.01)
output_box = widgets.VBox([integration_output,Vds_output_widgets,Vgs_output_widgets])

#GateDiodde
Vgs_gatediode_widgets,Vgs_gatediode=primary('Vgs',-8,0.05,2,0.02)
gatediode_box = widgets.VBox([integration_gatediode,Vgs_gatediode_widgets])

#the tab widget
children = [transfer_box,output_box,gatediode_box]
titles = ["Transfer","Output","Gatediode"]
tab = widgets.Tab()
tab.children = children
tab.titles = titles

display(tab)
print()

# the button
button = widgets.Button(description ='Start Measurement')
output = widgets.Output()

export_ini_button = widgets.Button(description = 'Export as ini')
import_ini_button = widgets.Button(description='Import from ini')

all_widgets =[button,transfer_check,integration_transfer,output_check,integration_output,gatediode_check,integration_gatediode,test_contacts,export_ini_button,import_ini_button]

add_widgets_to_list(sample,all_widgets)
add_widgets_to_list(Vds_transfer,all_widgets)
add_widgets_to_list(Vgs_transfer,all_widgets)
add_widgets_to_list(Vds_output,all_widgets)
add_widgets_to_list(Vgs_output,all_widgets)
add_widgets_to_list(Vgs_gatediode,all_widgets)

line = widgets.HBox([button,import_ini_button,export_ini_button])
display(line,output)
device = hp4155a.HP4155a('GPIB0::17::INSTR')

def on_start_clicked(b):
    with output:
        clear_output()
        #disable all widgets
        disable_widgets(all_widgets)
        width = sample["width"].value
        norm=normalization_factor(width)
        norm_unit = sample["normalization"].value
        Setup(device) #setup the device
        
        #start measurements
        if test_contacts.value == True:
            Test(device)
            answer = ask_to_continue()
            if answer == False:
                enable_widgets(all_widgets)
                return
        
        if transfer_check.value == True:
            #check the values
            vgs_ok = check_values(Vgs_transfer['start'],Vgs_transfer['step'],Vgs_transfer['stop'],'primary')
            vds_ok=check_values(Vds_transfer['start'],Vds_transfer['step'],Vds_transfer['stop'],'secondary')

            if vgs_ok == True and vds_ok == True:
                #calculate number of points for var2(VDS)
                points = number_of_points(Vds_transfer['start'],Vds_transfer['step'],Vds_transfer['stop'])

                #configure smus
                smu1 = device.smu_dict()
                smu1.update(vname ='VS1',iname = 'IS1',mode = 'COMM',func='CONS')

                smu2 = device.smu_dict()
                smu2.update(vname ='VDS',iname ='ID',mode='V',func='VAR2')
                var2=device.var2_dict()
                var2.update(
                    start=Vds_transfer['start'].value,
                    step=Vds_transfer['step'].value,
                    points=points,
                    comp=Vds_transfer['comp'].value,
                    pcomp=Vds_transfer['pcomp'].value
                )
                smu2.update(VAR2=var2)

                smu3 = device.smu_dict()
                smu3.update(vname='VGS',iname ='IG',mode='V',func='VAR1')
                var1=device.var1_dict()
                var1.update(
                    mode=Vgs_transfer['hyst'].value,
                    start=Vgs_transfer['start'].value,
                    stop=Vgs_transfer['stop'].value,
                    step=Vgs_transfer['step'].value,
                    comp =Vgs_transfer['comp'].value,
                    pcomp=Vgs_transfer['pcomp'].value
                )
                smu3.update(VAR1=var1)

                smu4 = device.smu_dict()
                smu4.update(vname ='VS2',iname = 'IS2',mode = 'COMM',func='CONS')

                #execute measurement
                values=Transfer(smu1,smu2,smu3,smu4,integration_transfer.value,norm,device)
                #Id = np.divide(Idmm,normalization_factor(width))
                #Ig = np.divide(Igmm,normalization_factor(width))
                #gm_S = np.divide(gm,normalization_factor(width))
                #plot results
                #clear_output()
                plot_transfer(values["VGS"],values["IDmm"],values["Gmmm"],points,norm_unit)
                #save results
                if norm_unit =='mA/mm':   
                    header=['VGS/V','VDS/V','ID/A',"IG/A",'gm/S',"IDmm/mA/mm","IGmm/mA/mm","gm/mS/mm"]
                else:
                    header=['VGS/V','VDS/V','ID/A',"IG/A",'gm/S',"IDmm/uA/um","IGmm/uA/um","gm/uS/um"]
                data = {header[0]:values["VGS"],header[1]:values["VDS"],header[2]:values["ID"],header[3]:values["IG"],header[4]:values["Gm"],header[5]:values["IDmm"],header[6]:values["IGmm"],header[7]:values["Gmmm"]}
                df = pd.DataFrame(data)

                #write to file
                filename = f"{sample['sample'].value}_{sample['field'].value}_{sample['device'].value}_U.txt"
                file = create_file(filename)
                save_to_file('Transfer Curve',file,sample,integration_transfer,Vgs_transfer,Vds_transfer)
                df.to_csv(file,sep=" ",mode='a')
            
        if output_check.value == True:
            vds_ok = check_values(Vds_output['start'],Vds_output['step'],Vds_output['stop'],'primary')
            vgs_ok = check_values(Vgs_output['start'],Vgs_output['step'],Vgs_output['stop'],'secondary')
            
            if vds_ok== True and vgs_ok == True:  
                #calculate number of points for var2(VGS)
                points = number_of_points(Vgs_output['start'],Vgs_output['step'],Vgs_output['stop'])

                #configure smus
                smu1 = device.smu_dict()
                smu1.update(vname ='VS1',iname = 'IS1',mode = 'COMM',func='CONS')

                smu2 = device.smu_dict()
                smu2.update(vname ='VDS',iname ='ID',mode='V',func='VAR1')
            

                smu3 = device.smu_dict()
                smu3.update(vname='VGS',iname ='IG',mode='V',func='VAR2')

                smu4 = device.smu_dict()
                smu4.update(vname ='VS2',iname = 'IS2',mode = 'COMM',func='CONS')

                #set var1(Vds) and var2(Vfs)
                var2=device.var2_dict()         
                var2.update(
                    start=Vgs_output['start'].value,
                    step=Vgs_output['step'].value,
                    points=points,
                    comp=Vgs_output['comp'].value,
                    pcomp=Vgs_output['pcomp'].value
                )
                var1=device.var1_dict()
                var1.update(
                    mode=Vds_output['hyst'].value,
                    start=Vds_output['start'].value,
                    stop=Vds_output['stop'].value,
                    step=Vds_output['step'].value,
                    comp =Vds_output['comp'].value,
                    pcomp=Vds_output['pcomp'].value
                )

                smu2.update(VAR1=var1)
                smu3.update(VAR2=var2)
            
                #execute measurement
                values= Output(smu1,smu2,smu3,smu4,integration_output.value,norm,device)
                #plot results
                #clear_output()
                plot_output(values["VDS"],values["IDmm"],values["IGmm"],points,norm_unit)

                #save results
                if norm_unit == 'mA/mm':
                    header=['VGS/V','VDS/V',"ID/A","IG/A",'IDmm/mA/mm',"IGmm/mA/mm"]
                else:
                    header=['VGS/V','VDS/V',"ID/A","IG/A",'IDmm/uA/um',"IGmm/uA/um"]
                data = {header[0]:values["VGS"],header[1]:values["VDS"],header[2]:values["ID"],header[3]:values["IG"],header[4]:values["IDmm"],header[5]:values["IGmm"]}
                df = pd.DataFrame(data)

                #write to file
                filename = f"{sample['sample'].value}_{sample['field'].value}_{sample['device'].value}_A.txt"
                file = create_file(filename)
                save_to_file('Output Curve',file,sample,integration_output,Vgs_output,Vds_output)
                df.to_csv(file,sep=" ",mode='a')

        if gatediode_check.value == True:
            #check the values
            vgs_ok = check_values(Vgs_gatediode['start'],Vgs_gatediode['step'],Vgs_gatediode['stop'],'primary')

            if vgs_ok == True:
                #configure smus
                smu1 = device.smu_dict()
                smu1.update(vname ='VS1',iname = 'IS1',mode = 'COMM',func='CONS')

                smu3 = device.smu_dict()
                smu3.update(vname='VGS',iname ='IG',mode='V',func='VAR1')

                smu4 = device.smu_dict()
                smu4.update(vname ='VS2',iname = 'IS2',mode = 'COMM',func='CONS')

                var1=device.var1_dict()
                var1.update(
                    mode=Vgs_gatediode['hyst'].value,
                    start=Vgs_gatediode['start'].value,
                    stop=Vgs_gatediode['stop'].value,
                    step=Vgs_gatediode['step'].value,
                    comp =Vgs_gatediode['comp'].value,
                    pcomp=Vgs_gatediode['pcomp'].value
                )

                smu3.update(VAR1=var1)
                #execute measurement
                values=Gatediode(smu1,smu3,smu4,integration_gatediode.value,norm,device)
                
                #clear_output()
                plot_gatediode(values["VGS"],values["IGmm"],norm_unit)

                #save results
                if norm_unit == 'mA/mm':
                    header=['VGS/V','IG/A','IGmm/mA/mm']
                else:
                    header=['VGS/V','IG/A','IGmm/uA/um']
                data = {header[0]:values["VGS"],header[1]:values["IG"],header[2]:values["IGmm"]}
                df = pd.DataFrame(data)

                filename =f"{sample['sample'].value}_{sample['field'].value}_{sample['device'].value}_D.txt"
                file = create_file(filename)
                save_to_file('Gatediode',file,sample,integration_gatediode,Vgs_gatediode)
                df.to_csv(file,sep=" ",mode='a')

        information_box("Measurement finished!")
        enable_widgets(all_widgets)
            
def on_export_ini_clicked(b):
    with output:
        disable_widgets(all_widgets)
        config = configparser.ConfigParser()
        default_filename = 'ADU.ini'
        file = save_as_ini(default_filename)
        with open(file,'w') as configfile:
            config.add_section('ALL VALUES ARE IN SI-UNITS!')
            config.add_section('IT IS RECOMMENDED TO CHANGE THE INI FILE FROM THE INTERFACE AND DO NOT CHANGE ANY VALUES MANUALLY')
            config.add_section('Transfer')
            config.set('Transfer','Integration',integration_transfer.value)

            config.add_section("Vgs_transfer")
            for parameter,widget in Vgs_transfer.items():
                config.set('Vgs_transfer',parameter,str(widget.value))

            config.add_section('Vds_transfer')
            for parameter,widget in Vds_transfer.items():
                config.set('Vds_transfer',parameter,str(widget.value))

            config.add_section('Output')
            config.set('Output','Integration',integration_output.value)

            config.add_section("Vgs_output")
            for parameter,widget in Vgs_output.items():
                config.set('Vgs_output',parameter,str(widget.value))

            config.add_section('Vds_output')
            for parameter,widget in Vds_output.items():
                config.set('Vds_output',parameter,str(widget.value))

            config.add_section('Gatediode')
            config.set('Gatediode','Integration',integration_gatediode.value)

            config.add_section("Vgs_gatediode")
            for parameter,widget in Vgs_gatediode.items():
                config.set('Vgs_gatediode',parameter,str(widget.value))        

            config.write(configfile)
            enable_widgets(all_widgets)


def on_import_ini_clicked(b):
    with output:
        disable_widgets(all_widgets)
        #load values to the interface
        config = configparser.ConfigParser()
        file = load_ini()
        #print(file)
        #read the values from each section
        try:
            config.read(file)

            #transfer curve
            integration_transfer.value = config.get('Transfer', "integration")
            for parameter,widget in Vgs_transfer.items():
                widget.value = config.get('Vgs_transfer',parameter)
            for parameter,widget in Vds_transfer.items():
                widget.value = config.get('Vds_transfer',parameter)

            #output curve
            integration_output.value = config.get('Output','integration')
            for parameter,widget in Vgs_output.items():
                widget.value = config.get('Vgs_output',parameter)
            for parameter,widget in Vds_output.items():
                widget.value = config.get('Vds_output',parameter)

            # gatediode
            integration_gatediode.value = config.get('Gatediode','integration')
            for parameter,widget in Vgs_gatediode.items():
                widget.value = config.get('Vgs_gatediode',parameter)

            information_box("all parameters loaded succesfully")
        except Exception as error:
            if type(error).__name__ =='NoSectionError':
                information_box(f"{error}.Explanation: Section(header) [section] does not exist. Create a new ini file or compare it with functional ini files!")
            elif type(error).__name__=='NoOptionError':
                information_box(f'{error}.Explanation: The variable name before the equal sign is not recognized. Create a new ini file or compare it with functional ini files!')
            elif type(error).__name__ == 'TraitError':
                information_box(f'{error}.Explanation: Invalid Parameter Setting. Check if you set an invalid value!')
            elif type(error).__name__ =="DuplicateOptionError":
                information_box(f"{error}. Explaination: The section contains the setted parameter more than once!")
            else:
                information_box(f"A {type(error).__name__} has occurred. Create A new ini file")
        enable_widgets(all_widgets)

button.on_click(on_start_clicked)
#link the new widgets
import_ini_button.on_click(on_import_ini_clicked)
export_ini_button.on_click(on_export_ini_clicked)












import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np


directory = r"\\fileserver.cst.rwth-aachen.de\public\Datentransfer\Asonitis, Alexandros\ADU Measurement\for presentation"
os.chdir(directory)
files = os.listdir()

#iterate through the files

for file in files:
    if file.endswith("U_labview.txt"):
        df = pd.read_csv(file,sep=" ",skiprows=3,skip_blank_lines=True)
        Vgs_tl = df.iloc[:,1].values
        Idmm_tl = df.iloc[:,2].values
        Igmm_tl = df.iloc[:,3].values
        gm_tl = df.iloc[:,4].values

    if file.endswith("A_labview.txt"):
        df = pd.read_csv(file,sep=" ", skiprows=3,skip_blank_lines=True)
        Vds_ol = df.iloc[:,1].values
        Idmm_ol = df.iloc[:,2].values
        Igmm_ol = df.iloc[:,3].values
    
    if file.endswith("D_labview.txt"):
        df = pd.read_csv(file,sep=" ", skiprows=3,skip_blank_lines=True)
        Vgs_dl = df.iloc[:,1].values
        Igmm_dl = df.iloc[:,2].values

    if file.endswith("U.txt"):
        df = pd.read_csv(file,delim_whitespace=True, skiprows=15, skip_blank_lines=True,encoding='unicode_escape')
        Vgs_tp =df["VGS(V)"].values
        Igmm_tp = df["IGmm(mA/mm)"].values
        Idmm_tp = df["IDmm(mA/mm)"].values
        gm_tp = df["gm(mS/mm)"].values

    if file.endswith("A.txt"):
        df = pd.read_csv(file,delim_whitespace=True,skiprows=15,skip_blank_lines=True,encoding='unicode_escape')
        Vds_op = df["VDS(V)"].values
        Idmm_op = df["IDmm(mA/mm)"].values
        Igmm_op = df["IGmm(mA/mm)"].values
    
    if file.endswith("D.txt"):
        df = pd.read_csv(file,delim_whitespace=True,skiprows=13,skip_blank_lines=True,encoding='unicode_escape')
        Vgs_dp =  df["VGS(V)"].values
        Igmm_dp = df["IGmm(mA/mm)"].values

#plot transfer results

fig,axs=plt.subplots(3,sharex=True)
axs[2].set_xlabel('$V_{GS} (V)$')
axs[0].set_ylabel('$I_{D} (mA/mm)$')
axs[1].set_ylabel('$g_{m} (mS/mm)$')
axs[2].set_ylabel('$I_{G} (mA/mm)$')

axs[0].plot(Vgs_tl,Idmm_tl,label="labview")
axs[0].plot(Vgs_tp,Idmm_tp,label = "python")

axs[1].plot(Vgs_tl,gm_tl,label="labview")
axs[1].plot(Vgs_tp,gm_tp,label = "python")

axs[2].plot(Vgs_tl,Igmm_tl,label ="labview")
axs[2].plot(Vgs_tp,Igmm_tp,label = "python")

for ax in axs:
    ax.legend(loc="best")

fig.suptitle("Transfer Results Comparison")
fig.tight_layout()
plt.show()


fig,ax=plt.subplots()
ax.set_xlabel('$V_{DS} (V)$')
ax.set_ylabel('$I_{D} (mA/mm)$')
#axs[1].set_ylabel('$I_{G} (mA/mm)$')


Idmm_ol = np.array_split(Idmm_ol,11)
Idmm_op = np.array_split(Idmm_op,11)
Vds_op = np.array_split(Vds_op,11)
Vds_ol = np.array_split(Vds_ol,11)
Igmm_ol = np.array_split(Igmm_ol,11)
Igmm_op = np.array_split(Igmm_op,11)


for i in range(len(Idmm_ol)):
    if i ==0:
        ax.plot(Vds_ol[i],Idmm_ol[i],color = 'tab:blue',label ="labview")
        ax.plot(Vds_op[i],Idmm_op[i],color = 'tab:orange',label = "python")

        #axs[1].plot(Vds_ol[i],Igmm_ol[i],color ='tab:blue',label="labview")
        #axs[1].plot(Vds_op[i],Igmm_op[i],color='tab:orange',label ="python")
    else:
        ax.plot(Vds_ol[i],Idmm_ol[i],color = 'tab:blue')
        ax.plot(Vds_op[i],Idmm_op[i],color = 'tab:orange')

        #axs[1].plot(Vds_ol[i],Igmm_ol[i],color ='tab:blue')
        #axs[1].plot(Vds_op[i],Igmm_op[i],color='tab:orange')

ax.legend(loc="best")

fig.suptitle("Output Results Comparison")
fig.tight_layout()
plt.show()


fig,ax=plt.subplots()
ax.set_xlabel('$V_{GS} (V)$')
ax.set_ylabel('$I_{G} (mA/mm)$')
ax.set_yscale("log")

ax.plot(Vgs_dl,np.abs(Igmm_dl),label="labview")
ax.plot(Vgs_dp,np.abs(Igmm_dp),label = "python")

ax.legend(loc="best")

fig.suptitle("Gatediode Results Comparison")
fig.tight_layout()
plt.show()
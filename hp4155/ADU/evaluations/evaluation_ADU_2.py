import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np

import tkinter as tk
from tkinter import filedialog

def get_index(values_list):
    index_list = []
    for j in range(len(values_list)):
       index_list.append(values_list[j][0])
    print(index_list)

labview_files = filedialog.askopenfilenames(title="select labview files")

labview_transfer =[]
labview_output = []
labview_gatediode = []

for file in labview_files: 
    for i in range(1,4): 
        if file.endswith(str(i)+"U.txt"):
            df = pd.read_csv(file,sep=" ",skiprows=3,skip_blank_lines=True)
            Vgs = df.iloc[:,1].values
            Idmm = df.iloc[:,2].values
            Igmm = df.iloc[:,3].values
            gm = df.iloc[:,4].values


            values =[i,Vgs,Igmm,Idmm,gm]
            labview_transfer.append(values)


        if file.endswith(str(i)+"A.txt"):
            df = pd.read_csv(file,sep=" ", skiprows=3,skip_blank_lines=True)
            Vds = df.iloc[:,1].values
            Idmm = df.iloc[:,2].values
            Igmm = df.iloc[:,3].values

            values = [i,Vds,Idmm,Igmm]
            labview_output.append(values)
    
        if file.endswith(str(i)+"D.txt"):
            df = pd.read_csv(file,sep=" ", skiprows=3,skip_blank_lines=True)
            Vgs = df.iloc[:,1].values
            Igmm = df.iloc[:,2].values


            values = [i,Vgs,Igmm]
            labview_gatediode.append(values)
    

#get_index(labview_transfer)
#get_index(labview_output)
#get_index(labview_gatediode)

python_files = filedialog.askopenfilenames(title="select python files")

python_transfer = []
python_output = []
python_gatediode = []

for file in python_files:
    for i in range(1,4):
        if file.endswith(str(i)+"U.txt"):
            df = pd.read_csv(file,delim_whitespace=True, skiprows=15, skip_blank_lines=True,encoding='unicode_escape')
            Vgs =df["VGS(V)"].values
            Igmm = df["IGmm(mA/mm)"].values
            Idmm = df["IDmm(mA/mm)"].values
            gm = df["gm(mS/mm)"].values

            values =[i,Vgs,Igmm,Idmm,gm]
            python_transfer.append(values)

        if file.endswith(str(i)+"A.txt"):
            df = pd.read_csv(file,delim_whitespace=True,skiprows=15,skip_blank_lines=True,encoding='unicode_escape')
            Vds = df["VDS(V)"].values
            Idmm = df["IDmm(mA/mm)"].values
            Igmm = df["IGmm(mA/mm)"].values

            values = [i,Vds,Idmm,Igmm]
            python_output.append(values)
    
        if file.endswith(str(i)+"D.txt"):
            df = pd.read_csv(file,delim_whitespace=True,skiprows=13,skip_blank_lines=True,encoding='unicode_escape')
            Vgs =  df["VGS(V)"].values
            Igmm = df["IGmm(mA/mm)"].values
            
            values = [i,Vgs,Igmm]
            python_gatediode.append(values)


#get_index(python_transfer)
#get_index(python_output)
#get_index(python_gatediode)

#plot transfer results

fig,axs=plt.subplots(3,sharex=True,figsize=(16,9))
axs[2].set_xlabel('$V_{GS} (V)$')
axs[0].set_ylabel('$I_{D} (mA/mm)$')
axs[1].set_ylabel('$g_{m} (mS/mm)$')
axs[2].set_ylabel('$I_{G} (mA/mm)$')

for i in range(3):
    # VGS-Idmm
    axs[0].scatter(labview_transfer[i][1],labview_transfer[i][3],label="l"+str(i+1),s=10)
    axs[0].scatter(python_transfer[i][1],python_transfer[i][3],label ="p"+str(i+1),s=10)

    # VGS-gm
    axs[1].scatter(labview_transfer[i][1],labview_transfer[i][4],label="l"+str(i),s=10)
    axs[1].scatter(python_transfer[i][1],python_transfer[i][4],label ="p"+str(i+1),s=10)

    #Vgs _Igmm

    axs[2].scatter(labview_transfer[i][1],labview_transfer[i][2],label="l"+str(i+1),s=10)
    axs[2].scatter(python_transfer[i][1],python_transfer[i][2],label ="p"+str(i+1),s=10)


for ax in axs:
    ax.legend(loc="best")


fig.suptitle("Transfer Results Comparison")

fig.tight_layout()
plt.show()


#output results
fig,ax=plt.subplots(figsize=(16,9))
ax.set_xlabel('$V_{DS} (V)$')
ax.set_ylabel('$I_{D} (mA/mm)$')
#axs[1].set_ylabel('$I_{G} (mA/mm)$')

for i in range(3):
    ax.scatter(labview_output[i][1],labview_output[i][2],label="l"+str(i+1),s = 10)
    ax.scatter(python_output[i][1],python_output[i][2],label="p"+str(i+1),s=10)

ax.legend(loc="best")

fig.suptitle("Output Results Comparison")
fig.tight_layout()
plt.show()


#gatediode results
fig,ax=plt.subplots(figsize=(16,9))
ax.set_xlabel('$V_{GS} (V)$')
ax.set_ylabel('$I_{G} (mA/mm)$')
ax.set_yscale("log")

for i in range(3):
    ax.scatter(labview_gatediode[i][1],np.abs(labview_gatediode[i][2]),label="l"+str(i+1),s=10)
    ax.scatter(python_gatediode[i][1],np.abs(python_gatediode[i][2]),label="p"+str(i+1),s=10)


ax.legend(loc="best")

fig.suptitle("Gatediode Results Comparison")
fig.tight_layout()
plt.show()

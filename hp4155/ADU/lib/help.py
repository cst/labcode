import matplotlib.pyplot as plt
import numpy as np
import time
from datetime import datetime

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox

import pandas as pd

def plot_transfer(x,y1,y2,curves,norm_unit='mA/mm'):
    
    x = np.array_split(x,curves)
    y1 = np.array_split(y1,curves)
    y2 = np.array_split(y2,curves)
    
    fig, ax1 = plt.subplots() 
 
    color = 'tab:red'
    ax1.set_xlabel('$V_{GS} (V)$') 
    if norm_unit == 'mA/mm':
        ax1.set_ylabel('$I_{D} (mA/mm)$', color = color)
    else:
        ax1.set_ylabel('$I_{D} (uA/um)$', color = color)

    for i in range(curves):
        ax1.plot(x[i], y1[i], color = color) 
    #ax1.plot(Vgs, Idmm, color = color) 
    ax1.tick_params(axis ='y', labelcolor = color)
 
    # Adding Twin Axes 
    ax2 = ax1.twinx() 
 
    color = 'tab:green'
    if norm_unit == "mA/mm":
        ax2.set_ylabel('$g_{m} (mS/mm)$', color = color)
    else:
        ax2.set_ylabel('$g_{m} (uS/um)$', color = color)

    for i in range(curves):
        ax2.plot(x[i], y2[i], color = color)
    #ax2.plot(Vgs, gm, color = color) 
    ax2.tick_params(axis ='y', labelcolor = color)
 
    # Adding title
    fig.suptitle('Transfer Curve', fontweight ="bold") 
    fig.tight_layout()
 
    # Show plot
    display(fig)


def plot_output(x,y1,y2,curves,norm_unit = "mA/mm"):    

    x = np.array_split(x,curves)
    y1 = np.array_split(y1,curves)
    y2 = np.array_split(y2,curves)
    
    fig,ax1 = plt.subplots()
    color='tab:red'
    ax1.set_xlabel('$V_{DS} (V)$')
    if norm_unit == 'mA/mm':
        ax1.set_ylabel('$I_{D} (mA/mm)$', color = color)
    else:
        ax1.set_ylabel('$I_{D} (uA/um)$', color = color)
    
    for i in range(curves):
        ax1.plot(x[i],y1[i],color=color)
    ax1.tick_params(axis ='y', labelcolor = color)
    
    ax2 = ax1.twinx()
    color = 'tab:green'
    if norm_unit == 'mA/mm':
        ax2.set_ylabel('$I_{G} (mA/mm)$', color = color)
    else:
        ax2.set_ylabel('$I_{G} (uA/um)$', color = color)

    
    for i in range(curves):
        ax2.plot(x[i], y2[i], color = color) 
    ax2.tick_params(axis ='y', labelcolor = color)

    fig.suptitle("Output Curve",fontweight ="bold")
    fig.tight_layout()
    display(fig)

def plot_gatediode(x,y,norm_unit = "mA/mm"):
    fig, ax1 = plt.subplots()

    color = 'tab:red'
    ax1.set_xlabel('$V_{GS} (V)$') 
    if norm_unit == "mA/mm":
        ax1.set_ylabel('$I_{G} (mA/mm)$', color = color)
    else:
        ax1.set_ylabel('$I_{G} (uA/um)$', color = color)
    ax1.set_yscale('log')

    y = np.absolute(y)

    ax1.plot(x,y, color = color) 
    ax1.tick_params(axis ='y', labelcolor = color)

    fig.suptitle('Gatediode Curve', fontweight ="bold")
    fig.tight_layout()
    display(fig)

        

def number_of_points(start,step,stop):
    try:
        diff = stop.value - start.value
        ratio = abs(diff/step.value)
        points = int(ratio+1)
    
    except ZeroDivisionError:
        points = 1

    #the programm crashed because for secondary step we had no problem setting start = stop and then it was dividing by zero
    
    if points>128:
        points = 128
    return points

def check_values(start,step,stop,function):
    valid = True

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    if function =='primary':
        if abs(step.value) > abs(stop.value-start.value) or step.value==0:#invalid parameter setting 
            valid = False
            tkinter.messagebox.showerror(message="Invalid parameter setting!")

        if start.value<stop.value and step.value<0: #change polarity
            step.value =(-1)*step.value

        elif start.value>stop.value and step.value>0:
            step.value = (-1)*step.value

        else:
            pass
    
    if function == 'secondary':
        if start.value == stop.value:
            pass
        elif abs(step.value) > abs(stop.value-start.value) or step.value==0:#invalid parameter setting 
            valid = False
            tkinter.messagebox.showerror(message="Invalid parameter setting!")
        if start.value<stop.value and step.value<0: #change polarity
            step.value =(-1)*step.value

        elif start.value>stop.value and step.value>0:
            step.value = (-1)*step.value
        
        
    root.destroy()
    return valid 

def ask_to_continue():
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    answer = tk.messagebox.askokcancel(message="Test of the contacts completed. Continue?")
    root.destroy()
    return answer

def add_widgets_to_list(source_dictionary,target_list):
    for widget in source_dictionary.values():
        target_list.append(widget)

def change_state(widgets_list):
    for widget in widgets_list:
        widget.disabled = not widget.disabled

def enable_widgets(widgets_list):
    for widget in widgets_list:
        widget.disabled = False

def disable_widgets(widgets_list):
    for widget in widgets_list:
        widget.disabled = True

def information_box(information):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #display meaagebox
    tkinter.messagebox.showinfo(message=information)
    root.destroy()

#saving functions : save parameters and dataframe to txt
def create_file(filename):
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =filename)
    root.destroy()
    return file

def save_to_file(measurement,file,sample,integration,Vgs,Vds=None):
    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"{measurement} at {date}"+"\n")
        f.write(f"Series:{sample['processing_number'].value}"+"\n")
        f.write(f"Sample:{sample['sample'].value}"+"\n")
        f.write(f"Field:{sample['field'].value}"+"\n")
        f.write(f"Device:{sample['device'].value}"+"\n")
        f.write(f"Device Width/um:{sample['width'].value}"+"\n\n")

        f.write('Parameters\n')
        f.write(f"VGS from {Vgs['start'].value}V to {Vgs['stop'].value}V with step {Vgs['step'].value}V"+"\n")
        if Vds!=None:
             f.write(f"VDS from {Vds['start'].value}V to {Vds['stop'].value}V with step {Vds['step'].value}V"+"\n")
        
        #calculate the values
        Vgs_comp=Vgs["comp"].value
        Vgs_pcomp=Vgs["pcomp"].value
        if Vgs_pcomp==0:
            f.write(f"Gate Current Compliance/A:{Vgs_comp}"+"\n")
        else:
            f.write(f"Gate Power Compliance/A:{Vgs_pcomp}"+"\n")
        if Vds!=None:
            Vds_comp=Vds["comp"].value
            Vds_pcomp=Vds["pcomp"].value
            if Vds_pcomp ==0:
                f.write(f"Drain Current Compliance/A:{Vds_comp}"+"\n")
            else:
                f.write(f"Drain Power Compliance/A:{Vds_pcomp}"+"\n")
        f.write(f'Integration Time:{integration.value}'+"\n")

        #all results start from the same line
        if Vds == None:
            f.write("\n\n")
        f.write("\nResults\n")
    

#normalization factor to is for both normalizations 10**6/width mA/mm = uA/um = 10**(-6)A/um (returned from the tool)
def normalization_factor(width):
    factor = 10**6/width
    return factor


def save_as_ini(default_filename):
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".ini", filetypes=[("Ini files","*.ini")],title = "save as ini",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".ini") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".ini", filetypes=[("Ini files","*.ini")],title = "save as ini",initialfile =default_filename)
    root.destroy()
    return file

def load_ini():
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    
    file = filedialog.askopenfilename(filetypes=[("Ini files","*.ini")],title ='Select ini file')
    while file.endswith(".ini") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.askopenfilename(filetypes=[("Ini files","*.ini")],title = "Select ini file")
    root.destroy()
    return file
    
import ipywidgets as widgets
from ipywidgets import GridspecLayout,Layout
from IPython.display import clear_output
import sys
import os

width = "50%"
height = 'auto'
style = {'description_width': 'initial'}
floatbox_width = "80%"
def header(name):
    style = {'description_width': 'initial'}
    options_integration=["SHORt","MEDium","LONG"]

    check=widgets.Checkbox(description = name,value = True,indent = False)
    integration= widgets.Dropdown(options=options_integration,value="MEDium",description='Integration Time',style =style,layout=Layout(height='auto', width="30%"))#integration time
    
    return check, integration


def primary(name,start,step,stop,comp):
    primary_grid = GridspecLayout(4,4)
    primary_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))


    #first line
    primary_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    primary_grid[0,1]=widgets.Label("Step(V)",layout=Layout(height='auto', width='auto'))
    primary_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    primary_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))
    primary_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width=floatbox_width))
    primary_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))

    #third line 
    primary_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    primary_grid[2,1] =widgets.Label("Power Compliance(W) (0=OFF)",layout=Layout(height='auto', width='auto'))#mind the gap
    primary_grid[2,2] =widgets.Label("Hysterisis",layout=Layout(height='auto', width='auto'))#mind the gap

    #fourth line
    primary_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width=floatbox_width))
    primary_grid[3,1]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width=floatbox_width))#mind the gap
    primary_grid[3,2]=widgets.Dropdown(options=['SINGle','DOUBle'],value='SINGle',layout=Layout(height='auto', width=floatbox_width))#mind the gap

    parameters = {
        'start': primary_grid[1,0],
        'step': primary_grid[1,1],
        'stop': primary_grid[1,2],
        'comp': primary_grid[3,0],
        'hyst':primary_grid[3,2],
        'pcomp':primary_grid[3,1]
    }
    

    return primary_grid,parameters


def secondary(name,start,step,stop,comp):
    secondary_grid = GridspecLayout(4,4)
    secondary_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))

    #first line
    secondary_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,1]=widgets.Label("Step(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    secondary_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))

    #third line 
    secondary_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    secondary_grid[2,2] =widgets.Label("Power Compliance(W) (0=OFF)",layout=Layout(height='auto', width='auto'))#mind the gap

    #fourth line
    secondary_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width=floatbox_width))
    secondary_grid[3,2]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width=floatbox_width))#mind the gap

    parameters = {
        'start': secondary_grid[1,0],
        'step': secondary_grid[1,1],
        'stop':secondary_grid[1,2],
        'comp':secondary_grid[3,0],
        'pcomp':secondary_grid[3,2]
    }
    

    return secondary_grid,parameters


def information_box_new():
    width = '90%'
    sample_information=widgets.Label("Sample Information",layout=Layout(height=height, width='50%'))
    sample_information.style.font_weight='bold'
    information_grid=GridspecLayout(3,2)
    
    for i in range(3):
        for j in range(2):
            if i ==2 and j == 1:
               information_grid[i,j]=widgets.Dropdown(options=['mA/mm','uA/um'],value='mA/mm',layout=Layout(height=height, width=width))#mind the gap
            elif i == 2 and j == 0:                
                information_grid[i,j]=widgets.BoundedFloatText(
                    value=100,
                    min=1e-3,
                    max=sys.float_info.max,step=1,
                    layout=Layout(height=height, width=width)
                )
            else:
                information_grid[i,j]=widgets.Text(layout=Layout(height=height, width=width))

    information_grid[0,0].description = "Processing-Nr:"
    information_grid[1,0].description = "Sample:"
    information_grid[2,0].description = "Device Width(um):"
    information_grid[0,1].description = "Field(XYY):"
    information_grid[1,1].description = "Device:"
    information_grid[2,1].description = "Normalization:"

    for i in range(3):
        for j in range(2):
            information_grid[i,j].style = style

    image_title = widgets.Label("SMU Configuration",layout=Layout(height=height, width='50%'))
    image_title.style.font_weight='bold'

    filename = os.getcwd()+r"\lib\ADU_SMU_configuration.png"
    #print(filename)

    file = open(filename, "rb")
    image = file.read()
    image_widget =widgets.Image(
        value = image,
        format='png',
        width='auto',
        height='auto',
    )
    
    display(widgets.HBox([sample_information,image_title]))
    display(widgets.HBox([information_grid,image_widget]))
    print()

    information = {
        'processing_number': information_grid[0,0],
        'sample' : information_grid[1,0],
        'field': information_grid[0,1],
        'device':information_grid[1,1],    
        'width': information_grid[2,0],
        'normalization': information_grid[2,1]
    }

    return information
            
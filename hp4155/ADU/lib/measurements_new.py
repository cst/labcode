# New measurements file for ADU

# The new measurements will have the smus configuration as a parameter and written from the interface

import sys
sys.path.insert(0, '..') #append parent directory
import hp4155a

def Setup(device):
    device.reset()

    #setup sweep measurement mode
    device.measurement_mode('SWE')

    #disable all irrelevant units
    device.disable_not_smu()

def Test(device):
    #here the parameters are default 

    #setup smu
    smu1 = device.smu_dict()
    smu1.update(vname ='VS1',iname = 'IS1',mode = 'COMM',func='CONS')

    smu2 = device.smu_dict()
    smu2.update(vname ='VDS',iname ='ID',mode='V',func='CONS')

    smu3 = device.smu_dict()
    smu3.update(vname='VGS',iname ='IG',mode='V',func='VAR1')

    smu4 = device.smu_dict()
    smu4.update(vname ='VS2',iname = 'IS2',mode = 'COMM',func='CONS')

    #list all the smus
    smus=[smu1,smu2,smu3,smu4]

    #send the configurations to the device
    for i in range(4):
        device.setup_smu(i+1,smus[i])
        
    #setup var1
    var1 = device.var1_dict()
    var1.update(mode='SING',start=2,stop=-3,step=-0.2,comp = 10e-3,pcomp=0)
    device.setup_var1(var1)

    #set constant VDS
    cons_smu2=device.cons_smu_dict()
    cons_smu2.update(value=10,comp=0.1)
    device.setup_cons_smu(2,cons_smu2)

    device.integration_time('SHOR')
    device.user_function('Gm','mS/mm','1E4*DIFF(ID,VGS)')
    device.user_function('IDmm','mA/mm','1E4*ID')
    device.user_function('IGmm','mA/mm','1E4*IG')

    #display
    device.display_variable('X','VGS')
    device.display_variable('Y1','IDmm')
    device.display_variable('Y2','IGmm')

    device.display_variable_min_max('X','MIN',-3)
    device.display_variable_min_max('X','MAX',2)
    device.display_variable_min_max('Y1','MIN',0)
    device.display_variable_min_max('Y1','MAX',1000)
    device.display_variable_min_max('Y2','MIN',0)
    device.display_variable_min_max('Y2','MAX',10)

    device.single_measurement()
    while device.operation_completed()==False:
        pass

    device.autoscaling()

def Transfer(smu1,smu2,smu3,smu4,integration,norm,device):
    device.del_user_functions()
    #set all the smus
    device.setup_smu(1,smu1)
    device.setup_smu(2,smu2)
    device.setup_smu(3,smu3)
    device.setup_smu(4,smu4)

    device.setup_var1(smu3['VAR1'])
    device.setup_var2(smu2['VAR2'])
    device.integration_time(integration)

    #device.user_function('Gm','S','DIFF(ID,VGS)')

    device.user_function('Gm','S',f'DIFF(ID,VGS)')
    device.user_function('Gmmm','mS/mm',f'Gm*{norm}')
    device.user_function('IDmm','mA/mm',f'ID*{norm}')
    device.user_function('IGmm','mA/mm',f'IG*{norm}')
    
    
    #display
    device.display_variable('X','VGS')
    device.display_variable('Y1','IDmm')
    device.display_variable('Y2','Gmmm')

    if smu3['VAR1']['start']<smu3['VAR1']['stop']:
        device.display_variable_min_max('X','MIN',smu3['VAR1']['start'])
        device.display_variable_min_max('X','MAX',smu3['VAR1']['stop'])

    else:
        device.display_variable_min_max('X','MAX',smu3['VAR1']['start'])
        device.display_variable_min_max('X','MIN',smu3['VAR1']['stop'])

    if smu2['VAR2']['comp']!=0:
        device.display_variable_min_max('Y1','MIN',-abs(smu2['VAR2']['comp'])*norm)
        device.display_variable_min_max('Y1','MAX',abs(smu2['VAR2']['comp'])*norm)

    variables_list = ["VGS","VDS","ID","IG","Gm","IDmm","IGmm","Gmmm"]
    device.variables_to_save(variables_list)

    device.single_measurement()
    while device.operation_completed()==False:
        pass

    device.autoscaling()

    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    return values
    
    
def Output(smu1,smu2,smu3,smu4,integration,norm,device):
    device.del_user_functions()
    #setup all smus
    device.setup_smu(1,smu1)
    device.setup_smu(2,smu2)
    device.setup_smu(3,smu3)
    device.setup_smu(4,smu4)

    device.setup_var2(smu3['VAR2'])
    device.setup_var1(smu2['VAR1'])
    device.integration_time(integration)

    
    device.user_function('IDmm','mA/mm',f'ID*{norm}')
    device.user_function('IGmm','mA/mm',f'IG*{norm}')

    #display
    device.display_variable('X','VDS')
    device.display_variable('Y1','IDmm')
    device.display_variable('Y2','IGmm')

    if smu2['VAR1']['start']<smu2['VAR1']['stop']:
        device.display_variable_min_max('X','MIN',smu2['VAR1']['start'])
        device.display_variable_min_max('X','MAX',smu2['VAR1']['stop'])

    else:
        device.display_variable_min_max('X','MAX',smu2['VAR1']['start'])
        device.display_variable_min_max('X','MIN',smu2['VAR1']['stop'])

    if smu2['VAR1']['comp']!=0:
        device.display_variable_min_max('Y1','MIN',-abs(smu2['VAR1']['comp'])*norm)
        device.display_variable_min_max('Y1','MAX',abs(smu2['VAR1']['comp'])*norm)

    if smu3['VAR2']['comp']!=0:
        device.display_variable_min_max('Y2','MIN',-abs(smu3['VAR2']['comp'])*norm)
        device.display_variable_min_max('Y2','MAX',abs(smu3['VAR2']['comp'])*norm)

    variables_list = ["VGS","VDS","ID","IG","IDmm","IGmm"]
    device.variables_to_save(variables_list)
    
    device.single_measurement()
    while device.operation_completed()==False:
        pass

    device.autoscaling()

    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    return values

def Gatediode(smu1,smu3,smu4,integration,norm,device):
    device.del_user_functions()
    
    #setup all smus
    device.setup_smu(1,smu1)
    device.smu_disable(2)
    device.setup_smu(3,smu3)
    device.setup_smu(4,smu4)

    device.setup_var1(smu3['VAR1'])
    device.integration_time(integration)
    
    #define user functions
    device.user_function('IGmm','mA/mm',f'IG*{norm}')
    device.user_function('ABSIGm','mA/mm','ABS(IGmm)')

    #display
    device.display_variable('X','VGS')
    device.display_variable('Y1','IGmm')
    device.display_variable('Y2','ABSIGm')

    if smu3['VAR1']['start']<smu3['VAR1']['stop']:
        device.display_variable_min_max('X','MIN',smu3['VAR1']['start'])
        device.display_variable_min_max('X','MAX',smu3['VAR1']['stop'])

    else:
        device.display_variable_min_max('X','MAX',smu3['VAR1']['start'])
        device.display_variable_min_max('X','MIN',smu3['VAR1']['stop'])

    if smu3['VAR1']['comp'] !=0:
        device.display_variable_min_max('Y1','MIN',-abs(smu3['VAR1']['comp'])*norm)
        device.display_variable_min_max('Y1','MAX',abs(smu3['VAR1']['comp'])*norm)

    
        device.display_variable_min_max('Y2','MIN',0)
        device.display_variable_min_max('Y1','MAX',abs(smu3['VAR1']['comp'])*norm)

    device.axis_scale('Y2','LOG')

    variables_list = ["VGS","IG","IGmm","ABSIGm"]
    device.variables_to_save(variables_list)
    
    device.single_measurement()
    while device.operation_completed()==False:
        pass

    device.autoscaling()

    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    return values

    
    

    

    

    
    
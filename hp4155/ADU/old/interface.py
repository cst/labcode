import ipywidgets as widgets
from ipywidgets import GridspecLayout,Layout
from IPython.display import clear_output
import sys

width = "50%"
height = 'auto'
style = {'description_width': 'initial'}
def header(name):
    header_grid = GridspecLayout(1,3)
    style = {'description_width': 'initial'}
    options_integration=["SHORt","MEDium","LONG"]

    header_grid[0,0]=widgets.Checkbox(description = name,value = True,indent = False)
    header_grid[0,2] = widgets.Dropdown(layout=Layout(height='auto', width='auto'),options=options_integration,value="MEDium",description='Integration Time',style =style)#integration time
    
    display(header_grid)
    print()

    check = header_grid[0,0]
    integration = header_grid[0,2]
    return check, integration


def primary(name,start,step,stop,comp):
    primary_grid = GridspecLayout(4,4)
    primary_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))


    #first line
    primary_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    primary_grid[0,1]=widgets.Label("Step(V)",layout=Layout(height='auto', width='auto'))
    primary_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    primary_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width='auto'))
    primary_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width='auto'))
    primary_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width='auto'))

    #third line 
    primary_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    primary_grid[2,1] =widgets.Label("Power Compliance(W) (0=OFF)",layout=Layout(height='auto', width='auto'))#mind the gap
    primary_grid[2,2] =widgets.Label("Hysterisis",layout=Layout(height='auto', width='auto'))#mind the gap

    #fourth line
    primary_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width='auto'))
    primary_grid[3,1]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width='auto'))#mind the gap
    primary_grid[3,2]=widgets.Dropdown(options=['SINGle','DOUBle'],value='SINGle',layout=Layout(height='auto', width='auto'))#mind the gap

    parameters = {
        'start': primary_grid[1,0],
        'step': primary_grid[1,1],
        'stop': primary_grid[1,2],
        'comp': primary_grid[3,0],
        'hyst':primary_grid[3,2],
        'pcomp':primary_grid[3,1]
    }
    
    display(primary_grid)
    print()
    return parameters


def secondary(name,start,step,stop,comp):
    secondary_grid = GridspecLayout(4,4)
    secondary_grid[:,3]=widgets.Label(name,layout=Layout(height='auto', width='auto'))

    #first line
    secondary_grid[0,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,1]=widgets.Label("Step(V)",layout=Layout(height='auto', width='auto'))
    secondary_grid[0,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    secondary_grid[1,0]=widgets.BoundedFloatText(value=start,min=-100,max=100,step=1,layout=Layout(height='auto', width='auto'))
    secondary_grid[1,1]=widgets.BoundedFloatText(value=step,min=-200,max=200,step=1,layout=Layout(height='auto', width='auto'))
    secondary_grid[1,2]=widgets.BoundedFloatText(value=stop,min=-100,max=100,step=1,layout=Layout(height='auto', width='auto'))

    #third line 
    secondary_grid[2,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    secondary_grid[2,2] =widgets.Label("Power Compliance(W) (0=OFF)",layout=Layout(height='auto', width='auto'))#mind the gap

    #fourth line
    secondary_grid[3,0]=widgets.BoundedFloatText(value=comp,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width='auto'))
    secondary_grid[3,2]=widgets.BoundedFloatText(value=0,min=0,max=2,step=0.1,layout=Layout(height='auto', width='auto'))#mind the gap

    parameters = {
        'start': secondary_grid[1,0],
        'step': secondary_grid[1,1],
        'stop':secondary_grid[1,2],
        'comp':secondary_grid[3,0],
        'pcomp':secondary_grid[3,2]
    }
    
    display(secondary_grid)
    print()
    return parameters

def information_grid():
    sample_information=widgets.Label("Sample Information",layout=Layout(height=height, width='50%'))
    sample_information.style.font_weight='bold'
    information_grid=GridspecLayout(3,4)

    #first and third column
    #first column

    information_grid[0,0]=widgets.Label("Processing-Nr.",layout=Layout(height=height, width=width))
    information_grid[1,0]=widgets.Label("Sample",layout=Layout(height=height, width=width))
    #information_grid[2,0]=widgets.Label("Piece",layout=Layout(height='auto', width='auto'))

    #last column

    information_grid[0,3]=widgets.Label("Field(XYY)",layout=Layout(height=height, width=width))
    information_grid[1,3]=widgets.Label("Device",layout=Layout(height=height, width=width))
    #information_grid[2,3]=widgets.Label("Testlevel",layout=Layout(height='auto', width='auto'))

    #columns 2 and 3
    for i in range(2):
        for j in range(1,3):
            information_grid[i,j]=widgets.Text(layout=Layout(height=height, width=width))


    # last line with normalization
    information_grid[2,0] = widgets.Label("Device Width(µm)",layout=Layout(height=height, width=width))
    information_grid[2,3] = widgets.Label("Normalization",layout=Layout(height=height, width=width))

    information_grid[2,1] = widgets.BoundedFloatText(value=100,min=sys.float_info.min,max=sys.float_info.max,step=1,layout=Layout(height=height, width=width))
    information_grid[2,2] = widgets.Dropdown(options=['mA/mm','µA/µm'],value='mA/mm',layout=Layout(height=height, width=width))#mind the gap

    display(sample_information)
    display(information_grid)
    print()

    information = {
        'processing_number': information_grid[0,1],
        'sample' : information_grid[1,1],
        'field': information_grid[0,2],
        'device':information_grid[1,2],    
        'width': information_grid[2,1],
        'normalization': information_grid[2,2]
    }

    return information

def information_box_new():
    sample_information=widgets.Label("Sample Information",layout=Layout(height=height, width='50%'))
    sample_information.style.font_weight='bold'
    information_grid=GridspecLayout(3,2)
    
    for i in range(3):
        for j in range(2):
            if i ==2 and j == 1:
               information_grid[i,j]=widgets.Dropdown(options=['mA/mm','µA/µm'],value='mA/mm',layout=Layout(height=height, width=width))#mind the gap
            else:
                information_grid[i,j]=widgets.Text(layout=Layout(height=height, width=width))

    information_grid[0,0].description = "Processing-Nr:"
    information_grid[1,0].description = "Sample:"
    information_grid[2,0].description = "Device Width(µm):"
    information_grid[0,1].description = "Field(XYY):"
    information_grid[1,1].description = "Device:"
    information_grid[2,1].description = "Normalization:"

    for i in range(3):
        for j in range(2):
            information_grid[i,j].style = style

    display(sample_information)
    display(information_grid)
    print()

    information = {
        'processing_number': information_grid[0,0],
        'sample' : information_grid[1,0],
        'field': information_grid[0,1],
        'device':information_grid[1,1],    
        'width': information_grid[2,0],
        'normalization': information_grid[2,1]
    }

    return information
            
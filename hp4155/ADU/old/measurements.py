import sys
sys.path.insert(0, '..') #append parent directory
import module 

def Setup(device):
    #reset device
    device.reset()

    #setup sweep measurement mode
    device.measurement_mode('SWE')

    #disable VMU-VSU
    device.disable_vsu(1)
    device.disable_vsu(2)
    device.disable_vmu(1)
    device.disable_vmu(2)


#Test of the contacts
def Test(device):
    #smu1 
    device.smu_vname(1,'VS1')
    device.smu_iname(1,'IS1')
    device.smu_mode_meas(1,'COMM')
    device.smu_function_sweep(1,'CONS')
    
    #smu2
    device.smu_vname(2,'VDS')
    device.smu_iname(2,'ID')
    device.smu_mode_meas(2,'V')
    device.smu_function_sweep(2,'CONS')
    
    #smu3
    device.smu_vname(3,'VGS')
    device.smu_iname(3,'IG')
    device.smu_mode_meas(3,'V')
    device.smu_function_sweep(3,'VAR1')
    
    #smu4
    device.smu_vname(4,'VS2')
    device.smu_iname(4,'IS2')
    device.smu_mode_meas(4,'COMM')
    device.smu_function_sweep(4,'CONS')
    
    #set values var1 and smu2
    device.start_value_sweep(2)
    device.stop_value_sweep(-3)
    device.step_sweep(0.2)
    device.comp('VAR1',10e-3)

    device.cons_smu_value(2,10)
    device.const_comp(2,0.1)
    device.integration_time('SHOR')

    device.del_ufun()
    device.user_function('Gm','mS/mm','1E4*DIFF(ID,VGS)')
    device.user_function('IDmm','mA/mm','1E4*ID')
    device.user_function('IGmm','mA/mm','1E4*IG')
    
    
    #display
    device.display_variable('X','VGS')
    device.display_variable('Y1','IDmm')
    device.display_variable('Y2','IGmm')
    
    device.single_measurement()
    while device.operation_completed() == False:
        pass
        
    device.autoscaling()
  
#Transfer Curve
def Transfer(Vgs_start,Vgs_step,Vgs_stop,Vgs_comp,Vds_start,Vds_step,Vds_points,Vds_comp,integration,hyst,device):
    
    #smu1 
    device.smu_vname(1,'VS1')
    device.smu_iname(1,'IS1')
    device.smu_mode_meas(1,'COMM')
    device.smu_function_sweep(1,'CONS')
    
    #smu3
    device.smu_vname(3,'VGS')
    device.smu_iname(3,'IG')
    device.smu_mode_meas(3,'V')
    device.smu_function_sweep(3,'VAR1')

    #smu2
    device.smu_vname(2,'VDS')
    device.smu_iname(2,'ID')
    device.smu_mode_meas(2,'V')
    device.smu_function_sweep(2,'VAR2')
    
    #smu4
    device.smu_vname(4,'VS2')
    device.smu_iname(4,'IS2')
    device.smu_mode_meas(4,'COMM')
    device.smu_function_sweep(4,'CONS')
    
    #var1 setup
    device.var1_mode(hyst)
    device.start_value_sweep(Vgs_start)
    device.stop_value_sweep(Vgs_stop)
    device.step_sweep(Vgs_step)
    device.comp('VAR1',Vgs_comp)
    device.integration_time(integration)

    #var2 setup
    #device.cons_smu_value(2,Vds)
    device.var2_start(Vds_start)
    device.var2_step(Vds_step)
    device.var2_points(Vds_points)
    device.var2_comp(Vds_comp)

    device.del_ufun()
    device.user_function('Gm','mS/mm','1E4*DIFF(ID,VGS)')
    device.user_function('IDmm','mA/mm','1E4*ID')
    device.user_function('IGmm','mA/mm','1E4*IG')
    
    #display
    device.display_variable('X','VGS')
    device.display_variable('Y1','IDmm')
    device.display_variable('Y2','Gm')
    
    device.single_measurement()

    device.inst.write("*WAI")

    device.autoscaling()

    Vgs=device.return_data('VGS')
    Idmm=device.return_data('IDmm')
    gm=device.return_data('Gm')
    Vds= device.return_data('VDS')

    return Vgs,Vds,Idmm,gm

#Output Curve
def Output(Vds_start,Vds_step,Vds_stop,Vds_comp,Vgs_start,Vgs_step,Vgs_points,Vgs_comp,Vgs_pcomp,integration,hyst,device):
    
    #smu1 
    device.smu_vname(1,'VS1')
    device.smu_iname(1,'IS1')
    device.smu_mode_meas(1,'COMM')
    device.smu_function_sweep(1,'CONS')
    
    #smu2
    device.smu_vname(2,'VDS')
    device.smu_iname(2,'ID')
    device.smu_mode_meas(2,'V')
    device.smu_function_sweep(2,'VAR1')
    
    #smu3
    device.smu_vname(3,'VGS')
    device.smu_iname(3,'IG')
    device.smu_mode_meas(3,'V')
    device.smu_function_sweep(3,'VAR2')
    
    #smu4
    device.smu_vname(4,'VS2')
    device.smu_iname(4,'IS2')
    device.smu_mode_meas(4,'COMM')
    device.smu_function_sweep(4,'CONS')
    
    #var1 and var2 setup
    device.var1_mode(hyst)
    device.start_value_sweep(Vds_start)
    device.stop_value_sweep(Vds_stop)
    device.step_sweep(abs(Vds_step))
    device.comp('VAR1',Vds_comp)
   
    
    #var2
    device.var2_start(Vgs_start)
    device.var2_step(Vgs_step)
    device.var2_points(Vgs_points)
    device.var2_comp(Vgs_comp)
    device.var2_pcomp(Vgs_pcomp)
    
    device.integration_time(integration)

    device.del_ufun()
    device.user_function('IDmm','mA/mm','1E4*ID')
    device.user_function('IGmm','mA/mm','1E4*IG')

    #display
    device.display_variable('X','VDS')
    device.display_variable('Y1','IDmm')
    device.display_variable('Y2','IGmm')
    
    device.single_measurement()
    while device.operation_completed() == False:
        pass
        
    device.autoscaling()

    Vds=device.return_data('VDS')
    Idmm=device.return_data('IDmm')
    Igmm=device.return_data('IGmm')
    Vgs=device.return_data('VGS')

    return Vgs,Vds,Idmm,Igmm

def GateDiode(Vgs_start,Vgs_step,Vgs_stop,Vgs_comp,integration,hyst,device):
    #smu1 
    device.smu_vname(1,'VS1')
    device.smu_iname(1,'IS1')
    device.smu_mode_meas(1,'COMM')
    device.smu_function_sweep(1,'CONS')
    
    #smu2
    device.smu_disable_sweep(2)
    
    #smu3
    device.smu_vname(3,'VGS')
    device.smu_iname(3,'IG')
    device.smu_mode_meas(3,'V')
    device.smu_function_sweep(3,'VAR1')
    
    #smu4
    device.smu_vname(4,'VS2')
    device.smu_iname(4,'IS2')
    device.smu_mode_meas(4,'COMM')
    device.smu_function_sweep(4,'CONS')
    
    #var1 setup
    device.var1_mode(hyst)
    device.start_value_sweep(Vgs_start)
    device.stop_value_sweep(Vgs_stop)
    device.step_sweep(abs(Vgs_step))
    device.comp('VAR1',Vgs_comp)
    device.integration_time(integration) 

    device.del_ufun()
    #define user functions
    device.user_function('IGmm','mA/mm','1E4*IG')
    device.user_function('ABSIGm','mA/mm','ABS(IGmm)')
    
    #display
    device.display_variable('X','VGS')
    device.display_variable('Y1','IGmm')
    device.display_variable('Y2','ABSIGm')
    
    device.single_measurement()
    while device.operation_completed() == False:
        pass
        
    device.autoscaling()
    #return data from the device
    Vgs=device.return_data('VGS')
    Igmm=device.return_data('IGmm')
    abs_Igmm=device.return_data('ABSIGm')

    return Vgs,Igmm,abs_Igmm

import ipywidgets as widgets
from IPython.display import clear_output
from ipywidgets import GridspecLayout,Layout

# The smu configuration page
def page_1():
    first_page =  GridspecLayout(5,6)
    
    #first column
    first_page[0,0]= widgets.Label("UNIT",layout=Layout(height='auto', width='auto'))
    first_page[1,0] = widgets.Label('SMU1',layout=Layout(height='auto', width='auto'))
    first_page[2,0] = widgets.Label('SMU2',layout=Layout(height='auto', width='auto'))
    first_page[3,0] = widgets.Label('SMU3',layout=Layout(height='auto', width='auto'))
    first_page[4,0] = widgets.Label('SMU4',layout=Layout(height='auto', width='auto'))
    
    #second column voltage name
    first_page[0,1]= widgets.Label('VNAME',layout=Layout(height='auto', width='auto'))
    for i in range(1,5):
        first_page[i,1]= widgets.Text(layout=Layout(height='auto', width='auto'), value =f'V{i}' )

    #Third column Iname
    first_page[0,2]= widgets.Label('INAME',layout=Layout(height='auto', width='auto'))
    for i in range(1,5):
        first_page[i,2]= widgets.Text(layout=Layout(height='auto', width='auto'),value = f'I{i}')

   
    #Fourth column mode 
    options_mode = ['V','I','COMM','VPULSE','IPULSE']
    first_page[0,3]= widgets.Label('MODE',layout=Layout(height='auto', width='auto'))
    first_page[1,3]= widgets.Dropdown(layout=Layout(height='auto', width='auto'),options = options_mode,value = 'COMM') #smu1
    first_page[2,3]= widgets.Dropdown(layout=Layout(height='auto', width='auto'),options = options_mode,value = 'I') #smu2
    first_page[3,3]= widgets.Dropdown(layout=Layout(height='auto', width='auto'),options = options_mode,value = 'V') #smu3
    first_page[4,3]= widgets.Dropdown(layout=Layout(height='auto', width='auto'),options = options_mode,value = 'V') #smu4
    

    #Fifth column function
    options_function= ['VAR1','VAR2','VARD','CONS']
    first_page[0,4]= widgets.Label('FUNC',layout=Layout(height='auto', width='auto'))
    first_page[1,4]= widgets.Dropdown(layout=Layout(height='auto', width='auto'),options = options_function,value ='CONS') #smu1
    first_page[2,4]= widgets.Dropdown(layout=Layout(height='auto', width='auto'),options = options_function,value ='VAR2') #smu2
    first_page[3,4]= widgets.Dropdown(layout=Layout(height='auto', width='auto'),options = options_function,value ='VAR1') #smu3
    first_page[4,4]= widgets.Dropdown(layout=Layout(height='auto', width='auto'),options = options_function,value ='CONS') #smu4

    # Sixth column ask if smus are disabled
    first_page[0,5] = widgets.Label("DISABLE")
    for i in range(1,5):
        first_page[i,5]= widgets.Checkbox(layout=Layout(height='auto', width='auto'),value = False,indent = False)

    return first_page


#page 2 the user function page
def page_2():
    second_page = GridspecLayout(7,3)

    # first line (descriptions)
    second_page[0,0]= widgets.Label('NAME',layout=Layout(height='auto', width='auto'))
    second_page[0,1]= widgets.Label('UNIT',layout=Layout(height='auto', width='auto'))
    second_page[0,2]= widgets.Label('DEFINITION',layout=Layout(height='auto', width='auto'))

    # Iterate through the lines and columns for text boxes 
    for i in range(1,7):
        for j in range(3):
            second_page[i,j] = widgets.Text(layout=Layout(height='auto', width='auto'))

    return second_page

#parameters setting
def page_3():
    pass 

#display variables
def page_4():
    fourth_page = GridspecLayout(5,4)

    #first line headers
    fourth_page[0,1]= widgets.Label('X',layout=Layout(height='auto', width='auto'))
    fourth_page[0,2]= widgets.Label('Y1',layout=Layout(height='auto', width='auto'))
    fourth_page[0,3]= widgets.Label('Y2',layout=Layout(height='auto', width='auto'))

    #first column 
    fourth_page[1,0] = widgets.Label('NAME',layout=Layout(height='auto', width='auto'))
    fourth_page[2,0] = widgets.Label('SCALE',layout=Layout(height='auto', width='auto'))
    fourth_page[3,0] = widgets.Label('MIN',layout=Layout(height='auto', width='auto'))
    fourth_page[4,0] = widgets.Label('MAX',layout=Layout(height='auto', width='auto'))

    #iterate through the second line (NAME)
    fourth_page[1,1]=widgets.Text(layout=Layout(height='auto', width='auto'),value = 'V3')
    fourth_page[1,2]=widgets.Text(layout=Layout(height='auto', width='auto'),value = 'I3')
    fourth_page[1,3]=widgets.Text(layout=Layout(height='auto', width='auto'))

    #Iterate through the third line (scale) 
    options_scale = ['LIN','LOG']
    for j in range(1,4):
        fourth_page[2,j] = widgets.Dropdown(value = 'LIN',options = options_scale, layout=Layout(height='auto', width='auto'))

    #iterate throuh the last 2 lines(min-max)
    for j in range(1,4):
        fourth_page[3,j] = widgets.FloatText(value = 0) #min

    fourth_page[3,1]=widgets.FloatText(value = 1) #max X-axis
    fourth_page[3,2]=widgets.FloatText(value = 0.1) #max Y1-axis
    fourth_page[3,3]=widgets.FloatText(value = 0) #max Y2-axis

    return fourth_page
    
    

    

import sys
sys.path.insert(0, './lib')
sys.path.insert(0, '..') #append parent directory

import hp4155a
from interface import *
from help import *

sample,save_plot_button = sample_information_interface()
parameters =parameters_interface()
button = widgets.Button(description ='Start Measurement')
output = widgets.Output()

display(button,output)

all_widgets = [button]
add_widgets_to_list(sample,all_widgets)
add_widgets_to_list(parameters,all_widgets)

fig = None

device = hp4155a.HP4155a('GPIB0::17::INSTR')
setup(device)
def on_button_clicked(b):
    with output:
        global fig,file
        clear_output(wait = True)
        change_state(all_widgets)
        area=circle_area(sample['radius'].value)
        
        valid = check_values(parameters)
        if valid == True:
            values = measure(parameters,area,device)
            fig=plot_results(values['VS'],values['ABSNOR'])
            filename = f"DIODE_{sample['field'].value}.txt"
            file = create_file(filename)
            save_to_file(values,sample,parameters,file)
        
        change_state(all_widgets)
        save_plot_button.disabled = False

def on_save_plot_button_clicked(b):
    with output:
        global fig
        save_plot_button.disabled = True
        change_state(all_widgets)

        filename = f"DIODE_{sample['field'].value}.png"
        root = tk.Tk()
        root.withdraw()
        root.lift() #show window above all other applications
    
        root.attributes("-topmost", True)#window stays above all other applications
    
        file = filedialog.asksaveasfilename(defaultextension=".png", filetypes=[("Portable Network Graphics","*.png")],title = "Save Plot",initialfile =filename)
    
        #check if the file path is correct(.txt)
        while file.endswith(".png") == False:
            #open again filedialog with error message box
            answer = tk.messagebox.askyesno(message = "Do you want to cancel the file operation?")
            if answer == True:
                file = None
                break
            file = filedialog.asksaveasfilename(defaultextension=".png", filetypes=[("Portable Network Graphics","*.png")],title = "Save Plot",initialfile =filename)
        root.destroy()

        if file != None:
            fig.savefig(file)
            print("Plot Saved Succesfully!")

        change_state(all_widgets)

button.on_click(on_button_clicked)
save_plot_button.on_click(on_save_plot_button_clicked)
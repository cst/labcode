import matplotlib.pyplot as plt
import numpy as np
import time
from datetime import datetime

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox

import pandas as pd

#returns area in cm^2
def circle_area(radius):
    #input is in um
    radius = radius*10**(-6) #m^2
    area = np.pi*radius**2 # m^2
    area = area * 10**4 # cm^2
    return area

def add_widgets_to_list(source_dictionary,target_list):
    for widget in source_dictionary.values():
        target_list.append(widget)

def change_state(widgets_list):
    for widget in widgets_list:
        widget.disabled = not widget.disabled

#saving functions : save parameters and dataframe to txt
def create_file(filename):
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        answer = tk.messagebox.askyesno(message = "Do you want to cancel the file operation?")
        if answer == True:
            file = None
            break
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =filename)
    root.destroy()
    return file


def check_values(parameters):
    valid = True

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #get better variable names
    start = parameters['start']
    stop = parameters['stop']
    step = parameters['step']

    #check values
    if abs(step.value) > abs(stop.value-start.value) or step.value==0:#invalid parameter setting 
        valid = False
        tkinter.messagebox.showerror(message="Invalid parameter setting!")

    if start.value<stop.value and step.value<0: #change polarity
        step.value =(-1)*step.value

    elif start.value>stop.value and step.value>0:
        step.value = (-1)*step.value

    else:
        pass

    root.destroy()
    return valid

#Setup The device for diode measurement 
def setup(device):
    device.reset()

    #setup sweep measurement mode
    device.measurement_mode('SWE')

    #disable all irrelevant units
    device.disable_not_smu()

    #disable smu 2 and 4
    device.smu_disable(2)
    device.smu_disable(4)


def measure(parameters,area,device):
    #delete user functions
    device.del_user_functions()
    
    smu1 = device.smu_dict()
    smu1.update(vname ='VGND',iname = 'IGND',mode = 'COMM',func='CONS')

    smu3 = device.smu_dict()
    smu3.update(vname='VS',iname='IS',mode ='V',func='VAR1')


    device.setup_smu(1,smu1)
    device.setup_smu(3,smu3)
    

    #define user functions
    device.user_function("AREA",'CM^2',str(area))
    device.user_function("INORM",'A/CM^2','IS/AREA')
    device.user_function("ABSNOR","A/CM^2","ABS(INORM)")
    device.user_function("ABSRAW",'A','ABS(IS)')
    

    var1 = device.var1_dict()
    var1.update(
        mode = parameters['hyst'].value,
        start = parameters['start'].value,
        stop = parameters['stop'].value,
        step = parameters['step'].value,
        comp = parameters['comp'].value,
        pcomp = 0
    )

    device.setup_var1(var1)
    
    device.integration_time(parameters['integration'].value)
    

    #display
    device.display_variable('X','VS')
    
    device.display_variable('Y1','ABSNOR')
    

    device.axis_scale('Y1','LOG')

    if parameters['start'].value < parameters['stop'].value:
        device.display_variable_min_max('X','MIN',parameters['start'].value)
        device.display_variable_min_max('X','MAX',parameters['stop'].value)

    else:
        device.display_variable_min_max('X','MAX',parameters['start'].value)
        device.display_variable_min_max('X','MIN',parameters['stop'].value)

    if parameters['comp'].value !=0:
        device.display_variable_min_max('Y1','MIN',0)
        device.display_variable_min_max('Y1','MAX',abs(parameters['comp'].value)/area)

 
    variables_list = ['VS','IS','ABSRAW','INORM','ABSNOR']# look at the tool
    device.variables_to_save(variables_list)
    

    device.single_measurement()
    while device.operation_completed() == False:
        pass
    
    device.autoscaling()
    
    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    return values

def plot_results(x,y):
    fig, ax1 = plt.subplots()

    color = 'tab:red'
    ax1.set_xlabel('Voltage (V)') 
    ax1.set_ylabel('Current density $(A/{cm}^2)$')
    ax1.set_yscale('log')

    ax1.plot(x,y, color = color) 

    ax1.grid(True,linestyle = '--',axis = 'y',color ="k",linewidth = 0.5)
    ax1.axvline(linestyle='--',color = 'k',linewidth =0.5)

    fig.suptitle('Diode Plot', fontweight ="bold")
    fig.tight_layout()

    display(fig)

    return fig

#also for file
def save_to_file(values,sample,parameters,file):
    if file != None:
        with open(file,'w') as f:
            date = str(datetime.today().replace(microsecond=0))
            f.write(f"Diode Measurement at {date}"+"\n")
            f.write(f"Series:{sample['processing_number'].value}"+"\n")
            f.write(f"Sample:{sample['sample'].value}"+"\n")
            f.write(f"Field:{sample['field'].value}"+"\n")
            f.write(f"Radius/um:{sample['radius'].value}"+"\n\n")
    
            f.write('Parameters\n')
            f.write(f"V from {parameters['start'].value}V to {parameters['stop'].value}V with step {parameters['step'].value}V"+"\n")
            f.write(f"I Compliance/A:{parameters['comp'].value}"+"\n")
            f.write(f"Integration Time:{parameters['integration'].value}"+"\n")
            
            f.write("\nResults\n")
    
        #create pandas dataframe
        df = pd.DataFrame(values)
        #df =df.drop(columns=['ABSNOR'])
        df =df.rename(columns={'VS':'VS/V','IS':'IRAW/A','ABSRAW':'ABSRAW/A','INORM':'INORM/A/CM^2','ABSNOR':'ABSNOR/A/CM^2'})
    
        df.to_csv(file,sep= " ",mode = 'a')
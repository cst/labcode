import ipywidgets as widgets
from ipywidgets import GridspecLayout,Layout
from IPython.display import clear_output,display
import sys
import os

width = "50%"
height = 'auto'
style = {'description_width': 'initial'}
floatbox_width = "80%"

def parameters_interface():
    grid = GridspecLayout(5,4)
    grid[0,:]=widgets.Label("V (SMU3)",layout=Layout(height='auto', width='auto'))
    grid[0,:].style.font_weight='bold'


    #first line
    grid[1,0]=widgets.Label("Start(V)",layout=Layout(height='auto', width='auto'))
    grid[1,1]=widgets.Label("Step(V)",layout=Layout(height='auto', width='auto'))
    grid[1,2]=widgets.Label("Stop(V)",layout=Layout(height='auto', width='auto'))

    #second line
    grid[2,0]=widgets.BoundedFloatText(value=-8,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))
    grid[2,1]=widgets.BoundedFloatText(value=0.05,min=-200,max=200,step=1,layout=Layout(height='auto', width=floatbox_width))
    grid[2,2]=widgets.BoundedFloatText(value=4,min=-100,max=100,step=1,layout=Layout(height='auto', width=floatbox_width))

    #third line 
    grid[3,0]=widgets.Label("Compliance(A)",layout=Layout(height='auto', width='auto'))
    grid[3,1] =widgets.Label("Integration Time",layout=Layout(height='auto', width='auto'))#mind the gap
    grid[3,2] =widgets.Label("Hysterisis",layout=Layout(height='auto', width='auto'))#mind the gap

    #fourth line
    grid[4,0]=widgets.BoundedFloatText(value=0.10,min=-0.1,max=0.1,step=0.01,layout=Layout(height='auto', width=floatbox_width))
    grid[4,1]=widgets.Dropdown(options=["SHORt","MEDium","LONG"],value="MEDium",style =style,layout=Layout(height='auto', width=floatbox_width))
    grid[4,2]=widgets.Dropdown(options=['SINGle','DOUBle'],value='SINGle',layout=Layout(height='auto', width=floatbox_width))#mind the gap

    parameters = {
        'start': grid[2,0],
        'step': grid[2,1],
        'stop': grid[2,2],
        'comp': grid[4,0],
        'hyst':grid[4,2],
        'integration':grid[4,1]
    }
    

    display(grid)
    return parameters


def sample_information_interface():
    width = '90%'
    sample_information=widgets.Label("Sample Information",layout=Layout(height=height, width='50%'))
    sample_information.style.font_weight='bold'
    information_grid=GridspecLayout(3,2)
    
    for i in range(3):
        for j in range(2):
            if i ==2 and j == 1:
                information_grid[i,j]=widgets.Button(description = "Save Plot",disabled = True)#mind the gap
                pass
            elif i == 2 and j == 0: #look at the tool for diode area          
                information_grid[i,j]=widgets.BoundedFloatText(
                    value=50,
                    min=1,
                    max=200,step=1,
                    layout=Layout(height=height, width=width)
                )
            else:
                information_grid[i,j]=widgets.Text(layout=Layout(height=height, width=width))

    information_grid[0,0].description = "Processing-Nr:"
    information_grid[1,0].description = "Sample:"
    information_grid[2,0].description = "Radius(um):"
    information_grid[0,1].description = "Field(XYY):"
    information_grid[1,1].description = "Device:"
    information_grid[1,1].disabled = True
    #information_grid[2,1].description = "Normalization:"

    for i in range(3):
        for j in range(2):
            try:
                information_grid[i,j].style = style
            except:
                pass

    image_title = widgets.Label("SMU Configuration",layout=Layout(height=height, width='50%'))
    image_title.style.font_weight='bold'

    filename = os.getcwd()+r"\lib\diode_smu_configuration.png"
    #print(filename)

    file = open(filename, "rb")
    image = file.read()
    image_widget =widgets.Image(
        value = image,
        format='png',
        width='auto',
        height='auto',
    )
    
    #display(widgets.HBox([sample_information,image_title]))
    #display(widgets.HBox([information_grid,image_widget]))
    vbox1 =widgets.VBox([sample_information,information_grid])
    vbox2 =widgets.VBox([image_title,image_widget])

    display(widgets.HBox([vbox1,vbox2]))

    information = {
        'processing_number': information_grid[0,0],
        'sample' : information_grid[1,0],
        'field': information_grid[0,1],
        'radius': information_grid[2,0],
    }
    save_plot_button = information_grid[2,1]

    return information,save_plot_button

import matplotlib.pyplot as plt
import numpy as np
import time
from datetime import datetime

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox

import pandas as pd

def normalization_factor(width):
    factor = 10**6/width
    return factor

def add_widgets_to_list(source_dictionary,target_list):
    for widget in source_dictionary.values():
        target_list.append(widget)

def change_state(widgets_list):
    for widget in widgets_list:
        widget.disabled = not widget.disabled

#saving functions : save parameters and dataframe to txt
def create_file(filename):
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =filename)
    root.destroy()
    return file


def check_values(parameters):
    valid = True

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #get better variable names
    start = parameters['start']
    stop = parameters['stop']
    step = parameters['step']

    #check values
    if abs(step.value) > abs(stop.value-start.value) or step.value==0:#invalid parameter setting 
        valid = False
        tkinter.messagebox.showerror(message="Invalid parameter setting!")

    if start.value<stop.value and step.value<0: #change polarity
        step.value =(-1)*step.value

    elif start.value>stop.value and step.value>0:
        step.value = (-1)*step.value

    else:
        pass

    root.destroy()
    return valid

#Setup The device for mesa measurement 
def setup(device):
    device.reset()

    #setup sweep measurement mode
    device.measurement_mode('SWE')

    #disable all irrelevant units
    device.disable_not_smu()

    #disable smu 2 and 4
    device.smu_disable(2)
    device.smu_disable(4)

def measure(parameters,norm,device):
    #delete user functions
    device.del_user_functions()
    
    smu1 = device.smu_dict()
    smu1.update(vname ='VGND',iname = 'IGND',mode = 'COMM',func='CONS')

    smu3 = device.smu_dict()
    smu3.update(vname='V',iname='I',mode ='V',func='VAR1')

    device.setup_smu(1,smu1)
    device.setup_smu(3,smu3)

    #define user functions
    device.user_function('Imm','mA/mm',f'I*{norm}')
    device.user_function('ABSImm','mA/mm','ABS(Imm)')

    var1 = device.var1_dict()
    var1.update(
        mode = parameters['hyst'].value,
        start = parameters['start'].value,
        stop = parameters['stop'].value,
        step = parameters['step'].value,
        comp = parameters['comp'].value,
        pcomp = 0
    )

    device.setup_var1(var1)
    device.integration_time(parameters['integration'].value)


    #display
    device.display_variable('X','V')
    device.display_variable('Y1','ABSImm')

    device.axis_scale('Y1','LOG')

    if parameters['start'].value < parameters['stop'].value:
        device.display_variable_min_max('X','MIN',parameters['start'].value)
        device.display_variable_min_max('X','MAX',parameters['stop'].value)

    else:
        device.display_variable_min_max('X','MAX',parameters['start'].value)
        device.display_variable_min_max('X','MIN',parameters['stop'].value)

    if parameters['comp'].value !=0:
        device.display_variable_min_max('Y1','MIN',0)
        device.display_variable_min_max('Y1','MAX',abs(parameters['comp'].value)*norm)

    
    variables_list = ['V','I','Imm','ABSImm']
    device.variables_to_save(variables_list)

    device.single_measurement()
    while device.operation_completed() == False:
        pass
    
    device.autoscaling()
    
    values = dict([(variable,device.return_values(variable)) for variable in variables_list])
    return values

def plot_results(x,y):
    fig, ax1 = plt.subplots()

    color = 'tab:red'
    ax1.set_xlabel('V/V') 
    ax1.set_ylabel('I/mA/mm', color = color)
    ax1.set_yscale('log')

    ax1.plot(x,y, color = color) 
    ax1.tick_params(axis ='y', labelcolor = color)

    fig.suptitle('MESA Plot', fontweight ="bold")
    fig.tight_layout()
    fig.show() #interactve Figures

def save_to_file(values,sample,parameters,file):
    with open(file,'w') as f:
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Mesa Measurement at {date}"+"\n")
        f.write(f"Series:{sample['processing_number'].value}"+"\n")
        f.write(f"Sample:{sample['sample'].value}"+"\n")
        f.write(f"Field:{sample['field'].value}"+"\n")
        f.write(f"Width/um:{sample['width'].value}"+"\n\n")

        f.write('Parameters\n')
        f.write(f"V from {parameters['start'].value}V to {parameters['stop'].value}V with step {parameters['step'].value}V"+"\n")
        f.write(f"I Compliance/A:{parameters['comp'].value}"+"\n")
        f.write(f"Integration Time:{parameters['integration'].value}"+"\n")
        
        f.write("\nResults\n")

    #create pandas dataframe
    df = pd.DataFrame(values)

    df = df.drop(columns=['ABSImm'])
    df = df.rename(columns={'V':'V/V','I':'I/A','Imm':'Imm/mA/mm'})

    df.to_csv(file,sep= " ",mode = 'a')
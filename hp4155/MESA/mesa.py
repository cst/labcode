import sys
sys.path.insert(0, './lib')
sys.path.insert(0, '..') #append parent directory

import hp4155a
from interface import *
from help import *

sample = sample_information_interface()
parameters =parameters_interface()
button = widgets.Button(description ='Start Measurement')
output = widgets.Output()

display(button,output)

all_widgets = [button]
add_widgets_to_list(sample,all_widgets)
add_widgets_to_list(parameters,all_widgets)

device = hp4155a.HP4155a('GPIB0::17::INSTR')
setup(device)
def on_button_clicked(b):
    with output:
        clear_output(wait = True)
        change_state(all_widgets)
        norm=normalization_factor(sample['width'].value)
        
        valid = check_values(parameters)
        if valid == True:
            values = measure(parameters,norm,device)
            plot_results(values['V'],values['ABSImm'])
            filename = f"MESA_ISO_{sample['field'].value}.txt"
            file = create_file(filename)
            save_to_file(values,sample,parameters,file)
        
        change_state(all_widgets)

button.on_click(on_button_clicked)
    
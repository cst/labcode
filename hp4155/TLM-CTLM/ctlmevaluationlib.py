"""Contains all evaluation classes and functions.

class LinearRegressionFitting: Represents a Linear Regression Fitting of a 2D x,y plot
"""

import sys
import os
import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt

#plt.ioff()  # Disable interactive mode.
# for interactive graphs add "%matplotlib widget" in the first cell, but this still has bugs

# Colors
rwthcolors = {
    "blau": "#00549F",
    "schwarz": "#000000",
    "magenta": "#E30066",
    "gelb": "#FFED00",
    "petrol": "#006165",
    "türkis": "#0098A1",
    "grün": "#57AB27",
    "maigrün": "#BDCD00",
    "orange": "#F6A800",
    "rot": "#CC071E",
    "bordeaux": "#A11035",
    "violett": "#612158",
    "lila": "#7A6FAC",
}
colorlist = [
    rwthcolors["blau"],
    rwthcolors["türkis"],
    rwthcolors["grün"],
    rwthcolors["orange"],
    rwthcolors["violett"],
]


# Function Definitions
def graphLRF(
    x, y, modelx, model, labeldata, labelfit, title, xlabel=None, ylabel=None, text=None
):
    """graphLRF graph for a Linear Regresion Fit
    x, y,
    modelx x values for the model,
    model, labeldata, labelfit, title, text=None"""
    fig, ax = plt.subplots()
    graph_input = ax.scatter(x, y, color=rwthcolors["blau"], label=labeldata)
    graph_fit = ax.plot(modelx, model(modelx), color=rwthcolors["rot"], label=labelfit)
    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.legend(loc="upper left")
    fig.tight_layout()
    ax.text(0.75, 0.1, text, transform=ax.transAxes, fontfamily="monospace")
    if "ipympl" in sys.modules and False:
        fig.canvas.toolbar_visible = True
        display(fig.canvas)
    else:
        display(fig)
        fig.clear()
        plt.close(fig)
    #fig.clear()
    #plt.close(fig)
    return


def np1darraytostring(array):
    return str(array.tolist())
    

# Class Definitions
class LinearRegressionFitting:
    """
    Represents a Linear Regression Fitting of a 2D x,y plot

    Attributes:
        x : numpy array (PandaDataFrame.to_numpy())
        y : numpy array (PandaDataFrame.to_numpy())
        xlabel : string
        ylabel : string
        slope : float
        intercept : float
        residuals
        diagnostics
        model : numpy poly1d class
        R2 : float
        df: panda DataFrame

    Methods:
        graph()
    """

    def __init__(self, PandaDataFrame, xlabel=None, ylabel=None):
        """
        Parameters
            PandaDataFrame
        """
        self.input = PandaDataFrame
        self.x = PandaDataFrame.to_numpy()[:, 0]
        self.y = PandaDataFrame.to_numpy()[:, 1]
        self.xlabel = xlabel
        self.ylabel = ylabel

        # https://numpy.org/doc/stable/reference/generated/numpy.polyfit.html
        [self.slope, self.intercept], self.residuals, *self.diagnostics = np.polyfit(
            self.x, self.y, 1, full=True
        )

        # model = expression of polynom from polyfit, here basicly
        # y = model(x) <=> same as writing  y = slope * x + intercept
        self.model = np.poly1d([self.slope, self.intercept])

        # Bestimmtheitsmaß / Coefficient of determination / R²
        #  1 - ( residual sum of squares / SUM( (yi - y_mean)² ) )
        self.R2 = 1 - (self.residuals[0] / np.sum(pow(self.y - np.average(self.y), 2)))

        self.df = pd.DataFrame({"x": self.x, "f(x)": self.model(self.x)})

    def graph(self):
        self.text = f"R²: {self.R2:8.4f}"
        graphLRF(
            self.x,
            self.y,
            self.x,
            self.model,
            "Input Data Values",
            "Linear Regression Fit",
            "Linear Regression Fitting",
            self.xlabel,
            self.ylabel,
            text=self.text,
        )


class TLM_Evaluation(LinearRegressionFitting):
    """
    Represents a TLM Evaluation derived from a Linear Regression Fitting of a 2D x,y plot

    Attributes:
        x : numpy array (PandaDataFrame.to_numpy())
        y : numpy array (PandaDataFrame.to_numpy())
        xlabel : string
        ylabel : string
        slope : float
        intercept : float
        residuals
        diagnostics
        model : numpy poly1d class
        R2 : float
        df : panda DataFrame
        contactlenght : float
        comment : string
        Rc : float
        Rsh : float
        LT : float
        rhoc : float

    Methods:
        graph()
    """

    def __init__(self, PandaDataFrame, contactlenght, comment=""):
        super().__init__(PandaDataFrame)
        # self.input
        # self.x
        # self.y
        self.xlabel = "Distances / µm"
        self.ylabel = "Resistance / Ohm"
        # self.slope
        # self.intercept
        # self.residuals
        # self.diagnostics
        # self.model
        # self.R2
        # self.df
        self._model_df_x_ = np.linspace(0, self.x[-1])
        self.df = pd.DataFrame(
            {"x": self._model_df_x_, "f(x)": self.model(self._model_df_x_)}
        )

        self.contactlenght = contactlenght

        self.comment = comment

        # Kontaktwiderstand Rc [Ohm*mm]
        # = RT(d=0)/2 [Ohm] * Contactlenght [µm/1000]
        self.Rc = (self.intercept / 2) * (contactlenght / 1000)

        # Schichtwiderstand [Ohm/sq = Ohm]
        # = slope [Ohm/µm] * Contactlenght [µm]
        self.Rsh = self.slope * contactlenght

        # Transferlänge LT [mm] RT(d) = 0
        # = slope [Ohm/µm] / RT(d=0) [Ohm] / 1000
        # self.LT = self.intercept / self.slope / 1000

        # Transferlänge LT [µm] RT(d) = 0
        # = slope [Ohm/µm] / RT(d=0) [Ohm]
        self.LT = self.intercept / self.slope / 2

        # LT = sqrt(rhoc/Rsh).
        # "Semiconductor Material and Device Characterization Third Edition",
        # D. Schroder, p. 140, Eq. 3.21
        # Spezifischer Kontaktwiderstand rhoc = LT² * Rsh
        # = Ohm cm² | µm²*0.00000001 = cm²
        self.rhoc = self.LT * self.LT * self.Rsh * 1e-4 * 1e-4

    def graph(self):
        self.text = f"{self.comment}\n\nR²  : {self.R2:8.4f}\n\nRc  : {self.Rc:8.2f}\nRsh : {self.Rsh:8.2f}\nLT  : {self.LT:8.2f}\nrhoc: {self.rhoc:8.2e}"
        graphLRF(
            self.x,
            self.y,
            self._model_df_x_,
            self.model,
            "Total Resistance",
            "linear regression fit",
            "(C)TLM evaluation",
            xlabel=self.xlabel,
            ylabel=self.ylabel,
            text=self.text,
        )


class CTLMandTLMMeasurement(object):
    """
    Represents a TLM Measurement

    Attributes:

    Methods:

    """

    def __init__(
        self,
        measurement_type,
        filelist,
        distances,
        contactlenght=50,
        inner_radius=55,
        output=False,
        file_type = None,
    ):
        """
        filelist as tuple
        distances as tuple ## Abstände der TLM in µm
        contactlenght # Kontaktweite der TLM Strukturen in µm
        """
        self._creation_date = datetime.datetime.now()
        if file_type == None:
            file_type = "jupyter"
        self.file_type = file_type.lower()
        self.measurement_type = measurement_type.lower()
        self.filelist = filelist
        self.path, self.files = self.importfiles(filelist)
        self.distances = distances
        if self.measurement_type == "tlm":
            del inner_radius
            self.contactlenght = contactlenght
            self.contactlenghtorcircumference = self.contactlenght
        if self.measurement_type == "ctlm":
            del contactlenght
            self.inner_radius = inner_radius
            self.inner_circumference = 2 * np.pi * self.inner_radius
            self.contactlenghtorcircumference = self.inner_circumference
            # "Semiconductor Material and Device Characterization Third Edition",
            # D. Schroder, p. 144, Eq. 3.31
            self.correction_factor = []
            for i in range(len(self.distances)):
                self.correction_factor.append(
                    (self.inner_radius / self.distances[i])
                    * np.log(1 + self.distances[i] / self.inner_radius)
                )
            # print(self.correction_factor)
        #print(self.contactlenghtorcircumference)
        self.df = self.construct_dataframes(self.filelist, self.file_type)
        # self.df_org = self.df
        self.R, self.lin_reg_dfs, self.R_statistics = self.R_from_lin_reg()
        self.RT0 = pd.DataFrame(
            {"d/µm": self.distances[: len(self.R)], "R_T/Ohm": self.R}
        )
        if len(self.R) > 2:
            self.eval0 = TLM_Evaluation(
                self.RT0,
                self.contactlenghtorcircumference,
                #comment=f"Distances:\n{self.distances[:len(self.R)]}",
            )
        if len(self.R) == 5:
            self.eval1, self.eval2 = self.find_RT1_RT2()
            self.results = self.build_results()
        self.refined = False
        if output and self.measurement_type == "tlm":
            print(
                f"Measurement Type: TLM \ndistances = {self.distances}, contact lenght = {self.contactlenght}\n"
            )
        if output and self.measurement_type == "ctlm":
            print(
                f"Measurement Type: CTLM \ndistances = {self.distances}, inner radius = {self.inner_radius}, inner_circumference = {self.inner_circumference:.2f}\n"
            )
        if self.refined:
            print(f"Evaluation only with current from {self.newrange[0]} to {self.newrange[0]}\n")
        if output:
            print(self.R_statistics)
        if output and len(self.R) == 5:
            print("\n\n--- --- Results --- ---", self.results, sep="\n")

    def importfiles(self, filelist):
        # if not len(filelist) == 5:
        #    raise Exception("Files Missing - I need 5 files for TLM or CTLM")
        filenames = []
        name = ""
        measurement = []
        for i in range(len(filelist)):
            filenames.append(os.path.split(filelist[i])[1])
            name, end = filenames[i].rsplit(sep="_", maxsplit=1)
            # print(name, "_", end)
            if i == 0:
                firstname = name
            elif not name == firstname:
                print("Files:", filenames)
                raise Exception("Filenames differ")
            measurement.append(end.split(sep=".")[0])
            # print(measurement)
            # print(i, len(files)-1)
        #   if (i == len(filelist)-1)  and (not measurement == ['1', '2', '3', '4', '5']):
        #       print("Files:", filenames)
        #       raise Exception("Not Measurement _1 to _5?")
        path = os.path.split(filelist[0])[0]
        return (path, filenames)

    def construct_dataframes(self, filelist=None, filetype=None):
        if filelist is None:
            filelist = self.filelist
        df = []
        
        if filetype is None:
            filetype = "jupyter"
            
        if filetype in ["new", "jupyter"]:
            for i in range(len(filelist)):
                df.append(
                    pd.read_csv(
                        filelist[i],
                        sep="\s+",
                        skip_blank_lines=True,
                        header=10,
                        index_col=0,
                        usecols=[0, 1, 2],
                        names=[None, "V/V", "I/A"],
                    )
                )
                
        #"new" or "old", "jupyter" or "labview"
        
        
        if filetype in ["old", "labview"]:
            for i in range(len(filelist)):
                df.append(
                    pd.read_csv(
                        filelist[i],
                        sep=" ",
                        skip_blank_lines=True,
                        header=3,
                        index_col=0,
                        usecols=[0, 1, 2, 4],
                        names=["#", "U/V", "I/A", "R/Ohm"],
                    )
                )
        
        #display(df)
        return df

    def R_from_lin_reg(self, PandaDataFrames=None):
        if PandaDataFrames is None:
            PandaDataFrames = self.df
        lin_reg_results = []  # R
        lin_reg_dfs = []
        statistics_string = (
            f"Statistics for Fitting of R Values \n    d / µm | R²     | RT / Ohm"
        )
        if self.measurement_type == "ctlm":
            statistics_string = statistics_string + "   | C-Values"
        # print(f"Statistics for Fitting of R Values \n {'d / µm' : >9} | {'R²' : <6} | RT / Ohm ")
        for i in range(len(PandaDataFrames)):
            fit = LinearRegressionFitting(PandaDataFrames[i])
            if self.measurement_type == "tlm":
                lin_reg_results.append(1 / (fit.slope))
            if self.measurement_type == "ctlm":
                lin_reg_results.append(1 / (fit.slope * self.correction_factor[i]))
            lin_reg_dfs.append(fit.df)
            # print(f"{self.distances[i] : 10d} | {fit.R2 : >5.4f} | {lin_reg_results[i] : 10.4f}")
            statistics_string += (
                "\n"
                + f"{self.distances[i] : 10d} | {fit.R2 : >5.4f} | {lin_reg_results[i] : 10.4f}"
            )
            if self.measurement_type == "ctlm":
                statistics_string = (
                    statistics_string + f" |{self.correction_factor[i] : .4f}"
                )
            # print(statistics_string)
        return lin_reg_results, lin_reg_dfs, statistics_string

    def find_RT1_RT2(self, R=None):
        if R is None:
            R = self.RT0

        # Remove One Measurement
        # Find maximal Bestimmtheitsmaß / Coefficient of determination / R²
        save_RT1 = None
        max = 0
        for i in range(len(R)):
            Ri = R.drop(labels=i, axis=0)
            evali = TLM_Evaluation(Ri, self.contactlenghtorcircumference)
            if evali.R2 >= max:
                max = evali.R2
                save_RT1 = evali
                # RT1_included_distances = Ri

        # Remove Two Measurements
        list_of_rows_to_remove = [
            [0, 1],
            [0, 2],
            [0, 3],
            [0, 4],
            [1, 2],
            [1, 3],
            [1, 4],
            [2, 3],
            [2, 4],
            [3, 4],
        ]
        save_RT2 = None
        max = 0
        for rows in list_of_rows_to_remove:
            Ri = R.drop(labels=rows, axis=0)
            evali = TLM_Evaluation(Ri, self.contactlenghtorcircumference)
            if evali.R2 >= max:
                max = evali.R2
                save_RT2 = evali

        return save_RT1, save_RT2  # , RT1_included_distances

    def uigraph(self, size=(5, 4), PandaDataFrames=None, lin_reg_dfs=None):
        if PandaDataFrames is None:
            PandaDataFrames = self.df
        if lin_reg_dfs is None:
            lin_reg_dfs = self.lin_reg_dfs

        fig, ax = plt.subplots(figsize=size)

        uicharts = []
        for i in range(len(PandaDataFrames)):
            x = PandaDataFrames[i].to_numpy()[:, 0]
            y = PandaDataFrames[i].to_numpy()[:, 1]
            modelx = lin_reg_dfs[i].to_numpy()[:, 0]
            modely = lin_reg_dfs[i].to_numpy()[:, 1]
            # print(x, y, modelx, modely)
            ax.scatter(x, y, color=colorlist[i], label=self.distances[i])
            ax.plot(modelx, modely, color=colorlist[i])  # , label=labelfit)

        # ax.set_title("UI")
        ax.set_xlabel("Voltage [V]")
        ax.set_ylabel("Current [I]")
        ax.legend(loc="upper left")
        ax.grid(which='both', axis='both',)#which='major', axis='both',)
        fig.tight_layout()
        # ax.text(.75,.1, text, transform=ax.transAxes, fontfamily='monospace')
        if "ipympl" in sys.modules:
            fig.canvas.toolbar_visible = True
            display(fig.canvas)
        else:
            display(fig)
            fig.clear()
            plt.close(fig)
        #fig.clear()
        #plt.close(fig)
        return
    
    def plotclosetozero(self, size=(5, 4), PandaDataFrames=None, number = 10):
        if PandaDataFrames is None:
            PandaDataFrames = self.df
        fig, ax = plt.subplots(figsize=size)
        for i in range(len(PandaDataFrames)):
            x = PandaDataFrames[i].to_numpy()[:, 0]
            y = PandaDataFrames[i].to_numpy()[:, 1]
            xabs = np.absolute(x)
            x_abs_min_index = np.where(xabs == np.min(xabs))
            #x_backup = np.copy(x)
            #y_backup = np.copy(y)
            numb = (number*2)+1
            if numb <= len(x):
                #print(x_abs_min_index)
                x = x[x_abs_min_index[0][0]-number:x_abs_min_index[0][0]+number+1]
                y = y[x_abs_min_index[0][0]-number:x_abs_min_index[0][0]+number+1]
                #print(x,y)
            else:
                pass
                #print("no change")
                
                
            
            ax.scatter(x, y, color=colorlist[i], label=self.distances[i])
            ax.plot(x, y, color=colorlist[i], label=self.distances[i])

        ax.set_title(f"UI close to origin (0 V, 0 A), {(number*2)+1} data points")
        ax.set_xlabel("Voltage / V")
        ax.set_ylabel("Current / I")
        ax.legend(loc="upper left")
        ax.grid(which='both', axis='both',)
        fig.tight_layout()
        # ax.text(.75,.1, text, transform=ax.transAxes, fontfamily='monospace')
        display(fig)
        fig.clear()
        plt.close(fig)
        return


    def rtgraph(self, size=(5, 4)):  # Resistance Transfer
        fig, ax = plt.subplots(figsize=size)

        ax.scatter(
            self.distances[: len(self.R)],
            self.R,
            color=rwthcolors["blau"],
            label="Total Resistance",
        )

        text = "Not enough measurements for fit"
        # text = f"""R² 5P: {self.eval0.R2:6.4f}\nR² 4P: {self.eval1.R2:6.4f}\nR² 3P: {self.eval2.R2:6.4f}\n"""

        if len(self.R) > 2:
            ax.plot(
                self.eval0.df.to_numpy()[:, 0],
                self.eval0.df.to_numpy()[:, 1],
                color=rwthcolors["bordeaux"],
                label=f"Lin. reg. fit with distances {str(self.eval0.x.astype(int).tolist())}",
            )
            text = f"R² all: {self.eval0.R2:6.4f}"
        if len(self.R) == 5:
            ax.plot(
                self.eval1.df.to_numpy()[:, 0],
                self.eval1.df.to_numpy()[:, 1],
                color=rwthcolors["violett"],
                label=f"Lin. reg. fit with distances {str(self.eval1.x.astype(int).tolist())}",
            )
            ax.plot(
                self.eval2.df.to_numpy()[:, 0],
                self.eval2.df.to_numpy()[:, 1],
                color=rwthcolors["lila"],
                label=f"Lin. reg. fit with distances {str(self.eval2.x.astype(int).tolist())}",
            )
            text = f"""R² all: {self.eval0.R2:6.4f}\nR²  4P: {self.eval1.R2:6.4f}\nR²  3P: {self.eval2.R2:6.4f}\n"""

        ax.set_xlabel("Distance [µm]")
        ax.set_ylabel("Resistance [Ohm]")
        ax.legend(loc="upper left")
        fig.tight_layout()
        ax.text(0.75, 0.1, text, transform=ax.transAxes, fontfamily="monospace")
        ax.grid(which='both', axis='both',)
        if "ipympl" in sys.modules and False:
            fig.canvas.toolbar_visible = True
            display(fig.canvas)
        else:
            display(fig)
            fig.clear()
            plt.close(fig)
        #fig.clear()
        #plt.close(fig)
        return

    def build_results(self, header=True, comment=""):
        results_string = ""
        if header:
            results_string += f"Path: {self.path}\n"
            results_string += "Files: {}, {}, {}, {}, {} \n".format(*self.files)
            results_string += comment + " \n"
            results_string += "Feld Rsh      R²     Rc       LT     rhoc      Distances \n"
            results_string += "-    [Ohm/sq] -      [Ohm mm] [µm]   [Ohm cm²] [µm] \n"
        # header = "Feld Rsh      R²     Rc       LT     rhoc      min I max I # removed values"
        # header = "Feld Rsh      R²     Rc       LT     rhoc      # removed values"
        # units =  "-    [Ohm/sq] -      [Ohm mm] [µm]   [Ohm cm²] [A]   [A]   -"
        # units =  "-    [Ohm/sq] -      [Ohm mm] [µm]   [Ohm cm²] -"
        format_string = "{:<4} {:8.2f} {:6.4f} {:8.2f} {:6.2f} {:9.2e} {:<20} \n"
        results_string += format_string.format(
            self.files[0].rsplit("_")[0],
            self.eval0.Rsh,
            self.eval0.R2,
            self.eval0.Rc,
            self.eval0.LT,
            self.eval0.rhoc,
            # minI,
            # maxI,
            np1darraytostring(self.eval0.x.astype(int)),
        )
        results_string += format_string.format(
            self.files[0].rsplit("_")[0],
            self.eval1.Rsh,
            self.eval1.R2,
            self.eval1.Rc,
            self.eval1.LT,
            self.eval1.rhoc,
            # minI,
            # maxI,
            np1darraytostring(self.eval1.x.astype(int)),
        )
        results_string += format_string.format(
            self.files[0].rsplit("_")[0],
            self.eval2.Rsh,
            self.eval2.R2,
            self.eval2.Rc,
            self.eval2.LT,
            self.eval2.rhoc,
            # minI,
            # maxI,
            np1darraytostring(self.eval2.x.astype(int)),
        )
        return results_string

    def refine_evaluated_range(self, newrange = None):
        if not type(newrange) is tuple:
            raise Exception("Wrong format for Newrange! Should be tuple (float, float)")
        if not len(newrange) == 2:
            raise Exception("Two few or many values in newrange! Should be tuple (float, float)")
        if not len(self.R) == 5:
            raise Exception("Newrange only with all five (C)TLM measurments implemented")
        minI, maxI = newrange
        self.newrange = newrange
        refined_df = self.df
        for i in range(len(refined_df)):
            # First vector where values for I/A are bigger than min and smaller than max = True
            # (dataframe.iloc[:,1] > min) & (dataframe.iloc[:,1] < max )
            # then this boolean vector is used as indexer for the dame dataframe
            # every value with False is sliced
            work = refined_df[i]
            #print(work)
            #print((dataframe.iloc[:,1] > minI) & (dataframe.iloc[:,1] < maxI ))
            work = work [ (work.iloc[:,1] > minI) & (work.iloc[:,1] < maxI ) ]
            #print(work)
            refined_df[i] = work
        self.df = refined_df
        self.R, self.lin_reg_charts, self.R_statistics = self.R_from_lin_reg()
        self.RT0 = pd.DataFrame({'d/µm':self.distances, 'R_T/Ohm':self.R})
        self.eval0 = TLM_Evaluation(self.RT0, self.contactlenghtorcircumference, comment=f"Evaluation only with current from {self.newrange[0]} to {self.newrange[0]}",)
        self.eval1, self.eval2 = self.find_RT1_RT2()
        self.results = self.build_results(comment=f"Evaluation only with current from {self.newrange[0]} to {self.newrange[0]}\n")
        self.refined = True

import ipywidgets as widgets
from ipywidgets import GridspecLayout,Layout
from IPython.display import clear_output

import sys
sys.path.insert(0, '..') #append parent directory
import hp4155a

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox

import time
import matplotlib.pyplot as plt
import os
from datetime import datetime

import pandas as pd
import configparser

# Interface to collect information about the sample
def sample_interface():
    width = "90%"
    height = "auto"
    style = {'description_width': 'initial'}
    
    sample = GridspecLayout(6,3)

    sample[0,0] = widgets.Label("Sample Information")
    sample[0,0].style.font_weight = 'bold'

    #iterate over the first two columns
    for i in range(1,6,2):
        sample[i:i+2,0]= widgets.Text(layout=Layout(height=height, width=width),style = style)

    
    sample[1:3,0].description = "Processing Number:"
    sample[3:5,0].description = "Sample Series:"
    sample[5:7,0].description = "Field:"
    
    
    #measurement information in the next columns
    sample[1:3,1]= widgets.Dropdown(options=["TLM","CTLM"],value ='TLM',layout=Layout(height=height, width='60%'),description="Type:") #TLM or CTLM
    sample[3:5,1]= widgets.BoundedIntText(value=50,min=1,max=sys.maxsize,step=1,layout=Layout(height=height, width=width),description="Contactlength(um):",style = style)
    sample[5:7,1]= widgets.BoundedIntText(value=100,min=1,max=sys.maxsize,step=1,layout=Layout(height=height, width='60%'),description="Width(um):",style = style)
    
    # distannces is the 3rd column
    sample[0,2] = widgets.Label("Distances(um)")
    sample[0,2].style.font_weight = 'bold'

    distances = [5,10,15,20,50]

    for i,distance in enumerate(distances):
        sample[i+1,2]= widgets.BoundedIntText(value=int(distance),min=1,max=sys.maxsize,step=1,layout=Layout(height=height, width="60%"),description=f"d{i+1}:",style = style)

    def on_type_change(change):
        if change['new'] =="TLM":
            sample[3:5,1].value = 50
            sample[3:5,1].description = 'Contactlength(um)'
        else:
            sample[3:5,1].value = 55
            sample[3:5,1].description = 'Inner Radius(um)'

    sample[1:3,1].observe(on_type_change, names='value')
    display(sample)

    sample_dict = {
        'processing_number':sample[1:3,0],
        'sample_series':sample[3:5,0],
        'field':sample[5:7,0],
        'type':sample[1:3,1],
        'length':sample[3:5,1],
        'width':sample[5:7,1],
        'd1':sample[1,2],
        'd2':sample[2,2],
        'd3':sample[3,2],
        'd4':sample[4,2],
        'd5':sample[5,2]
    }
    
    return sample_dict

#function to get interface from measurement parameters
def parameters_interface():
    width = "60%"
    height = "auto"
    style = {'description_width': 'initial'}
    
    parameters_title=widgets.Label("I(SMU3)",layout=Layout(height=height, width='50%'))
    parameters_title.style.font_weight='bold'
    
    parameters = GridspecLayout(4,3)

    #start value
    parameters[0,0]=widgets.Label("start",layout=Layout(height=height, width=width))
    parameters[1,0]=widgets.BoundedFloatText(value=-50e-3,min=-0.1,max=0.1,step=1e-3,layout=Layout(height=height, width=width))

    
    #compliance value
    parameters[2,0] = widgets.Label("compliance",layout=Layout(height=height, width=width))
    parameters[3,0] = widgets.BoundedFloatText(value=10,min=-100,max=100,step=1,layout=Layout(height=height, width=width))

    #stop value
    parameters[0,1]=widgets.Label("stop",layout=Layout(height=height, width=width))
    parameters[1,1]=widgets.BoundedFloatText(value=50e-3,min=-0.1,max=0.1,step=1e-3,layout=Layout(height=height, width=width))

    #hysterisis
    parameters[2,1]= widgets.Label("hysterisis",layout=Layout(height=height, width=width))
    parameters[3,1]=widgets.Dropdown(options=['SINGle','DOUBle'],value='SINGle',layout=Layout(height=height, width=width))

    #step
    parameters[0,2] = widgets.Label("step",layout=Layout(height=height, width=width))
    parameters[1,2] = widgets.BoundedFloatText(value=1e-3,min=-0.1,max=0.1,step=1e-3,layout=Layout(height=height, width=width))

    #integration time
    parameters[2,2]=widgets.Label("Integration Time",layout=Layout(height=height, width=width))
    parameters[3,2]= widgets.Dropdown(options=["SHORt","MEDium","LONG"],value="MEDium",layout=Layout(height=height, width=width))
    
    display(parameters_title)
    display(parameters)

    parameters_dict = {
        'start':parameters[1,0],
        'comp':parameters[3,0],
        'stop':parameters[1,1],
        'hyst':parameters[3,1],
        'step':parameters[1,2],
        'integration':parameters[3,2]           
    }
    return parameters_dict


#functions for the widgets enabling and disabling

def add_widgets_to_list(source_dictionary,target_list):
    for widget in source_dictionary.values():
        target_list.append(widget)

def change_state(widgets_list):
    for widget in widgets_list:
        widget.disabled = not widget.disabled

def enable_widgets(widgets_list):
    for widget in widgets_list:
        widget.disabled = False

def disable_widgets(widgets_list):
    for widget in widgets_list:
        widget.disabled = True


#check if start stop and step are correct
def check_values(start,stop,step):
    valid = True

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    if abs(step.value) > abs(stop.value-start.value) or step.value==0:#invalid parameter setting 
        valid = False
        tkinter.messagebox.showerror(message="Invalid parameter setting!")

    if start.value<stop.value and step.value<0: #change polarity
        step.value =(-1)*step.value

    elif start.value>stop.value and step.value>0:
        step.value = (-1)*step.value

    else:
        pass

    return valid

# Setup TLM measurement in the tool
def setup_TLM(smu1,smu2,smu3,smu4,integration,device):
    #reset the device
    device.reset()
    
    #set all the smus
    device.setup_smu(1,smu1)
    device.setup_smu(2,smu2)
    device.setup_smu(3,smu3)
    device.setup_smu(4,smu4)
    
    device.setup_var1(smu3["VAR1"])
    device.setup_cons_smu(2,smu2["CONS"])
    device.setup_cons_smu(4,smu4["CONS"])

    device.integration_time(integration)

    device.user_function('I','A','IS')
    device.user_function('V','V','V4-V2')
    device.user_function('R','OHM','V/I')

    device.display_variable("X",'V')
    device.display_variable("Y1","I")
    device.display_variable("Y2",'R')

    if smu3['VAR1']['start']<smu3['VAR1']['stop']:
        device.display_variable_min_max('Y1','MIN',smu3['VAR1']['start'])
        device.display_variable_min_max('Y1','MAX',smu3['VAR1']['stop'])

    else:
        device.display_variable_min_max('Y1','MAX',smu3['VAR1']['start'])
        device.display_variable_min_max('Y1','MIN',smu3['VAR1']['stop'])

    if smu3['VAR1']['comp'] !=0:
        device.display_variable_min_max('X','MIN',-abs(smu3['VAR1']['comp']))
        device.display_variable_min_max('X','MAX',abs(smu3['VAR1']['comp']))

    device.display_variable_min_max('Y2','MAX',200)
    device.display_variable_min_max('Y2','MIN',0)

# Measure TLM
def measure_TLM(device):
    device.single_measurement()
    while device.operation_completed()==False:
        pass

    device.autoscaling()

    V=device.return_values("V")
    I = device.return_values("I")
    return V,I

#ask to exit the measurement
def ask_to_exit():
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    answer = tk.messagebox.askokcancel(message="Do you want to exit/abort the measurement? Please note that any unsaved measuerement results will be deleted!")
    root.destroy()
    return answer

#this function displays an information box!
def information_box(information):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #display meaagebox
    tkinter.messagebox.showinfo(message=information)
    root.destroy()


#check if a folder is writable
def check_writable(folder):
    filename = "test.txt"
    file = os.path.join(folder,filename)

    #protection against removing existing file in python
    i=1
    while os.path.exists(file):
        filename=f"test{i}.txt"
        file = os.path.join(folder,filename)
    try:
        with open(file,'a'):
            writable = True
        os.remove(file)
    except:
        writable = False
        information_box(f"{folder} is not writable!")
    
    return writable  

#choose directory for saving the results
def choose_folder():
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #choose nonemty folder
    folder = tk.filedialog.askdirectory()
    
    while folder == '':
        folder = tk.filedialog.askdirectory()

    #check if writable in a while loop
    writable=check_writable(folder)

    while writable == False:
        #choose a correct folder
        folder = tk.filedialog.askdirectory()
    
        while folder == '':
            folder = tk.filedialog.askdirectory()
        
        #check writable if not repeat
        writable=check_writable(folder)
        
    root.destroy()
    return folder

#save parameters as ini
def save_as_ini(default_filename):
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".ini", filetypes=[("Ini files","*.ini")],title = "save as ini",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".ini") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".ini", filetypes=[("Ini files","*.ini")],title = "save as ini",initialfile =default_filename)
    root.destroy()
    return file

# load ini file
def load_ini():
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    
    file = filedialog.askopenfilename(filetypes=[("Ini files","*.ini")],title ='Select ini file')
    while file.endswith(".ini") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.askopenfilename(filetypes=[("Ini files","*.ini")],title = "Select ini file")
    root.destroy()
    return file

def select_files():
    def valid_files(files):
        if len(files)>5:
            tk.messagebox.showerror(message='More than 5 files selected!')
            return False
        for file in files:
            if file.endswith(".txt") == False:
                #open again filedialog with error message box
                tk.messagebox.showerror(message='invalid filename!')
                return True
        return True
    
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    
    files = filedialog.askopenfilenames(filetypes=[("Txt files","*.txt")],title ='Select files')
    valid = valid_files(files)
    while valid == False:
        files = filedialog.askopenfilenames(filetypes=[("Txt files","*.txt")],title ='Select files')
        valid = valid_files(files) 
    root.destroy()
    return files
import matplotlib.pyplot as plt
import pandas as pd
from datetime import datetime
import os 
import sys
import numpy as np
from IPython.display import display, clear_output
import ipywidgets as widgets
import configparser
from ipyfilechooser import FileChooser
from pathlib import Path

sys.path.append(r"C:\Users\user\labcode\hp4155")
import module

def ctlm():
    #default parameters
    plt.ion()
    innen=0
    distances=(5,10,15,25,45)
    field_name ='M00'
    start=-50*10**(-3)
    stop=50*10**(-3)
    step=10**(-3)
    comp=10
    time='MED'
    j=-1
    contactlength=0
    
    #initialze variables for removing the plots later
    curve_1=0
    curve_2=0
    V=0
    I=0
    R=0
    
    #default files and paths
    ini_file_path=os.getcwd()
    ini_file_name=r"default.ini"
    ini_file=os.path.join(ini_file_path,ini_file_name)
    
    save_file_path = os.getcwd()
    save_file_path = Path(save_file_path)
    
    #connect to the device
    device = module.HP4155a('GPIB0::17::INSTR')
    date = str(datetime.today().replace(microsecond=0))
    
    #initialize figure
    fig=0
    ax1=0
    ax2=0
    
    ### buttons####

    # Create a text input widget
    text_input = widgets.Text(
        value='M00',               # Initial value (can be left empty)
        placeholder='Enter text here',
        description='Field:',
    )

    # Display the text input widget
    display(text_input)

    # Function to handle the input
    def handle_text_input(change):
        input_value = change.new
        #print("Input value:", input_value)

    # Attach the event handler to the text input widget
    text_input.observe(handle_text_input, names='value')

    # To access the current value as a variable:
    #current_value = text_input.value
    #print("Current value:", current_value)
    
    #filechooser and path chooser
    fc = FileChooser(select_desc='load .ini')
    fc.default_path = r"\\FILESERVER\public"
    fc.filter_pattern = '*.ini'
    display(fc)


    pc = FileChooser(select_desc="save path")
    pc.default_path = r"\\FILESERVER\public"
    pc.show_only_dirs = True
    display(pc)
    
    #display buttons
    start_button = widgets.Button(description="Start NEW!")
    start_output = widgets.Output()
    
    display(start_button, start_output)
    
    repeat_button = widgets.Button(description="repeat last")
    repeat_output = widgets.Output()
    
    display(repeat_button, repeat_output)
    
    continue_button = widgets.Button(description="save+continue")
    continue_output = widgets.Output()
    
    display(continue_button, continue_output)

    exit_button = widgets.Button(description='exit')
    exit_output = widgets.Output()

    display(exit_button,exit_output)

    #list of buttons
    buttons = [start_button,continue_button, repeat_button,exit_button]
    #FUNCTIONS OF BUTTONS
    def on_start_button_clicked(b):
        nonlocal innen,distances,field_name,start,stop,step,comp,time,j,curve_1,curve_2,V,I,R,ini_file,save_file_path,device,date,fig,ax1,ax2,contactlength,buttons
        with continue_output:
            clear_output()
        with repeat_output:
            clear_output()
        with exit_output:
            clear_output()
        with start_output:
            clear_output()
            #disable buttons
            for button in buttons:
                button.disabled=True
            j=0
            #ensure that choose ini file has been clicked
            if  fc.selected != None:
                ini_file = fc.selected
            #configparser for decoding
            try:
                config=configparser.ConfigParser()
                config.read(ini_file)
    
                #get all parameters and raise exceptions
                start=config.getfloat('parameters','start')
                if abs(start)>1:
                    raise Exception("current start value can be from -1A to 1A")
                
                stop=config.getfloat('parameters','stop')
                if abs(stop)>1:
                    raise Exception('current stop value can be from -1A to 1A')
                elif stop==start:
                    raise Exception('start and stop values cannot be equal!')
                else:
                    pass
                
                step=config.getfloat('parameters','step')
                step=abs(step)#polarity doesnt matter and probably will lead to errors(tested)
                # the absolute value in the if abs(step) is not neccesary because now step>0
                if abs(step)>abs(stop-start) or step==0:
                    raise Exception("Invalid step")
                
                comp=config.getfloat('parameters','comp')
                if abs(comp)>200:
                    raise Exception("voltage compliance can be from -200V to 200V")
                
                time=config.get('parameters','time')
                if time!='SHOR' and time!='MED' and time!='LONG':
                    raise Exception('Integration time can be SHOR(short),MED(medium) or LONG(long)')

                distances = config.get('information','distances')
                distances = distances.split(',')
                distances= [int(i) for i in distances]
                distances=tuple(distances)

                innen = config.getfloat('information','innen')
                contactlength = config.getfloat('information','contactlength')
                field_name= text_input.value
                
                if (innen==0 and contactlength==0) or (innen!=0 and contactlength!=0):
                    raise Exception("invalid length!")
                
                 #initialize figure
                plt.figure().clear()
                fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
                if innen==0:
                    fig.suptitle('TLM plot')
                    print('TLM with:')
                else: #contactlength=0 
                    fig.suptitle('CTLM plot')
                    print('CTLM with:')
                ax1.set_title('I(V)')
                ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
                ax2.set_title('R(V)')
                ax2.set(xlabel='Voltage(V)',ylabel='Resistance(Ohm)')
              
                print(f"  I[A]={start} to {stop}")
                print(f"  Absolute Step[A]={step}")
                print(f"  Distances[µm]:{distances}")
                print(f"  Ini-file:{ini_file}")
                if  pc.selected != None:
                    save_file_path = pc.selected
                print(f'  Path:{save_file_path}')

                #setup
                device.reset()
                device.inst.write(":PAGE:MEAS")
                device.inst.write(":PAGE:CHAN:MODE SWEEP") #go to sweep page and prepare sweep measurement

                #disable vmus and vsus
                device.disable_vsu(1)
                device.disable_vsu(2)
                device.disable_vmu(1)
                device.disable_vmu(2)

                #smu1 is constant and common
                device.smu_mode_meas(1,'COMM')
                device.smu_function_sweep(1,'CONS')
    
                #smu2 is constant and I
                device.smu_mode_meas(2,'I')
                device.smu_function_sweep(2,'CONS')
                device.cons_smu_value(2,0)
    
                #smu3 is var1 and I
                device.smu_mode_meas(3,'I')
                device.smu_function_sweep(3,'VAR1')
    
                #smu4 is constant and I
                device.smu_mode_meas(4,'I')
                device.smu_function_sweep(4,'CONS')
                device.cons_smu_value(4,0)
    
                #select compliance of smu3
                device.comp('VAR1',comp)
    
                #compliance of smu2 and smu4 is 10V
                device.const_comp(2,10)
                device.const_comp(4,10)

                #define user functions
                device.user_function('I','A','I3')
                device.user_function('V','V','V4-V2')
                device.user_function('R','OHM','DIFF(V,I)')
                device.user_function('VS','V','V3')
        

                #integration time
                device.integration_time(time)
    
                #define start-step-stop
                device.start_value_sweep(start)
                device.step_sweep(step)#positive value
                #polarity doesn't matter and probably will lead to errors(tested)
                device.stop_value_sweep(stop)

                #display variables
                device.display_variable('X','V')
                device.display_variable('Y1','I')
                device.display_variable('Y2','R')

                device.display_variable_min_max('X','MIN',-comp)
                device.display_variable_min_max('X','MAX', comp)
                device.display_variable_min_max('Y1','MIN',start)
                device.display_variable_min_max('Y1','MAX',stop)
                device.display_variable_min_max('Y2','MIN',0)
                device.display_variable_min_max('Y2','MAX',200)
                
                #start measurement
                device.single_measurement()
                while device.operation_completed() == False:
                    pass
        
                device.autoscaling()
                #return data from the device
        
                V=device.return_data('V')
                I=device.return_data('I')
                R=device.return_data('R')
        
                # now we have to remove resistance values that R=inf(nan) that means that the current is zero
                for i in range(len(R)):
                    if abs(R[i])>10**6:
                        R[i]=float('NAN')
                
                # plot the results
                curve_1=ax1.plot(V,I,label=f"distance={distances[j]}")
                curve_2=ax2.plot(V,R,label=f"distance={distances[j]}")
                ax1.legend(loc='best')
                ax2.legend(loc="best")
                fig.tight_layout()
            except Exception as e:
                print(e)
                print('please ensure that all parameters are correct and do not leave any spaces')
            #activate buttons
            for button in buttons:
                button.disabled=False
        with exit_output:
            clear_output(wait=True)
            display(fig)

    def on_repeat_button_clicked(b):
        nonlocal innen,distances,field_name,start,stop,step,comp,time,j,curve_1,curve_2,V,I,R,ini_file,save_file_path,device,date,fig,ax1,ax2,contactlength,buttons
        with repeat_output:
            for button in buttons:
                button.disabled=True
            clear_output()
            if j==-1:
                print("cannot repeat before first measurement!")
            else:
                #remove last measurement
                line = curve_2.pop(0)
                line.remove()
                line= curve_1.pop(0)
                line.remove()
                #start measurement
                device.single_measurement()
                while device.operation_completed() == False:
                    pass
        
                device.autoscaling()
                #return data from the device
        
                V=device.return_data('V')
                I=device.return_data('I')
                R=device.return_data('R')
        
                # now we have to remove resistance values that R=inf(nan) that means that the current is zero
                for i in range(len(R)):
                    if abs(R[i])>10**6:
                        R[i]=float('NAN')
                
                # plot the results
                curve_1=ax1.plot(V,I,label=f"distance={distances[j]}")
                curve_2=ax2.plot(V,R,label=f"distance={distances[j]}")
                ax1.legend(loc='best')
                ax2.legend(loc="best")
                fig.tight_layout()
                for button in buttons:
                    button.disabled=False
        with exit_output:
            clear_output(wait=True)
            display(fig)
                

    def on_continue_button_clicked(b):
        nonlocal innen,distances,field_name,start,stop,step,comp,time,j,curve_1,curve_2,V,I,R,ini_file,save_file_path,device,date,fig,ax1,ax2,contactlength
        with continue_output:
            clear_output()
            for button in buttons:
                button.disabled=True
            if j==-1:
                print('first click start New!')
        
            elif j>=0 or j<len(distances):
                #save previous measurement
                #ensure that pathchooser has been clicked
                if  pc.selected != None:
                    save_file_path = pc.selected

                print("save location:"+str(save_file_path))
            
                #export data frame to csv(for evaluation) and txt 
                header = ['Voltage(V)', 'Current(A)','Resistance(Ohm)']

                data = {header[0]:V,header[1]:I,header[2]:R}
                df = pd.DataFrame(data)
                #print(df)
        
                #export to txt 
                #check tlm or ctlm
                if innen==0:
                    #specify path and file_name
                    file_name = field_name+"_TLM_"+str(j+1)+".txt"
                    path= os.path.join(save_file_path,file_name)
        
                    #check if file name exists
                    i=1
                    while os.path.exists(path):
                        file_name = field_name+"_TLM_"+str(j+1)+"_"+str(i)+".txt"
                        path= os.path.join(save_file_path,file_name)
                        i=i+1
                else: #contactlength=0
                    #specify path and file_name
                    file_name = field_name+"_CTLM_"+str(j+1)+".txt"
                    path= os.path.join(save_file_path,file_name)
        
                    #check if file name exists
                    i=1
                    while os.path.exists(path):
                        file_name = field_name+"_CTLM_"+str(j+1)+"_"+str(i)+".txt"
                        path= os.path.join(save_file_path,file_name)
                        i=i+1
            
                title = "measured field:"+field_name+"\nContact length:"+str(contactlength)+"µm\nInnen radius:"+str(innen)+"µm \ndistance:"+str(distances[j])+"µm \nI:"+str(start)+"A to "+str(stop)+"A with step:"+str(step)+"\nDate:"+date+"\n"
        
                f=open(path, 'a')
                f.write(title)
                df_string = df.to_string()
                f.write(df_string)
                f.close()
        
                #export to csv for evaluataion
        
                if innen==0:
                #specify path and file_name
                    file_name = field_name+"_TLM_"+str(j+1)+".csv"
                    path= os.path.join(save_file_path,file_name)
        
                    #check if file name exists
                    i=1
                    while os.path.exists(path):
                        file_name = field_name+"_TLM_"+str(j+1)+"_"+str(i)+".csv"
                        path= os.path.join(save_file_path,file_name)
                        i=i+1
                else:
                    #specify path and file_name
                    file_name = field_name+"_CTLM_"+str(j+1)+".csv"
                    path= os.path.join(save_file_path,file_name)
        
                    #check if file name exists
                    i=1
                    while os.path.exists(path):
                        file_name = field_name+"_CTLM_"+str(j+1)+"_"+str(i)+".csv"
                        path= os.path.join(save_file_path,file_name)
                        i=i+1
        
                df.to_csv(path)
            
                if j == len(distances)-1:
                    print("programm finished!")
                    #reset counter! user could press repeat measurement
                    j=-1
                #execute next!
                else:

                    j=j+1
                    #start measurement
                    device.single_measurement()
                    while device.operation_completed() == False:
                        pass
        
                    device.autoscaling()
                    #return data from the device
        
                    V=device.return_data('V')
                    I=device.return_data('I')
                    R=device.return_data('R')
        
                    # now we have to remove resistance values that R=inf(nan) that means that the current is zero
                    for i in range(len(R)):
                        if abs(R[i])>10**6:
                            R[i]=float('NAN')
                
                    # plot the results
                    curve_1=ax1.plot(V,I,label=f"distance={distances[j]}")
                    curve_2=ax2.plot(V,R,label=f"distance={distances[j]}")
                    ax1.legend(loc='best')
                    ax2.legend(loc="best")
                    fig.tight_layout()
            else:
                print('programm finished!')
                #reset counter! user could press repeat measurement
                j=-1
            for button in buttons:
                button.disabled=False
        with exit_output:
            clear_output(wait=True)
            display(fig)

    def on_exit_button_clicked(b):
        with exit_output:
            os._exit(00)

    #lINK FUNCTIONS WITH THE BUTTONS
    start_button.on_click(on_start_button_clicked)
    repeat_button.on_click(on_repeat_button_clicked)
    continue_button.on_click(on_continue_button_clicked)
    exit_button.on_click(on_exit_button_clicked)

    
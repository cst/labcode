from help import *
import ctlmevaluationlib
#from ctlmevaluationlib import *

#the interface
sample = sample_interface()
print()
parameters = parameters_interface()
print()

#the buttons

#initialize figure
fig,ax = plt.subplots()


print('Controls')
start_button = widgets.Button(description="Start NEW!")
repeat_button = widgets.Button(description="repeat last")
continue_button = widgets.Button(description="save+continue")
exit_button = widgets.Button(description='exit/abort')
evaluation_button = widgets.Button(description='evaluation')

control_buttons = widgets.HBox([start_button,repeat_button,continue_button,exit_button,evaluation_button])
display(control_buttons)
print()

print("Save Parameters in .ini file")
export_ini_button = widgets.Button(description = 'Export as ini')
import_ini_button = widgets.Button(description='Import from ini')

ini_buttons = widgets.HBox([import_ini_button,export_ini_button])
output = widgets.Output()

display(ini_buttons,output)

#create the lists for controlling the interactivty
input_fields = []
add_widgets_to_list(sample,input_fields)
add_widgets_to_list(parameters,input_fields)

all_buttons =[start_button,repeat_button,continue_button,exit_button,import_ini_button,export_ini_button]
first_time_buttons= [repeat_button,continue_button,exit_button]
disable_widgets(first_time_buttons)

#disable evaluation button (for now)
evaluation_button.disabled = True 


#connect to the device
device = hp4155a.HP4155a('GPIB0::17::INSTR')

#initalize counter 
counter = -1

#initialze global variables for removing the plots and saving results later
curve = 0
V=0
I=0
distances = []
folder = None

#initialize smus (empty)
smu1 = device.smu_dict()
smu2 = device.smu_dict()
smu3 = device.smu_dict()
smu4 = device.smu_dict()
var1 = device.var1_dict()

smu1.update(vname = 'VGND',iname ='IGND',mode='COMM',func='CONS')
smu2.update(vname = 'V2',iname = 'I2',mode='I',func='CONS')
smu3.update(vname ='VS',iname='IS',mode='I',func='VAR1')
smu4.update(vname ='V4',iname ='I4',mode='I',func='CONS')

#update the dictionaries of the constant smus
const_parameters = device.cons_smu_dict()
const_parameters.update(value=0,comp = 10)

smu2.update(CONS=const_parameters)
smu4.update(CONS=const_parameters)

#list to save the filenames
filenames = []

def on_start_button_clicked(b):
    with output:
        global V,I,curve,distances,counter,folder,filenames
        clear_output(wait = True)
        
        #disable all widgets
        disable_widgets(input_fields)
        disable_widgets(all_buttons)

        #check if values are correct
        valid = check_values(start=parameters['start'],stop=parameters['stop'],step=parameters['step'])

        if valid == True:
            #load parameters,exexute measurement and plot results 
            filenames = []

            #clear the figure
            ax.cla()
        
            #first reset counter
            counter= 0

            #retrive distances
            distances = [sample[f"d{i}"].value for i in range(1,6)]

            #choose folder
            folder = choose_folder()
            
            #load parameters
            #update the dictionary var1
            var1.update(
                mode = parameters['hyst'].value,
                start = parameters['start'].value,
                stop = parameters['stop'].value,
                step = parameters['step'].value,
                comp = parameters['comp'].value,
                pcomp = 0
            )
            smu3.update(VAR1=var1)

            #setup the tlm measurement
            setup_TLM(smu1,smu2,smu3,smu4,parameters["integration"].value,device)

            # execute the TLM measurement
            V,I=measure_TLM(device)

            #plot results
            fig.suptitle(sample["type"].value)
            ax.set_xlabel("V / V")
            ax.set_ylabel("I / A")
            curve=ax.plot(V,I,label =f"distance (um):{distances[counter]}")
            ax.legend()
            fig.show()

            change_state(first_time_buttons)
        else:#enable the widgets again
            enable_widgets(input_fields)
            enable_widgets(all_buttons)

def on_repeat_button_clicked(b):
    with output:
        global V,I,curve,counter
        change_state(first_time_buttons)
        clear_output(wait = True)

        #remove last measurement
        line = curve.pop(0)
        line.remove()

        #measure again
        V,I=measure_TLM(device)

        #update results
        curve=ax.plot(V,I,label =f"distance (um):{distances[counter]}")
        ax.legend()
        fig.show()

        if counter==4:
            information_box('You have completed the 5th Measurement. Press save+continue to save results')

        change_state(first_time_buttons)

def on_continue_button_clicked(b):
    with output:
        global V,I,curve,counter,folder,filenames
        change_state(first_time_buttons)
        clear_output(wait = True)

        #save results(counter = 0,1,2,3,4)

        # fstrings are problematic extract values
        field =sample["field"].value
        type = sample["type"].value
        date = str(datetime.today().replace(microsecond=0))
        processing_number = sample["processing_number"].value
        sample_series = sample["sample_series"].value
        length = sample["length"].value
        width = sample["width"].value

        start = parameters["start"].value
        stop = parameters["stop"].value
        step = parameters["step"].value
        comp = parameters["comp"].value
        integration = parameters["integration"].value
        
        filename = f"{field}_{type}_{counter+1}.txt"
        path = os.path.join(folder,filename)

        #check if filename exists
        i = 1
        while os.path.exists(path):
            filename = f"{field}_{type}_{counter+1}({i}).txt"
            path= os.path.join(folder,filename)
            i=i+1

        #write to file
        if type =="TLM":
            title_sample = f"{type} Measurement at {date}"+"\n"+f"Processing_Number:{processing_number}"+"\n"+f"Sample_Series:{sample_series}"+"\n"+f"Field:{field}"+"\n"+f"Contact_Length/um:{length}"+"\n"+f"Width/um:{width}"+"\n"+f"Distance/um:{distances[counter]}"+"\n\n"

        else: # CTLM
            title_sample = f"{type} Measurement at {date}"+"\n"+f"Processing_Number:{processing_number}"+"\n"+f"Sample_Series:{sample_series}"+"\n"+f"Field:{field}"+"\n"+f"Inner_Radius/um:{length}"+"\n"+f"Width/um:{width}"+"\n"+f"Distance/um:{distances[counter]}"+"\n\n"

        title_parameters = f"Current/A:{start} to {stop} with step {step}"+"\n"+f"Compliance/V:{comp}"+"\n"+f"Integration_Time:{integration}"+"\n\n"

        #create the dataframe
        header = ['V/V', 'I/A']
        data = {header[0]:V,header[1]:I}
        df = pd.DataFrame(data)

        with open(path,"w") as f:
            f.write(title_sample)
            f.write(title_parameters)

        df.to_csv(path,sep=" ",mode='a')

        #append to filenames
        filenames.append(path)

        #increase counter
        counter = counter +1 # this is the value before the next measurement starts
        if counter <= 4 : #measure the next structure
            V,I=measure_TLM(device)
            curve=ax.plot(V,I,label =f"distance (um):{distances[counter]}")
            ax.legend()
            fig.show()
            if counter==4:
                information_box('You have completed for the first time the 5th Measurement. Press save+continue to save results')
            change_state(first_time_buttons)
        else: #measurement finished
            evaluation_button.disabled = False
            enable_widgets(all_buttons) # all buttons active
            enable_widgets(input_fields)
            disable_widgets(first_time_buttons) #only start new active

def on_exit_button_clicked(b):
    with output:
        change_state(first_time_buttons)

        #ask first
        exit = ask_to_exit()
        if exit == True:
            enable_widgets(all_buttons)
            enable_widgets(input_fields)
            disable_widgets(first_time_buttons)
            evaluation_button.disabled = False
        else:
            change_state(first_time_buttons)

def on_export_ini_clicked(b):
    with output:
        #disable all widgets
        disable_widgets(input_fields)
        disable_widgets(all_buttons)

        #open file
        type = sample["type"].value
        config = configparser.ConfigParser()
        default_filename = f'{type}.ini'
        file = save_as_ini(default_filename)

        with open(file,'w') as configfile:
            config.add_section('ALL VALUES ARE IN THE UNITS WRITTEN IN THE INTERFACE')
            config.add_section('IT IS RECOMMENDED TO CHANGE THE INI FILE FROM THE INTERFACE AND DO NOT CHANGE ANY VALUES MANUALLY')

            config.add_section("Sample Information")

            for information,widget in sample.items():
                #remove field,sample_series, processing number
                if information =="field" or information == "sample_series" or information =="processing_number":
                    continue
                config.set("Sample Information",information,str(widget.value))
            
            config.add_section("Measurement Parameters")
            for parameter,widget in parameters.items():
                 config.set("Measurement Parameters",parameter,str(widget.value))
                
            config.write(configfile)
            
            enable_widgets(input_fields)
            enable_widgets(all_buttons)
            disable_widgets(first_time_buttons)

def on_import_ini_clicked(b):
    with output:
        #disable all widgets
        disable_widgets(input_fields)
        disable_widgets(all_buttons)

        #load values to the interface
        config = configparser.ConfigParser()
        file = load_ini()

        try:
            config.read(file)

            #load sample information
            for information,widget in sample.items():
                if information =="field" or information == "sample_series" or information =="processing_number":
                    continue
                widget.value = config.get('Sample Information',information)

            #load measurement parameters
            for parameter,widget in parameters.items():
                widget.value = config.get('Measurement Parameters',parameter)

            information_box("all parameters loaded succesfully")

        except Exception as error:
            if type(error).__name__ =='NoSectionError':
                information_box(f"{error}.Explanation: Section(header) [section] does not exist. Create a new ini file or compare it with functional ini files!")
            elif type(error).__name__=='NoOptionError':
                information_box(f'{error}.Explanation: The variable name before the equal sign is not recognized. Create a new ini file or compare it with functional ini files!')
            elif type(error).__name__ == 'TraitError':
                information_box(f'{error}.Explanation: Invalid Parameter Setting. Check if you set an invalid value!')
            elif type(error).__name__ =="DuplicateOptionError":
                information_box(f"{error}. Explaination: The section contains the setted parameter more than once!")
            else:
                information_box(f"A {type(error).__name__} has occurred. Create A new ini file")
        
        enable_widgets(input_fields)
        enable_widgets(all_buttons)
        disable_widgets(first_time_buttons)

def on_evaluation_button_clicked(b):
    with output:
        global filenames
        #disable all widgets
        disable_widgets(input_fields)
        disable_widgets(all_buttons)
        evaluation_button.disabled = True

        #filenames =select_files()

        #retrive distances
        distances = [sample[f"d{i}"].value for i in range(1,6)]
        
        if len(filenames)==0:
            information_box("No files to evaluate")
            
        else:
            measurement = ctlmevaluationlib.CTLMandTLMMeasurement(sample["type"].value, filenames, distances, sample["length"].value,sample["length"].value, True)
            origin = True
            graphs = True
            refine = False
            if origin:
                measurement.plotclosetozero()
            if graphs:
                measurement.uigraph(size=(7,5))
                measurement.rtgraph(size=(7,5))
            if refine:
                newrange = (-0.03, 0.03)
                measurement.refine_evaluated_range(newrange)
            if refine and graphs:
                measurement.uigraph(size=(7,5))
                measurement.rtgraph(size=(7,5))
                print(measurement.R_statistics,"\n")
                print(measurement.results,"\n")
                    
        enable_widgets(input_fields)
        enable_widgets(all_buttons)
            
    
start_button.on_click(on_start_button_clicked)
continue_button.on_click(on_continue_button_clicked)
repeat_button.on_click(on_repeat_button_clicked)
exit_button.on_click(on_exit_button_clicked)
export_ini_button.on_click(on_export_ini_clicked)
import_ini_button.on_click(on_import_ini_clicked)
evaluation_button.on_click(on_evaluation_button_clicked)
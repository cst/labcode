import pyvisa

class HP4155a(object):
    
    def __init__(self,adress:str):
        self.adress = adress
        self.rm = pyvisa.ResourceManager()
        self.inst = self.rm.open_resource(adress)
        self.inst.timeout = None
   #----------------------------------------------------General functions-------------------------------------------------------------------------- 
    def idn(self):
        return self.inst.query("*IDN?")
        
    def __del__(self):
        self.rm.close()
    
    def reset(self):
        self.inst.write("*RST")
#------------------------------------------------------stress functions--------------------------------------------------------------------

    def smu_stress(self,number:int,smu:dict):
        command = f":PAGE:STR:SMU{number}:MODE {smu['mode']};FUNC {smu['func']};NAME {repr(smu['name'])}"
        self.inst.write(command)

        command =f":PAGE:STR:SET:CONS:SMU{number} {smu['value']}"
        self.inst.write(command)
        
        command = f":PAGE:STR:SET:CONS:SMU{number}:COMP {smu['comp']}"
        self.inst.write(command)
    
    #set the stess time in seconds
    def stress_time(self,time:float):
        command = f":PAGE:STR:SET:DUR {time}"
        self.inst.write(command)
        
    #start stress operation
    def start_stress(self):
        self.inst.write(":PAGE:SCON:STR")
    
    def single_measurement(self):
        self.inst.write(":PAGE:SCON:SING")
    
    #go to stress page 
    def stress_page(self):
        self.inst.write(":PAGE:STR")
        
    def error(self):
        error = self.inst.query(":SYST:ERR?")
        error_code = int(error.split(',',1)[0])
        error_message = error.split(',',1)[1]
        return error_code,error_message
        
    def error_occured(self):
        code,message = self.error()
        if code != 0:
            raise Exception(message)

    def clear_error_stack(self):
        code,_ = self.error()
        while code != 0:
            code,_ = self.error() 

    def operation_completed(self):
        text = self.inst.query('*OPC?')
        text = text.replace('\n','')
        finished = bool(text)
        return finished

    def wait(self):
        self.inst.write("*WAI")
    
    def show_variables(self):
        return self.inst.query(":DATA:CAT?") 

     #-----------------------------------------------------------sweep functions---------------------------------------------------------------
    def smu_disable(self,number:int):
        command= f":PAGE:CHAN:SMU{number}:DIS"
        self.inst.write(command)

    def setup_smu(self,number:int,smu:dict):
        command=f":PAGE:CHAN:SMU{number}:MODE {smu['mode']};FUNC {smu['func']};VNAME {repr(smu['vname'])};INAME {repr(smu['iname'])}"
        self.inst.write(command)


    def setup_var1(self,var1:dict):
        command =f":PAGE:MEAS:VAR1:MODE {var1['mode']};START {var1['start']};STOP {var1['stop']};STEP {var1['step']};COMP {var1['comp']};PCOM {var1['pcomp']}" 
        self.inst.write(command)

    def setup_cons_smu(self,number:int,smu:dict):
        command = f"PAGE:MEAS:CONS:SMU{number} {smu['value']}"
        self.inst.write(command)

        command = f":PAGE:MEAS:CONS:SMU{number}:COMP {smu['comp']}"
        self.inst.write(command)

    def setup_var2(self,var2:dict):
        command =f":PAGE:MEAS:VAR2:START {var2['start']};STEP {var2['step']};POINTS {var2['points']};COMP {var2['comp']};PCOM {var2['pcomp']}" 
        self.inst.write(command)

    def setup_vard(self,vard:dict):
        command = f":PAGE:MEAS:VARD:OFFS {vard['offset']};RAT {vard['ratio']};COMP {vard['comp']};PCOM {vard['pcomp']}"
        self.inst.write(command)

    def setup_pulse(self,pulse:dict):
        command = f":PAGE:MEAS:PULS:BASE {pulse['base']};PER {pulse['period']};WIDTH {pulse['width']}"
        self.inst.write(command)

    #for pulse
    def range_mode(self,smu_number,range):
        command = f":PAGE:MEAS:MSET:SMU{smu_number}:RANG:MODE {range}"
        self.inst.write(command)

    #----------------------------------------------------------------------sampling measure functions part2 first goal----------------------------
    def setup_sampling(self,parameters:dict):
        command = f"PAGE:MEAS:SAMP:MODE {parameters['mode']};HTIM {parameters['hold']};IINT {parameters['interval']};POIN {parameters['points']};FILT {parameters['filter']}"
        self.inst.write(command)

    def setup_smu_sampling(self,number:int,smu:dict):
        command =f":PAGE:MEAS:SAMP:CONS:SMU{number} {smu['value']}"
        self.inst.write(command)

        command=f":PAGE:MEAS:SAMP:CONS:SMU{number}:COMP {smu['comp']}"
        self.inst.write(command)
    
    #integration time is SHOR,MED,LONG   
    def integration_time(self,time):
        command=f":PAGE:MEAS:MSET:ITIM {time}"
        self.inst.write(command)
    
    def measurement_mode(self,mode):
        command =f":PAGE:CHAN:MODE {mode}"
        self.inst.write(command)
        
    #this is a method that returns data from a variable and converts them to a list of real numbers
    def return_values(self, variable):
        command = f":DATA? {repr(variable)}" 
        values = self.inst.query_ascii_values(command)
        return values

    #for memristor we need as a sampling completiton the Total sampling time

    #only for linear and thinned out 
    def total_sampling_time(self,period):
        command = f":PAGE:MEAS:SAMP:PER {period}"
        self.inst.write(command)
    
    #set auto sampling time
    def auto_sampling_time(self,status):
        command =f":PAGE:MEAS:SAMP:PER:AUTO {status}"
        self.inst.write(command)

    #------------------------------------------ functions for plots,variables and disabling vmu vsu-----------------------------------------------------------
    
    #disables vmu and vsu
    def disable_not_smu(self):
        command = f":PAGE:CHAN:VSU1:DIS"
        self.inst.write(command)

        command = f":PAGE:CHAN:VSU2:DIS"
        self.inst.write(command)

        command = f":PAGE:CHAN:VMU1:DIS"
        self.inst.write(command)

        command = f":PAGE:CHAN:VMU2:DIS"
        self.inst.write(command)
        
    #this command is for defining a new user function
    def user_function(self,name,unit,expression):
        command = f":PAGE:CHAN:UFUN:DEF '{name}','{unit}','{expression}'"
        self.inst.write(command)

    #this command is for displaying the correct variables
    def display_variable(self,axis,variable):
        command=f":PAGE:DISP:GRAP:{axis}:NAME '{variable}'"
        self.inst.write(command)
    
    #linear logarithmic scale for axis
    def axis_scale(self,axis,scale):
        command = f":PAGE:DISP:GRAP:{axis}:SCAL {scale}"
        self.inst.write(command)
    
    def display_variable_min_max(self,axis,extremum,value):
        command = f":PAGE:DISP:GRAP:{axis}:{extremum} {value}"
        self.inst.write(command)

    def get_axis_variable(self,axis):
        command = f":PAGE:DISP:GRAP:{axis}:NAME?"
        var_name = self.inst.query(command)
        return var_name.rstrip() #remove \n
    
    def autoscaling(self):
        self.inst.write(":PAGE:GLIS:SCAL:AUTO ONCE")

    def append_measurement(self):
        self.inst.write(":PAGE:SCON:APP")
    
    def stop_measurement(self):
        self.inst.write(":PAGE:SCON:STOP")

    # list to save variables in files up to 8 variables
    def variables_to_save(self,variables_list):
        command = ":PAGE:DISP:LIST "
        for index,variable in enumerate(variables_list):
            command = command + f"{repr(variable)}"
            if index != len(variables_list)-1:
                command = command + ","
        self.inst.write(command)

    # What to display during measurement
    def display_mode(self,mode):
        # mode is GRAP or LIST
        command = f":PAGE:DISP:MODE {mode}"
        self.inst.write(command)

    def delete_axis(self,axis):
        command = f":PAGE:DISP:GRAP:{axis}:DEL"
        self.inst.write(command)

    
    #delete all user functions
    def del_user_functions(self):
        command = f":PAGE:CHAN:UFUN:DEL:ALL"
        self.inst.write(command)

####------------------------------------------------ create the empty dictionaries ---------------------------------------------------------------------
    #for sweep and sampling if name of the measurement is not written 
    def smu_dict(self):
        keys = ('vname','iname','mode','func')
        smu = dict.fromkeys(keys)
        return smu

    def smu_dict_stress(self):
        keys =('name','func','mode','value','comp')
        smu = dict.fromkeys(keys)
        return smu

    def var1_dict(self):
        keys =('mode','start','stop','step','comp','pcomp')
        var1 = dict.fromkeys(keys)
        return var1

    def var2_dict(self):
        keys =('start','points','step','comp','pcomp')
        var2 = dict.fromkeys(keys)
        return var2

    def vard_dict(self):
        keys = ('offset','ratio','comp','pcomp')
        vard = dict.fromkeys(keys)
        return vard

    def cons_smu_dict(self):
        keys =('value','comp')
        cons_smu = dict.fromkeys(keys)
        return cons_smu

    def sampling_parameters_dict(self):
        keys =('mode','interval','hold','points','filter')
        parameters = dict.fromkeys(keys)
        return parameters

    def pulse_dict(self):
        keys = ('base','width','period')
        parameters = dict.fromkeys(keys)
        return parameters
        
        
        
    
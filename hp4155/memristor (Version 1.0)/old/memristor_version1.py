import sys
sys.path.append(r"C:\Users\user\labcode\hp4155")

import module
import matplotlib.pyplot as plt
import numpy as np
from IPython.display import display, clear_output
import pandas as pd
from datetime import datetime
import configparser
from ipyfilechooser import FileChooser
import ipywidgets as widgets
import time
import os


def memristor():
    #parameters set by the user
    Vset=1
    CC_vset=10**(-3)
    Vreset=-1
    CC_vreset=10**(-3)
    step = 0.02
    integration_time='MED'
    
    #additional variables
    counter=0
    V12=V34=I12=I34=None
    file_path =os.getcwd()
    file = None

    #default ini file
    ini_file_path=os.getcwd()
    ini_file_name=r"default.ini"
    ini_file=os.path.join(ini_file_path,ini_file_name)

    #filechooser 
    fc = FileChooser(select_desc='load .ini')
    fc.default_path = r"\\FILESERVER\public"
    fc.filter_pattern = '*.ini'
    display(fc)
    #pathchooser 
    pc = FileChooser(select_desc="save path")
    pc.default_path = r"\\FILESERVER\public"
    pc.show_only_dirs = True
    display(pc)
    print()
    
    # the three naming fields

    sample_series= widgets.Text(
        value= '',
        placeholder ='Enter text here:',
        description = 'sample series:',
        style = {'description_width': 'initial'}
    )
    field = widgets.Text(
        value= '',
        placeholder ='Enter text here:',
        description = 'Field:',
        style = {'description_width': 'initial'},
    )

    DUT = widgets.Text(
        value= '',
        placeholder ='Enter text here:',
        description = 'DUT:',
        style = {'description_width': 'initial'},
    )
    
    all_text_boxes = widgets.VBox([sample_series,field,DUT])
    display(all_text_boxes)
    print()

    #connecting to the device
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset()

##################################################################################################################################################################
    #setup the device
    def disable():
        nonlocal device
        #smu1 and smu3 are disabled
        device.smu_disable_sweep(1)
        device.smu_disable_sweep(3)

        #disable vmus and vsus
        device.disable_vsu(1)
        device.disable_vsu(2)
        device.disable_vmu(1)
        device.disable_vmu(2)

    #define each different measurement as a function

    #first sweep from start to stop and stop to start
    def sweep(start,stop,step,comp,integration):
        nonlocal device
        device.reset()
        disable()
        
        device.measurement_mode('SWE')
        #smu2 is constant and common
        device.smu_mode_meas(2,'COMM')
        device.smu_function_sweep(2,'CONS')

        #smu4 is VAR1 and V
        device.smu_mode_meas(4,'V')
        device.smu_function_sweep(4,'VAR1')

        device.integration_time(integration)

        #define double sweep
        device.var1_mode('DOUB')
        
        #start stop step and comp
        device.start_value_sweep(start)
        #time.sleep(5)
        device.step_sweep(step)
        #time.sleep(5)
        device.stop_value_sweep(stop)
        #time.sleep(5)
        device.comp('VAR1',comp)

        #display variables
        device.display_variable('X','V4')
        device.display_variable('Y1','I4')

        #execute measurement
        device.single_measurement()
        while device.operation_completed()==False:
            pass
        
        device.autoscaling()

        #return values
        V=device.return_data('V4')
        I=device.return_data('I4')

        #convert the list to np.array to return the absolute values for the logarithmic scale
        V = np.array(V)
        I = np.array(I)

        #return all values to the function
        return V, I

    #plot sweep results
    def plot_sweep(x,y,title):
        #plot results
        plt.figure().clear()
        fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
        fig.suptitle(title)
        ax1.set_title('Linear I')
        ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
        ax2.set_title('Logarithmic I')
        ax2.set(xlabel='Voltage(V)',ylabel='Current(A)')
        ax2.set_yscale('log')

        ax1.plot(x,y)
        ax2.plot(x,np.absolute(y))
        plt.tight_layout()
        plt.show()

    def initialize_file():
        nonlocal DUT,file_path,file
        if pc.selected!=None:
            file_path = pc.selected
        print(f"save results in:{file_path}")
        file_name = DUT.value+"_Memristor.txt"
        file = os.path.join(file_path,file_name)
        
        #check if file name exists 
        i=1
        while os.path.exists(file):
            file_name = DUT.value+"_Memristor_"+str(i)+".txt"
            file= os.path.join(file_path,file_name)
            i=i+1
        return file

    def create_data_frame(x,y):
        header = ['V','ABSV(V)',"I(A)",'ABSI(A)']
        data = {header[0]:x,header[1]:np.absolute(x),header[2]:y,header[3]:np.absolute(y)}
        df = pd.DataFrame(data)
        print(df)
        return df

    def decode_ini(process):
        nonlocal ini_file
        
        if fc.selected!=None:
            ini_file= fc.selected
        config = configparser.ConfigParser()
        config.read(ini_file)
        
        #read the values
        integration_time = config.get('Memristor','integration_time')
        if integration_time!='SHOR' and integration_time!='MED' and integration_time!='LONG':
            raise Exception('Integration time can be SHOR(short),MED(medium) or LONG(long)')
       

        comp=config.getfloat(process,'comp')
        if abs(comp)>1:
            raise Exception("current compliance can be from -1A to 1A")
        
        if process == 'SET':
            voltage = config.getfloat(process,'voltage')
            if voltage<=0 or voltage>200:
                raise Exception('set voltage can be from 0V to 200V')

        else:
            voltage = config.getfloat(process,'voltage')
            if voltage>=0 or voltage<-200:
                raise Exception('set voltage can be from -200V to 0V')
        
        step=config.getfloat('Memristor','step')
        step = abs(step)
        if step>abs(voltage) or step ==0:
            raise Exception('invalid step')

        return voltage,comp,integration_time,step
                        
##############################################################################################################################################################        
    #sampling follows in a bit...

    #create buttons as it shown in the how_buttons_look 
    set=widgets.Button(description='SET')
    reset=widgets.Button(description='RESET')
    sampling=widgets.Checkbox(description='sampling check')
    full=widgets.Checkbox(description='full sweep')

    #align a button with a checkbox horizontaly
    line1 = widgets.HBox([set,sampling])
    line2 = widgets.HBox([reset,full])

    #pack them into a single vertical box
    all = widgets.VBox([line1,line2])
    output = widgets.Output()

    #dispaly them all
    display(all,output)

    #handling events for the buttons:

    widgets_list=[sampling,full,DUT,sample_series,field]

    def disable_widgets():#only for not buttons
        nonlocal widgets_list
        for widget in widgets_list:
            widget.disabled=True 

    def on_set_button_clicked(b):
        nonlocal Vset,CC_vset,step,integration_time,device,counter,full,V12,I12,V34,I34,file,DUT,sample_series,field,file_path,ini_file,widgets_list,set,reset
        with output:
            clear_output()
            
            #decode ini file
            Vset,CC_vset,integration_time,step=decode_ini('SET')
            
            #disable the buttons set and reset
            set.disabled = True
            reset.disabled = True
            
            if counter == 0:
                disable_widgets()
                file = initialize_file()
            counter=counter+1

            #execute measurement,plot results and create dataframe
            V12,I12=sweep(0,Vset,step,CC_vset,integration_time)
            plot_sweep(V12,I12,'SET')
            df=create_data_frame(V12,I12)
            
            #save results
            with open(file, 'a', encoding='utf-8-sig') as f:    
                title = f"SET Memristor:"+"\n"+f"sample series:{sample_series.value}" +"\n"+f"field:{field.value}"+"\n"+f"DUT:{DUT.value}"+"\n"+f"Set Voltage={Vset}V"+"\n"+f"current compliance={CC_vset}A"+"\n"

                f.write(title)
                f.write(df.to_string(index=False))
                f.write("\n")
                #f.close() outside the with block the file is closed

            #plot all results if checkbox is clicked
            if counter==2 and full.value == True:
                time.sleep(3)
                clear_output()
                V = np.concatenate((V12,V34))
                I = np.concatenate((I12,I34))
                plot_sweep(V,I,'SET+RESET')

            if counter == 1:
                reset.disabled = False

    def on_reset_button_clicked(b):
        nonlocal Vreset,CC_vreset,step,integration_time,device,counter,full,V12,I12,V34,I34,file,DUT,sample_series,field,file_path,widgets_list,set,reset
        with output:
            clear_output()

            #decode ini
            Vreset,CC_vreset,integration_time,step=decode_ini('RESET')

            #disable the buttons set and reset
            set.disabled = True
            reset.disabled = True
            
            if counter == 0:
                disable_widgets()
                file=initialize_file()
                
            counter=counter+1
            
            #execute measurement and plot results and create dataframe
            V34,I34=sweep(0,Vreset,step,CC_vreset,integration_time)
            plot_sweep(V34,I34,'RESET')
            df = create_data_frame(V34,I34)

            #save results
            with open(file, 'a', encoding='utf-8-sig') as f:
                title = f"RESET Memristor:"+"\n"+f"sample series:{sample_series.value}" +"\n"+f"field:{field.value}"+"\n"+f"DUT:{DUT.value}"+"\n"+f"Reset Voltage={Vreset}V"+"\n"+f"current compliance={CC_vreset}A"+"\n"

                f.write(title)
                f.write(df.to_string(index=False))
                f.write("\n")
                #f.close() outside the with block the file is closed 

            #plot all results if checkbox is clicked
            if counter==2 and full.value == True:
                time.sleep(3)
                clear_output()
                V = np.concatenate((V12,V34))
                I = np.concatenate((I12,I34))
                plot_sweep(V,I,'SET+RESET')
            
            if counter == 1:
                set.disabled = False             

    #link buttons with functions
    set.on_click(on_set_button_clicked)
    reset.on_click(on_reset_button_clicked)
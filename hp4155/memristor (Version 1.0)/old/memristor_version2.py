### this is the new memrstor measurement (set and reset as many times as the user wants and full sweeps with a button)
from help import *
import ipywidgets as widgets
from keyboard import add_hotkey,remove_hotkey

def memristor():
    #parameters set by the user
    Vset=1
    CC_vset=10**(-3)
    Vreset=-1
    CC_vreset=10**(-3)
    step = 0.02
    integration_time='MED'
    
    #additional variables
    first = True #first measurement
    file_path =os.getcwd()
    file = None

    #default ini file
    ini_file_path=os.getcwd()
    ini_file_name=r"default.ini"
    ini_file=os.path.join(ini_file_path,ini_file_name)

    #filechooser 
    fc = FileChooser(select_desc='load .ini')
    fc.default_path = r"\\FILESERVER\public"
    fc.filter_pattern = '*.ini'
    display(fc)
    #pathchooser 
    pc = FileChooser(select_desc="save path")
    pc.default_path = r"\\FILESERVER\public"
    pc.show_only_dirs = True
    display(pc)
    print()
    
    # the three naming fields

    sample_series= widgets.Text(
        value= '',
        placeholder ='Enter text here:',
        description = 'sample series:',
        style = {'description_width': 'initial'}
    )
    field = widgets.Text(
        value= '',
        placeholder ='Enter text here:',
        description = 'Field:',
        style = {'description_width': 'initial'},
    )

    DUT = widgets.Text(
        value= '',
        placeholder ='Enter text here:',
        description = 'DUT:',
        style = {'description_width': 'initial'},
    )
    
    all_text_boxes = widgets.VBox([sample_series,field,DUT])
    display(all_text_boxes)
    print()

    # THE BUTTONS 
    #create buttons as it shown in the how_buttons_look 
    set=widgets.Button(description='SET')
    reset=widgets.Button(description='RESET')
    sampling=widgets.Checkbox(description='sampling check')
    full=widgets.Button(description='full sweep')
    number = widgets.BoundedIntText(value=1,min=1,max=sys.maxsize,step=1,description='full sweeps:',disabled=False) #number of measuremts for the full sweep

    #align a button with a checkbox or integer bounded texts horizontaly
    line1 = widgets.HBox([set,sampling])
    line3 = widgets.HBox([full,number])

    #pack them into a single vertical box
    all = widgets.VBox([line1,reset,line3])
    output = widgets.Output()

    #dispaly them all
    display(all,output)

    #connect to the device 
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset()

    #disable all irrelevant units for the measurement
    #smu1 and smu3 are disabled
    device.smu_disable_sweep(1)
    device.smu_disable_sweep(3)

    #disable vmus and vsus
    device.disable_vsu(1)
    device.disable_vsu(2)
    device.disable_vmu(1)
    device.disable_vmu(2)

    """ the above is what happens when the programm starts all the rest have to be written into button trigger functions"""
    
    def on_set_button_clicked(b):
        nonlocal Vset,CC_vset,step,integration_time,device,first,DUT,sample_series,field,file_path,ini_file,file
        with output:
            #disable buttons
            set.disabled = True
            reset.disabled=True
            full.disabled =True
            number.disabled =True

            clear_output()

            #during first button press
            if first == True:
                sample_series.disabled = True
                field.disabled = True
                DUT.disabled = True
                sampling.disabled = True
                
                #get ini file,and path from filechooser and open a new file 
                if fc.selected!=None:
                    ini_file = fc.selected
                print(f"read parameters from ini:{ini_file}")
                if pc.selected!=None:
                    file_path = pc.selected
                print(f"save results in:{file_path}")
                file = initialize_file(sample_series,field,DUT,file_path)
                first = False
            
            try:
                #decode ini file
                Vset,CC_vset,integration_time,step=decode_ini('SET',ini_file)

                #execute measurement,plot results and save them
                V12,I12 = sweep(0,Vset,step,CC_vset,integration_time,device)
                plot_sweep(V12,I12,'SET')
                df = create_data_frame(V12,I12)
                title = f"SET Memristor:"+"\n\n"+f"Set Voltage={Vset}V"+"\n"+f"current compliance={CC_vset}A"+"\n"
                write_to_file(file,title,df)
                
                #enable buttons
                set.disabled = False
                reset.disabled=False
                full.disabled =False
                number.disabled=False

            
            except Exception as e:
                print(e)
                print('please ensure that all parameters are correct')
                set.disabled = False
                reset.disabled=False
                full.disabled =False
                number.disabled=False


    def on_reset_button_clicked(b):
        nonlocal Vreset,CC_vreset,step,integration_time,device,first,DUT,sample_series,field,file_path,ini_file,file
        with output:
            set.disabled = True
            reset.disabled=True
            full.disabled =True
            number.disabled=True

            clear_output()

            #during first button press
            if first == True: 
                #disable checkboxes, text fields etc.
                sample_series.disabled = True
                field.disabled = True
                DUT.disabled = True
                sampling.disabled = True

                #get ini file,and path from filechooser and open a new file 
                if fc.selected!=None:
                    ini_file = fc.selected
                print(f"read parameters from ini:{ini_file}")
                if pc.selected!=None:
                    file_path = pc.selected
                print(f"save results in:{file_path}")
                file = initialize_file(sample_series,field,DUT,file_path)
                first = False #set first to false irrelvant if it is in the if statement or not
            
            try:
                #decode ini file
                Vreset,CC_vreset,integration_time,step=decode_ini('RESET',ini_file)

                #execute measurement,plot results and save them
                V34,I34 = sweep(0,Vreset,step,CC_vreset,integration_time,device)
                plot_sweep(V34,I34,'RESET')
                df = create_data_frame(V34,I34)
                title =f"RESET Memristor:"+"\n\n"+f"Reset Voltage={Vreset}V"+"\n"+f"current compliance={CC_vreset}A"+"\n"
                write_to_file(file,title,df)
            
                #enable buttons 
                set.disabled = False
                reset.disabled=False
                full.disabled =False
                number.disabled=False
            
            except Exception as e:
                print(e)
                print('please ensure that all parameters are correct')
                set.disabled = False
                reset.disabled=False
                full.disabled =False
                number.disabled=False
    
    def on_full_button_clicked(b):
        nonlocal Vset,CC_vset,step,integration_time,device,first,DUT,sample_series,field,file_path,ini_file,file,Vreset,CC_vreset,number
        with output:
            
            #disable buttons
            set.disabled = True
            reset.disabled=True
            full.disabled =True
            number.disabled=True
            clear_output()

            #during first button press
            if first == True: 
                #disable checkboxes, text fields etc.
                sample_series.disabled = True
                field.disabled = True
                DUT.disabled = True
                sampling.disabled = True

                #get ini file,and path from filechooser and open a new file 
                if fc.selected!=None:
                    ini_file = fc.selected
                print(f"read parameters from ini:{ini_file}")
                if pc.selected!=None:
                    file_path = pc.selected
                print(f"save results in:{file_path}")
                file = initialize_file(sample_series,field,DUT,file_path)
                first = False

            try:
                #decode ini
                Vreset,CC_vreset,integration_time,step=decode_ini('RESET',ini_file)
                Vset,CC_vset,integration_time,step=decode_ini('SET',ini_file)


                with open(file,'a') as f:
                    f.write(f"{number.value} full sweeps with parameters:")
                    f.write("\n")
                    f.write(f"Set Voltage = {Vset}V")
                    f.write("\n")
                    f.write(f"Current compliance set = {CC_vset}")
                    f.write("\n")
                    f.write(f"Reset Voltage = {Vreset}V")
                    f.write("\n")
                    f.write(f"Current compliance reset = {CC_vreset}")
                    f.write("\n\n")
                
                
                    plt.figure().clear()
                    fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
                    fig.suptitle('FULL SWEEP')
                    ax1.set_title('Linear I')
                    ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
                    ax2.set_title('Logarithmic I')
                    ax2.set(xlabel='Voltage(V)',ylabel='Current(A)')
                    ax2.set_yscale('log')

                    stop = False

                    def break_loop():
                        nonlocal stop
                        stop = True
                        
                    add_hotkey("esc",break_loop)
                    #execute number of measurements
                    for i in range(number.value):
                        V12,I12 = sweep(0,Vset,step,CC_vset,integration_time,device) #set
                        V34,I34 = sweep(0,Vreset,step,CC_vreset,integration_time,device) #reset

                        #butterfly curve
                        V=np.concatenate((V12,V34))
                        I=np.concatenate((I12,I34))

                        #create data frame and save to file
                        df = create_data_frame(V,I)
                        f.write(f"{i+1} Iteration")
                        f.write("\n")
                        f.write(df.to_string())
                        f.write("\n\n")

                        #plot results
                        ax1.plot(V,I)
                        ax2.plot(V,np.absolute(I))
                        fig.tight_layout()

                        #update plot 
                        clear_output(wait = True)
                        display(fig)
                        print(df)

                        #check for loop termination
                        if stop == True:
                            print("endurance stopped after esc!")
                            f.write("endurance stopped!\n\n")
                            break
                    else:
                        print("endurance completed!")
                        f.write("endurance completed!\n\n")
                        

                    remove_hotkey('esc')
                    stop = False
                    
                    set.disabled = False
                    reset.disabled=False
                    full.disabled =False
                    number.disabled=False
            
            except Exception as e:
                print(e)
                print('please ensure that all parameters are correct')
                set.disabled = False
                reset.disabled=False
                full.disabled =False
                number.disabled=False
    
    #link buttons with functions
    set.on_click(on_set_button_clicked)
    reset.on_click(on_reset_button_clicked)
    full.on_click(on_full_button_clicked)

                    
            
                
                
                
                
                
                
                
                

            
    
            
                    
        

    

    
    
    
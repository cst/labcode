"""These are old help functions and their dependances"""

from ipyfilechooser import FileChooser
import configparser


def decode_ini(process,ini_file):     
    #if fc.selected!=None:
        #ini_file= fc.selected
    config = configparser.ConfigParser()
    config.read(ini_file)
        
    #read the values
    integration_time = config.get('Memristor','integration_time')
    if integration_time!='SHOR' and integration_time!='MED' and integration_time!='LONG':
        raise Exception('Integration time can be SHOR(short),MED(medium) or LONG(long)')
       
    comp=config.getfloat(process,'comp')
    if abs(comp)>1:
        raise Exception("current compliance can be from -1A to 1A")
        
    if process == 'SET':
        voltage = config.getfloat(process,'voltage')
        if voltage<=0 or voltage>200:
            raise Exception('set voltage can be from 0V to 200V')

    else:
        voltage = config.getfloat(process,'voltage')
        if voltage>=0 or voltage<-200:
            raise Exception('set voltage can be from -200V to 0V')
    #here an elif for the full sweep or in a full sweep function
        
    step=config.getfloat('Memristor','step')
    step = abs(step)
    if step>abs(voltage) or step ==0:
        raise Exception('invalid step')

    return voltage,comp,integration_time,step


def initialize_file(sample_series,field,DUT,file_path):
    #if pc.selected!=None:
        #file_path = pc.selected
    #print(f"save results in:{file_path}")
    file_name = DUT.value+"_Memristor.txt"
    file = os.path.join(file_path,file_name)
        
    #check if file name exists 
    i=1
    while os.path.exists(file):
        file_name = DUT.value+"_Memristor_"+str(i)+".txt"
        file= os.path.join(file_path,file_name)
        i=i+1
    
    #write sample series
    with open(file,'a') as f:
        title = f"Memristor Measurement"+"\n\n"+f"Sample series:{sample_series.value}" +"\n"+f"field:{field.value}"+"\n"+f"DUT:{DUT.value}"+"\n\n"
        f.write(title)
    return file

#works
def initialize_tkinter(sample_series,field,DUT):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file_path = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile = f'{sample_series.value}_{field.value}_{DUT.value}.txt')

    #check if the file path is correct(.txt)
    while file_path.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file_path = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile = f'{sample_series.value}_{field.value}_{DUT.value}.txt')

    #the first time open the file in write mode!(replaces the old file)
    #write sample series
    with open(file_path,'w') as f:
        title = f"Memristor Measurement"+"\n\n"+f"Sample series:{sample_series.value}" +"\n"+f"field:{field.value}"+"\n"+f"DUT:{DUT.value}"+"\n\n"
        f.write(title)

    root.destroy()
    #return the file path
    return file_path

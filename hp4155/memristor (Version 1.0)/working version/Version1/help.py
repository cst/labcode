"""
This is a python file containing all the important functions for memristor measurement

Available Functions

measurements in the HP4155a
plot results
create data frame 
ini file decoder
enabing and disabling widgets for jupyter(lists)
"""

import sys
sys.path.insert(0, '..') #append parent directory

import module
import matplotlib.pyplot as plt

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox

import numpy as np
from IPython.display import display, clear_output
import pandas as pd
from datetime import datetime
import ipywidgets as widgets
import time
import os


#double sweep from start to stop and then from start to stop
def sweep(start,stop,step,comp,integration,device):
    device.measurement_mode('SWE')

    #changed smu2 is source and 4 is ground
    #smu2 is constant and common
    device.smu_mode_meas(4,'COMM')
    device.smu_function_sweep(4,'CONS')

    #smu4 is VAR1 and V
    device.smu_mode_meas(2,'V')
    device.smu_function_sweep(2,'VAR1')

    device.integration_time(integration)

    #define double sweep
    device.var1_mode('DOUB')
        
    #start stop step and comp
    device.start_value_sweep(start)
    #time.sleep(5)
    device.stop_value_sweep(stop)
    #time.sleep(5)
    device.step_sweep(step)
    #time.sleep(5)
    device.comp('VAR1',comp)

    #display variables
    device.display_variable('X','V2')
    device.display_variable('Y1','I2')

    #execute measurement
    device.single_measurement()
    while device.operation_completed()==False:
        pass
        
    device.autoscaling()

    #return values
    V=device.return_data('V2')
    I=device.return_data('I2')

    #convert the list to np.array to return the absolute values for the logarithmic scale
    V = np.array(V)
    I = np.array(I)

    #return all values to the function
    return V, I

#sampling check
def sampling_check(voltage,device):
    
    device.measurement_mode('SAMP')
    
    device.smu_mode_meas(2,'V')
    device.smu_mode_meas(4,'COMM')

    #set voltage and compliance
    device.constant_smu_sampling(2,voltage)
    device.constant_smu_comp(2,'MAX')

    device.sampling_mode('LIN')
    device.number_of_points(5)
    device.integration_time('MED')
    device.initial_interval(2e-3)
    device.filter_status('OFF')

    device.display_variable('X','@TIME')
    device.display_variable('Y1','R')
    device.single_measurement()
    while device.operation_completed() == False:
        pass
    
    device.autoscaling()
    try: 
        TIME = device.return_data('@TIME')
        R = device.return_data('R')
        TIME = np.array(TIME)
        R = np.array(R)
        R_mean = np.average(R)
        return R_mean
    except:
        return 0

#new (retention)
def retention(voltage,period,duration,device):
    device.measurement_mode('SAMP')
    
    device.smu_mode_meas(2,'V')
    device.smu_mode_meas(4,'COMM')

    #set voltage and compliance
    device.constant_smu_sampling(2,voltage)
    device.constant_smu_comp(2,'MAX')

    device.sampling_mode('LIN')
    device.initial_interval(period)

    device.total_sampling_time(duration)

    if int(duration/period)<=10001:
        device.number_of_points(int(duration/period))
    else:
        device.number_of_points('MAX')
    device.integration_time('MED')
    device.filter_status('OFF')

    device.display_variable('X','@TIME')
    device.display_variable('Y1','R')
    device.single_measurement()
    while device.operation_completed() == False:
        pass

    device.autoscaling()
    try: 
        TIME = device.return_data('@TIME')
        R = device.return_data('R')
        TIME = np.array(TIME)
        R = np.array(R)
        return TIME,R
    except:
        return 0,0
    

#plot sweep results
def plot_sweep(x,y,title):
    #plot results
    plt.figure().clear()
    fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
    fig.suptitle(title)
    ax1.set_title('Linear I')
    ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
    ax2.set_title('Logarithmic I')
    ax2.set(xlabel='Voltage(V)',ylabel='Current(A)')
    ax2.set_yscale('log')

    ax1.plot(x,y)
    ax2.plot(x,np.absolute(y))
    plt.tight_layout()
    plt.show()

def create_data_frame(x,y):
    header = ['V(V)','ABSV(V)',"I(A)",'ABSI(A)',"R(Ohm)"]
    data = {header[0]:x,header[1]:np.absolute(x),header[2]:y,header[3]:np.absolute(y),header[4]:np.divide(x,y)}
    df = pd.DataFrame(data)
    #print(df)
    return df


#write results to file
def write_to_file(file,title,df):
    with open(file,'a') as f:
        f.write(title)
        f.write("\n")
        f.write(df.to_string())
        f.write("\n\n")

#### new functions ##############
def change_state(widgets_list):
    for widget in widgets_list:
        widget.disabled = not widget.disabled


#works
def initialize_tkinter(sample_series,field,DUT):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file_path = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile = f'{DUT.value}_Memristor.txt')

    #check if the file path is correct(.txt)
    while file_path.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file_path = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile = f'{DUT.value}_Memristor.txt')

    #the first time open the file in write mode!(replaces the old file)
    #write sample series
    with open(file_path,'w') as f:
        title = f"Memristor Measurement"+"\n\n"+f"Sample series:{sample_series.value}" +"\n"+f"field:{field.value}"+"\n"+f"DUT:{DUT.value}"+"\n\n"
        f.write(title)

    root.destroy()
    #return the file path
    return file_path

#a check values function
def check_values(step,set_voltage,reset_voltage):
    valid = True

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    if step > abs(set_voltage) or step > abs(reset_voltage):#invalid parameter setting 
        valid = False
        tkinter.messagebox.showerror(message="Invalid parameter setting(step>voltage)")

    #now if the set-reset voltages have the same polarity show a warning
    elif set_voltage*reset_voltage>0:
        valid = tk.messagebox.askokcancel(message="Set-Reset voltages have the same polarity. Continue?")

    else:
        pass
        
    root.destroy()
    return valid
        

def information_box(information):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #display meaagebox
    tkinter.messagebox.showinfo(message=information)
    root.destroy()
    
    
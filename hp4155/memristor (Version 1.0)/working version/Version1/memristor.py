### this is the new memrstor measurement (set and reset as many times as the user wants and full sweeps with a button)
from help import *
import ipywidgets as widgets
from keyboard import add_hotkey,remove_hotkey


#additional variables
first = True #first measurement
file_path = None
first_sampling = True #indicates first sampling for set and reset buttons because we cannot add two at each button

# the three naming fields

sample_series= widgets.Text(
    value= '',
    placeholder ='Enter text here:',
    description = 'sample series:',
    style = {'description_width': 'initial'}
    )

field = widgets.Text(
    value= '',
    placeholder ='Enter text here:',
    description = 'Field:',
    style = {'description_width': 'initial'},
    )

DUT = widgets.Text(
    value= '',
    placeholder ='Enter text here:',
    description = 'DUT:',
    style = {'description_width': 'initial'},
    )
    
all_text_boxes = widgets.VBox([sample_series,field,DUT])
display(all_text_boxes)
print()


#first series of parameters
step = widgets.BoundedFloatText(
    value=0.01,
    min=0,
    max=200,
    step=0.01,
    description='Step(V):',
)

integration_time=widgets.Dropdown(
    options=['SHORt', 'MEDium', 'LONG'],
    value='MEDium',
    description='Integration:',
    #style = {'description_width': 'initial'},
)

sampling=widgets.Checkbox(description='sampling check')

#align the widgets horizontaly
line0=widgets.HBox([step,integration_time,sampling])
display(line0)
print()


# THE BUTTONS 
#create buttons as it shown in the how_buttons_look 
set=widgets.Button(description='SET')
reset=widgets.Button(description='RESET')
full=widgets.Button(description='full sweep')
number = widgets.BoundedIntText(value=1,min=1,max=sys.maxsize,step=1,description='full sweeps:',disabled=False) #number of measuremts for the full sweep

#parameter boxes
Vset=widgets.BoundedFloatText(
    value=1,
    min=-200,
    max=200,
    step=0.1,
    description='Voltage(V):',
)

#parameter buttons
CC_vset=widgets.BoundedFloatText(
    value=1e-3,
    min=-1,
    max=1,
    step=0.1,
    description= 'Comp(A):',
)

#parameter buttons
Vreset=widgets.BoundedFloatText(
    value=-1,
    min=-200,
    max=200,
    step=0.1,
    description='Voltage(V):',
)

#parameter buttons
CC_vreset=widgets.BoundedFloatText(
    value=1e-3,
    min=-1,
    max=1,
    step=0.1,
    description='Comp(A):',
)


#align a button with a checkbox or integer bounded texts horizontaly
line1 = widgets.HBox([set,Vset,CC_vset])
line2 = widgets.HBox([reset,Vreset,CC_vreset])
line3 = widgets.HBox([full,number])

#pack them into a single vertical box
all = widgets.VBox([line1,line2,line3])
output = widgets.Output()

#dispaly them all
display(all,output)

#help lists for changing state of the buttons
information = [sample_series,field,DUT]
buttons = [set,reset,full]
parameters = [Vset,CC_vset,Vreset,CC_vreset,step,integration_time,number,sampling]


#connect to the device 
device = module.HP4155a('GPIB0::17::INSTR')
device.reset()

#disable all irrelevant units for the measurement
#smu1 and smu3 are disabled
device.smu_disable_sweep(1)
device.smu_disable_sweep(3)

#disable vmus and vsus
device.disable_vsu(1)
device.disable_vsu(2)
device.disable_vmu(1)
device.disable_vmu(2)

# R user function
device.user_function('R','OHM','V2/I2')

""" the above is what happens when the programm starts all the rest have to be written into button trigger functions"""
    
def on_set_button_clicked(b):
    global first,file_path,first_sampling
    with output:
        #disable buttons
        change_state(buttons)
        change_state(parameters)

        clear_output()

        #during first button press
        if first == True:
            change_state(information)#disable all widgets that are relevant about the information of the sample
            file_path = initialize_tkinter(sample_series,field,DUT)
            first = False
            

        #check values
        valid = check_values(step.value,Vset.value,Vreset.value)

        if valid == True:
            if sampling.value == True: #do sampling set before set process(100mV)
                R_mean = sampling_check(0.1,device)
                print(f"Average Resistance(Sampling Check):{R_mean} Ohm")
                first_sampling = False
               
            #execute measurement,plot results and save them
            V12,I12 = sweep(0,Vset.value,step.value,CC_vset.value,integration_time.value,device)
            plot_sweep(V12,I12,'SET')
            df = create_data_frame(V12,I12)
            print(df)
            title = f"SET Memristor:"+"\n\n"+f"Set Voltage={Vset.value}V"+"\n"+f"current compliance={CC_vset.value}A"+"\n"
            write_to_file(file_path,title,df)

            if sampling.value == True: #do sampling set after set process(10mV)
                R_mean = sampling_check(0.01,device)
                print(f"Average Resistance(Sampling Check):{R_mean} Ohm")
                first_sampling = False
        
        #show messagebox
        information_box("Measurement finished!")

        change_state(buttons)
        change_state(parameters)
 
def on_reset_button_clicked(b):
    global first,file_path,first_sampling
    with output:
        change_state(buttons)
        change_state(parameters)

        clear_output()

        #during first button press
        if first == True: 
            #disable checkboxes, text fields etc.
            change_state(information)
            file_path= initialize_tkinter(sample_series,field,DUT)
            first = False #set first to false irrelvant if it is in the if statement or not

        #check values
        valid = check_values(step.value,Vset.value,Vreset.value)

        if valid == True:
            
            if sampling.value == True: #do sampling set before reset process(10mV)
                R_mean = sampling_check(0.01,device)
                print(f"Average Resistance(Sampling Check):{R_mean} Ohm")
                first_sampling = False
            
            #execute measurement,plot results and save them
            V34,I34 = sweep(0,Vreset.value,step.value,CC_vreset.value,integration_time.value,device)
            plot_sweep(V34,I34,'RESET')
            df = create_data_frame(V34,I34)
            print(df)
            title =f"RESET Memristor:"+"\n\n"+f"Reset Voltage={Vreset.value}V"+"\n"+f"current compliance={CC_vreset.value}A"+"\n"
            write_to_file(file_path,title,df)

            if sampling.value == True: #do sampling set after reset process(100mV)
                R_mean = sampling_check(0.1,device)
                print(f"Average Resistance(Sampling Check):{R_mean} Ohm")
                first_sampling = False
        
        #show messagebox
        information_box("Measurement finished!")

        change_state(buttons)
        change_state(parameters)

def on_full_button_clicked(b):
    global first,file_path
    with output:
        change_state(buttons)
        change_state(parameters)

        clear_output()

        #during first button press
        if first == True: 
            #disable checkboxes, text fields etc.
            change_state(information)
            file_path= initialize_tkinter(sample_series,field,DUT)
            first = False #set first to false irrelvant if it is in the if statement or not

        #check values
        valid = check_values(step.value,Vset.value,Vreset.value)
        if valid == True:
            with open(file_path,'a') as f:
                f.write(f"{number.value} full sweeps with parameters:")
                f.write("\n")
                f.write(f"Set Voltage = {Vset.value}V")
                f.write("\n")
                f.write(f"Current compliance set = {CC_vset.value}A")
                f.write("\n")
                f.write(f"Reset Voltage = {Vreset.value}V")
                f.write("\n")
                f.write(f"Current compliance reset = {CC_vreset.value}A")
                f.write("\n\n")
                
                
                plt.figure().clear()
                fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
                fig.suptitle('FULL SWEEP')
                ax1.set_title('Linear I')
                ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
                ax2.set_title('Logarithmic I')
                ax2.set(xlabel='Voltage(V)',ylabel='Current(A)')
                ax2.set_yscale('log')

                stop = False

                def break_loop():
                    nonlocal stop
                    stop = True
                #help list with the resistances
                resistances = []    
                        
                add_hotkey("esc",break_loop)
                #execute number of measurements
                for i in range(number.value):#here it is easier to implement the sampling checks
                    if sampling.value == True: #before set(100mv)
                        R_mean = sampling_check(0.1,device)
                        resistances.append(R_mean)
                        
                    V12,I12 = sweep(0,Vset.value,step.value,CC_vset.value,integration_time.value,device) #set
                    
                    #after set/before set
                    if sampling.value == True: #before set(10mv)
                        R_mean = sampling_check(0.01,device)
                        resistances.append(R_mean)
                    
                    V34,I34 = sweep(0,Vreset.value,step.value,CC_vreset.value,integration_time.value,device) #reset

                    #no reason to do check at the end because the next loop will do that

                    #butterfly curve
                    V=np.concatenate((V12,V34))
                    I=np.concatenate((I12,I34))

                    #create data frame and save to file
                    df = create_data_frame(V,I)
                    f.write(f"{i+1} Iteration")
                    f.write("\n")
                    f.write(df.to_string())
                    f.write("\n\n")

                    #plot results
                    ax1.plot(V,I)
                    ax2.plot(V,np.absolute(I))
                    fig.tight_layout()

                    #update plot 
                    clear_output(wait = True)
                    #display(fig)
                    plt.show()
                    print(df)

                    #check for loop termination
                    if stop == True:
                        information_box("Endurance stopped after esc!")
                        f.write("endurance stopped!\n\n")
                        break
                else:
                    information_box("Endurance completed!")
                    f.write("endurance completed!\n\n")
                        
                remove_hotkey('esc')
                stop = False

                #plot resistances if sampling value == True or len(resistances) !=0
                if len(resistances)!=0:
                    indexes = np.arange(1,len(resistances)+1)
                    resistances = np.array(resistances)
                
                    plt.figure().clear()
                    fig, ax = plt.subplots()
                    
                    fig.suptitle('Resistance')
                    ax.set(xlabel='Index',ylabel='Resistance(Ohm)')
                    ax.set_yscale('log')
                    plt.scatter(indexes,resistances)
                    plt.show()
                    print(len(resistances))
                    print(indexes)
        change_state(buttons)
        change_state(parameters)

                                  
#link buttons with functions
set.on_click(on_set_button_clicked)
reset.on_click(on_reset_button_clicked)
full.on_click(on_full_button_clicked)


### this is the new memrstor measurement (set and reset as many times as the user wants and full sweeps with a button)
from help import *
import ipywidgets as widgets
from keyboard import add_hotkey,remove_hotkey





#additional variables
first = True #first measurement
"""
This is not anymore the first time you start a measurement but the first time you write a header
"""

file = None #complete filename with path
#first_sampling = True #indicates first sampling for set and reset buttons because we cannot add two at each button
#we dont need this variable anymore


#create temporary file to store the results localy
temp_file= os.path.join(os.getcwd(),'tempfile.txt')

# the three naming fields

sample_series= widgets.Text(
    value= '',
    placeholder ='Enter text here:',
    description = 'sample series:',
    style = {'description_width': 'initial'}
    )

field = widgets.Text(
    value= '',
    placeholder ='Enter text here:',
    description = 'Field:',
    style = {'description_width': 'initial'},
    )

DUT = widgets.Text(
    value= '',
    placeholder ='Enter text here:',
    description = 'DUT:',
    style = {'description_width': 'initial'},
    )

#start new measurement button(new sample)
new=widgets.Button(description='next sample')

#choose a new folder button
new_folder = widgets.Button(description='change folder')


horizontal = widgets.HBox([sample_series,new])
horizontal3= widgets.HBox([DUT,new_folder])
all_text_boxes = widgets.VBox([horizontal,field,horizontal3])



#first series of parameters
step = widgets.BoundedFloatText(
    value=0.01,
    min=0,
    max=100,
    step=0.01,
    description='Step(V):',
)

integration_time=widgets.Dropdown(
    options=['SHORt', 'MEDium', 'LONG'],
    value='MEDium',
    description='Integration:',
    #style = {'description_width': 'initial'},
)

sampling=widgets.Checkbox(description='sampling check')

#align the widgets horizontaly
line0=widgets.HBox([step,integration_time,sampling])



# THE BUTTONS 
#create buttons as it shown in the how_buttons_look 
set=widgets.Button(description='SET')
reset=widgets.Button(description='RESET')
full=widgets.Button(description='full sweep')
number = widgets.BoundedIntText(value=1,min=1,max=sys.maxsize,step=1,description='full sweeps:',disabled=False) #number of measuremts for the full sweep
retention_button=widgets.Button(description='retention')


#parameter boxes
Vset=widgets.BoundedFloatText(
    value=1,
    min=-100,
    max=100,
    step=0.1,
    description='Voltage(V):',
)

#parameter buttons
CC_vset=widgets.BoundedFloatText(
    value=1e-3,
    min=-0.1,
    max=0.1,
    step=0.01,
    description= 'Comp(A):',
)

#parameter buttons
Vreset=widgets.BoundedFloatText(
    value=-1,
    min=-100,
    max=100,
    step=0.1,
    description='Voltage(V):',
)

#parameter buttons
CC_vreset=widgets.BoundedFloatText(
    value=1e-3,
    min=-0.1,
    max=0.1,
    step=0.01,
    description='Comp(A):',
)

Vretention=widgets.BoundedFloatText(
    value=1,
    min=-100,
    max=100,
    step=1,
    description='Voltage(V):',
)

period=widgets.BoundedFloatText(
    value=1,
    min=2e-3,
    max=65.535,
    step=1,
    description='Period(s):',
)

duration=widgets.BoundedFloatText(
    value=60,
    min=60e-6,
    max=1e11,
    step=1,
    description='Duration(s):',
)

#align a button with a checkbox or integer bounded texts horizontaly
line1 = widgets.HBox([set,Vset,CC_vset])
line2 = widgets.HBox([reset,Vreset,CC_vreset])
line3 = widgets.HBox([full,number])
line4 = widgets.HBox([retention_button,Vretention,period,duration])

#pack them into a single vertical box
all = widgets.VBox([line1,line2,line3,line4])
output = widgets.Output()


#help lists for changing state of the buttons
information = [sample_series,field,DUT]
buttons = [set,reset,full,new,new_folder,retention_button]
parameters = [Vset,CC_vset,Vreset,CC_vreset,step,integration_time,number,sampling,Vretention,period,duration]


#connect to the device 
device = module.HP4155a('GPIB0::17::INSTR')
device.reset()

#disable all irrelevant units for the measurement
#smu1 and smu3 are disabled
device.smu_disable_sweep(1)
device.smu_disable_sweep(3)

#disable vmus and vsus
device.disable_vsu(1)
device.disable_vsu(2)
device.disable_vmu(1)
device.disable_vmu(2)

# R user function
device.user_function('R','OHM','V2/I2')

#choose folder directory
folder=choose_folder()


#display all at the end
display(all_text_boxes)
print()
display(line0)
print()

#display the buttons
display(all,output)
""" the above is what happens when the programm starts all the rest have to be written into button trigger functions"""
    
def on_set_button_clicked(b):
    global first,folder,file,temp_file
    with output:
        #disable buttons
        change_state(buttons)
        change_state(parameters)

        #lock the  device
        device.inst.lock_excl()

        clear_output()

        #check values
        valid = check_values(step.value,Vset.value,Vreset.value)

        #during first button press
        if first == True and valid == True:
            change_state(information)#disable all widgets that are relevant about the information of the sample
            filename=f"{sample_series.value}_{field.value}_{DUT.value}.txt"
            file = os.path.join(folder,filename)
            #write header to temp_file
            write_header(temp_file,sample_series,field,DUT)
            first = False

        if valid == True:
            if sampling.value == True: #do sampling set before set process(100mV)
                R_mean_before = sampling_check(0.1,device)
                R_mean_before = round(R_mean_before,1)#round 1 decimal point
                print(f"Average Resistance(Sampling Check):{R_mean_before:e} Ohm")
                first_sampling = False
               
            #execute measurement,plot results and save them
            V12,I12 = sweep(0,Vset.value,step.value,CC_vset.value,integration_time.value,device)
            plot_sweep(V12,I12,'SET')
            df = create_data_frame(V12,I12)
            print(df)
            

            if sampling.value == True: #do sampling set after set process(10mV)
                R_mean_after = sampling_check(0.01,device)
                R_mean_after = round(R_mean_after,1)
                print(f"Average Resistance(Sampling Check):{R_mean_after:e} Ohm")
                first_sampling = False

            title = f"SET Memristor:"+"\n\n"+f"Set Voltage={Vset.value}V"+"\n"+f"current compliance={CC_vset.value}A"+"\n"
            if sampling.value == True:
                title = title + f"R(Ohm)  Before/After"+"\n"+f"{R_mean_before}  {R_mean_after}"+"\n"
            write_to_file(temp_file,title,df)

            #upload results
            temp_file,file,folder=upload_results(temp_file,file,folder)
        
        #show messagebox
        information_box("Measurement finished!")

        #unlock device
        device.inst.unlock()
        
        change_state(buttons)
        change_state(parameters)
 
def on_reset_button_clicked(b):
    global first,folder,file,temp_file
    with output:
        change_state(buttons)
        change_state(parameters)

        #lock device
        device.inst.lock_excl()

        clear_output()

        #check values
        valid = check_values(step.value,Vset.value,Vreset.value)

        #during first button press
        if first == True and valid == True: 
            #disable checkboxes, text fields etc.
            change_state(information)
            filename=f"{sample_series.value}_{field.value}_{DUT.value}.txt"
            file = os.path.join(folder,filename)
            #write header to temp_file
            write_header(temp_file,sample_series,field,DUT)
            first = False #set first to false irrelvant if it is in the if statement or not

        if valid == True:
            if sampling.value == True: #do sampling set before reset process(10mV)
                R_mean_before = sampling_check(0.01,device)
                R_mean_before = round(R_mean_before,1)#round 1 decimal point
                print(f"Average Resistance(Sampling Check):{R_mean_before:e} Ohm")
                first_sampling = False
            
            #execute measurement,plot results and save them
            V34,I34 = sweep(0,Vreset.value,step.value,CC_vreset.value,integration_time.value,device)
            plot_sweep(V34,I34,'RESET')
            df = create_data_frame(V34,I34)
            print(df)
            
            if sampling.value == True: #do sampling set after reset process(100mV)
                R_mean_after = sampling_check(0.1,device)
                R_mean_after = round(R_mean_after,1)
                print(f"Average Resistance(Sampling Check):{R_mean_after:e} Ohm")
                first_sampling = False
            
            title =f"RESET Memristor:"+"\n\n"+f"Reset Voltage={Vreset.value}V"+"\n"+f"current compliance={CC_vreset.value}A"+"\n"
            if sampling.value == True:
                title = title + f"R(Ohm)  Before/After"+"\n"+f"{R_mean_before}  {R_mean_after}"+"\n"
            write_to_file(temp_file,title,df)
            
            #upload results
            temp_file,file,folder=upload_results(temp_file,file,folder)

        #show messagebox
        information_box("Measurement finished!")

        #unlock device
        device.inst.unlock()

        change_state(buttons)
        change_state(parameters)

def on_full_button_clicked(b):
    global first,folder,file,temp_file
    with output:
        change_state(buttons)
        change_state(parameters)

        # lock device
        device.inst.lock_excl()
        
        clear_output()

        #check values
        valid = check_values(step.value,Vset.value,Vreset.value)

        #during first button press
        if first == True and valid == True: 
            #disable checkboxes, text fields etc.
            change_state(information)
            filename=f"{sample_series.value}_{field.value}_{DUT.value}.txt"
            file = os.path.join(folder,filename)
            #write header to temp_file
            write_header(temp_file,sample_series,field,DUT)
            first = False #set first to false irrelvant if it is in the if statement or not

        
        if valid == True:
            with open(temp_file,'a') as f:
                f.write(f"{number.value} full sweeps with parameters:")
                f.write("\n")
                f.write(f"Set Voltage = {Vset.value}V")
                f.write("\n")
                f.write(f"Current compliance set = {CC_vset.value}A")
                f.write("\n")
                f.write(f"Reset Voltage = {Vreset.value}V")
                f.write("\n")
                f.write(f"Current compliance reset = {CC_vreset.value}A")
                f.write("\n\n")
                
                
                plt.figure().clear()
                fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
                fig.suptitle('FULL SWEEP')
                ax1.set_title('Linear I')
                ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
                ax2.set_title('Logarithmic I')
                ax2.set(xlabel='Voltage(V)',ylabel='Current(A)')
                ax2.set_yscale('log')

                stop = False

                def break_loop():
                    nonlocal stop
                    stop = True
                #help list with the resistances
                resistances = []    
                        
                add_hotkey("esc",break_loop)
                #execute number of measurements
                for i in range(number.value):#here it is easier to implement the sampling checks
                    if sampling.value == True: #before set(100mv)
                        R_mean_init = sampling_check(0.1,device)
                        R_mean_init = round(R_mean_init,1)
                        resistances.append(R_mean_init)
                        
                    V12,I12 = sweep(0,Vset.value,step.value,CC_vset.value,integration_time.value,device) #set
                    
                    #after set/before set
                    if sampling.value == True: #before set(10mv)
                        R_mean_set = sampling_check(0.01,device)
                        R_mean_set = round(R_mean_set,1)
                        resistances.append(R_mean_set)
                    
                    V34,I34 = sweep(0,Vreset.value,step.value,CC_vreset.value,integration_time.value,device) #reset

                    #no reason to do check at the end because the next loop will do that(not anymore) more sampling checks

                    #after reset
                    if sampling.value == True:#-0.1V
                        R_mean_reset = sampling_check(-0.1,device)
                        R_mean_reset = round(R_mean_reset,1)
                        resistances.append(R_mean_reset)


                    #butterfly curve
                    V=np.concatenate((V12,V34))
                    I=np.concatenate((I12,I34))

                    #create data frame and save to file
                    df = create_data_frame(V,I)
                    f.write(f"{i+1} Iteration")
                    f.write("\n")
                    if sampling.value == True:
                        f.write(f"R(Ohm)  INIT/SET/RESET"+"\n"+f"{R_mean_init}  {R_mean_set} {R_mean_reset}"+"\n")
                    f.write(df.to_string())
                    f.write("\n\n")


                    #plot results
                    ax1.plot(V,I)
                    ax2.plot(V,np.absolute(I))
                    fig.tight_layout()

                    #update plot 
                    clear_output()
                    display(fig)
                    #plt.show()
                    print(df)

                    #check for loop termination
                    if stop == True:
                        information_box("Endurance stopped after esc!")
                        f.write("endurance stopped!\n\n")
                        break
                else:
                    information_box("Endurance completed!")
                    f.write("endurance completed!\n\n")
                        
                remove_hotkey('esc')
                stop = False

                #plot resistances if sampling value == True or len(resistances) !=0
                if len(resistances)!=0:
                    indexes = np.arange(1,len(resistances)+1)
                    resistances = np.array(resistances)
                
                    plt.figure().clear()
                    fig, ax = plt.subplots()
                    
                    fig.suptitle('Resistance')
                    ax.set(xlabel='Index',ylabel='Resistance(Ohm)')
                    ax.set_yscale('log')
                    plt.scatter(indexes,resistances)
                    plt.show()
                    #print(len(resistances))
                    #print(indexes)
        
            #upload results
            temp_file,file,folder=upload_results(temp_file,file,folder)
        
        #unlock the device
        device.inst.unlock()
        
        change_state(buttons)
        change_state(parameters)

#move to next sample
def on_new_sample_button_clicked(b):
    global first
    with output:
        #the if is to ensure that is not pressed many times
        #just in case the user presses anything
        change_state(buttons)
        change_state(parameters)
        
        first = True
        #change_state(information) not anymore creating changing state but enabling the widgets
        enable_widgets(information)
        #sample_series.value=''
        #field.value=''
        DUT.value=''

        #enable again
        change_state(buttons)
        change_state(parameters)

#new_folder clicked
def on_new_folder_button_clicked(b):
    global folder,file,first
    with output:
        change_state(buttons) #just to be sure
        change_state(parameters)
        
        folder = choose_folder()#choose new folder
        #file = create_file(sample_series,field,DUT,folder) #and create the new file (creates multiple headers!!!)
        first = True #that will write header if the directory is the same as the previous one!
        change_state(buttons)
        change_state(parameters)

def on_retention_button_clicked(b):
    global first,folder,file,temp_file
    with output:
        change_state(buttons)
        change_state(parameters)

        device.inst.lock_excl()
        
        clear_output()

        #during first button press
        if first == True: 
            #disable checkboxes, text fields etc.
            change_state(information)
            filename=f"{sample_series.value}_{field.value}_{DUT.value}.txt"
            file = os.path.join(folder,filename)
            #write header to temp_file
            write_header(temp_file,sample_series,field,DUT)
            first = False #set first to false irrelvant if it is in the if statement or not

        #execute measurement
        t,R=retention(Vretention.value,period.value,duration.value,device)
        plot_retention(t,R)
        df=create_retention_data_frame(t,R)
        title =f"Retention Memristor:"+"\n\n"+f"Voltage={Vretention.value}V"+"\n"+f"period={period.value}s"+"\n"+f"duration={duration.value}s"+"\n"

        write_to_file(temp_file,title,df)
        #upload results
        temp_file,file,folder=upload_results(temp_file,file,folder)
        #show messagebox
        information_box("Measurement finished!")

        device.inst.unlock()
    
        change_state(buttons)
        change_state(parameters)
            
#link buttons with functions
set.on_click(on_set_button_clicked)
reset.on_click(on_reset_button_clicked)
full.on_click(on_full_button_clicked)
new.on_click(on_new_sample_button_clicked)
new_folder.on_click(on_new_folder_button_clicked)
retention_button.on_click(on_retention_button_clicked)

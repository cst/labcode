"""
This is a python file containing all the important functions for memristor measurement

Available Functions

measurements in the HP4155a
plot results
create data frame 
ini file decoder
enabing and disabling widgets for jupyter(lists)
"""

import sys
sys.path.insert(0, '..') #append parent directory

import hp4155a
import matplotlib.pyplot as plt

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox

import numpy as np
from IPython.display import display, clear_output
import pandas as pd
from datetime import datetime
import ipywidgets as widgets
import time
import os


#contact check between two SMUs (i,j)
def contact_check(i,j,device):
    device.del_user_functions()
    smu = [1,2,3,4]
    device.measurement_mode('SAMP')
    parameters ={
        'mode' : 'LIN',
        'hold': 0,
        'interval':2e-3,
        'points': 1,
        'filter': 'OFF',
        'value':0.01, #voltage value
        'comp':0.1 #compliance value
    }
    
    device.setup_sampling(parameters)
    
    device.auto_sampling_time('ON')
    device.integration_time('MED')

    smu_v = device.smu_dict()
    smu_v.update(
        vname = f'V{i}',
        iname = f'I{i}',
        mode = 'V',
        func = 'CONS'
    )
    device.setup_smu(i,smu_v)

    smu_ground = device.smu_dict()
    smu_ground.update(
        vname =f'V{j}',
        iname = f'I{j}',
        mode = 'COMM',
        func='CONS'
    )
    device.setup_smu(j,smu_ground)
                                                        
    #one smu is measuring
    #one smu is ground

    #set voltage and compliance
    device.setup_smu_sampling(i,parameters)

    #smus to remove
    smu_disable = smu.copy()
    smu_disable.remove(i)
    smu_disable.remove(j)

    for number in smu_disable:
         device.smu_disable(number)

    device.user_function(f'R{i}{j}','OHM',f'V{i}/I{i}')
    device.display_variable('X','@TIME')
    device.axis_scale('X','LIN')
    device.display_variable('Y1',f'R{i}{j}')
    device.axis_scale('Y1',"LIN")
    
    device.single_measurement()
    while device.operation_completed() == False:
        time.sleep(2)
   
    R = device.return_values(f'R{i}{j}')[0] #only the first value
    print(f"R{i}{j}:{'{:.2e}'.format(R)} Ohm")
    device.autoscaling()
    return R
    
    
#these are all the  sampling checks 
def regular_contact_check(device):
    resistances = {}
    for i in range(1,4): # iterate through smus 1-4
        for j in range(4,i,-1): 
            """ 
            We have the following pairs in order
            1-4,1-3,1-2,2-4,2-3,3-4
            """
            R=contact_check(i,j,device)
            resistances[f"{i}-{j}"] = R

    #convert dictionary to df
    df = pd.DataFrame(resistances.items(), columns=['SMU pair', 'Resistance (Ohm)'])
    return df

def EBL(device):
    # EBL are SMUs 1-4 and 2-3
    resistances = {}
    for i,j in zip(range(1,3),range(4,2,-1)): #loop simultaneously 1-4,2-3 pairs
        R = contact_check(i,j,device)
        resistances[f"{i}-{j}"] = R
    
    #convert dictionary to df
    df = pd.DataFrame(resistances.items(), columns=['SMU pair', 'Resistance (Ohm)'])
    return df
            
def OL(device):
    # OL smu 3-4,1-2
    resistances= {}
    for i,j in zip(range(3,0,-2),range(4,1,-2)): #loop simultaneously 3-4 , 1-2 pairs
        R = contact_check(i,j,device)
        resistances[f"{i}-{j}"] = R

    #convert dictionary to df
    df = pd.DataFrame(resistances.items(), columns=['SMU pair', 'Resistance (Ohm)'])
    return df

#double sweep from start to stop and then from start to stop
def sweep(start,stop,step,comp,integration,device): #step cannot be negative
    device.del_user_functions()
    if start < stop and step < 0 :
        step = -step
    elif start > stop and step > 0 :
        step = -step

    smu_v = device.smu_dict()
    smu_ground = device.smu_dict()
    parameters = device.var1_dict()

    smu_v.update(
        iname = 'I2',
        vname = 'V2',
        mode = 'V',
        func = 'VAR1'
    )
    smu_ground.update(
        iname ='I4',
        vname = 'V4',
        mode = 'COMM',
        func = 'CONS'
    )
    parameters.update(
        mode ='DOUB',
        start = start,
        stop = stop,
        step = step,
        comp = comp,
        pcomp = 0
    )

    #disable smus 1 and 3
    device.measurement_mode('SWE')
    device.smu_disable(1)
    device.smu_disable(3)

    device.setup_smu(2,smu_v)
    device.setup_smu(4,smu_ground)
    device.setup_var1(parameters)
    device.integration_time(integration)
    
    #display variables
    device.display_variable('X','V2')
    device.axis_scale('X','LIN')
    device.display_variable_min_max('X','MIN',min(start,stop))
    device.display_variable_min_max('X','MAX',max(start,stop))
    device.display_variable('Y1','I2')
    device.axis_scale('Y1','LIN')

    #execute measurement
    device.single_measurement()
    while device.operation_completed()==False:
        time.sleep(2)
        
    device.autoscaling()

    #return values
    V=device.return_values('V2')
    I=device.return_values('I2')

    #convert the list to np.array to return the absolute values for the logarithmic scale
    V = np.array(V)
    I = np.array(I)

    #return all values to the function
    return V, I

#sampling check
def sampling_check(voltage,device):
    device.del_user_functions()
    parameters ={
        'mode' : 'LIN',
        'hold': 0,
        'interval':2e-3,
        'points': 5,
        'filter': 'OFF',
        'value':voltage, #voltage value
        'comp':0.1 #compliance value
    }
    smu_v = device.smu_dict()
    smu_ground = device.smu_dict()


    smu_v.update(
        iname = 'I2',
        vname = 'V2',
        mode = 'V',
        func = 'CONS'
    )
    smu_ground.update(
        iname ='I4',
        vname = 'V4',
        mode = 'COMM',
        func = 'CONS'
    )
    
    device.measurement_mode('SAMP')
    device.smu_disable(1)
    device.smu_disable(3)
    device.setup_smu(2,smu_v)
    device.setup_smu(4,smu_ground)

    device.setup_smu_sampling(2,parameters)
    device.setup_sampling(parameters)

    device.integration_time('LONG')
    
    #remove total sampling time
    device.auto_sampling_time('ON')

    device.user_function('R','OHM','V2/I2')

    device.display_variable('X','@INDEX')
    device.axis_scale('X',"LIN")
    device.display_variable('Y1','R')
    device.axis_scale('Y1','LIN')
    device.single_measurement()
    while device.operation_completed() == False:
        time.sleep(2)
       
    index = np.array(device.return_values('@INDEX'))
    R = np.array(device.return_values('R'))
    R_mean = np.average(R)
    device.autoscaling()

    # Plot the results
    fig,ax = plt.subplots()
    
    ax.set_title(f"Average Resistance(Sampling Check):{'{:.2e}'.format(R_mean)} Ohm")
    ax.set_yscale('log')
    ax.set_ylabel('Resistance (Ohm)')
    ax.set_xlabel('Sampling Index')
    ax.set_xticks(index)
    ax.scatter(index,np.absolute(R),label = f"Voltage={voltage}V")
    ax.legend()
    display(fig)
    
    return R_mean

#new (retention)
def retention(voltage,period,duration,device):
    device.del_user_functions()
    parameters ={
        'mode' : 'LIN',
        'hold': 0,
        'interval':2e-3,
        'points': 0,
        'filter': 'OFF',
        'value':voltage, #voltage value
        'comp':0.1 #compliance value
    }
    smu_v = device.smu_dict()
    smu_ground = device.smu_dict()

    smu_v.update(
        iname = 'I2',
        vname = 'V2',
        mode = 'V',
        func = 'CONS'
    )
    smu_ground.update(
        iname ='I4',
        vname = 'V4',
        mode = 'COMM',
        func = 'CONS'
    )
    
    device.measurement_mode('SAMP')
    device.smu_disable(1)
    device.smu_disable(3)
    device.setup_smu(2,smu_v)
    device.setup_smu(4,smu_ground)

    device.setup_smu_sampling(2,parameters)

    device.integration_time('LONG')
    device.total_sampling_time(duration)

    if int(duration/period)+1<=10001:
        parameters.update(points=int(duration/period)+1)
    else:
        parameters.update(points = 'MAX')
    device.setup_sampling(parameters)    
    
    device.integration_time('MED')
    device.user_function('R','OHM','V2/I2')
    device.user_function('ABSR','OHM', 'ABS(R)')
    
    
    device.display_variable('X','@TIME')
    device.axis_scale('X','LIN')
    device.display_variable_min_max('X1','MIN',0)
    device.display_variable_min_max('X1','MAX',duration)
    
    device.display_variable('Y1','ABSR')
    device.axis_scale('Y1','LOG')
    device.display_variable_min_max('Y1','MIN',10)
    device.display_variable_min_max('Y1','MAX',10**8)
    
    device.single_measurement()
    while device.operation_completed() == False:
        time.sleep(2)

    
    TIME = device.return_values('@TIME')
    R = device.return_values('R')
    TIME = np.array(TIME)
    R = np.array(R)
    device.autoscaling()
    return TIME,R
    

#plot sweep results
def plot_sweep(x,y,title):
    #plot results
    fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
    fig.suptitle(title)
    ax1.set_title('Linear I')
    ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
    ax2.set_title('Logarithmic I')
    ax2.set(xlabel='Voltage(V)',ylabel='Current(A)')
    ax2.set_yscale('log')

    ax1.plot(x,y)
    ax2.plot(x,np.absolute(y))
    fig.tight_layout()
    display(fig)

def plot_retention(x,y):
    fig, ax = plt.subplots() 
    fig.suptitle('Retention')
    ax.set(xlabel='time(s)',ylabel='Resistance(Ohm)')
    ax.set_yscale('log')
    ax.set_xscale('linear')
    ax.plot(x,y)
    display(fig)
    

def create_data_frame(x,y):
    header = ['V(V)','ABSV(V)',"I(A)",'ABSI(A)',"R(Ohm)"]
    data = {header[0]:x,header[1]:np.absolute(x),header[2]:y,header[3]:np.absolute(y),header[4]:np.divide(x,y)}
    df = pd.DataFrame(data)
    return df

def create_retention_data_frame(x,y):
    header = ['Time(s)','R(Ohm)']
    data = {header[0]:x,header[1]:y}
    df =  pd.DataFrame(data)
    return df


#write results to file
def write_to_file(file,title:list,df):
    #append escape character after each element
    index = 1
    while index <= len(title):
        title.insert(index,"\n")
        index = index+2

    #write to file
    with open(file,'a') as f:    
        f.writelines(title)
        f.write("\n")
        f.write(df.to_string())
        f.write("\n\n")

#### new functions ##############
def disable_widgets(widgets_list):
    for widget in widgets_list:
        widget.disabled = True

def change_state(widgets_list):
    for widget in widgets_list:
        widget.disabled = not widget.disabled

def enable_widgets(widgets_list):
    for widget in widgets_list:
        widget.disabled = False

#a check values function
def check_values(step,set_voltage,reset_voltage):
    valid = True

    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    if step > abs(set_voltage) or step > abs(reset_voltage) or step==0:#invalid parameter setting 
        valid = False
        tkinter.messagebox.showerror(message="Invalid parameter setting!")

    #now if the set-reset voltages have the same polarity show a warning
    elif set_voltage*reset_voltage>0:
        valid = tk.messagebox.askokcancel(message="Set-Reset voltages have the same polarity. Continue?")

    else:
        pass
        
    root.destroy()
    return valid
        

def information_box(information):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #display meaagebox
    tkinter.messagebox.showinfo(message=information)
    root.destroy()

#choose directory to save measurement results
#and check if you have access
def check_writable(folder):
    filename = "test.txt"
    file = os.path.join(folder,filename)

    #protection against removing existing file in python
    i=1
    while os.path.exists(file):
        filename=f"test{i}.txt"
        file = os.path.join(folder,filename)
    try:
        with open(file,'a'):
            writable = True
        os.remove(file)
    except:
        writable = False
        information_box(f"{folder} is not writable!")
    
    return writable  
    
def choose_folder():
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #choose nonemty folder
    folder = tk.filedialog.askdirectory()
    
    while folder == '':
        folder = tk.filedialog.askdirectory()

    #check if writable in a while loop
    writable=check_writable(folder)

    while writable == False:
        #choose a correct folder
        folder = tk.filedialog.askdirectory()
    
        while folder == '':
            folder = tk.filedialog.askdirectory()
        
        #check writable if not repeat
        writable=check_writable(folder)
        
    root.destroy()
    return folder


def upload_results(source_file,target_file,target_file_dir):
    """
    New function (UPLOAD RESULTS) 
    IMPORTANT FOR ALL MEASUREMENTS
    THE RESULTS ARE MOVED FROM SOURCE FILE TO TARGET FILE EVEN LOCALLY
    """
    while True:
        try:
            with (open(source_file,'r') as source,open(target_file,'a') as target):
                target.write(source.read())
            os.remove(source_file)
            return source_file,target_file,target_file_dir
        except:
            information_box(f"{target_file} is no longer accessible. Please change directory")
            target_file_dir = choose_folder()
            filename = os.path.basename(target_file)
            target_file =os.path.join(target_file_dir,filename)
            #and then try again


#ini file functions
def save_as_ini(default_filename):
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".ini", filetypes=[("Ini files","*.ini")],title = "save as ini",initialfile =default_filename)

    #check if the file path is correct(.txt)
    while file.endswith(".ini") == False:
        #open again filedialog with error message box
        answer=tk.messagebox.askyesno(message='Do you want to cancel the ini file Save?')
        if answer == True:
            raise Exception("Ini File Operation aborted!")
        else:
            file = filedialog.asksaveasfilename(defaultextension=".ini", filetypes=[("Ini files","*.ini")],title = "save as ini",initialfile =default_filename)
    root.destroy()
    return file

def load_ini():
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    
    file = filedialog.askopenfilename(filetypes=[("Ini files","*.ini")],title ='Select ini file')
    while file.endswith(".ini") == False:
        #open again filedialog with error message box
        answer=tk.messagebox.askyesno(message='Do you want to cancel the ini file load?')
        if answer == True:
            raise Exception("Ini File Operation aborted!")
        else:
            file = filedialog.askopenfilename(filetypes=[("Ini files","*.ini")],title = "Select ini file")
    root.destroy()
    return file


        

        
        
    
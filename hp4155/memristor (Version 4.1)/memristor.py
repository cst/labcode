### this is the new memrstor measurement (set and reset as many times as the user wants and full sweeps with a button)
from help import *
import ipywidgets as widgets
from keyboard import add_hotkey,remove_hotkey
import configparser

# pulsed libraries
from help_pulse import *

#create temporary file to store the results localy
temp_file= os.path.join(os.getcwd(),'tempfile.txt')

# the three naming fields

sample_series= widgets.Text(
    value= '',
    placeholder ='Enter text here:',
    description = 'sample series:',
    style = {'description_width': 'initial'}
    )

field = widgets.Text(
    value= '',
    placeholder ='Enter text here:',
    description = 'Field:',
    style = {'description_width': 'initial'},
    )

DUT = widgets.Text(
    value= '',
    placeholder ='Enter text here:',
    description = 'DUT:',
    style = {'description_width': 'initial'},
    )


#choose a new folder button
new_folder = widgets.Button(description='change folder')

image = widgets.Image(
    value=open("schematic.png", "rb").read(),
    format='png',
    width=300,
    height=100,
)

contact_check =  widgets.Button(description = 'CONTACT CHECK')
qcc = widgets.Button(description = 'QUICK CONTACT CHECK',layout=widgets.Layout(width='80%'),style={"button_width": "auto"})
qcc_select = widgets.RadioButtons(description = 'QCC type:',options = ['EBL','OL'])

vertical1 = widgets.VBox([sample_series,field,DUT,new_folder,contact_check,qcc,qcc_select])
vertical2 = widgets.VBox([image])
all_text_boxes = widgets.HBox([vertical1,vertical2])



#first series of parameters
step = widgets.BoundedFloatText(
    value=0.01,
    min=0,
    max=100,
    step=0.01,
    description='Step(V):',
)

integration_time=widgets.Dropdown(
    options=['SHORt', 'MEDium', 'LONG'],
    value='MEDium',
    description='Integration:',
    #style = {'description_width': 'initial'},
)

sampling=widgets.Checkbox(description='sampling check')

auto_qcc = widgets.Checkbox(
    description = 'Auto QCC after Reset',
    style = {'description_width': 'initial'},
    value = True
)

# THE BUTTONS 
#create buttons as it shown in the how_buttons_look 
set=widgets.Button(description='SET')
reset=widgets.Button(description='RESET')
full=widgets.Button(description='FULL SWEEP')
number = widgets.BoundedIntText(value=1,min=1,max=sys.maxsize,step=1,description='full sweeps:',disabled=False) #number of measuremts for the full sweep
retention_button=widgets.Button(description='RETENTION')
export_ini_button = widgets.Button(description = 'Export as ini')
import_ini_button = widgets.Button(description='Import from ini')



#parameter boxes
Vset=widgets.BoundedFloatText(
    value=1,
    min=-100,
    max=100,
    step=0.1,
    description='Voltage(V):',
)

#parameter buttons
CC_vset=widgets.BoundedFloatText(
    value=1e-3,
    min=-0.1,
    max=0.1,
    step=0.01,
    description= 'Comp(A):',
)

#parameter buttons
Vreset=widgets.BoundedFloatText(
    value=-1,
    min=-100,
    max=100,
    step=0.1,
    description='Voltage(V):',
)

#parameter buttons
CC_vreset=widgets.BoundedFloatText(
    value=1e-3,
    min=-0.1,
    max=0.1,
    step=0.01,
    description='Comp(A):',
)

Vretention=widgets.BoundedFloatText(
    value=1,
    min=-100,
    max=100,
    step=1,
    description='Voltage(V):',
)

period=widgets.BoundedFloatText(
    value=1,
    min=2e-3,
    max=65.535,
    step=1,
    description='Period(s):',
)

duration=widgets.BoundedFloatText(
    value=60,
    min=60e-6,
    max=1e11,
    step=1,
    description='Duration(s):',
)

# for automatic stop of endurance
auto_stop = widgets.Checkbox(
    description = 'Auto Stop',
    style = {'description_width': 'initial'},
    value = False
)

threshold = widgets.FloatText(
    description = "Stop Condition: R(HRS)/R(LRS)<",
    style = {'description_width': 'initial'},
    value = 1000
)

#align a button with a checkbox or integer bounded texts horizontaly
line0 = widgets.HBox([step,integration_time,sampling,auto_qcc])
line1 = widgets.HBox([set,Vset,CC_vset])
line2 = widgets.HBox([reset,Vreset,CC_vreset])
line3 = widgets.HBox([full,number,auto_stop,threshold])
line4 = widgets.HBox([retention_button,Vretention,period,duration])

#pack them into a single vertical box
all = widgets.VBox([line0,line1,line2,line3,line4])
output = widgets.Output()

#choose folder directory
folder=choose_folder() #here buttons dont work yet!
#display all at the end
display(all_text_boxes)

cons_widgets,cons_dict = constant_pulse()
sweep_widgets,sweep_dict = sweep_pulse()

sweep_button = widgets.Button(description = "SWEEP PULSE")
cons_button = widgets.Button(description = "CONSTANT PULSE")


children = [all,widgets.VBox([sweep_widgets,sweep_button]),widgets.VBox([cons_widgets,cons_button])]
titles = ["Regular","Sweep Pulse","Constant Pulse"]
tab = widgets.Tab()
tab.children = children
tab.titles = titles

display(tab)
display(widgets.HBox([import_ini_button,export_ini_button]))
display(output)

all_widgets=[sweep_button,cons_button,sample_series,field,DUT,set,reset,full,new_folder,retention_button,contact_check,qcc,qcc_select,Vset,CC_vset,Vreset,CC_vreset,step,integration_time,number,sampling,Vretention,period,duration,auto_qcc,auto_stop,threshold,export_ini_button,import_ini_button]
add_widgets_to_list(cons_dict,all_widgets)
add_widgets_to_list(sweep_dict,all_widgets)

# The regular dictionary for ini
regular_parameters={
    'step': step,
    'integration': integration_time,
    'set_voltage': Vset,
    'set_compliance': CC_vset,
    'reset_voltage': Vreset,
    'reset_compliance':CC_vreset,
    'number_of_cycles': number,
    'threshold':threshold,
    'retention_voltage':Vretention,
    'retention_period': period,
    'retention_duration':duration
}

device = hp4155a.HP4155a('GPIB0::17::INSTR')
device.reset()
device.disable_not_smu()

def on_contact_check_clicked(b):
    global folder,temp_file
    with output:
        clear_output()
        change_state(all_widgets)
        device.inst.lock_excl()

        filename=f"{sample_series.value}_{field.value}_{DUT.value}.txt"
        file = os.path.join(folder,filename)

        R = regular_contact_check(device)
        date = str(datetime.today().replace(microsecond=0))
        title = [f"Full Contact Check at {date}"]

        write_to_file(temp_file,title,R)

        #upload results
        temp_file,file,folder=upload_results(temp_file,file,folder)

        information_box("Contact Check Completed")
        device.inst.unlock()

        change_state(all_widgets)

def on_qcc_clicked(b):
    global folder,temp_file
    with output:
        clear_output()
        change_state(all_widgets)
        device.inst.lock_excl()

        filename=f"{sample_series.value}_{field.value}_{DUT.value}.txt"
        file = os.path.join(folder,filename)
        device.inst.lock_excl()
        
        if qcc_select.value == 'EBL':
            R = EBL(device)
        else: # OL
            R = OL(device) #df

        date = str(datetime.today().replace(microsecond=0))
        title = [f"Quick Contact Check ({qcc_select.value}) at {date}"]

        write_to_file(temp_file,title,R)

        #upload results
        temp_file,file,folder=upload_results(temp_file,file,folder)

        information_box("Quick Contact Check Completed")

        device.inst.unlock()

        change_state(all_widgets)
    
        
def on_set_button_clicked(b):
    global folder,temp_file
    with output:
        #disable buttons
        change_state(all_widgets)

        filename=f"{sample_series.value}_{field.value}_{DUT.value}.txt"
        file = os.path.join(folder,filename)

        #lock the  device
        device.inst.lock_excl()

        clear_output()

        #check values
        valid = check_values(step.value,Vset.value,Vreset.value)


        if valid == True:
            if sampling.value == True: #do sampling set before set process(100mV)
                R_mean_before= sampling_check(-0.01,device)
                 
               
            #execute measurement,plot results and save them
            V12,I12 = sweep(0,Vset.value,step.value,CC_vset.value,integration_time.value,device)
            plot_sweep(V12,I12,'SET')
            df = create_data_frame(V12,I12)
            display(df)
            

            if sampling.value == True: #do sampling set after set process(10mV)
                R_mean_after = sampling_check(0.01,device)
            
            date = str(datetime.today().replace(microsecond=0))
            title = [f"SET Memristor at {date}",f"Set Voltage={Vset.value}V",f"current compliance={CC_vset.value}A"]
            if sampling.value == True:
                title.extend([f"R(Ohm)  Before/After",f"{R_mean_before}  {R_mean_after}"])
            write_to_file(temp_file,title,df)

            #upload results
            temp_file,file,folder=upload_results(temp_file,file,folder)
        
        #show messagebox
        information_box("Measurement finished!")

        #unlock device
        device.inst.unlock()
        
        change_state(all_widgets)
 
def on_reset_button_clicked(b):
    global folder,temp_file
    with output:
        change_state(all_widgets)

        filename=f"{sample_series.value}_{field.value}_{DUT.value}.txt"
        file = os.path.join(folder,filename)

        #lock device
        device.inst.lock_excl()

        clear_output()

        #check values
        valid = check_values(step.value,Vset.value,Vreset.value)


        if valid == True:
            if sampling.value == True: #do sampling set before reset process(10mV)
                R_mean_before = sampling_check(0.01,device)
            
            #execute measurement,plot results and save them
            V34,I34 = sweep(0,Vreset.value,step.value,CC_vreset.value,integration_time.value,device)
            plot_sweep(V34,I34,'RESET')
            df = create_data_frame(V34,I34)
            display(df)
            
            if sampling.value == True: #do sampling set after reset process(100mV)
                R_mean_after = sampling_check(-0.01,device)

            date = str(datetime.today().replace(microsecond=0))
            title =[f"RESET Memristor at {date}",f"Reset Voltage={Vreset.value}V",f"current compliance={CC_vreset.value}A"]
            if sampling.value == True:
                title.extend([f"R(Ohm)  Before/After",f"{R_mean_before}  {R_mean_after}"])
            write_to_file(temp_file,title,df)

            #Quick Contact Check after reset Process 
            if auto_qcc.value == True:
                if qcc_select.value == 'EBL':
                    R=EBL(device)
                else: # OL
                    R=OL(device)

                title = [f"Automatic Quick Contact Check({qcc_select.value}) after Reset"]
                write_to_file(temp_file,title,R)
            
            #upload results
            temp_file,file,folder=upload_results(temp_file,file,folder)

        #show messagebox
        information_box("Measurement finished!")

        #unlock device
        device.inst.unlock()

        change_state(all_widgets)

def on_full_button_clicked(b):
    global folder,temp_file
    with output:
        change_state(all_widgets)

        filename=f"{sample_series.value}_{field.value}_{DUT.value}.txt"
        file = os.path.join(folder,filename)

        # lock device
        device.inst.lock_excl()
        
        clear_output()

        #check values
        valid = check_values(step.value,Vset.value,Vreset.value)
        date = str(datetime.today().replace(microsecond=0))
        
        if valid == True:
            with open(temp_file,'a') as f:
                header =[f"{number.value} full sweeps with parameters:",f"Set Voltage = {Vset.value}V",f"Current compliance set = {CC_vset.value}A",f"Reset Voltage = {Vreset.value}V",f"Current compliance reset = {CC_vreset.value}A"]
                
                fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
                fig.suptitle('FULL SWEEP')
                ax1.set_title('Linear I')
                ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
                ax2.set_title('Logarithmic I')
                ax2.set(xlabel='Voltage(V)',ylabel='Current(A)')
                ax2.set_yscale('log')

                stop = False

                def break_loop():
                    nonlocal stop
                    stop = True
                #help list with the resistances
                resistances = []    
                        
                add_hotkey("esc",break_loop)
                #execute number of measurements
                for i in range(number.value):#here it is easier to implement the sampling checks
                    clear_output(wait = True)
                    if i>0:
                        display(fig)
                    if sampling.value == True: #before set(100mv)
                        R_mean_init = sampling_check(-0.01,device)
                        resistances.append(R_mean_init)
                        
                    V12,I12 = sweep(0,Vset.value,step.value,CC_vset.value,integration_time.value,device) #set
                    plot_sweep(V12,I12,f"SET Iteration {i+1}")
                    
                    #after set/before reset
                    if sampling.value == True: #before set(10mv)
                        R_mean_set = sampling_check(0.01,device) # Here HRS
                        resistances.append(R_mean_set)
                        if auto_stop.value == True and abs(R_mean_init/R_mean_set)< abs(threshold.value):
                            stop = True
                        
                    if stop == False: # do the reset everything ok
                        V34,I34 = sweep(0,Vreset.value,step.value,CC_vreset.value,integration_time.value,device) #reset
                        plot_sweep(V34,I34,f"RESET Iteration {i+1}")


                        #after reset
                        if sampling.value == True:#-0.1V
                            R_mean_reset = sampling_check(-0.01,device) #here LRS
                            resistances.append(R_mean_reset)
                            if auto_stop.value == True and abs(R_mean_reset/R_mean_set)< abs(threshold.value):
                                stop = True

                        #butterfly curve
                        V=np.concatenate((V12,V34))
                        I=np.concatenate((I12,I34))
 
                    else: # Set failed keep only the set data
                        V = np.copy(V12)
                        I = np.copy(I12)
                        R_mean_reset = float('nan') # Indicates that also the reset sampling check did not happen not a number!
                        
                    #Quick Contact Check after reset Process even if it fails
                    if auto_qcc.value == True:
                        if qcc_select.value == 'EBL':
                            R = EBL(device)
                        else: # OL
                            R = OL(device)

                   
                    #create data frame and save to file
                    df = create_data_frame(V,I)
                    display(df)
                    if i == 0 :
                        header.extend([f"{i+1} Iteration"])
                        title = header.copy()
                    else:
                        title = [f"{i+1} Iteration"]
                    if sampling.value == True:
                        title.extend([f"R(Ohm)  INIT/SET/RESET",f"{R_mean_init} {R_mean_set} {R_mean_reset}"])

                    write_to_file(temp_file,title,df)
    
                    if auto_qcc.value == True:
                        title= [f"Quick Contact Check({qcc_select.value}) after Reset"]
                        write_to_file(temp_file,title,R)

                    #plot results
                    ax1.plot(V,I)
                    ax2.plot(V,np.absolute(I))
                    fig.tight_layout()
                    
                    #check for loop termination
                    if stop == True:
                        clear_output(wait = True)
                        information_box("Endurance stopped after esc (manually) or automatically!")
                        display(fig)
                        f.write("endurance stopped!\n\n")
                        break
                else:
                    clear_output(wait = True)
                    information_box("Endurance completed!")
                    display(fig)
                    f.write("endurance completed!\n\n")
                    
                remove_hotkey('esc')
                stop = False

                #plot resistances if sampling value == True or len(resistances) !=0
                if len(resistances)!=0:
                    indexes = np.arange(1,len(resistances)+1)
                    resistances = np.absolute(resistances)
                
                    fig, ax = plt.subplots()
                    
                    fig.suptitle('Average Resistances from sampling checks')
                    ax.set(xlabel='Index',ylabel='Resistance(Ohm)',yscale='log')
                    ax.scatter(indexes,resistances)
                    display(fig)


            #upload results
            temp_file,file,folder=upload_results(temp_file,file,folder)
        
        #unlock the device
        device.inst.unlock()
        change_state(all_widgets)


#new_folder clicked
def on_new_folder_button_clicked(b):
    global folder
    with output:
        change_state(all_widgets)
        
        folder = choose_folder() #choose new folder
    
        change_state(all_widgets)

def on_retention_button_clicked(b):
    global folder,temp_file
    with output:
        
        change_state(all_widgets)


        device.inst.lock_excl()
        
        clear_output()

        filename=f"{sample_series.value}_{field.value}_{DUT.value}.txt"
        file = os.path.join(folder,filename)

        #execute measurement
        t,R=retention(Vretention.value,period.value,duration.value,device)
        plot_retention(t,R)
        df=create_retention_data_frame(t,R)
        date = str(datetime.today().replace(microsecond=0))
        title =[f"Retention Memristor at {date}",f"Voltage={Vretention.value}V",f"period={period.value}s",f"duration={duration.value}s"]

        write_to_file(temp_file,title,df)
        #upload results
        temp_file,file,folder=upload_results(temp_file,file,folder)
        #show messagebox
        information_box("Measurement finished!")

        device.inst.unlock()
    
        change_state(all_widgets)


def on_sweep_button_clicked(b):
    with output:
        clear_output()
        change_state(all_widgets)
        device.inst.lock_excl()
        check_pulse(sweep_dict)

        sample_dict= {
            'series':sample_series,
            'field':field,
            'dut':DUT
        }

        times,values = sweep_meas(sweep_dict,device)
        plot_sweep_pulse(values)
        save_sweep(folder,sample_dict,values,times,sweep_dict)
        device.inst.unlock()
        change_state(all_widgets)


def on_constant_button_clicked(b):
    with output:
        clear_output()
        change_state(all_widgets)
        device.inst.lock_excl()
        check_pulse(sweep_dict)

        sample_dict= {
            'series':sample_series,
            'field':field,
            'dut':DUT
        }

        times,values = constant_meas(cons_dict,device)
        plot_constant_pulse(values)
        save_constant(folder,sample_dict,values,times,cons_dict)
        device.inst.unlock()
        change_state(all_widgets)

def on_export_ini_clicked(b):
    with output:
        change_state(all_widgets)
        config = configparser.ConfigParser()
        default_filename = 'memristor.ini'
        try:
            file = save_as_ini(default_filename)
            with open(file,'w') as configfile:
                config.add_section('ALL VALUES ARE IN SI-UNITS!')
                config.add_section('IT IS RECOMMENDED TO CHANGE THE INI FILE FROM THE INTERFACE AND DO NOT CHANGE ANY VALUES MANUALLY')

                #Regular Parameters
                config.add_section('Set-Reset-Endurance-Retention')
                for parameter,widget in regular_parameters.items():
                    config.set('Set-Reset-Endurance-Retention',parameter,str(widget.value))
                
                # Sweep_pulse
                config.add_section('Sweep Pulse')
                for parameter,widget in sweep_dict.items():
                    config.set('Sweep Pulse',parameter,str(widget.value))

                # Constant Pulse
                config.add_section('Constant Pulse')
                for parameter,widget in cons_dict.items():
                    config.set('Constant Pulse',parameter,str(widget.value))

                config.write(configfile)
        except Exception as e:
            information_box(e)
        
        change_state(all_widgets)
    
def on_import_ini_clicked(b):
    with output:
        disable_widgets(all_widgets)
        #load values to the interface
        config = configparser.ConfigParser()
        try:
            file = load_ini()
        except Exception as e:
            information_box(e)
            enable_widgets(all_widgets)
            return

        try:    
            #read the values from each section
            config.read(file)

            #Regular
            for parameter,widget in regular_parameters.items():
                widget.value = config.get('Set-Reset-Endurance-Retention',parameter)
            
            #Sweep Pulse
            for parameter,widget in sweep_dict.items():
                widget.value = config.get('Sweep Pulse',parameter)
            for parameter,widget in cons_dict.items():
                widget.value = config.get('Constant Pulse',parameter)

            information_box("all parameters loaded succesfully")
        except Exception as error:
            if type(error).__name__ =='NoSectionError':
                information_box(f"{error}.Explanation: Section(header) [section] does not exist. Create a new ini file or compare it with functional ini files!")
            elif type(error).__name__=='NoOptionError':
                information_box(f'{error}.Explanation: The variable name before the equal sign is not recognized. Create a new ini file or compare it with functional ini files!')
            elif type(error).__name__ == 'TraitError':
                information_box(f'{error}.Explanation: Invalid Parameter Setting. Check if you set an invalid value!')
            elif type(error).__name__ =="DuplicateOptionError":
                information_box(f"{error}. Explaination: The section contains the setted parameter more than once!")
            else:
                information_box(f"A {type(error).__name__} has occurred. Create A new ini file")
        change_state(all_widgets)
            

#link buttons to widgets (pulsed)
sweep_button.on_click(on_sweep_button_clicked)
cons_button.on_click(on_constant_button_clicked)

#link buttons with functions
set.on_click(on_set_button_clicked)
reset.on_click(on_reset_button_clicked)
full.on_click(on_full_button_clicked)
new_folder.on_click(on_new_folder_button_clicked)
retention_button.on_click(on_retention_button_clicked)
contact_check.on_click(on_contact_check_clicked)
qcc.on_click(on_qcc_clicked)

import_ini_button.on_click(on_import_ini_clicked)
export_ini_button.on_click(on_export_ini_clicked)

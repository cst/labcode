import sys
sys.path.insert(0, '..') #append parent directory

import ipywidgets as widgets
import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox
import os
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import module
import time
import pandas as pd
from IPython.display import display, clear_output

#widgets interactivity

def add_widgets_to_list(source_dictionary,target_list):
    for widget in source_dictionary.values():
        target_list.append(widget)

def change_state(widgets_list):
    for widget in widgets_list:
        widget.disabled = not widget.disabled

def enable_widgets(widgets_list):
    for widget in widgets_list:
        widget.disabled = False

def disable_widgets(widgets_list):
    for widget in widgets_list:
        widget.disabled = True

def information_box(information):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #display meaagebox
    tkinter.messagebox.showinfo(message=information)
    root.destroy()

#choose directory to save measurement results
#and check if you have access
def check_writable(folder):
    filename = "test.txt"
    file = os.path.join(folder,filename)

    #protection against removing existing file in python
    i=1
    while os.path.exists(file):
        filename=f"test{i}.txt"
        file = os.path.join(folder,filename)
    try:
        with open(file,'a'):
            writable = True
        os.remove(file)
    except:
        writable = False
        information_box(f"{folder} is not writable!")
    
    return writable

def choose_folder():
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #choose nonemty folder
    folder = tk.filedialog.askdirectory()
    
    while folder == '':
        folder = tk.filedialog.askdirectory()

    #check if writable in a while loop
    writable=check_writable(folder)

    while writable == False:
        #choose a correct folder
        folder = tk.filedialog.askdirectory()
    
        while folder == '':
            folder = tk.filedialog.askdirectory()
        
        #check writable if not repeat
        writable=check_writable(folder)
        
    root.destroy()
    return folder

def check_values(dictionary):
    #check if number of pulses is ok
    if dictionary['pulses'].value < 0:
        dictionary['pulses'].value = -dictionary['pulses'].value
    elif dictionary['pulses'].value==0:
        dictionary['pulses'].value = 1
    else:
        pass

    # Restriction: pulse period ≥ pulse width + 4 ms
    if dictionary['period'].value < dictionary['width'].value+4e-3:
        dictionary['period'].value = dictionary['width'].value+4e-3


#sweep pulse measurement
def sweep_meas(dict):
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset()
    device.smu_disable_sweep(1)
    device.smu_disable_sweep(3)

    #disable vmus and vsus
    device.disable_vsu(1)
    device.disable_vsu(2)
    device.disable_vmu(1)
    device.disable_vmu(2)

    device.measurement_mode("SWE")
    device.smu_function_sweep(2,"VAR1")
    device.smu_mode_meas(4,"COMMON")
    device.smu_function_sweep(4,"CONS")

    device.smu_mode_meas(2,"VPULSE")
    device.start_value_sweep(dict["start"].value)
    device.stop_value_sweep(dict["stop"].value)

    #define the number of steps given specific pulses
    
    step = (dict["stop"].value-dict["start"].value)/(dict["pulses"].value-1)
    device.step_sweep(step)
    
    device.comp("VAR1",dict["comp"].value)
    
    device.display_variable("X","V2")
    device.display_variable("Y1",'I2')

    device.range_mode(4,"AUTO")
    device.range_mode(2,"AUTO")

    device.pulse_base(dict["base"].value)
    device.pulse_width(dict["width"].value)
    device.pulse_period(dict["period"].value)
    device.integration_time(dict["integration"].value)

    t0 = time.time()
    device.single_measurement()
    while device.operation_completed()== False:
        pass
    
    t1 = time.time()
    # get the execution time
    elapsed_time = t1 - t0
    device.autoscaling()

    I_i=device.return_data("I2")
    V_i=device.return_data("V2")
    R_i = np.divide(V_i,I_i)
    
    
    expected_time = dict["period"].value*dict["pulses"].value

    times = (elapsed_time,expected_time)
    values = (V_i,I_i,R_i)

    del device

    return times,values

def plot_sweep(values):    
    fig, ax1 = plt.subplots() 
    color = 'tab:red'

    ax1.set_xlabel("V(V)")
    ax1.set_ylabel("I(A)",color=color)
    ax1.set_yscale('log')

    ax1.plot(values[0],np.abs(values[1]),color=color)
    ax1.tick_params(axis ='y', labelcolor = color,which = 'both') 

    # Adding Twin Axes 
    ax2 = ax1.twinx() 
    color = 'tab:green'

    ax2.set_ylabel("R(Ohm)",color = color)
    ax2.plot(values[0],np.abs(values[2]),color = color)

    ax2.tick_params(axis ='y', labelcolor = color,which = 'both')
    ax2.set_yscale('log')

    fig.suptitle("Sweep Pulse Measurement Results")

    plt.show()

def save_sweep(folder,sample_dict,values,times,sweep_dict):
    filename = f"{sample_dict['series'].value}_{sample_dict['field'].value}_{sample_dict['dut'].value}.txt"

    file = os.path.join(folder,filename)

    with open(file,"a") as f: 
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Sweep Pulse Measurement at {date}"+"\n")
        f.write(f"period(s):{sweep_dict['period'].value}"+"\n")
        f.write(f"width(s):{sweep_dict['width'].value}"+"\n")
        f.write(f"base value(V):{sweep_dict['base'].value}"+"\n")
        f.write(f"execution time(s):{times[0]}"+"\n")
        f.write(f"expected time(s):{times[1]}"+"\n")
        f.write(f"number of pulses:{sweep_dict['pulses'].value}"+"\n")
        f.write(f"current compliance(A):{sweep_dict['comp'].value}"+"\n")
        f.write(f"voltage:{sweep_dict['start'].value}V to {sweep_dict['stop'].value}V"+"\n")
        f.write(f"integration time:{sweep_dict['integration'].value}"+"\n\n")

        zipped = list(zip(values[0],values[1], values[2]))
        df = pd.DataFrame(zipped, columns=['VPULSE(V)', 'IPULSE(A)', 'RPULSE(Ohm)'])

        f.write("Results Sweep Pulse:\n")
        f.write(df.to_string())
        f.write("\n\n\n")

def constant_meas(dict):
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset()
    device.user_function('V','V','V2')
    device.user_function('I','A','I2')

    #disable vmus and vsus
    device.disable_vsu(1)
    device.disable_vsu(2)
    device.disable_vmu(1)
    device.disable_vmu(2)
    device.smu_disable_sweep(1)
    #device.smu_disable_sweep(3)
    
    device.measurement_mode("SWE")
    device.smu_mode_meas(2,"VPULSE")
    device.smu_function_sweep(2,'CONS')
    device.smu_mode_meas(4,"COMM")
    device.smu_function_sweep(2,"CONS")
    device.smu_function_sweep(4,'CONS')

    #smu 3 is used to define the number of pulses not contacted
    device.smu_mode_meas(3,'V')
    device.smu_function_sweep(3,"VAR1")

    device.start_value_sweep(0)
    device.stop_value_sweep(10)

    #define the number of steps given specific pulses
    step = 10/(dict["pulses"].value-1)
    device.step_sweep(step)
    
    device.comp("VAR1","MAX")
    device.const_comp(2,dict["comp"].value)

    device.cons_smu_value(2,dict["voltage"].value)
    
    device.display_variable("X","@INDEX")
    device.display_variable("Y1",'I')

    device.range_mode(4,"AUTO")
    device.range_mode(2,"AUTO")
    device.range_mode(3,"AUTO")

    device.pulse_base(dict["base"].value)
    device.pulse_width(dict["width"].value)
    device.pulse_period(dict["period"].value)
    device.integration_time(dict["integration"].value)

    t0 = time.time()
    device.single_measurement()
    while device.operation_completed()== False:
        pass
    
    t1 = time.time()
    # get the execution time
    elapsed_time = t1 - t0
    device.autoscaling()

    I_i=device.return_data("I")
    V_i=device.return_data("V")
    R_i = np.divide(V_i,I_i)

    
    expected_time = dict["period"].value*dict["pulses"].value

    times = (elapsed_time,expected_time)
    values = (V_i,I_i,R_i)

    del device

    return times,values

def plot_constant(values):
    index =[]
    for i in range(len(values[0])):
        index.append(i+1)
    
    fig, ax1 = plt.subplots() 
    color = 'tab:red'

    ax1.set_xlabel("Index(Pulse number)")
    ax1.set_ylabel("I(A)",color=color)
    ax1.set_yscale('log')

    ax1.plot(index,np.abs(values[1]),color=color,label = "Voltage(V):"+str(min(values[0])))
    ax1.tick_params(axis ='y', labelcolor = color,which = 'both') 

    # Adding Twin Axes 
    ax2 = ax1.twinx() 
    color = 'tab:green'

    ax2.set_ylabel("R(Ohm)",color = color)
    ax2.plot(index,np.abs(values[2]),color=color)
    ax2.set_yscale('log')

    ax2.tick_params(axis ='y', labelcolor = color,which = 'both') 

    fig.suptitle("Constant Pulse Measurement Results")
    ax1.set_title("Voltage:"+str(min(values[0]))+"V")

    plt.show()

def save_constant(folder,sample_dict,values,times,cons_dict):
    filename = f"{sample_dict['series'].value}_{sample_dict['field'].value}_{sample_dict['dut'].value}.txt"

    file = os.path.join(folder,filename)

    with open(file,"a") as f: 
        date = str(datetime.today().replace(microsecond=0))
        f.write(f"Constant Pulse Measurement at {date}"+"\n")
        f.write(f"period(s):{cons_dict['period'].value}"+"\n")
        f.write(f"width(s):{cons_dict['width'].value}"+"\n")
        f.write(f"base value(V):{cons_dict['base'].value}"+"\n")
        f.write(f"execution time(s):{times[0]}"+"\n")
        f.write(f"expected time(s):{times[1]}"+"\n")
        f.write(f"number of pulses:{cons_dict['pulses'].value}"+"\n")
        f.write(f"current compliance(A):{cons_dict['comp'].value}"+"\n")
        f.write(f"constant voltage:{cons_dict['voltage'].value}V"+"\n")
        f.write(f"integration time:{cons_dict['integration'].value}"+"\n\n")

        zipped = list(zip(values[0],values[1], values[2]))
        df = pd.DataFrame(zipped, columns=['VPULSE(V)', 'IPULSE(A)', 'RPULSE(Ohm)'])

        f.write("Results Constant Pulse:\n")
        f.write(df.to_string())
        f.write("\n\n\n")


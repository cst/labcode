import ipywidgets as widgets

#sample interface

style = {'description_width': 'initial'}
def sample():
    # the three naming fields
    sample_series= widgets.Text(
        value= '',
        placeholder ='Enter text here:',
        description = 'sample series:',
        style = {'description_width': 'initial'}
        )
    
    field = widgets.Text(
        value= '',
        placeholder ='Enter text here:',
        description = 'Field:',
        style = {'description_width': 'initial'},
        )
    
    DUT = widgets.Text(
        value= '',
        placeholder ='Enter text here:',
        description = 'DUT:',
        style = {'description_width': 'initial'},
        )

    sample_information = widgets.VBox([sample_series,field,DUT])
    
    #choose a new folder button
    new_folder = widgets.Button(description='change folder')
    sample_widgets = widgets.HBox([sample_information,new_folder])

    sample_dict = {
        'series':sample_series,
        'field':field,
        'dut':DUT,
        'button':new_folder
    }
    return sample_widgets,sample_dict


def constant_pulse():
    voltage = widgets.BoundedFloatText(
        value = 10,
        min = -100,
        max = 100,
        step = 1,
        description = 'Constant Voltage(V):',
        style=style,
    )

    comp = widgets.BoundedFloatText(
        value = 0.1,
        min = -0.1,
        max = 0.1,
        step = 0.01,
        description = 'Compliance(A):',
        style=style,
    )
    
    pulses = widgets.IntText(
        value = 100,
        description = 'Number of Pulses:',
        style=style,                    
    )
    period = widgets.BoundedFloatText(
        value = 5e-3,
        min = 5e-3,
        max = 1,
        step = 5e-3,
        description ='Pulse Period(s):',
        style=style,

    )
    width = widgets.BoundedFloatText(
        value = 5e-4,
        min = 5e-4,
        max = 1e-1,
        step= 5e-4,
        description ='Pulse Width(s):',
        style=style,
    )
    base = widgets.BoundedFloatText(
        value = 0,
        min = -100,
        max = 100,
        step = 1,
        description = 'Base Voltage(V):',
        style=style
    )

    integration =widgets.Dropdown(
        options=['SHORt', 'MEDium', 'LONG'],
        value='MEDium',
        description='Integration:',
        style=style
    )

    pulse_parameters = widgets.VBox([pulses,period,width,base])
    smu_parameters = widgets.VBox([voltage,comp,integration])

    constant_pulse_widgets = widgets.HBox([smu_parameters,pulse_parameters])
    constant_pulse_dict = {
        'voltage': voltage,
        'comp':comp,
        'pulses':pulses,
        'period':period,
        'width':width,
        'base':base,
        'integration':integration
    }
    return constant_pulse_widgets,constant_pulse_dict

def sweep_pulse():
    start_voltage = widgets.BoundedFloatText(
        value = 0,
        min = -100,
        max = 100,
        step = 1,
        description = 'Start Voltage(V):',
        style=style,
    )
    stop_voltage = widgets.BoundedFloatText(
        value = 15,
        min = -100,
        max = 100,
        step = 1,
        description = 'Stop Voltage(V):',
        style=style,
    )

    comp = widgets.BoundedFloatText(
        value = 0.1,
        min = -0.1,
        max = 0.1,
        step = 0.01,
        description = 'Compliance(A):',
        style=style,
    )
    
    pulses = widgets.IntText(
        value = 100,
        description = 'Number of Pulses:',
        style=style,                    
    )
    period = widgets.BoundedFloatText(
        value = 5e-3,
        min = 5e-3,
        max = 1,
        step = 5e-3,
        description ='Pulse Period(s):',
        style=style,

    )
    width = widgets.BoundedFloatText(
        value = 5e-4,
        min = 5e-4,
        max = 1e-1,
        step= 5e-4,
        description ='Pulse Width(s):',
        style=style,
    )
    base = widgets.BoundedFloatText(
        value = 0,
        min = -100,
        max = 100,
        step = 1,
        description = 'Base Voltage(V):',
        style=style
    )

    integration =widgets.Dropdown(
        options=['SHORt', 'MEDium', 'LONG'],
        value='MEDium',
        description='Integration:',
        style=style
    )

    pulse_parameters = widgets.VBox([pulses,period,width,base])
    smu_parameters = widgets.VBox([start_voltage,stop_voltage,comp,integration])

    sweep_pulse_widgets = widgets.HBox([smu_parameters,pulse_parameters])
    sweep_pulse_dict = {
        'start': start_voltage,
        'stop':stop_voltage,
        'comp':comp,
        'pulses':pulses,
        'period':period,
        'width':width,
        'base':base,
        'integration':integration
    }
    return sweep_pulse_widgets,sweep_pulse_dict

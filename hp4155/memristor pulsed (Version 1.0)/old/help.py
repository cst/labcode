import sys
sys.path.insert(0, '..') #append parent directory


import matplotlib.pyplot as plt
import numpy as np

import module
import time
import pandas as pd
import tkinter as tk
from tkinter import filedialog

def sweep_pulse(start,stop,comp,points,width,period):
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset()
    device.smu_disable_sweep(1)
    device.smu_disable_sweep(3)

    #disable vmus and vsus
    device.disable_vsu(1)
    device.disable_vsu(2)
    device.disable_vmu(1)
    device.disable_vmu(2)

    device.measurement_mode("SWE")
    device.smu_function_sweep(2,"VAR1")
    device.smu_mode_meas(4,"COMMON")
    device.smu_function_sweep(4,"CONS")

    device.smu_mode_meas(2,"VPULSE")
    device.start_value_sweep(start)
    device.stop_value_sweep(stop)

    #define the number of steps given specific pulses
    
    step = (stop-start)/(points-1)
    device.step_sweep(step)
    
    device.comp("VAR1",comp)
    
    device.display_variable("X","V2")
    device.display_variable("Y1",'I2')

    device.range_mode(4,"AUTO")
    device.range_mode(2,"AUTO")

    device.pulse_base(0)
    device.pulse_width(width)
    device.pulse_period(period)
    device.integration_time("MED")

    t0 = time.time()
    device.single_measurement()
    while device.operation_completed()== False:
        pass
    
    t1 = time.time()
    # get the execution time
    elapsed_time = t1 - t0
    device.autoscaling()

    I_i=device.return_data("I2")
    V_i=device.return_data("V2")
    R_i = np.divide(V_i,I_i)
    
    
    expected_time = period*int(points)

    times = (elapsed_time,expected_time)
    values = (V_i,I_i,R_i)

    del device

    return times,values

def constant_pulse(const,comp,points,period,width):
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset()
    device.user_function('V','V','V2')
    device.user_function('I','A','I2')

    #disable vmus and vsus
    device.disable_vsu(1)
    device.disable_vsu(2)
    device.disable_vmu(1)
    device.disable_vmu(2)
    device.smu_disable_sweep(1)
    #device.smu_disable_sweep(3)
    
    device.measurement_mode("SWE")
    device.smu_mode_meas(2,"VPULSE")
    device.smu_function_sweep(2,'CONS')
    device.smu_mode_meas(4,"COMM")
    device.smu_function_sweep(2,"CONS")
    device.smu_function_sweep(4,'CONS')

    #smu 3 is used to define the number of pulses not contacted
    device.smu_mode_meas(3,'V')
    device.smu_function_sweep(3,"VAR1")

    device.start_value_sweep(0)
    device.stop_value_sweep(10)

    #define the number of steps given specific pulses
    step = 10/(points-1)
    device.step_sweep(step)
    
    device.comp("VAR1","MAX")
    device.const_comp(2,comp)

    device.cons_smu_value(2,const)
    
    device.display_variable("X","V")
    device.display_variable("Y1",'I')

    device.range_mode(4,"AUTO")
    device.range_mode(2,"AUTO")
    device.range_mode(3,"AUTO")

    device.pulse_base(0)
    device.pulse_width(width)
    device.pulse_period(period)
    device.integration_time("MED")

    t0 = time.time()
    device.single_measurement()
    while device.operation_completed()== False:
        pass
    
    t1 = time.time()
    # get the execution time
    elapsed_time = t1 - t0
    device.autoscaling()

    I_i=device.return_data("I")
    V_i=device.return_data("V")
    R_i = np.divide(V_i,I_i)

    
    expected_time = period*int(points)

    times = (elapsed_time,expected_time)
    values = (V_i,I_i,R_i)

    del device

    return times,values

def create_file(measurement,period,width,pulses,values,times,comp):

    filename = " "
    
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =filename)

    #check if the file path is correct(.txt)
    while file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =filename)
    root.destroy()

    with open(file,"w") as f: 
        f.write(f"{measurement}"+"\n")
        f.write(f"period(s):{period}"+"\n")
        f.write(f"width(s):{width}"+"\n")
        f.write(f"execution time(s):{times[0]}"+"\n")
        f.write(f"expected time(s):{times[1]}"+"\n")
        f.write(f"number of pulses:{pulses}"+"\n")
        f.write(f"current compliance(A):{comp}"+"\n\n")

        zipped = list(zip(values[0],values[1], values[2]))
        df = pd.DataFrame(zipped, columns=['VPULSE(V)', 'IPULSE(A)', 'RPULSE(Ohm)'])

        f.write("Results Pulse:\n")
        f.write(df.to_string())

def plot_sweep(values):    
    fig, ax1 = plt.subplots() 
    color = 'tab:red'

    ax1.set_xlabel("V(V)")
    ax1.set_ylabel("I(A)",color=color)
    ax1.set_yscale('log')

    ax1.plot(values[0],np.abs(values[1]),color=color)
    ax1.tick_params(axis ='y', labelcolor = color,which = 'both') 

    # Adding Twin Axes 
    ax2 = ax1.twinx() 
    color = 'tab:green'

    ax2.set_ylabel("R(Ohm)",color = color)
    ax2.plot(values[0],np.abs(values[2]),color = color)

    ax2.tick_params(axis ='y', labelcolor = color,which = 'both')
    ax2.set_yscale('log')

    plt.show()


def plot_constant(values):
    index =[]
    for i in range(len(values[0])):
        index.append(i+1)
    
    fig, ax1 = plt.subplots() 
    color = 'tab:red'

    ax1.set_xlabel("Index(Pulse number)")
    ax1.set_ylabel("I(A)",color=color)
    ax1.set_yscale('log')

    ax1.plot(index,np.abs(values[1]),color=color,label = "Voltage(V):"+str(min(values[0])))
    ax1.tick_params(axis ='y', labelcolor = color,which = 'both') 

    # Adding Twin Axes 
    ax2 = ax1.twinx() 
    color = 'tab:green'

    ax2.set_ylabel("R(Ohm)",color = color)
    ax2.plot(index,np.abs(values[2]),color=color,label = "Voltage(V):"+str(min(values[0])))
    ax2.set_yscale('log')

    ax2.tick_params(axis ='y', labelcolor = color,which = 'both') 

    fig.legend()
    plt.show()




 








import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from matplotlib.markers import MarkerStyle

from measurement import Meas
from measurement import list_measurements
from measurement import *

os.chdir(r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros\memristor test\pulse_measurements\Drain sweep")

files = os.listdir(os.getcwd())

measurements = []

for file in files:
    measurement = Meas(file)
    measurement.get_information()
    measurements.append(measurement)

#split in short and medium integration time
measurements_shor,measurements_med = split_integration(measurements)
measurements_med= sort_by_period(measurements_med)
measurements_shor = sort_by_period(measurements_med)

# convert to the plotting class
measurements_med = list_measurements(measurements_med)
measurements_shor = list_measurements(measurements_shor)

measurements_med.retrive_values()
measurements_shor.retrive_values()


fig, (ax1,ax2) = plt.subplots(2 ,layout='constrained')

#colormap
cmap = plt.get_cmap('jet')

#the plot
fig.suptitle("High Current Pulse Sweep ")
ax1.set_xlabel("Pulse Period (s)")
ax1.set_ylabel("Delay(s)")
ax1.set_title("Delay")

im=ax1.scatter(measurements_med.periods,measurements_med.delays , c=measurements_med.pulses, cmap=cmap,marker='o',label ='MED')

im =ax1.scatter(measurements_shor.periods,measurements_shor.delays , c=measurements_shor.pulses, cmap=cmap,marker='^',label ='SHOR')


ax1.grid(True)
ax1.legend()

ax2.set_xlabel("Pulse Period (s)")
ax2.set_ylabel("Relative error (%)")
ax2.set_title("Relative error")

im = ax2.scatter(measurements_med.periods, measurements_med.relative_errors, c=measurements_med.pulses, cmap=cmap,marker='o',label='MED')
im=ax2.scatter(measurements_shor.periods, measurements_shor.relative_errors, c=measurements_shor.pulses, cmap=cmap,marker='^',label='SHOR')


c_bar = plt.colorbar(im,ax=(ax1,ax2))
c_bar.set_label("Number of pulses")

ax2.grid(True)
ax2.legend()

plt.show()

#plot results

#get the nessecary files
plot_med=measurements_med.get_indexes()
plot_shor=measurements_shor.get_indexes()

# convert to lists of measurements
for i in range(len(plot_med)):
    plot_med[i]= list_measurements(plot_med[i])
    plot_med[i].retrive_values()

for i in range(len(plot_shor)):
    plot_shor[i]= list_measurements(plot_shor[i])
    plot_shor[i].retrive_values()

#create figure
fig,axs = plt.subplots(3)

for i in range(3):
    period = max(plot_med[i].periods) #one element
    for df in plot_med[i].results:
        axs[i].plot(df["VPULSE(V)"],df['IPULSE(A)'],marker='o')
    for df in plot_shor[i].results:
        axs[i].plot(df["VPULSE(V)"],df['IPULSE(A)'],marker='^')
    axs[i].legend([f"Period(s):{period}"])

print(measurements_med.medium)
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from measurement import Meas
import warnings

warnings.filterwarnings("ignore")


# get the 14 measurements
def split_dataframe_index(df):
    subsets= []
    for i in range(10):
        subset = df[df["Index"]==i+1]
        subsets.append(subset)
    return subsets   

#get the 3 measureemnts
def plot_3(periods,df):
    subsets = []
    for period in periods:
        subset = df[df["Period(s)"]==period]
        subsets.append(subset)
    return subsets

#get measurement results from dataframe

os.chdir(r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros\memristor test\pulse_measurements\Drain sweep")

files = os.listdir(os.getcwd())

data = pd.DataFrame() 

for file in files:
    if file.endswith(".txt"):
        measurement = Meas(file)
        dictionary = {"Index":measurement.index,"Period(s)":measurement.period,"Pulses":measurement.pulses,"Ideal(s)":measurement.ideal,"Real(s)":measurement.real,"Delay(s)":measurement.delay,"Relative Error(%)":measurement.relative_error,"Integration":measurement.integration,"file location":measurement.file}
        data = data._append(dictionary,ignore_index=True)
        
#save the data for later use
data.to_csv("Results.csv", encoding='utf-8',index=False)


#split to short and medium integration time
med = data[data['Integration']=='MED']
shor = data[data['Integration']=='SHOR']


#get the 10 measuremets
measurements_med = split_dataframe_index(med)
measurements_shor = split_dataframe_index(shor)

for subset in measurements_med:
    subset.sort_values(by='Pulses',inplace=True)
for subset in measurements_shor:
    subset.sort_values(by='Pulses',inplace=True)

for subset in measurements_med:
    plt.plot(subset["Pulses"],subset["Delay(s)"],label =f"MED:{round(subset["Period(s)"].mean(),3)}s",marker="o")
for subset in measurements_shor:
    plt.plot(subset["Pulses"],subset["Delay(s)"],label =f"SHOR:{round(subset["Period(s)"].mean(),3)}s",marker="v")


plt.title("Delay of the High Current Pulse Sweep Measurements")
plt.xlabel("Number of Pulses")
plt.ylabel("Delay(s)")


plt.legend()
plt.show()


for subset in measurements_med:
    plt.plot(subset["Pulses"],subset["Relative Error(%)"],label =f"MED:{round(subset["Period(s)"].mean(),3)}s",marker="o")
for subset in measurements_shor:
    plt.plot(subset["Pulses"],subset["Relative Error(%)"],label =f"SHOR:{round(subset["Period(s)"].mean(),3)}s",marker="v")

plt.title("Relative Error of the High Current Pulse Sweep Measurements")

plt.xlabel("Number of Pulses")
plt.ylabel("Relative Error(%)")
plt.yscale("log")

plt.legend()
plt.show()

print("Average Delay for medium integration time:",med["Delay(s)"].mean(),"±",med["Delay(s)"].std())
print("Average Delay for short integration time:",shor["Delay(s)"].mean(),"±",shor["Delay(s)"].std())

data = data.sort_values(by='Period(s)')
data.boxplot(column=["Delay(s)"],by="Period(s)")

plt.show()

data.boxplot(column=["Relative Error(%)"],by="Period(s)")
plt.yscale("log")
plt.show()


#plot the results

#first retrieve the periods and sort them
periods =list(set(data["Period(s)"].to_numpy()))
periods.sort()

periods_to_plot = [periods[0],periods[4],periods[9]]

plot_med = plot_3(periods_to_plot,med)
plot_shor = plot_3(periods_to_plot,shor)

fig,axs = plt.subplots(1,3,figsize=(15,9),sharey=True)

for index,subset in enumerate(plot_med): #the different periods
    subset.sort_values(by='Pulses',inplace= True)
    subset.reset_index(drop=True, inplace=True)
    subset.drop(index=[1,3],inplace=True)
    subset.reset_index(drop=True, inplace=True)

    
    for i in range(3): # 3 number of pulses
        measurement = subset.iloc[i]
        #get the values
        df = pd.read_csv(str(measurement["file location"]),delim_whitespace=True,skiprows=7)
        axs[index].plot(df["VPULSE(V)"],df['RPULSE(Ohm)'],label = f'pulses(MED):{int(measurement["Pulses"])}')
        print(f"Average Resistance for {int(measurement["Pulses"])} Pulses and Medium Integration Time:{df['RPULSE(Ohm)'].mean()}±{df['RPULSE(Ohm)'].std()}") 

    
for index,subset in enumerate(plot_shor): #the different periods
    subset.sort_values(by='Pulses',inplace= True)
    subset.reset_index(drop=True, inplace=True)
    subset.drop(index=[1,3],inplace =True)
    subset.reset_index(drop=True, inplace=True)

    for i in range(3): #3 different pulses
        measurement = subset.iloc[i]
        
        #get the values
        df = pd.read_csv(str(measurement["file location"]),delim_whitespace=True,skiprows=7)
        axs[index].plot(df["VPULSE(V)"],df['RPULSE(Ohm)'],label = f'pulses(SHOR):{int(measurement["Pulses"])}')
        print(f"Average Resistance for {int(measurement["Pulses"])} Pulses and Short Integration Time:{df['RPULSE(Ohm)'].mean()}±{df['RPULSE(Ohm)'].std()}") 
 
fig.suptitle("Measurement Results for High Current Pulse Sweep Measurements")
for i in range(3):
    axs[i].set_title(f"Period(s):{periods_to_plot[i]}")
    axs[i].set_xlabel("Voltage(V)")
    axs[i].set_ylabel("Resistance(Ohm)")

axs[0].legend()
axs[1].legend()
axs[2].legend()
fig.tight_layout()
plt.show()



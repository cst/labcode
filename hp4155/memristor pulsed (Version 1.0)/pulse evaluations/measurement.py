import pandas as pd

class Meas():
    def __init__(self,file:str):
        self.file = file

        replaced_file =  self.file.replace(".","_")
        filename = replaced_file.split("_")

        #read the integration time and the number of pulses,index
        self.integration=filename[2]
        self.pulses=int(filename[3])

        self.index = int(filename[4])

        #read the period, execution time and expected time
        with open(self.file,'r') as f:
            lines = f.readlines()
                
            for i in range(1,4):
                lines[i]= lines[i].replace("\n",":")
                lines[i] = lines[i].split(":")

                if i == 1:
                    self.period = float(lines[i][1])
                    
                
                elif i == 2 :
                    self.real= float(lines[i][1])
                else: #i==3
                    self.ideal = float(lines[i][1])
        
        # calculate delay and relative error
        self.delay = self.real-self.ideal
        self.relative_error = (self.delay / self.ideal) * 100

        #get dataframe 
        self.results = pd.read_csv(self.file,delim_whitespace=True,skiprows=7)


class constant_pulse():
    def __init__(self,file:str):
        self.file = file

        replaced_file =  self.file.replace(".","_")
        filename = replaced_file.split("_")

        #read the integration time and the number of pulses,index
        self.integration=filename[3]
        self.pulses=int(filename[4])

        self.index = int(filename[5])

        #read the period, execution time and expected time
        with open(self.file,'r') as f:
            lines = f.readlines()
                
            for i in range(1,4):
                lines[i]= lines[i].replace("\n",":")
                lines[i] = lines[i].split(":")

                if i == 1:
                    self.period = float(lines[i][1])
                    
                
                elif i == 2 :
                    self.real= float(lines[i][1])
                else: #i==3
                    self.ideal = float(lines[i][1])
        
        # calculate delay and relative error
        self.delay = self.real-self.ideal
        self.relative_error = (self.delay / self.ideal) * 100

        #get dataframe 
        self.results = pd.read_csv(self.file,delim_whitespace=True,skiprows=7)


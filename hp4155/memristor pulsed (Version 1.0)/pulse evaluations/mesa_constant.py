import os

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from measurement import Meas
from measurement import constant_pulse
import warnings

warnings.filterwarnings("ignore")

# get the 10 measurements
def split_dataframe_index(df):
    subsets= []
    for i in range(10):
        subset = df[df["Index"]==i+1]
        subsets.append(subset)
    return subsets   

#get the 3 measureemnts
def plot_3(periods,df):
    subsets = []
    for period in periods:
        subset = df[df["Period(s)"]==period]
        subsets.append(subset)
    return subsets

#get measurement results from dataframe

os.chdir(r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros\memristor test\pulse_measurements\constant pulse low current")

files = os.listdir(os.getcwd())

data = pd.DataFrame() 

for file in files:
    if file.endswith(".txt"):
        measurement = constant_pulse(file)
        I_min=(measurement.results["IPULSE(A)"].abs()).min() 
        dictionary = {"Index":measurement.index,"Period(s)":measurement.period,"Pulses":measurement.pulses,"Ideal(s)":measurement.ideal,"Real(s)":measurement.real,"Delay(s)":measurement.delay,"Relative Error(%)":measurement.relative_error,"Integration":measurement.integration,"file location":measurement.file,"Min I(A)":I_min,"Mean R(Ohm)":measurement.results["RPULSE(Ohm)"].mean(),"Max R(Ohm)":measurement.results["RPULSE(Ohm)"].max()}
        data = data._append(dictionary,ignore_index=True)



#save the data for later use
data.to_csv("Results.csv", encoding='utf-8',index=False)


#split to short and medium integration time
med = data[data['Integration']=='MED']
shor = data[data['Integration']=='SHOR']

#remove overflows
#shor = shor[shor["Max R(Ohm)"]<1e30]
#med = med[med["Max R(Ohm)"]<1e30]



#get the 14 measuremets
measurements_med = split_dataframe_index(med)
measurements_shor = split_dataframe_index(shor)


#plot boxplot delay and relative error for both short and medium integration time in one plot

data = data.sort_values(by='Period(s)')
data.boxplot(column=["Delay(s)"],by="Period(s)")

plt.show()

data.boxplot(column=["Relative Error(%)"],by="Period(s)")
plt.yscale("log")
plt.show()


for subset in measurements_med:
    subset.sort_values(by='Pulses',inplace=True)
for subset in measurements_shor:
    subset.sort_values(by='Pulses',inplace=True)

#plot number of pulse vs delay and realtive error
for subset in measurements_med:
    plt.plot(subset["Pulses"],subset["Delay(s)"],label =f"MED:{round(subset["Period(s)"].mean(),3)}s",marker="o")
for subset in measurements_shor:
    plt.plot(subset["Pulses"],subset["Delay(s)"],label =f"SHOR:{round(subset["Period(s)"].mean(),3)}s",marker="v")


plt.title("Delay of the Low Current Pulse constant Measurements")
plt.xlabel("Number of Pulses")
plt.ylabel("Delay(s)")


plt.legend()
plt.show()


for subset in measurements_med:
    plt.plot(subset["Pulses"],subset["Relative Error(%)"],label =f"MED:{round(subset["Period(s)"].mean(),3)}s",marker="o")
for subset in measurements_shor:
    plt.plot(subset["Pulses"],subset["Relative Error(%)"],label =f"SHOR:{round(subset["Period(s)"].mean(),3)}s",marker="v")

plt.title("Relative Error of the Low Current Pulse constant Measurements")

plt.xlabel("Number of Pulses")
plt.ylabel("Relative Error(%)")
plt.yscale("log")

plt.legend()
plt.show()

print("Average Delay for medium integration time:",med["Delay(s)"].mean(),"±",med["Delay(s)"].std())
print("Average Delay for short integration time:",shor["Delay(s)"].mean(),"±",shor["Delay(s)"].std())

#plot 3

#plot the results

#first retrieve the periods and sort them
periods =list(set(data["Period(s)"].to_numpy()))
periods.sort()

periods_to_plot = [periods[0],periods[4],periods[9]]

plot_med = plot_3(periods_to_plot,med)
plot_shor = plot_3(periods_to_plot,shor)

fig,axs = plt.subplots(2,3,figsize=(15,9))

for index,subset in enumerate(plot_med): #the different periods
    subset.sort_values(by='Pulses',inplace= True)
    subset.reset_index(drop=True, inplace=True)
    subset.drop(index=[1,3],inplace=True)
    subset.reset_index(drop=True, inplace=True)

    
    for i in range(3): # 3 number of pulses
        measurement = subset.iloc[i]
        #get the values
        df = pd.read_csv(str(measurement["file location"]),delim_whitespace=True,skiprows=7)
        axs[0,index].plot(df["VPULSE(V)"],df["IPULSE(A)"],label = f'pulses(MED):{int(measurement["Pulses"])}')
        print(f"Average Resistance for {int(measurement["Pulses"])} Pulses and Medium Integration Time:{df['RPULSE(Ohm)'].mean()}±{df['RPULSE(Ohm)'].std()}") 

    
for index,subset in enumerate(plot_shor): #the different periods
    subset.sort_values(by='Pulses',inplace= True)
    subset.reset_index(drop=True, inplace=True)
    subset.drop(index=[1,3],inplace =True)
    subset.reset_index(drop=True, inplace=True)

    for i in range(3): #3 different pulses
        measurement = subset.iloc[i]
        
        #get the values
        df = pd.read_csv(str(measurement["file location"]),delim_whitespace=True,skiprows=7)
        axs[1,index].plot(df["VPULSE(V)"],df['IPULSE(A)'],label = f'pulses(SHOR):{int(measurement["Pulses"])}')
        print(f"Average Resistance for {int(measurement["Pulses"])} Pulses and Short Integration Time:{df['RPULSE(Ohm)'].mean()}±{df['RPULSE(Ohm)'].std()}") 
 
fig.suptitle("Measurement Results for Low Current Pulse constant Measurements")
for i in range(2):
    for j in range(3):
        axs[i,j].set_title(f"Period(s):{periods_to_plot[j]}")
        axs[i,j].set_xlabel("Voltage(V)")
        axs[i,j].set_ylabel("Current(A)")

        axs[i,j].legend()
fig.tight_layout()
plt.show()

#plot minimum current
med = med.sort_values(by='Period(s)')
med.boxplot(column=["Min I(A)"],by="Period(s)",label='MED')
plt.yscale('log')

plt.legend()
plt.show()

shor = shor.sort_values(by='Period(s)')
shor.boxplot(column=["Min I(A)"],by="Period(s)",label='SHOR')
plt.yscale('log')

plt.legend()
plt.show()


print("Average Minimum I(A) for medium integration time:",med['Min I(A)'].mean(),"±",med['Min I(A)'].std())
print("Average Minimum I(A) for short integration time:",shor['Min I(A)'].mean(),"±",shor['Min I(A)'].std())




import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from measurement import Meas
import warnings

warnings.filterwarnings("ignore")

#plot all the measurements

os.chdir(r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros\memristor test\pulse_measurements\MESA")

files = os.listdir(os.getcwd())
fig= plt.figure(1)

for file in files:
    if file.endswith(".txt"):
        meas = Meas(file)

        fig.clear()
        plt.plot(meas.results["VPULSE(V)"],meas.results["IPULSE(A)"])
        plt.title(f"Period(s):{meas.period},Pulses:{meas.pulses},IT:{meas.integration}")
        plt.xlabel("V(V)")
        plt.ylabel("I(A)")
        
        filename = os.path.splitext(os.path.basename(file))[0]
        plt.savefig(f"{filename}.png")
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

os.chdir(r"\\fileserver.cst.rwth-aachen.de\public\Datentransfer\Asonitis, Alexandros\memristor test\pulse_measurements\High current measurements")


files = os.listdir(os.getcwd())

periods =[]
execution_times =[]
expected_times = []


#read all the headers from  the txt files 
for file in files:
    if file.endswith(".txt"):
        with open(file,'r') as f:
            lines = f.readlines()

            for i in range(1,4):
                lines[i]= lines[i].replace("\n",":")
                lines[i] = lines[i].split(":")

                if i == 1:
                    period = float(lines[i][1])
                    periods.append(period)
                
                elif i == 2 :
                    execution_time = float(lines[i][1])
                    execution_times.append(execution_time)
                else: #i==3
                    expected_time = float(lines[i][1])
                    expected_times.append(expected_time)


# get the dataframes
dfs = []

for file in files:
    df = pd.read_csv(file,delim_whitespace=True,skiprows=6)
    dfs.append(df)


#sort the lists
sort_indexes = np.argsort(periods)
expected_times = [expected_times[index] for index in sort_indexes]
execution_times = [execution_times[index] for index in sort_indexes]
periods= [periods[index] for index in sort_indexes]
dfs = [dfs[index] for index in sort_indexes]

#calculate delays
delays = np.subtract(execution_times,expected_times)

# plots

fig,(ax1,ax2,ax3) = plt.subplots(3,sharex=True)
fig.suptitle("Duration Evaluation of High Current Pulse Sweep Measurements")

ax1.plot(periods,expected_times,label="Ideal")
ax1.plot(periods,execution_times,label = "Real")
ax1.set_ylabel("Duration (s)")
ax1.legend()

ax2.plot(periods,delays)
ax2.set_ylabel("Delay (s)")

relative_error = np.divide(delays,expected_times)*100
ax3.plot(periods,relative_error)
ax3.set_ylabel("Relative Error (%)")
ax3.set_xlabel("Period (s)")

plt.show()

fig,(ax1,ax2) = plt.subplots(2,sharex=True)

df = dfs[0]
ax1.plot(df["V(V)"],df["I(A)"])
ax2.plot(df["V(V)"],df["R(Ohm)"])

ax1.set_ylabel("I (A)")
ax2.set_xlabel("V (V)")
ax2.set_ylabel("R (Ohm)")

fig.suptitle("Results Evaluation of High Current Pulse Sweep Measurements")
plt.show()
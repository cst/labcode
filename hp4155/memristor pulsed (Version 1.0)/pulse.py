from interface import *
from help_new import *

sample_widgets,sample_dict = sample()
display(sample_widgets)
print()
print()

cons_widgets,cons_dict = constant_pulse()
sweep_widgets,sweep_dict = sweep_pulse()

children = [sweep_widgets,cons_widgets]
titles = ["Sweep Pulse","Constant Pulse"]
tab = widgets.Tab()
tab.children = children
tab.titles = titles

display(tab)

sweep_button = widgets.Button(description = "Sweep Pulse")
cons_button = widgets.Button(description = "Constant Pulse")

buttons = widgets.HBox([sweep_button,cons_button])
output = widgets.Output()
display(buttons,output)

all_widgets = [sweep_button,cons_button]
add_widgets_to_list(sample_dict,all_widgets)
add_widgets_to_list(cons_dict,all_widgets)
add_widgets_to_list(sweep_dict,all_widgets)

#choose directory
folder = choose_folder()
def on_sweep_button_clicked(b):
    with output:
        clear_output(wait = True)
        change_state(all_widgets)
        check_values(sweep_dict)

        times,values = sweep_meas(sweep_dict)
        plot_sweep(values)
        save_sweep(folder,sample_dict,values,times,sweep_dict)
        change_state(all_widgets)


def on_constant_button_clicked(b):
    with output:
        clear_output(wait = True)
        change_state(all_widgets)
        check_values(sweep_dict)

        times,values = constant_meas(cons_dict)
        plot_constant(values)
        save_constant(folder,sample_dict,values,times,cons_dict)
        change_state(all_widgets)

#new_folder clicked
def on_new_folder_button_clicked(b):
    global folder
    with output:
        change_state(all_widgets) #just to be sure        
        folder = choose_folder()#choose new folder
        change_state(all_widgets) #just to be sure  


#link buttons to widgets
sweep_button.on_click(on_sweep_button_clicked)
cons_button.on_click(on_constant_button_clicked)
sample_dict["button"].on_click(on_new_folder_button_clicked)
        
    
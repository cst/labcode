import pyvisa

class HP4155a(object):
    
    def __init__(self,adress):
        self.adress = adress
        self.rm = pyvisa.ResourceManager()
        self.inst = self.rm.open_resource(adress)
        self.inst.timeout = None
   #----------------------------------------------------General functions-------------------------------------------------------------------------- 
    def idn(self):
        return self.inst.query("*IDN?")
        
    def __del__(self):
        self.rm.close()
    
    def reset(self):
        self.inst.write("*RST")
#------------------------------------------------------stress functions--------------------------------------------------------------------
    #smu mode
    def smu_mode(self,smu_number,mode):
        command = f":PAGE:STR:SMU{smu_number}:MODE {mode}"
        self.inst.write(command)
    
    
    #smu constant value for stress measurement
    def smu_value(self,smu_number,value):
        command =f":PAGE:STR:SET:CONS:SMU{smu_number} {value}"
        self.inst.write(command)
    
    #set the stess time in seconds
    def stress_time(self,time):
        command = f":PAGE:STR:SET:DUR {time}"
        self.inst.write(command)
        
    #start stress operation
    def start_stress(self):
        #inst.write(":PAGE:SCONtrol:STRess[:STARt]")
        self.inst.write(":PAGE:SCON:STR")
        #inst.write("*TRG")
        #self.inst.query('*OPC?')
    
    #get data from HP4155a
    def get_data(self):
        self.inst.write(":FORM REAL")
        data = self.inst.query(":HCOPy:ITEM:ALL:DATA?")
        return data

    def sync(self,smu_number,s):
        if s == 0:
            mode = "NSYN"
        else:
            mode="SYNC"
        command = f":PAGE:STR:SMU{smu_number}:FUNC {mode}"
        self.inst.write(command)

    def single_measurement(self):
        self.inst.write(":PAGE:SCON:SING")
    
    #go to stress page 
    def stress_page(self):
        self.inst.write(":PAGE:STR")
    
    def error(self):
        return self.inst.query(":SYST:ERR?")

    def operation_completed(self):
        text = self.inst.query('*OPC?')
        text = text.replace('\n','')
        finished = bool(text)
        return finished
    
    def show_variables(self):
        return self.inst.query(":DATA:CAT?") 
    
    def comp_stress(self,smu_number,value):
        command = f":PAGE:STR:SET:CONS:SMU{smu_number}:COMP {value}"
        self.inst.write(command)
    
    def str_smu_name(self,smu_number,name):
        command=f":PAGE:STR:SMU{smu_number}:NAME '{name}'"
        self.inst.write(command)

    def copy_current_page(self):
        command=":HCOP:DATA?"
        data=self.inst.query(command)
        return data

     #-----------------------------------------------------------sweep functions---------------------------------------------------------------
    def smu_disable_sweep(self,number):
        command= f":PAGE:CHAN:SMU{number}:DIS"
        self.inst.write(command)
    
   #The following function is for both sampling and sweep
    def smu_mode_meas(self,number,mode):
        command=f":PAGE:CHAN:SMU{number}:MODE {mode}"
        self.inst.write(command)
    
    def smu_function_sweep(self,number,function):
        command=f":PAGE:CHAN:SMU{number}:FUNC {function}"
        self.inst.write(command)

    def start_value_sweep(self,value):
        command=f":PAGE:MEAS:VAR1:START {value}"
        self.inst.write(command)
    
    def step_sweep(self,value):
        command=f":PAGE:MEAS:VAR1:STEP {value}"
        self.inst.write(command)
        
    def stop_value_sweep(self,value):
        command=f":PAGE:MEAS:VAR1:STOP {value}"
        self.inst.write(command)

    def var1_mode(self,mode):
        command = f":PAGE:MEAS:VAR1:MODE {mode}"
        self.inst.write(command)
        
    def cons_smu_value(self,smu_number,value):
        command =f"PAGE:MEAS:CONS:SMU{smu_number} {value}"
        self.inst.write(command)
        
        ''' smu1 is constant and common, SMU3 is controlled through the Var1(v) which has a start value, a stop value, and a step
        We don't need SMU2 and SMU4
        
        :PAGE:CHANnels[:CDEFinition]:SMU<n>:FUNCtion this command is used to define the variable of SMU3
        :PAGE:CHANnels[:CDEFinition]:SMU<n>:MODE
        This command sets the output MODE of SMU<n>. This command also has a query
        form. It is different that the one we used for the stress setup. 
        :PAGE:CHANnels[:CDEFinition]:SMU<n>:DISable
        Important! we dont set a value for smu1.'''

    #----------------------------------------------------------------------sampling measure functions part2 first goal----------------------------

    ''' some instructions
    we need linear log10, log25 and thinned out (one function also log50 can be selected) SCPI COMMAND: :PAGE:MEASure:SAMPling:MODE
    Delay time(Hold time in manual) is to be seleceted :PAGE:MEASure:SAMPling:HTIMe
    Minimum initial interval :PAGE:MEASure:SAMPling:IINTerval
    number of points: :PAGE:MEASure:SAMPling:POINts
    integration time: :PAGE:MEASure:MSETup:ITIMe[:MODE]
    '''
    def sampling_mode(self,mode):
        command = f"PAGE:MEAS:SAMP:MODE {mode}"
        self.inst.write(command)
    
    def delay_time(self,time):
        command = f":PAGE:MEAS:SAMP:HTIM {time}"
        self.inst.write(command)

    def initial_interval(self,interval):
        command = f":PAGE:MEAS:SAMP:IINT {interval}"
        self.inst.write(command)

    def number_of_points(self,number):
        command = f":PAGE:MEAS:SAMP:POIN {number}"
        self.inst.write(command)

    #integration time is SHOR,MED,LONG   
    def integration_time(self,time):
        command=f":PAGE:MEAS:MSET:ITIM {time}"
        self.inst.write(command)

    ''' The smus need to be set to other values as seen in the labview script we can define the following functions
    the vgs is v3 and vds is v2 (constant values) we need current-time diagramm(id,ig) v3=2V, v2=10V
     then we have to save the results in a file'''

    def constant_smu_sampling(self,number,value):
        command =f":PAGE:MEAS:SAMP:CONS:SMU{number} {value}"
        self.inst.write(command)
    
    def measurement_mode(self,mode):
        command =f":PAGE:CHAN:MODE {mode}"
        self.inst.write(command)
    
    def constant_smu_comp(self, smu_number,value):
        command=f":PAGE:MEAS:SAMP:CONS:SMU{smu_number}:COMP {value}"
        self.inst.write(command)
        
    #this is a method that returns data from a variable and converts them to a list of real numbers
    def return_data(self, variable):
        
        #send command to instrument returns a string of comma seperated values
        command = f":DATA? '{variable}'" 
        data = self.inst.query(command)
        
        # separate the string to a list of strings
        values = data.replace("\n",",").split(",")
        values.pop()
        
        #convert the string to float numbers 
        for i in range(len(values)):
            values[i] = float(values[i])
        
        return values

    def filter_status(self, status):
        command=f":PAGE:MEASure:SAMPling:FILT {status}"
        self.inst.write(command)

    #for memristor we need as a sampling completiton the Total sampling time

    #only for linear and thinned out 
    def total_sampling_time(self,period):
        command = f":PAGE:MEAS:SAMP:PER {period}"
        self.inst.write(command)
    
    #set auto sampling time
    def auto_sampling_time(self,status):
        command =f":PAGE:MEAS:SAMP:PER:AUTO {status}"
        self.inst.write(command)

    #------------------------------------------ these are commands for the compliance-----------------------------------------------------------
    
    #set the power compliance for VAR1 and VAR2
    def pcomp(self,variable,value):
        command = f":PAGE:MEAS:{variable}:PCOM {value}"
        self.inst.write(command)
        
    #set normal compliance for VAR1 and VAR2
    def comp(self,variable,value):
        """ """
        command = f":PAGE:MEAS:{variable}:COMP {value}"
        self.inst.write(command)
    
    # constant voltage compiance of an smu(only use if the smu is constant and the mode is not COMMON)
    
    def const_comp(self,smu_number,value):
        command = f":PAGE:MEAS:CONS:SMU{smu_number}:COMP {value}"
        self.inst.write(command)

    #this command disables the VSU1 and VSU2
    def disable_vsu(self, vsu_number):
        command = f":PAGE:CHAN:VSU{vsu_number}:DIS"
        self.inst.write(command)

    def disable_vmu(self,vmu_number):
        command = f":PAGE:CHAN:VMU{vmu_number}:DIS"
        self.inst.write(command)
        
    #this command is for defining a new user function
    def user_function(self,name,unit,expression):
        command = f":PAGE:CHAN:UFUN:DEF '{name}','{unit}','{expression}'"
        self.inst.write(command)

    #this command is for displaying the correct variables
    def display_variable(self,axis,variable):
        command=f":PAGE:DISP:GRAP:{axis}:NAME '{variable}'"
        self.inst.write(command)
    
    #linear logarithmic scale for axis
    def axis_scale(self,axis,scale):
        command = f":PAGE:DISP:GRAP:{axis}:SCAL {scale}"
        self.inst.write(command)
    
    def display_variable_min_max(self,axis,extremum,value):
        command = f":PAGE:DISP:GRAP:{axis}:{extremum} {value}"
        self.inst.write(command)
    
    def autoscaling(self):
        self.inst.write(":PAGE:GLIS:SCAL:AUTO ONCE")

    def append_measurement(self):
        self.inst.write(":PAGE:SCON:APP")
    
    def stop_measurement(self):
        self.inst.write(":PAGE:SCON:STOP")
        
    #------------------------functions for adu VAR2(only sweep)-------------------------------------------------------
    def var2_start(self,value):
        command=f":PAGE:MEAS:VAR2:START {value}"
        self.inst.write(command)
    
    def var2_step(self,value):
        command=f":PAGE:MEAS:VAR2:STEP {value}"
        self.inst.write(command)
    
    def var2_points(self,points):
        command = f":PAGE:MEAS:VAR2:POINTS {points}"
        self.inst.write(command)
        
    def var2_comp(self,comp):
        command=f":PAGE:MEAS:VAR2:COMP {comp}"
        self.inst.write(command)
    
    #set power compliance
    def var2_pcomp(self,pcomp):
        if pcomp==0:
            command =":PAGE:MEAS:VAR2:PCOM:STATE OFF" #set 0 to disable pcompliance (labview)
        else:
            command = f":PAGE:MEAS:VAR2:PCOM {pcomp}" #set other value to enable pcomp
        self.inst.write(command)
   #--------------------------------------------end of var2 functions-------------------------------------------------- 
    #-----------------------------------------SMU NAMES----------------------------------------------------------------
    def smu_vname(self,smu_number,vname):
        command=f":PAGE:CHAN:SMU{smu_number}:VNAME '{vname}'"
        self.inst.write(command)
    
    def smu_iname(self,smu_number,iname):
        command=f":PAGE:CHAN:SMU{smu_number}:INAME '{iname}'"
        self.inst.write(command)

    #delete all user functions
    def del_ufun(self):
        command = f":PAGE:CHAN:UFUN:DEL:ALL"
        self.inst.write(command)

    # device.inst.write(":PAGE:MEAS:VAR1:MODE SING;SPAC LIN;STAR 1;STOP -7;STEP -3E-2;COMP 1.000000E-2;PCOM 0.000000E+0;") (how to write different commands in the same command)

    #for pulse
    def range_mode(self,smu_number,range):
        command = f":PAGE:MEAS:MSET:SMU{smu_number}:RANG:MODE {range}"
        self.inst.write(command)

    def pulse_base(self,value):
        command = f":PAGE:MEAS:PULS:BASE {value}"
        self.inst.write(command)

    def pulse_period(self,value):
        command = f":PAGE:MEAS:PULS:PER {value}"
        self.inst.write(command)

    def pulse_width(self,value):
        command = f":PAGE:MEAS:PULS:WIDTH {value}"
        self.inst.write(command)


  ''' this is a help python file that uses functions to do the setup of the device which are not useful to be seen in the ctlm 
'''
import module
import matplotlib.pyplot as plt
import pandas as pd
from datetime import datetime
import os 
from sklearn.linear_model import LinearRegression
import sys
import numpy as np
from IPython.display import display, clear_output
import ipywidgets as widget
import configparser
from ipyfilechooser import FileChooser


def setup_tlm(start,stop,step,comp,time,device):
    device.inst.write(":PAGE:MEAS")
    device.inst.write(":PAGE:CHAN:MODE SWEEP") #go to sweep page and prepare sweep measurement

    #disable vmus and vsus
    device.disable_vsu(1)
    device.disable_vsu(2)
    device.disable_vmu(1)
    device.disable_vmu(2)

    #smu1 is constant and common
    device.smu_mode_meas(1,'COMM')
    device.smu_function_sweep(1,'CONS')
    
    #smu2 is constant and I
    device.smu_mode_meas(2,'I')
    device.smu_function_sweep(2,'CONS')
    device.cons_smu_value(2,0)
    
    #smu3 is var1 and I
    device.smu_mode_meas(3,'I')
    device.smu_function_sweep(3,'VAR1')

    #smu4 is constant and I
    device.smu_mode_meas(4,'I')
    device.smu_function_sweep(4,'CONS')
    device.cons_smu_value(4,0)
    
    #select compliance of smu3
    device.comp('VAR1',comp)
    
    #compliance of smu2 and smu4 is 10V
    device.const_comp(2,10)
    device.const_comp(4,10)

    #define user functions
    device.user_function('I','A','I3')
    device.user_function('V','V','V4-V2')
    device.user_function('R','OHM','DIFF(V,I)')
    device.user_function('VS','V','V3')
        

    #integration time
    device.integration_time(time)
        
    #define start-step-stop
    device.start_value_sweep(start)
    device.step_sweep(step)
    device.stop_value_sweep(stop)

    #display variables
    device.display_variable('X','V')
    device.display_variable('Y1','I')
    device.display_variable('Y2','R')

    ###-----------------------------autoscaling after measurement------------------------------------------------------------------------------
    device.display_variable_min_max('X','MIN',-comp)
    device.display_variable_min_max('X','MAX', comp)
    device.display_variable_min_max('Y1','MIN',start)
    device.display_variable_min_max('Y1','MAX',stop)
    device.display_variable_min_max('Y2','MIN',0)
    device.display_variable_min_max('Y2','MAX',200)

### this function is helpful for exporting csv and txt(which was two loops in the beginnning)
def export(dataframe,filetype,innen,distances,field_name,j,start,stop,step,date):

    if filetype==".csv" or filetype==".txt":

        #check tlm or ctlm
        if innen==0:
            #specify path and file_name
            file_name = field_name+"_TLM_"+str(j+1)+filetype
            location =r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros\results"
            path= os.path.join(location,file_name)
        
            #check if file name exists
            i=1
            while os.path.exists(path):
                file_name = field_name+"_TLM_"+str(j+1)+"_"+str(i)+filetype
                path= os.path.join(location,file_name)
                i=i+1
        else:
            #specify path and file_name
            file_name = field_name+"_CTLM_"+str(j+1)+filetype
            location =r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros\results"
            path= os.path.join(location,file_name)
        
            #check if file name exists
            i=1
            while os.path.exists(path):
                file_name = field_name+"_CTLM_"+str(j+1)+"_"+str(i)+filetype
                path= os.path.join(location,file_name)
                i=i+1
            
        if filetype==".txt":
            title = "measured field:"+field_name+"\nInnen radius:"+str(innen)+"µm \ndistance:"+str(distances[j])+"µm \nI:"+str(start)+"A to "+str(stop)+"A with step:"+str(step)+"\nDate:"+date+"\n"
        
            f=open(path, 'a')
            f.write(title)
            dataframe_string = dataframe.to_string()
            f.write(dataframe_string)
            f.close()

        else:#csv
            dataframe.to_csv(path)
        
    else:
        print("Not supported filetype!")
##################################################################functions for buttons####
def upload_file():
    uploader=widgets.FileUpload(
        accept='.ini',  # Accepted file extension
        multiple=False,  # True to accept multiple files upload else False
        description='Upload ini file'
    )
    display(uploader)
    try:
        uploaded_file = uploader.value[0]
        return uploaded_file.name
    except IndexError:
        return "default.ini"

def read_ini(file_name):
    config=configparser.ConfigParser()
    config.read(file_name)
    
    #get all parameters
    start=config.getfloat('parameters','start')
    stop=config.getfloat('parameters','stop')
    step=config.getfloat('parameters','step')
    comp=config.getfloat('parameters','comp')
    time=config.get('parameters','time')

    distances = config.get('information','distances')
    distances = distances.split(',')
    distances= [int(i) for i in distances]
    distances=tuple(distances)
    print(distances)

    innen = config.getfloat('information','innen')
    name= config.get('information','name')
    return innen,distances,name,start,stop,step,comp,time
   
    

    
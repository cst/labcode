#prepare full measurement
def ctlm(field_name ='M00',start=-50*10**(-3),stop=50*10**(-3),step=10**(-3),comp=10,distances=(5,10,15,25,45),time='MED',innen=0):
    
    #connect to the device
    device = module.HP4155a('GPIB0::17::INSTR')
    date = str(datetime.today().replace(microsecond=0))
    
    #initilize figure
    plt.figure()
    plt.xlabel('Voltage(V)')
    plt.ylabel('Current(A)')
    plt.title("CTLM plot")
    
    #lists for appending all data values
    ctlm_voltage = []
    ctlm_current = []
    ctlm_resistance = []
    #execute five measurements
    for j in range(len(distances)):
        
        #setup
        device.reset()
        device.inst.write(":PAGE:MEAS")
        device.inst.write(":PAGE:CHAN:MODE SWEEP") #go to sweep page and prepare sweep measurement

        #vsus and vmus are disabled
        device.disable_vsu(1)
        device.disable_vsu(2)
        device.disable_vmu(1)
        device.disable_vmu(2)
        #smu1 is constant and common
        device.smu_mode_meas(1,'COMM')
        device.smu_function_sweep(1,'CONS')
    
        #smu2 is constant and I
        device.smu_mode_meas(2,'I')
        device.smu_function_sweep(2,'CONS')
        device.cons_smu_value(2,0)
    
        #smu3 is var1 and I
        device.smu_mode_meas(3,'I')
        device.smu_function_sweep(3,'VAR1')
    
        #smu4 is constant and I
        device.smu_mode_meas(4,'I')
        device.smu_function_sweep(4,'CONS')
        device.cons_smu_value(4,0)
    
        #select compliance of smu3
        device.comp('VAR1',comp)
    
        #compliance of smu2 and smu4 is 10V
        device.const_comp(2,10)
        device.const_comp(4,10)
    
        # smu1 is common and compliance is automatically set to maximum
        
        #define user functions 
        device.user_function('I','A','I3')
        print(device.error())
        device.user_function('V','V','V4-V2')
        print(device.error())
        device.user_function('R','OHM','DIFF(V,I)')
        print(device.error())
        
        #integration time
        device.integration_time(time)
    
        #define start-step-stop
        device.start_value_sweep(start)
        device.step_sweep(step)
        device.stop_value_sweep(stop)
    
        #start measurement
        device.single_measurement()
        while device.operation_completed() == False:
            pass
    
    
        voltage_values = device.return_data('V3')
        current_values = device.return_data('I3')
        voltage = device.return_data('V') 
        print(voltage_values)
        print(current_values)
        
        ctlm_voltage.append(voltage_values)
        ctlm_current.append(current_values)
        
        #plot results of the single measurement
        #plt.plot(voltage_values,current_values,label=f"distance={distances[j]}")
        plt.plot(voltage_values,current_values,label='ausgangvoltage')
        plt.legend()
        plt.show()
        plt.figure()
        plt.plot(voltage,current_values,label='Eingangsvoltage')
        plt.legend()
        plt.show()
        
        #save measurement as txt file
        #add title to the results
        header = ['Voltage(V)', 'Current(A)','Resistance(Ohm)']

        data = {header[0]:voltage_values,header[1]:current_values}#,header[2]:resisance_values}
        df = pd.DataFrame(data)
        print(df)
        
        file_name = field_name+"_CTLM_"+str(j+1)+".txt"
        path =r"\\fileserver.cst.rwth-aachen.de\public\Datentransfer\Asonitis, Alexandros"
        directory = os.path.join(path,file_name)
        #export DataFrame to text file (keep header row and index column)
        f=open(directory, 'a')
        f.write('title\n')
        df_string = df.to_string()
        f.write(df_string)
        
        #plot diagramm
        #plt.legend()
        #plt.show()

        #wait for confirmation from user after a measurement is done 
        while True:      
            answer = input('please press enter to continue with the next measurement or finish after the last measurement!')
            if answer == "":
                break

    #close the connection and plot all the diagramms 
    del device
    
def tlm_version3():
    #connect to the device
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset()
    date = str(datetime.today().replace(microsecond=0))


    #setup device
    setup_tlm(start,stop,step,comp,time,device)

    #initialize figure
    fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
    if innen==0:
        fig.suptitle('TLM plot')
    else:
        fig.suptitle('CTLM plot')
    ax1.set_title('I(V)')
    ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
    ax2.set_title('R(V)')
    ax2.set(xlabel='Voltage(V)',ylabel='Resistance(Ohm)')        

    j=0
    while j<len(distances):
        #start measurement
        device.single_measurement()
        while device.operation_completed() == False:
                pass
        
        device.autoscaling()
        #return data from the device
        
        V=device.return_data('V')
        I=device.return_data('I')
        R=device.return_data('R')
        
        # now we have to remove resistance values that R=inf(nan) that means that the current is zero
        for i in range(len(R)):
            if R[i]>10**6:
                R[i]=float('NAN')
        
        
        #create buttons
        
        # plot the results
        curve_1 = ax1.plot(V,I,label=f"distance={distances[j]}")
        curve_2 = ax2.plot(V,R,label=f"distance={distances[j]}")
        ax1.legend(loc='best')
        ax2.legend(loc="best")
        clear_output(wait=True)
        fig.tight_layout()
        display(fig)
        
        #export data frame to csv(for evaluation) and txt 
        header = ['Voltage(V)', 'Current(A)','Resistance(Ohm)']

        data = {header[0]:V,header[1]:I,header[2]:R}
        df = pd.DataFrame(data)
        print(df)

        #export to csv and txt
        export(df,".txt",innen,distances,field_name,j,start,stop,step,date)
        export(df,".csv",innen,distances,field_name,j,start,stop,step,date)

        
    del device

def ctlm_version1():
    #default parameters
    plt.ion()
    innen=0
    distances=(5,10,15,25,45)
    field_name ='M00'
    start=-50*10**(-3)
    stop=50*10**(-3)
    step=10**(-3)
    comp=10
    time='MED'
    j=-1
    
    #initialze variables for removing the plots later
    curve_1=0
    curve_2=0
    V=0
    I=0
    R=0
    
    #default files and paths
    ini_file_path=r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros"
    ini_file_name=r"default.ini"
    ini_file=os.path.join(ini_file_path,ini_file_name)
    
    save_file_path = r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros\results"
    save_file_path = Path(save_file_path)
    
    #connect to the device
    device = module.HP4155a('GPIB0::17::INSTR')
    date = str(datetime.today().replace(microsecond=0))
    
    #initialize figure
    fig=0
    ax1=0
    ax2=0
    
    ### buttons####
    
    #filechooser and path chooser
    fc = FileChooser(select_desc='load .ini')
    fc.default_path = r"\\FILESERVER\public"
    fc.filter_pattern = '*.ini'
    display(fc)


    pc = FileChooser(select_desc="save path")
    pc.default_path = r"\\FILESERVER\public"
    pc.show_only_dirs = True
    display(pc)
    
    
    start_button = widgets.Button(description="Start NEW!")
    start_output = widgets.Output()
    
    display(start_button, start_output)
    
    def on_start_button_clicked(b):
        nonlocal innen,distances,field_name,start,stop,step,comp,time,j,curve_1,curve_2,V,I,R,ini_file,save_file_path,device,date,fig,ax1,ax2
        with start_output:
            clear_output()
            j=0
            #ensure that choose ini file has been clicked
            if  fc.selected != None:
                ini_file = fc.selected
                
            print('get parametrers from:'+str(ini_file))
            
            #configparser for decoding
            
            try:
                config=configparser.ConfigParser()
                config.read(ini_file)
    
                #get all parameters
                start=config.getfloat('parameters','start')
                stop=config.getfloat('parameters','stop')
                step=config.getfloat('parameters','step')
                comp=config.getfloat('parameters','comp')
                time=config.get('parameters','time')

                distances = config.get('information','distances')
                distances = distances.split(',')
                distances= [int(i) for i in distances]
                distances=tuple(distances)

                innen = config.getfloat('information','innen')
                field_name= config.get('information','name')
                
                 #initialize figure
                plt.figure().clear()
                fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
                if innen==0:
                    fig.suptitle('TLM plot')
                else:
                    fig.suptitle('CTLM plot')
                ax1.set_title('I(V)')
                ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
                ax2.set_title('R(V)')
                ax2.set(xlabel='Voltage(V)',ylabel='Resistance(Ohm)')
            
                
                device.reset()
                device.inst.write(":PAGE:MEAS")
                device.inst.write(":PAGE:CHAN:MODE SWEEP") #go to sweep page and prepare sweep measurement

                #disable vmus and vsus
                device.disable_vsu(1)
                device.disable_vsu(2)
                device.disable_vmu(1)
                device.disable_vmu(2)

                #smu1 is constant and common
                device.smu_mode_meas(1,'COMM')
                device.smu_function_sweep(1,'CONS')
    
                #smu2 is constant and I
                device.smu_mode_meas(2,'I')
                device.smu_function_sweep(2,'CONS')
                device.cons_smu_value(2,0)
    
                #smu3 is var1 and I
                device.smu_mode_meas(3,'I')
                device.smu_function_sweep(3,'VAR1')
    
                #smu4 is constant and I
                device.smu_mode_meas(4,'I')
                device.smu_function_sweep(4,'CONS')
                device.cons_smu_value(4,0)
    
                #select compliance of smu3
                device.comp('VAR1',comp)
    
                #compliance of smu2 and smu4 is 10V
                device.const_comp(2,10)
                device.const_comp(4,10)

                #define user functions
                device.user_function('I','A','I3')
                device.user_function('V','V','V4-V2')
                device.user_function('R','OHM','DIFF(V,I)')
                device.user_function('VS','V','V3')
        

                #integration time
                device.integration_time(time)
    
                #define start-step-stop
                device.start_value_sweep(start)
                device.step_sweep(step)
                device.stop_value_sweep(stop)

                #display variables
                device.display_variable('X','V')
                device.display_variable('Y1','I')
                device.display_variable('Y2','R')

                device.display_variable_min_max('X','MIN',-comp)
                device.display_variable_min_max('X','MAX', comp)
                device.display_variable_min_max('Y1','MIN',start)
                device.display_variable_min_max('Y1','MAX',stop)
                device.display_variable_min_max('Y2','MIN',0)
                device.display_variable_min_max('Y2','MAX',200)
                
                #start measurement
                device.single_measurement()
                while device.operation_completed() == False:
                    pass
        
                device.autoscaling()
                #return data from the device
        
                V=device.return_data('V')
                I=device.return_data('I')
                R=device.return_data('R')
        
                # now we have to remove resistance values that R=inf(nan) that means that the current is zero
                for i in range(len(R)):
                    if abs(R[i])>10**6:
                        R[i]=float('NAN')
                
                # plot the results
                curve_1=ax1.plot(V,I,label=f"distance={distances[j]}")
                curve_2=ax2.plot(V,R,label=f"distance={distances[j]}")
                ax1.legend(loc='best')
                ax2.legend(loc="best")
                fig.tight_layout()
                display(fig)
            except:
                print("please ensure that the ini file is correct with no invalid parameters and do not leave any spaces!")
        
    start_button.on_click(on_start_button_clicked)  
    
    repeat_button = widgets.Button(description="repeat last")
    repeat_output = widgets.Output()
    
    display(repeat_button, repeat_output)
    
    def on_repeat_button_clicked(b):
        nonlocal innen,distances,field_name,start,stop,step,comp,time,j,curve_1,curve_2,V,I,R,ini_file,save_file_path,device,date,fig,ax1,ax2
        with repeat_output:
            clear_output()
            if j==-1:
                print("cannot repeat before first measurement!")
            else:
                #remove last measurement
                line = curve_2.pop(0)
                line.remove()
                line= curve_1.pop(0)
                line.remove()
                #start measurement
                device.single_measurement()
                while device.operation_completed() == False:
                    pass
        
                device.autoscaling()
                #return data from the device
        
                V=device.return_data('V')
                I=device.return_data('I')
                R=device.return_data('R')
        
                # now we have to remove resistance values that R=inf(nan) that means that the current is zero
                for i in range(len(R)):
                    if abs(R[i])>10**6:
                        R[i]=float('NAN')
                
                # plot the results
                curve_1=ax1.plot(V,I,label=f"distance={distances[j]}")
                curve_2=ax2.plot(V,R,label=f"distance={distances[j]}")
                ax1.legend(loc='best')
                ax2.legend(loc="best")
                fig.tight_layout()
                display(fig)
        
    
    repeat_button.on_click(on_repeat_button_clicked)
    
    continue_button = widgets.Button(description="continue")
    continue_output = widgets.Output()
    
    display(continue_button, continue_output)
    
    def on_continue_button_clicked(b):
        nonlocal innen,distances,field_name,start,stop,step,comp,time,j,curve_1,curve_2,V,I,R,ini_file,save_file_path,device,date,fig,ax1,ax2
        with continue_output:
            clear_output()
            if j==-1:
                print('first click start New!')
        
            elif j>=0 or j<len(distances):
                #save previous measurement
                #ensure that pathchooser has been clicked
                if  pc.selected != None:
                    save_file_path = pc.selected

                print("save location:"+str(save_file_path))
            
                #export data frame to csv(for evaluation) and txt 
                header = ['Voltage(V)', 'Current(A)','Resistance(Ohm)']

                data = {header[0]:V,header[1]:I,header[2]:R}
                df = pd.DataFrame(data)
                #print(df)
        
                #export to txt 
                #check tlm or ctlm
                if innen==0:
                    #specify path and file_name
                    file_name = field_name+"_TLM_"+str(j+1)+".txt"
                    path= os.path.join(save_file_path,file_name)
        
                    #check if file name exists
                    i=1
                    while os.path.exists(path):
                        file_name = field_name+"_TLM_"+str(j+1)+"_"+str(i)+".txt"
                        path= os.path.join(save_file_path,file_name)
                        i=i+1
                else:
                    #specify path and file_name
                    file_name = field_name+"_CTLM_"+str(j+1)+".txt"
                    path= os.path.join(save_file_path,file_name)
        
                    #check if file name exists
                    i=1
                    while os.path.exists(path):
                        file_name = field_name+"_CTLM_"+str(j+1)+"_"+str(i)+".txt"
                        path= os.path.join(location,file_name)
                        i=i+1
            
                title = "measured field:"+field_name+"\nInnen radius:"+str(innen)+"µm \ndistance:"+str(distances[j])+"µm \nI:"+str(start)+"A to "+str(stop)+"A with step:"+str(step)+"\nDate:"+date+"\n"
        
                f=open(path, 'a')
                f.write(title)
                df_string = df.to_string()
                f.write(df_string)
                f.close()
        
                #export to csv for evaluataion
        
                if innen==0:
                #specify path and file_name
                    file_name = field_name+"_TLM_"+str(j+1)+".csv"
                    path= os.path.join(save_file_path,file_name)
        
                    #check if file name exists
                    i=1
                    while os.path.exists(path):
                        file_name = field_name+"_TLM_"+str(j+1)+"_"+str(i)+".csv"
                        path= os.path.join(save_file_path,file_name)
                        i=i+1
                else:
                    #specify path and file_name
                    file_name = field_name+"_CTLM_"+str(j+1)+".csv"
                    path= os.path.join(save_file_path,file_name)
        
                    #check if file name exists
                    i=1
                    while os.path.exists(path):
                        file_name = field_name+"_CTLM_"+str(j+1)+"_"+str(i)+".csv"
                        path= os.path.join(save_file_path,file_name)
                        i=i+1
        
                df.to_csv(path)
            
                if j == len(distances)-1:
                    print("programm finished!")
                #execute next!
                else:

                    j=j+1
                    #start measurement
                    device.single_measurement()
                    while device.operation_completed() == False:
                        pass
        
                    device.autoscaling()
                    #return data from the device
        
                    V=device.return_data('V')
                    I=device.return_data('I')
                    R=device.return_data('R')
        
                    # now we have to remove resistance values that R=inf(nan) that means that the current is zero
                    for i in range(len(R)):
                        if abs(R[i])>10**6:
                            R[i]=float('NAN')
                
                    # plot the results
                    curve_1=ax1.plot(V,I,label=f"distance={distances[j]}")
                    curve_2=ax2.plot(V,R,label=f"distance={distances[j]}")
                    ax1.legend(loc='best')
                    ax2.legend(loc="best")
                    fig.tight_layout()
                    display(fig)
            else:
                print('programm finished!')
        
    continue_button.on_click(on_continue_button_clicked)

#tlm/ctlm final part
def tlm_final(innen=0,distances=(5,10,15,25,45),field_name ='M00',start=-50*10**(-3),stop=50*10**(-3),step=10**(-3),comp=10,time='MED'):
    
    #connect to the device
    device = module.HP4155a('GPIB0::17::INSTR')
    date = str(datetime.today().replace(microsecond=0))
    
    #initialize figure
    fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
    if innen==0:
        fig.suptitle('TLM plot')
    else:
        fig.suptitle('CTLM plot')
    ax1.set_title('I(V)')
    ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
    ax2.set_title('R(V)')
    ax2.set(xlabel='Voltage(V)',ylabel='Resistance(Ohm)')          
            
    #repeat five times
    for j in range(len(distances)):
        #setup
        device.reset()
        device.inst.write(":PAGE:MEAS")
        device.inst.write(":PAGE:CHAN:MODE SWEEP") #go to sweep page and prepare sweep measurement

        #disable vmus and vsus
        device.disable_vsu(1)
        device.disable_vsu(2)
        device.disable_vmu(1)
        device.disable_vmu(2)

        #smu1 is constant and common
        device.smu_mode_meas(1,'COMM')
        device.smu_function_sweep(1,'CONS')
    
        #smu2 is constant and I
        device.smu_mode_meas(2,'I')
        device.smu_function_sweep(2,'CONS')
        device.cons_smu_value(2,0)
    
        #smu3 is var1 and I
        device.smu_mode_meas(3,'I')
        device.smu_function_sweep(3,'VAR1')
    
        #smu4 is constant and I
        device.smu_mode_meas(4,'I')
        device.smu_function_sweep(4,'CONS')
        device.cons_smu_value(4,0)
    
        #select compliance of smu3
        device.comp('VAR1',comp)
    
        #compliance of smu2 and smu4 is 10V
        device.const_comp(2,10)
        device.const_comp(4,10)

        #define user functions
        device.user_function('I','A','I3')
        device.user_function('V','V','V4-V2')
        device.user_function('R','OHM','DIFF(V,I)')
        device.user_function('VS','V','V3')
        

        #integration time
        device.integration_time(time)
        
        #define start-step-stop
        device.start_value_sweep(start)
        device.step_sweep(step)
        device.stop_value_sweep(stop)

        #display variables
        device.display_variable('X','V')
        device.display_variable('Y1','I')
        device.display_variable('Y2','R')

        ###-----------------------------autoscaling after measurement------------------------------------------------------------------------------
        device.display_variable_min_max('X','MIN',-comp)
        device.display_variable_min_max('X','MAX', comp)
        device.display_variable_min_max('Y1','MIN',start)
        device.display_variable_min_max('Y1','MAX',stop)
        device.display_variable_min_max('Y2','MIN',0)
        device.display_variable_min_max('Y2','MAX',200)
       
        #start measurement
        device.single_measurement()
        while device.operation_completed() == False:
                pass
        
        device.autoscaling()
        #return data from the device
        
        V=device.return_data('V')
        I=device.return_data('I')
        R=device.return_data('R')
        
        # now we have to remove resistance values that R=inf(nan) that means that the current is zero
        for i in range(len(R)):
            if R[i]>10**6:
                R[i]=float('NAN')
                
        # plot the results
        ax1.plot(V,I,label=f"distance={distances[j]}")
        ax2.plot(V,R,label=f"distance={distances[j]}")
        ax1.legend(loc='best')
        ax2.legend(loc="best")
        clear_output(wait=True)
        fig.tight_layout()
        display(fig)
        
        #export data frame to csv(for evaluation) and txt 
        header = ['Voltage(V)', 'Current(A)','Resistance(Ohm)']

        data = {header[0]:V,header[1]:I,header[2]:R}
        df = pd.DataFrame(data)
        print(df)
        
        #export to txt 
        #check tlm or ctlm
        if(innen==0):
            #specify path and file_name
            file_name = field_name+"_TLM_"+str(j+1)+".txt"
            location =r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros\results"
            path= os.path.join(location,file_name)
        
            #check if file name exists
            i=1
            while os.path.exists(path):
                file_name = field_name+"_TLM_"+str(j+1)+"_"+str(i)+".txt"
                path= os.path.join(location,file_name)
                i=i+1
        else:
            #specify path and file_name
            file_name = field_name+"_CTLM_"+str(j+1)+".txt"
            location =r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros\results"
            path= os.path.join(location,file_name)
        
            #check if file name exists
            i=1
            while os.path.exists(path):
                file_name = field_name+"_CTLM_"+str(j+1)+"_"+str(i)+".txt"
                path= os.path.join(location,file_name)
                i=i+1
            
        title = "measured field:"+field_name+"\nInnen radius:"+str(innen)+"µm \ndistance:"+str(distances[j])+"µm \nI:"+str(start)+"A to "+str(stop)+"A with step:"+str(step)+"\nDate:"+date+"\n"
        
        f=open(path, 'a')
        f.write(title)
        df_string = df.to_string()
        f.write(df_string)
        f.close()
        
        #export to csv for evaluataion
        
        if innen==0:
            #specify path and file_name
            file_name = field_name+"_TLM_"+str(j+1)+".csv"
            location =r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros\results"
            path= os.path.join(location,file_name)
        
            #check if file name exists
            i=1
            while os.path.exists(path):
                file_name = field_name+"_TLM_"+str(j+1)+"_"+str(i)+".csv"
                path= os.path.join(location,file_name)
                i=i+1
        else:
            #specify path and file_name
            file_name = field_name+"_CTLM_"+str(j+1)+".csv"
            location =r"\\FILESERVER\public\Datentransfer\Asonitis, Alexandros\results"
            path= os.path.join(location,file_name)
        
            #check if file name exists
            i=1
            while os.path.exists(path):
                file_name = field_name+"_CTLM_"+str(j+1)+"_"+str(i)+".csv"
                path= os.path.join(location,file_name)
                i=i+1
        
        df.to_csv(path)
        
        # give user confirmation to do the next measurement
        
        while True:
            answer=input("Press enter to continue or anything else to stop the programm:")
            if answer=="":
                break
            else:
                sys.exit()

def tlm_version2(innen=0,distances=(5,10,15,25,45),field_name ='M00',start=-50*10**(-3),stop=50*10**(-3),step=10**(-3),comp=10,time='MED'):
    #connect to the device
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset()
    date = str(datetime.today().replace(microsecond=0))

    #setup device
    setup_tlm(start,stop,step,comp,time,device)

    #initialize figure
    fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
    if innen==0:
        fig.suptitle('TLM plot')
    else:
        fig.suptitle('CTLM plot')
    ax1.set_title('I(V)')
    ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
    ax2.set_title('R(V)')
    ax2.set(xlabel='Voltage(V)',ylabel='Resistance(Ohm)')        

    j=0
    while j<len(distances):
        #start measurement
        device.single_measurement()
        while device.operation_completed() == False:
                pass
        
        device.autoscaling()
        #return data from the device
        
        V=device.return_data('V')
        I=device.return_data('I')
        R=device.return_data('R')
        
        # now we have to remove resistance values that R=inf(nan) that means that the current is zero
        for i in range(len(R)):
            if R[i]>10**6:
                R[i]=float('NAN')
                
        # plot the results
        ax1.plot(V,I,label=f"distance={distances[j]}")
        ax2.plot(V,R,label=f"distance={distances[j]}")
        ax1.legend(loc='best')
        ax2.legend(loc="best")
        clear_output(wait=True)
        fig.tight_layout()
        display(fig)
        
        #export data frame to csv(for evaluation) and txt 
        header = ['Voltage(V)', 'Current(A)','Resistance(Ohm)']

        data = {header[0]:V,header[1]:I,header[2]:R}
        df = pd.DataFrame(data)
        print(df)

        #export to csv and txt
        export(df,".txt",innen,distances,field_name,j,start,stop,step,date)
        export(df,".csv",innen,distances,field_name,j,start,stop,step,date)

        while True:
            answer=input("Press enter to continue or anything else to stop the programm:")
            if answer=="":
                j=j+1
                break
            else:
                sys.exit()
    
    del device
    
def tlm_version3():
    #connect to the device
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset()
    date = str(datetime.today().replace(microsecond=0))


    #setup device
    setup_tlm(start,stop,step,comp,time,device)

    #initialize figure
    fig, (ax1, ax2) = plt.subplots(2,sharex=True,figsize=(8,6)) #the plots share the same x axis 
    if innen==0:
        fig.suptitle('TLM plot')
    else:
        fig.suptitle('CTLM plot')
    ax1.set_title('I(V)')
    ax1.set(xlabel='Voltage(V)',ylabel='Current(A)')
    ax2.set_title('R(V)')
    ax2.set(xlabel='Voltage(V)',ylabel='Resistance(Ohm)')        

    j=0
    while j<len(distances):
        #start measurement
        device.single_measurement()
        while device.operation_completed() == False:
                pass
        
        device.autoscaling()
        #return data from the device
        
        V=device.return_data('V')
        I=device.return_data('I')
        R=device.return_data('R')
        
        # now we have to remove resistance values that R=inf(nan) that means that the current is zero
        for i in range(len(R)):
            if R[i]>10**6:
                R[i]=float('NAN')
        
        
        #create buttons
        
        # plot the results
        curve_1 = ax1.plot(V,I,label=f"distance={distances[j]}")
        curve_2 = ax2.plot(V,R,label=f"distance={distances[j]}")
        ax1.legend(loc='best')
        ax2.legend(loc="best")
        clear_output(wait=True)
        fig.tight_layout()
        display(fig)
        
        #export data frame to csv(for evaluation) and txt 
        header = ['Voltage(V)', 'Current(A)','Resistance(Ohm)']

        data = {header[0]:V,header[1]:I,header[2]:R}
        df = pd.DataFrame(data)
        print(df)

        #export to csv and txt
        export(df,".txt",innen,distances,field_name,j,start,stop,step,date)
        export(df,".csv",innen,distances,field_name,j,start,stop,step,date)        
    del device
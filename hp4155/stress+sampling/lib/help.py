#This file contains help functions with plots,dataframes,widgets...

import matplotlib.pyplot as plt
from IPython.display import display, clear_output
import pandas as pd
import os
from datetime import datetime

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox

from keyboard import add_hotkey,remove_hotkey

def plot_sampling(t,IDmm,IGmm,index):
    plt.figure().clear()
    fig, ax1 = plt.subplots() 

    abs_IGmm = [abs(element) for element in IGmm]
    abs_IDmm = [abs(element) for element in IDmm]
 
    color = 'tab:red'
    ax1.set_xlabel('time(s)') 
    ax1.set_ylabel('IGmm (mA/mm)', color = color)
    ax1.set_yscale('log')#logaritmic scale
    ax1.plot(t, abs_IGmm, color = color) 
    ax1.tick_params(axis ='y', labelcolor = color,which='both') 
 
    # Adding Twin Axes to plot IGmm
    ax2 = ax1.twinx() 
 
    color = 'tab:green'
    ax2.set_ylabel('IDmm (mA/mm)', color = color) 
    ax2.plot(t, abs_IDmm , color = color)
    ax2.set_yscale('log') #logarithmic scale
    ax2.tick_params(axis ='y', labelcolor = color,which='both') 
 
    # Adding title
    plt.title(f'step {index}', fontweight ="bold") 

    # Show plot
    display(fig)

def change_state(widgets_list,disabled):
    for widget in widgets_list:
        if widget in disabled:
            continue
        widget.disabled = not widget.disabled

## uplaad the final file to user specified location
def upload_file(source_file,filename):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    target_file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =filename)

    #check if the file path is correct(.txt)
    while target_file.endswith(".txt") == False:
        #open again filedialog with error message box
        tk.messagebox.showerror(message='invalid filename!')
        target_file = filedialog.asksaveasfilename(defaultextension=".txt", filetypes=[("Text files","*.txt")],title = "save results path",initialfile =filename)

    #Now we upload the results (We don't need try except case because the filedialog ensures that)
    with (open(source_file,'r') as source, open(target_file,'w') as target):
        target.write(source.read())
    os.remove(source_file)

    root.destroy()

def information_box(information):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.lift() #show window above all other applications

    root.attributes("-topmost", True)#window stays above all other applications

    #display meaagebox
    tkinter.messagebox.showinfo(message=information)
    root.destroy()

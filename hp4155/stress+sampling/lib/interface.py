#this is the file that contains only the whole interface of the stress and sampling measurement

import ipywidgets as widgets
from ipywidgets import GridspecLayout,Layout


def interface():
    #some layout and sampling configurations
    style = {'description_width': 'initial'}

    """#create the widgets:(voltage and compliance is the same for stress,sampling)
    voltage=widgets.BoundedFloatText(
        value=1,
        min=-200,
        max=200,
        step=1,
    )

    comp=widgets.BoundedFloatText(
        value=1e-3,
        min=-0.1,
        max=0.1,
        step=0.01,
    )

    t_stress = widgets.BoundedFloatText(
        value = 10,
        min = 0,#free run
        max = 31536000, #1year is the maximum
        step = 1,
    )
    """
    #here are the sampling parameters(Later)


    # Create the grids

    sample_information=widgets.Label("Sample Information",layout=Layout(height='auto', width='auto'))
    sample_information.style.font_weight = 'bold'
    information_grid=GridspecLayout(2,4)

    #first and third column
    #first column

    information_grid[0,0]=widgets.Label("Processing-Nr.",layout=Layout(height='auto', width='auto'))
    information_grid[1,0]=widgets.Label("Sample-Nr.",layout=Layout(height='auto', width='auto'))
    #information_grid[2,0]=widgets.Label("Piece",layout=Layout(height='auto', width='auto'))

    #last column

    information_grid[0,3]=widgets.Label("Field(XYY)",layout=Layout(height='auto', width='auto'))
    information_grid[1,3]=widgets.Label("Transistor(Device)",layout=Layout(height='auto', width='auto'))
    #information_grid[2,3]=widgets.Label("Testlevel",layout=Layout(height='auto', width='auto'))

    #coluns 2 and 3
    for i in range(2):
        for j in range(1,3):
            information_grid[i,j]=widgets.Text(layout=Layout(height='auto', width='auto'))

    display(sample_information)
    display(information_grid)

    print()

    stress_grid = GridspecLayout(9, 5)

    #first line 

    stress_grid[0,0:4]=widgets.Label("Batch Stress Measurement Parameters",style = style)
    stress_grid[0,0:4].style.font_weight='bold'
    stress_grid[0,4]=widgets.BoundedFloatText(description="compliance(A)",layout=Layout(height='auto', width='auto'),style=style,min=-0.1,max=0.1,step=0.01,value=0.1)

    #second line
    stress_grid[1,2]=widgets.Label(f"Vg_stress (V)",layout=Layout(height='auto', width='auto'))
    stress_grid[1,3]=widgets.Label(f"Vd_stress (V)",layout=Layout(height='auto', width='auto'))
    stress_grid[1,4]=widgets.Label(f"t_stress (sec)",layout=Layout(height='auto', width='auto'))

    t_str = 0.1 #no default parameter(We need it only for the next default values!) 
    #third line and below
    for i in range(2,9):
        stress_grid[i,0] = widgets.Label(f"Step {i-1}",layout=Layout(height='auto', width='auto'))
        stress_grid[i,1] = widgets.Checkbox(description="skip",value=False)

        stress_grid[i,2]=widgets.BoundedFloatText(value=-12,min=-100,max=100,step=1,layout=Layout(height='auto', width='auto'))#VGS stress
        stress_grid[i,3]=widgets.BoundedFloatText(value=10,min=-100,max=100,step=1,layout=Layout(height='auto', width='auto'))#VDS stress
        stress_grid[i,4]= widgets.BoundedFloatText(value=t_str,min=0,max=31536000,step=10,layout=Layout(height='auto', width='auto'))#time stress

        #update default value
        t_str=t_str*10

        if i == 2 or i==8:
            for j in range(2,5):
                stress_grid[i,j].value=0
                stress_grid[i,j].disabled=True

    #last column(stress_time) 
    display(stress_grid)

    #now we start with the sampling widgets
    #transient (voltages)
    transient_title=widgets.Label("Transient Measurement Parameters")
    transient_title.style.font_weight = 'bold'
    bias_point=widgets.Label("Bias Point")
    bias_point.style.font_weight = 'bold'
    
    transient_grid=GridspecLayout(2, 4)
    
    #first line
    transient_grid[0,:2]=widgets.Label("VGS",layout =Layout(height='auto', width='auto'))
    transient_grid[0,2:]=widgets.Label('VDS',layout =Layout(height='auto', width='auto'))

    #second line
    transient_grid[1,0]=widgets.BoundedFloatText(value=2,min=-100,max=100,step=1,layout=Layout(height='auto', width='auto'),description="Constant(V)",style=style)
    transient_grid[1,1]=widgets.BoundedFloatText(description="compliance(A)",layout=Layout(height='auto', width='auto'),style=style,min=-0.1,max=0.1,step=0.01,value=0.1)
    transient_grid[1,2]=widgets.BoundedFloatText(value=1,min=-100,max=100,step=1,layout=Layout(height='auto', width='auto'),description="Constant(V)",style=style)
    transient_grid[1,3]=widgets.BoundedFloatText(description="compliance(A)",layout=Layout(height='auto', width='auto'),style=style,min=-0.1,max=0.1,step=0.01,value=0.1)

    print()
    display(transient_title)
    print()
    display(bias_point)
    display(transient_grid)

    #sampling parameters

    #first set the options for sampling mode and integration time
    options_mode=["LINEAR","LOGARITHMIC 10P/DEC","LOGARITHMIC 25P/DEC","LOGARITHMIC 50P/DEC","THINNED OUT"]
    options_integration=["SHORt","MEDium","LONG"]

    sampling_parameters_title=widgets.Label("Sampling Parameters")
    sampling_parameters_title.style.font_weight = 'bold'
    sampling_grid=GridspecLayout(4, 3)
    #first line
    sampling_grid[0,0] = widgets.Label("initial interval(sec)",layout =Layout(height='auto', width='auto'))
    sampling_grid[0,1]= widgets.Label("number of samples",layout =Layout(height='auto', width='auto'))
    sampling_grid[0,2] = widgets.Label("Integration Time",layout =Layout(height='auto', width='auto'))

    #third line
    sampling_grid[2,0] = widgets.Label("hold time(sec)",layout =Layout(height='auto', width='auto'))
    sampling_grid[2,1]= widgets.Label("filter(check for on)",layout =Layout(height='auto', width='auto'))
    sampling_grid[2,2] = widgets.Label("sampling mode",layout =Layout(height='auto', width='auto'))


    #second and fourth line(widgets)
    sampling_grid[1,0]=widgets.BoundedFloatText(layout=Layout(height='auto', width='auto'),min=2e-3,max=65.535,step=1e-3,value=2e-3)#initial interval
    sampling_grid[3,0]=widgets.BoundedFloatText(layout=Layout(height='auto', width='auto'),min=0,max=655.35,step=1,value=0)#hold time
    sampling_grid[1,1]=widgets.BoundedIntText(layout=Layout(height='auto', width='auto'),min=1,max=10001,step=10,value=100)#number of samples
    sampling_grid[3,1]=widgets.Checkbox(layout=Layout(height='auto', width='auto'),indent=False)#filter
    sampling_grid[1,2]=widgets.Dropdown(layout=Layout(height='auto', width='auto'),options=options_integration,value="MEDium")#integration time
    sampling_grid[3,2]=widgets.Dropdown(layout=Layout(height='auto', width='auto'),options=options_mode,value="LINEAR")#sampling mode


    print()
    display(sampling_parameters_title)
    display(sampling_grid)

    return information_grid,stress_grid,transient_grid,sampling_grid
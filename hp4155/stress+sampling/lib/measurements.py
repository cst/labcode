#this is the file of the measurements for the stress and sampling. It contains setup,stress,sampling

#import sys
#sys.path.insert(0, '..') #append parent directory

import module

def setup(device):
    device.reset()
    
    #define user functions
    device.user_function('t','s','@TIME')
    device.user_function('IDmm','mA/mm','1E4*ID')
    device.user_function('IGmm','mA/mm', '1E4*IG')
    device.user_function('ABSIGm','mA/mm','ABS(IGmm)')
    device.user_function('ABSIDm','mA/mm','ABS(IDmm)')

    #disable vmus and vsus
    device.disable_vsu(1)
    device.disable_vsu(2)
    device.disable_vmu(1)
    device.disable_vmu(2)


def sampling(VDS,VGS,VDS_comp,VGS_comp,sampling_mode,number_of_points,integration_time,initial_interval,hold_time,filter_status,device):
    device.measurement_mode('SAMP')
            
    #set the mode of smus for sampling
    device.smu_mode_meas(1,'COMM')
    device.smu_mode_meas(2,'V')
    device.smu_mode_meas(3,'V')
    device.smu_mode_meas(4,'COMM')

    #set the names of the smus
    device.smu_vname(1,'VS1')
    device.smu_iname(1,'IS1')
    device.smu_vname(2,'VDS')
    device.smu_iname(2,'ID')
    device.smu_vname(3,'VGS')
    device.smu_iname(3,'IG')
    device.smu_vname(4,'VS2')
    device.smu_iname(4,'IS2')

    
    #set the constant value of smus and compilance
    device.constant_smu_sampling(2,VDS)
    device.constant_smu_sampling(3,VGS)
    device.constant_smu_comp(2,VDS_comp)
    device.constant_smu_comp(3,VGS_comp)
            
    #set sampling parameters
    device.sampling_mode(sampling_mode)

    #the maximum number of samples for logarithmic mode is 11 decades +1 although it is allowed 10001 samples
    if sampling_mode == "L10" and number_of_points>111:
        device.number_of_points(111)
    elif sampling_mode == "L25" and number_of_points>276:
        device.number_of_points(276)
    elif sampling_mode=="L50" and number_of_points>551:
        device.number_of_points(551)
    else:   
        device.number_of_points(number_of_points)
    device.integration_time(integration_time)
    device.initial_interval(initial_interval)
    device.delay_time(hold_time)
    device.filter_status(filter_status)

    #display variables
    device.display_variable('X','t')
    device.display_variable('Y1','ABSIDm')
    device.display_variable('Y2','ABSIGm')

    #start measurement
    device.single_measurement()
    while device.operation_completed() == False:
        pass
    device.autoscaling()

    time = device.return_data('t')
    #ABSIGm = device.return_data('ABSIGm')
    #ABSIDm = device.return_data('ABSIDm')

    #we need this function for the dataframe at the txt.
    IDmm = device.return_data('IDmm')
    IGmm = device.return_data('IGmm')
    return time,IDmm,IGmm

def stress(VDS,VGS,duration,comp,device):
    device.stress_page() #go to stress page
    #define smus to numbers in mode and values for stress 

    #mode
    device.smu_mode(1,'COMM')
    device.smu_mode(2,'V')
    device.comp_stress(2,comp)
    device.smu_mode(3,'V')
    device.comp_stress(3,comp)
    device.smu_mode(4,'COMM')
    device.sync(4,0)

    #names 
    device.str_smu_name(2,'VD')
    device.str_smu_name(3,'VG')
    #values
    device.smu_value(2,VDS)
    device.smu_value(3,VGS)
    #time
    device.stress_time(duration)

    device.start_stress()
    while device.operation_completed() == False:
        pass
    return
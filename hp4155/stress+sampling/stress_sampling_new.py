#this is the main file of the stress and sampling measurement

import sys
sys.path.insert(0, './lib')
sys.path.insert(0, '..') #append parent directory


from measurements import *
from help import *
from interface import *

information_grid,stress_grid,transient_grid,sampling_grid= interface()

print()
button= widgets.Button(description="Start Measurement",style={'description_width': 'auto'})
output = widgets.Output()
display(button,output)

# use easier conventions for floatboxes

#widgets for the infromation list
processing_number = information_grid[0,1]
sample_series = information_grid[1,1]
field = information_grid[0,2]
transistor = information_grid[1,2]

#stress
comp_str=stress_grid[0,4]

Vgs_str = [stress_grid[i,2] for i in range(2,9)]
Vds_str = [stress_grid[i,3] for i in range(2,9)]
t_str = [stress_grid[i,4] for i in range(2,9)]

skip = [stress_grid[i,1] for i in range(2,9)]

#sampling voltages
Vgs_samp = transient_grid[1,0]
Vgs_samp_comp = transient_grid[1,1]
Vds_samp = transient_grid[1,2]
Vds_samp_comp = transient_grid[1,3]

#sampling parameters
initial_interval=sampling_grid[1,0]
hold_time = sampling_grid[3,0]
number_of_samples = sampling_grid[1,1]
filter_status = sampling_grid[3,1]
integration_time = sampling_grid[1,2]
sampling_mode = sampling_grid[3,2]

#decode options mode into a dictionary with the actual commands that have to be sent to the tool
options_mode_tool=["LIN","L10","L25","L50","THIN"]
options_mode_dict=dict(zip(list(sampling_mode.options), options_mode_tool))
#print(options_mode_dict)
#print(options_mode_dict.get(sampling_mode.value))

#sampling 
sampling_list = [Vgs_samp,Vgs_samp_comp,Vds_samp,Vds_samp_comp,initial_interval,hold_time,number_of_samples,filter_status,integration_time,sampling_mode]
#always disabled widgets
disabled_widgets = [t_str[0],t_str[6],Vgs_str[0],Vgs_str[6],Vds_str[0],Vds_str[6]]

information_list = [information_grid[i,j] for i in range(2) for j in range(1,3)]

#all widgets

all_widgets = [*information_list,*Vgs_str,*Vds_str,*t_str,*skip,*sampling_list]
all_widgets.append(comp_str)
all_widgets.append(button)
###################################################### end of widgets assignment   ####

#connect to the device 
device = module.HP4155a('GPIB0::17::INSTR')

#create tempfile
temp_file= os.path.join(os.getcwd(),'tempfile.txt')

setup(device)

def on_button_clicked(b):
    with output:
        #disable all widgets
        change_state(all_widgets,disabled_widgets)
        total_stress_time = 0
        date = str(datetime.today().replace(microsecond=0))

        #write header(easier here than in functions)
        with open(temp_file,'w') as f:
            #sample information
            f.write("Stress and Sampling measurement\n\n")
            f.write("Sample Infomation\n")
            f.write(f"Date:{date}"+"\n")
            f.write(f"Processing Number:{processing_number.value}"+"\n")
            f.write(f"Sample series:{sample_series.value}"+"\n")
            f.write(f"Field:{field.value}"+"\n")
            f.write(f"Transistor:{transistor.value}"+"\n\n")

            #parameters information
            f.write("Measurement Parameters\n")
            f.write(f"Stress Current Compliance(A):{comp_str.value}"+"\n")
            f.write(f"IG Sampling Compliance(A):{Vgs_samp_comp.value}"+"\n")
            f.write(f"ID Sampling Compliance(A):{Vds_samp_comp.value}"+"\n")
            f.write(f"Initial Interval(sec):{initial_interval.value}"+"\n")
            f.write(f"Integration Time:{integration_time.value}"+"\n")
            f.write(f"Hold Time(sec):{hold_time.value}"+"\n")
            f.write(f"Filter:{filter_status.value}"+"\n")
            f.write(f"Sampling Mode:{sampling_mode.value}"+"\n\n")
        
        #main loop
        #break loop hotkey
        
        stop = False
        def break_loop():
            nonlocal stop
            stop = True

        add_hotkey("esc",break_loop)
        
        for i in range(7):
            #check if skip is on
            if skip[i].value== True:
                with open(temp_file,'a') as f:
                    f.write(f"Step {i+1} skipped"+"\n\n")
                continue
            
            #t_str=0 means free run but a stress measurement should be skipped in that case 
            #stress sampling and plot
            if t_str[i].value != 0:
                stress(Vds_str[i].value,Vgs_str[i].value,t_str[i].value,comp_str.value,device)
            
            t,IDmm,IGmm = sampling(Vds_samp.value,Vgs_samp.value,Vds_samp_comp.value,Vgs_samp_comp.value,options_mode_dict.get(sampling_mode.value),number_of_samples.value,integration_time.value,initial_interval.value,hold_time.value,int(filter_status.value),device)

            clear_output()
            plot_sampling(t,IDmm,IGmm,i+1)

            #create the data frame(additional lists required) sampling
            Vgs_samp_i = [Vgs_samp.value for j in range(len(t))]
            Vds_samp_i = [Vds_samp.value for j in range(len(t))]

            #increase total stress time
            total_stress_time = total_stress_time + t_str[i].value
            total_stress_time_i = [total_stress_time for i in range(len(t))]

            #stress lists
            Vgs_str_i=[Vgs_str[i].value for j in range(len(t))]
            Vds_str_i = [Vds_str[i].value for j in range(len(t))]
            
            #create data frame 
            header = ["t(s)","VGS(V)","VDS(V)","IGmm(mA/mm)","IDmm(mA/mm)","Total stress time(s)","Stress VGS(V)", "Stress VDS(V)"]
            values = [t,Vgs_samp_i,Vds_samp_i,IGmm,IDmm,total_stress_time_i,Vgs_str_i,Vds_str_i]
            df = pd.DataFrame(dict(zip(header,values)))

            with open(temp_file,'a') as f:
                f.write(f"Step {i+1}"+"\n")
                f.write(df.to_string())
                f.write("\n\n")
            if stop == True:
                information_box("Measurement stopped by user!")
                break
        else:
            information_box("Measurement finished!")
    
        remove_hotkey("esc")
        stop = False
        default_filename=f"{sample_series.value}_{field.value}_{transistor.value}.txt"
        upload_file(temp_file,default_filename)
        
        #enable all widgets
        change_state(all_widgets,disabled_widgets)
    
            
        
#link functions with buttons
button.on_click(on_button_clicked)

'''
Here is the implementation of stress + sampling with buttons
The libraries are the same with the ctlm/tlm

Vds is smu2 and Vgs is smu3
'''

import matplotlib.pyplot as plt
import pandas as pd
from datetime import datetime
import os 
import sys
import numpy as np
from IPython.display import display, clear_output
import ipywidgets as widgets
import configparser
from ipyfilechooser import FileChooser
from pathlib import Path

sys.path.append(r"C:\Users\user\labcode\hp4155")
import module

def Stress_Sampling():
    
    #default parameters
    plt.ion()
    VDS_str=10 #vds
    VGS_str=-12 #vgs
    stress_time=10
    total_stress_time = 0
    VDS_sampling=10
    VGS_sampling=2
    sampling_mode='L10'
    number_of_points=21
    integration_time='MED'
    str_comp = 0.1
    samp_Vgs_comp = 10**(-3)
    samp_Vds_comp= 0.1
    hold_time = 0
    filter_status='OFF'
    initial_interval = 8*10**(-3) #this is a multichannel measurement the initial interval should be >2ms 
    # Input the name of the sample.
    sample = input("Give the name of the sample!:")
    #file
    f=0
    
    #counters 
    stress_counter = 0
    sampling_counter = 0

    #defqult ini file 
    ini_file_path = os.getcwd()
    ini_file_name = r"default.ini"     
    ini_file = os.path.join(ini_file_path,ini_file_name) 

    #default save path
    save_file_path = os.getcwd()
    save_file_path = Path(save_file_path)

    #connect to the device
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset() #always reset the device when you start a new measurement
    date = str(datetime.today().replace(microsecond=0))

    #remove all errors from the machine before starting the measurement!
    try:
        while int((device.error())[1])!=0:
            print(device.error())
    except:
        pass

    #select ini file:
    fc = FileChooser(select_desc='select ini') 
    fc.default_path = r"\\FILESERVER\public"
    fc.filter_pattern = '*.ini'
    display(fc)

    #save file path
    pc = FileChooser(select_desc="save path")
    pc.default_path = r"\\FILESERVER\public"
    pc.show_only_dirs = True
    display(pc)

    #stress button 
    stress_button = widgets.Button(description='Stress')
    stress_output = widgets.Output()
    display(stress_button,stress_output)
    
    #sampling button
    sampling_button = widgets.Button(description='Sampling')
    sampling_output = widgets.Output()
    display(sampling_button,sampling_output)
    
    #exit button (MUST BE PRESSED WHEN THE MEASURMENT IS FINISHED)
    exit_button = widgets.Button(description = 'Exit')
    exit_output = widgets.Output()
    display(exit_button,exit_output)

    #disable all buttons after a button is pressed
    buttons = [sampling_button,stress_button,exit_button]
    
    # Functions for buttons

    def on_stress_button_clicked(b):
        nonlocal VDS_str,VGS_str,stress_time,str_comp,device,stress_counter,sampling_counter,ini_file,save_file_path,sample,f,date,buttons,total_stress_time
        with exit_output:
            #disable or buttons after the button is clicked has to be done in all link functions
            for button in buttons:
                button.disabled = True

            #reset the device to avoid possible errors from the machine
            device.reset()
            try:
                #clear output from the previous measurement
                #Now we read the input for the save_file_path only at the first measurement
                if stress_counter==0 and sampling_counter==0:
                    if pc.selected != None:
                        save_file_path=pc.selected
                    print(f'save data to:{save_file_path}')
                    #open file
                    file_name ="Stress_Sampling_"+sample+".txt"
                    path= os.path.join(save_file_path,file_name)

                    #check if file name exists
                    i=1
                    while os.path.exists(path):
                        file_name = "Stress_Sampling_"+sample+"_"+str(i)+".txt"
                        path= os.path.join(save_file_path,file_name)
                        i=i+1
                    #Now we need to do this once again if the user starts with sampling
                    # write_title
                    title = f"stress and sampling measurement of:{sample} started at:{date}"+"\n\n"
                    f=open(path,'a')
                    f.write(title)

                else: #here we can clear the output the opposite is (stress_counter!=0 or sampling_counter!=0)
                    clear_output()
                #read the ini file
                if fc.selected!=None:
                    ini_file=fc.selected

                print(f"get parameters from:{ini_file}")
                #configparser to raed parameters from the ini file for stress
                cp=configparser.ConfigParser()
                cp.read(ini_file)
                
                VDS_str=cp.getfloat('stress','VDS')
                VGS_str=cp.getfloat('stress','VGS')
                stress_time=cp.getfloat('stress','time')
                str_comp=cp.getfloat('stress','comp')
                #setup the device
                device.stress_page() #go to stress page
                #define smus to numbers in mode and values for stress 

                #mode
                device.smu_mode(1,'COMM')
                device.smu_mode(2,'V')
                device.comp_stress(2,str_comp)
                device.smu_mode(3,'V')
                device.comp_stress(3,str_comp)
                device.smu_mode(4,'COMM')
                device.sync(4,0)

                #names 
                device.str_smu_name(2,'VD')
                device.str_smu_name(3,'VG')
                #values
                device.smu_value(2,VDS_str)
                device.smu_value(3,VGS_str)
                #time
                device.stress_time(stress_time)
                print(device.error())
    
                device.start_stress()
                #message for the user
                print("stress operation in progress please wait...")
                while device.operation_completed() == False:
                    pass
                #data = device.copy_current_page() #retuns the stress output from the machine
                #print(data)#test

                #increase total stress time
                total_stress_time = total_stress_time+stress_time 
                #increase stress counter 
                stress_counter= stress_counter+1
                subtitle=f"Stress #{stress_counter} with VGS={VGS_str}V and VDS={VDS_str}V"+"\n"+f"stress time:{stress_time}s"+"\n"+f"total stress time:{total_stress_time}s"+"\n\n"
                f.write(subtitle)
                print("stress operation completed!")
            except:
                print("please ensure that all parameters in the ini file are correct")
                if sampling_counter==0 and stress_counter==0:
                    f.close()
                    os.remove(path)

            #now enable the buttons
            for button in buttons:
                button.disabled = False

    def on_sampling_button_clicked(b):
        nonlocal VDS_sampling,VGS_sampling,samp_Vds_comp,samp_Vgs_comp,sampling_mode,number_of_points,integration_time,initial_interval,hold_time,stress_counter,sampling_counter,ini_file,save_file_path,sample,f,date,buttons,filter_status
        with exit_output:
            for button in buttons:
                button.disabled = True
            
            # reset the device to avoid possible errors from the machine
            device.reset()
            #we need also to read the path if the first measurement is sampling
            try:
                if stress_counter == 0 and sampling_counter == 0:
                    if pc.selected!=None:
                        save_file_path = pc.selected
                    print(f'save data to:{save_file_path}')
                    #open file
                    file_name ="Stress_Sampling_"+sample+".txt"
                    path= os.path.join(save_file_path,file_name)

                    #check if file name exists
                    i=1
                    while os.path.exists(path):
                        file_name = "Stress_Sampling_"+sample+"_"+str(i)+".txt"
                        path= os.path.join(save_file_path,file_name)
                        i=i+1
                
                    title = f"stress and sampling measurement of:{sample} started at:{date}"+"\n"
                    f=open(path,'a')
                    f.write(title)
                else:
                    clear_output()
                
                #read the ini file
                if fc.selected!=None:
                    ini_file = fc.selected

                print(f"get parameters from:{ini_file}")

                #get parameters from ini exception Handling will be added later
                cp=configparser.ConfigParser()
                cp.read(ini_file)

                VDS_sampling=cp.getfloat('sampling','VDS')
                VGS_sampling=cp.getfloat('sampling','VGS')
                samp_Vds_comp = cp.getfloat('sampling','VDS_comp')
                samp_Vgs_comp = cp.getfloat('sampling','VGS_comp')
                sampling_mode= cp.get('sampling','mode')
                number_of_points=cp.getint('sampling','number_of_samples')
                integration_time=cp.get('sampling','integration_time')
                initial_interval=cp.getfloat('sampling','initial_interval')
                #raise exception if initial interval is smaller than 2ms
                if initial_interval <= 2e-3:
                    raise Exception("Initial Interval should be > 2ms")
                hold_time=cp.getfloat('sampling','hold_time')
                filter_status=cp.get('sampling','filter')
                if filter_status!='OFF' and filter_status!='ON':
                    raise Exception('Invalid filter status')
            
                #start with sampling measurement
                device.measurement_mode('SAMP')
            
                #set the mode of smus for sampling
                device.smu_mode_meas(1,'COMM')
                device.smu_mode_meas(2,'V')
                device.smu_mode_meas(3,'V')
                device.smu_mode_meas(4,'COMM')

                #set the names of the smus
                device.smu_vname(1,'VS1')
                device.smu_iname(1,'IS1')
                device.smu_vname(2,'VDS')
                device.smu_iname(2,'ID')
                device.smu_vname(3,'VGS')
                device.smu_iname(3,'IG')
                device.smu_vname(4,'VS2')
                device.smu_iname(4,'IS2')

                #disable vmus and vsus
                device.disable_vsu(1)
                device.disable_vsu(2)
                device.disable_vmu(1)
                device.disable_vmu(2)

                #set the constant value of smus and compilance
                device.constant_smu_sampling(2,VDS_sampling)
                device.constant_smu_sampling(3,VGS_sampling)
                device.constant_smu_comp(2,samp_Vds_comp)
                device.constant_smu_comp(3,samp_Vgs_comp)
                print(device.error())
            
                #set sampling parameters
                device.sampling_mode(sampling_mode)
                device.number_of_points(number_of_points)
                device.integration_time(integration_time)
                device.initial_interval(initial_interval)
                device.delay_time(hold_time)
                device.filter_status(filter_status)
                print(device.error())
    
                #define user functions
                device.user_function('t','s','@TIME')
                print(device.error())
                device.user_function('IDmm','mA/mm','1E4*ID')
                print(device.error())
                device.user_function('IGmm','mA/mm', '1E4*IG')
                print(device.error())
                device.user_function('ABSIGm','mA/mm','ABS(IGmm)')
                print(device.error())
                device.user_function('ABSIDm','mA/mm','ABS(IDmm)')
                print(device.error())
                #as shown at text file two more user functions have to be defined

                device.display_variable('X','t')
                device.display_variable('Y1','ABSIDm')
                device.display_variable('Y2','ABSIGm')
                print(device.error())# show to user possible errors

                print('Measuring...')#only for user
                device.single_measurement()
                while device.operation_completed() == False:
                    pass
                device.autoscaling()
                try:    
                    time = device.return_data('t')
                    ABSIGm = device.return_data('ABSIGm')
                    ABSIDm = device.return_data('ABSIDm')

                    #we need this function for the dataframe at the txt.
                    IDmm = device.return_data('IDmm')
                    IGmm = device.return_data('IGmm')
                    #test
                    plt.figure().clear()
                    fig, ax1 = plt.subplots() 
 
                    color = 'tab:red'
                    ax1.set_xlabel('time(s)') 
                    ax1.set_ylabel('IG(mA/mm)', color = color)
                    ax1.set_yscale('log')#logaritmic scale
                    ax1.plot(time, ABSIGm, color = color) 
                    ax1.tick_params(axis ='y', labelcolor = color) 
 
                    # Adding Twin Axes to plot IGmm
                    ax2 = ax1.twinx() 
 
                    color = 'tab:green'
                    ax2.set_ylabel('ID(ma/mm)', color = color) 
                    ax2.plot(time, ABSIDm, color = color)
                    ax2.set_yscale('log') #logarithmic scale
                    ax2.tick_params(axis ='y', labelcolor = color) 
 
                    # Adding title
                    plt.title('sampling plot!', fontweight ="bold") 

                    # Show plot
                    plt.show()
            
                    #increase sampling_counter
                    sampling_counter = sampling_counter+1
                    subtitle=f"Sampling #{sampling_counter} with VGS={VGS_sampling}V and VDS={VDS_sampling}V"+"\n\n"
                    f.write(subtitle)

                    #export data frame
                    header = ['time(s)', 'IDmm(mA/mm)','IGmm(mA/mm)','ABSIDm(mA/mm)','ABSIGm(mA/mm)']
                    data ={header[0]:time,header[1]:IDmm,header[2]:IGmm,header[3]:ABSIDm,header[4]:ABSIGm}
                    df=pd.DataFrame(data)
                    #convert to string
                    df_string = df.to_string()
                    f.write(df_string)
                    f.write("\n\n")
                    print('Measurement finished!')#only for user
                except:
                    print("cannot return data from the device!")
                    if sampling_counter==0 and stress_counter==0:
                        f.close()
                        os.remove(path)
            except:
                print("please ensure that all parameters in the ini file are correct")
                if sampling_counter==0 and stress_counter==0:
                    f.close()
                    os.remove(path)

            for button in buttons:
                button.disabled = False

    def on_exit_button_clicked(b):
        nonlocal f, device, buttons
        with exit_output:
            try:
                for button in buttons:
                    button.disabled = True
                del device
                f.close()
            except:
                pass
            os._exit(00)
        
    #Link functions with buttons
    stress_button.on_click(on_stress_button_clicked)
    sampling_button.on_click(on_sampling_button_clicked)
    exit_button.on_click(on_exit_button_clicked)   
           
           
            
           

            

            


    
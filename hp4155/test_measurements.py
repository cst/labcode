# this is a python file that minimizes the amount of code seen to the user

import module
import matplotlib.pyplot as plt
import pandas as pd
from datetime import datetime
import os 
from sklearn.linear_model import LinearRegression
import sys
import numpy as np
from IPython.display import display, clear_output
import ipywidgets as widgets
#from help_ctlm import *


def I_V_Measurement(start,stop,step):
    
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset()
    device.inst.write(":PAGE:MEAS")
    device.inst.write(":PAGE:CHAN:MODE SWEEP") #go to sweep page and prepare sweep measurement
   
    #setup sweep
    device.inst.write(":PAGE:CHAN:MODE SWEEP") #go to sweep page and prepare sweep measurement

    #smu2 and smu4 are disabled
    device.smu_disable_sweep(2)
    device.smu_disable_sweep(4)

    #smu1 is constant and common
    device.smu_mode_meas(1,'COMM')
    device.smu_function_sweep(1,'CONS')

    #smu3 is VAR1 and V
    device.smu_mode_meas(3,'V')
    device.smu_function_sweep(3,'VAR1')

    #define start-step-stop
    device.start_value_sweep(start)
    device.step_sweep(step)
    device.stop_value_sweep(stop)
    
    #start measurement
    device.single_measurement()
    while device.operation_completed() == False:
        pass
    
    voltage_values = device.return_data('V3')
    current_values = device.return_data('I3')
    
    # show plot
    plt.plot(voltage_values,current_values)
    plt.xlabel('Voltage(V)')
    plt.ylabel('Current(A)')
    plt.title("I-V plot")
    plt.show()
    
    #export data to csv file
    #add title to the results
    
    header = ['Voltage(V)', 'Current(A)']

    data = {header[0]:voltage_values,header[1]:current_values}
    df = pd.DataFrame(data)
    date = str(datetime.today().replace(microsecond=0))
    
    print(df)
    
    #exporting the data frame in an excel file
    file_name ="results.csv"
    path = r"\\fileserver.cst.rwth-aachen.de\public\Datentransfer\Asonitis, Alexandros"
    #r"C:\Users\user\Desktop"
    directory = os.path.join(path,file_name)
    df.to_csv(directory)
    
    del device

def stress_sampling(V2_stress=0,V3_stress=0,stress_time=5,V2_sampling=1,V3_sampling=0,sampling_mode='L10',
                   number_of_points=26,integration_time='MED'):
    
    #connect to device
    device = module.HP4155a('GPIB0::17::INSTR')
    device.reset()
    
    device.stress_page()
    
    #define smus to numbers in mode and values for stress 

    #mode
    device.smu_mode(1,'COMM')
    device.smu_mode(2,'V')
    device.smu_mode(3,'V')
    device.smu_mode(4,'COMM')
    device.sync(4,0)

    #values
    device.smu_value(2,V2_stress)
    device.smu_value(3,V3_stress)
    #time
    device.stress_time(stress_time)
    
    device.start_stress()
    while device.operation_completed() == False:
        pass
    
    #start with sampling measurement
    device.measurement_mode('SAMP')

    #set the mode of smus for sampling
    device.smu_mode_meas(1,'COMM')
    device.smu_mode_meas(2,'V')
    device.smu_mode_meas(3,'V')
    device.smu_mode_meas(4,'COMM')


    #set the values of smu for sampling
    device.constant_smu_sampling(2,V2_sampling)
    device.constant_smu_sampling(3,V3_sampling)
    
    
    #time log10
    device.sampling_mode(sampling_mode)

    #minimum initial interval
    device.initial_interval(2*10**(-3))

    device.number_of_points(number_of_points)
    device.integration_time(integration_time)

    device.display_variable('X','@TIME')
    device.display_variable('Y1','I2')
    device.display_variable('Y2','I3')

    device.constant_smu_comp(2,100*10**(-3))
    device.constant_smu_comp(3,10**(-3))
    device.single_measurement()
    while device.operation_completed() == False:
        pass

    time_values = device.return_data('@TIME')
    I3_values = device.return_data('I3')
    I2_values = device.return_data('I2')
    
    fig = plt.figure()
    plt.plot(time_values,I2_values,label='I2')
    plt.plot(time_values,I3_values,label='I3')
    plt.xlabel('Time(s)')
    plt.ylabel('Current(A)')
    plt.title("stress + sampilng plot")
    plt.legend()
    plt.show()
    
    del device
    
def ausgangskennliniefeld():  
    
    #variables 
    Vds_start = 0
    Vds_stop = 15
    Vds_step = 0.1
    Vds_comp = 0.1
    time='MED'
    
    Vgs_start = 2
    Vgs_step = -1
    Vgs_points = 3
    Vgs_comp= 0.01
    Vgs_pcomp = 0
    
    device = module.HP4155a('GPIB0::17::INSTR')
    
    #setup device
    device.reset()
    device.measurement_mode('SWE')
    
    #smu1 
    device.smu_vname(1,'VS1')
    device.smu_iname(1,'IS1')
    device.smu_mode_meas(1,'COMM')
    device.smu_function_sweep(1,'CONS')
    
    #smu2
    device.smu_vname(2,'VDS')
    device.smu_iname(2,'ID')
    device.smu_mode_meas(2,'V')
    device.smu_function_sweep(2,'VAR1')
    
    #smu3
    device.smu_vname(3,'VGS')
    device.smu_iname(3,'IG')
    device.smu_mode_meas(3,'V')
    device.smu_function_sweep(3,'VAR2')
    
    #smu4
    device.smu_vname(4,'VS2')
    device.smu_iname(4,'IS2')
    device.smu_mode_meas(4,'COMM')
    device.smu_function_sweep(4,'CONS')
    
    #var1 and var2 setup
    device.start_value_sweep(Vds_start)
    device.step_sweep(Vds_step)
    device.stop_value_sweep(Vds_stop)
    device.comp('VAR1',Vds_comp)
    
    #var2
    device.var2_start(Vgs_start)
    device.var2_points(Vgs_points)
    device.var2_step(Vgs_step)
    device.var2_comp(Vgs_comp)
    device.var2_pcomp(Vgs_pcomp)
    
    device.integration_time(time)
    
    #user functions
    device.user_function('ISmm','mA/mm','1E4*IS1')
    device.user_function('IDmm','mA/mm','1E4*ID')
    device.user_function('IGmm','mA/mm','1E4*IG')
    device.user_function('IS2mm','mA/mm','1E4*IS2')
    device.user_function('ABSIDm','mA/mm','ABS(IDmm)')
    device.user_function('ABSIGm','mA/mm','ABS(IGmm)')
    
    #display
    device.display_variable('X','VDS')
    device.display_variable('Y1','IDmm')
    device.display_variable('Y2','IGmm')
    
    device.single_measurement()
    while device.operation_completed() == False:
        pass
        
    device.autoscaling()
    #return data from the device
        
    Vds=device.return_data('VDS')
    Idmm=device.return_data('IDmm')
    Igmm=device.return_data('Igmm')
    
    fig = plt.figure()
    plt.plot(Vds,Idmm,label='IDmm')
    plt.plot(Vds,Igmm,label='IGmm')
    plt.xlabel('Voltage')
    plt.ylabel('Current')
    plt.title("ausgangskennliniefeld")
    plt.legend()
    plt.show()
    del device
    
def ubertragungskennliniefeld():
    #vds
    Vds=10
    Vds_comp=0.1
    
    #vgs
    Vgs_start=2
    Vgs_stop=-8
    Vgs_step=-0.05
    Vgs_comp= 0.01
    
    time='MED'
    device = module.HP4155a('GPIB0::17::INSTR')
    
    #setup device
    device.reset()
    device.measurement_mode('SWE')
    
    #smu1 
    device.smu_vname(1,'VS1')
    device.smu_iname(1,'IS1')
    device.smu_mode_meas(1,'COMM')
    device.smu_function_sweep(1,'CONS')
    
    #smu2
    device.smu_vname(2,'VDS')
    device.smu_iname(2,'ID')
    device.smu_mode_meas(2,'V')
    device.smu_function_sweep(2,'CONS')
    device.cons_smu_value(2,Vds)
    device.const_comp(2,Vds_comp)
    
    #smu3
    device.smu_vname(3,'VGS')
    device.smu_iname(3,'IG')
    device.smu_mode_meas(3,'V')
    device.smu_function_sweep(3,'VAR1')
    
    #smu4
    device.smu_vname(4,'VS2')
    device.smu_iname(4,'IS2')
    device.smu_mode_meas(4,'COMM')
    device.smu_function_sweep(4,'CONS')
    
    #var1 setup
    device.start_value_sweep(Vgs_start)
    device.step_sweep(Vgs_step)
    device.stop_value_sweep(Vgs_stop)
    device.comp('VAR1',Vgs_comp)
    device.integration_time(time)
    
    #user functions
    device.user_function('Gm','mS/mm','1E4*DIFF(ID,VGS)')
    device.user_function('IDmm','mA/mm','1E4*ID')
    device.user_function('IGmm','mA/mm','1E4*IG')
    device.user_function('ABSIDm','mA/mm','ABS(IDmm)')
    device.user_function('ABSIGm','mA/mm','ABS(IGmm)')
    
    #display
    device.display_variable('X','VDS')
    device.display_variable('Y1','IDmm')
    device.display_variable('Y2','IGmm')
    
    device.single_measurement()
    while device.operation_completed() == False:
        pass
        
    device.autoscaling()
    #return data from the device
        
    Vgs=device.return_data('VGS')
    Idmm=device.return_data('IDmm')
    gm=device.return_data('Gm')
    
    fig = plt.figure()
    plt.plot(Vgs,Idmm,label='Idmm')
    plt.plot(Vds,gm,label='Gm')
    plt.xlabel('Voltage')
    plt.ylabel('Transconductance,Current')
    plt.title("ubetragungskennlinieFeld")
    plt.legend()
    plt.show()
    del device
    
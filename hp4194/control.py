import pyvisa
import enum
import pandas as pd
import warnings
import numpy as np
import time

class hp4194(object):

    def __init__(self,address="GPIB3::17::INSTR",library = "SCPI_command_summary.csv"):
        self.adress = address
        self.rm = pyvisa.ResourceManager()
        self.inst = self.rm.open_resource(address)
        self.inst.control_ren(5) # local lockout
        self.inst.timeout = None
        self.library = pd.read_csv(library,sep = ";")

    def __del__(self):
        self.inst.control_ren(0)
        self.inst.close()

    def __find_command(self,mnemonic):
        rows_list = self.library.index[self.library["Mnemonic"]== mnemonic].tolist()

        if len(rows_list)==0:
            raise Exception("Command not Found!")
        
        if len(rows_list)>1:
            warnings.warn(f"{mnemonic} found at multiple commands with indexes:{rows_list}. Only the first appearance is considered valid!")
       
        row = rows_list[0] #first appearance
        command =self.library.at[row,"Command"]
        return command

    def write(self,mnemonic):
        try:
            command = self.__find_command(mnemonic)
        except Exception as e:
            print(e)
            return 
        self.inst.write(command)

    def set_parameter(self,mnemonic,parameter):
        try :
            prefix = self.__find_command(mnemonic)
        except Exception as e:
            print(e)
            return 
        
        command = f"{prefix}={parameter}"
        self.inst.write(command)

    def query(self,mnemonic):
        try:
            prefix = self.__find_command(mnemonic)
        except Exception as e:
            print(e)
            return 
        
        command = f"{prefix}?"
        response = self.inst.query(command)
        return response

    def read_register(self,mnemonic):
        if mnemonic.startswith('reg')==False:
            return
        
        string_response = self.query(mnemonic)
        
        if string_response == None:
            return 

        string_response = string_response.strip() # remove termination characters
        values = string_response.split(",")

        for i in range(len(values)):
            values[i]= float(values[i])

        return values
        

    def get_info(self,column_name,entry):
        rows = self.library.loc[self.library[column_name] == entry]
        with pd.option_context('display.max_rows', None, 'display.max_columns', None,'display.max_colwidth',3000): 
            display(rows)

    def __doc__(self):
        with pd.option_context('display.max_rows', None, 'display.max_columns', None,'display.max_colwidth',3000): 
            display(self.library)

    
    def __bit_set(self):
        """
        Returns if the measurement has finished
        """
        decimal=self.inst.read_stb()
        status_byte_register= np.binary_repr(decimal,8)
        measurement_bit = int(status_byte_register[-2])
        return bool(measurement_bit)

    def wait(self):
        """
        Wait until the measurement is finished
        """
        time.sleep(2)
        while self.__bit_set()==False:
            time.sleep(0.1)
        
                        
        
        


    

    


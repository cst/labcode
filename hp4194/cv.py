import matplotlib
matplotlib.use('TkAgg',force = True)

from interface import *
import control
from help import *
import time
from IPython.display import clear_output,display 
import numpy as np
import datetime
import pandas as pd

import matplotlib.pyplot as plt

#connect to device
device = control.hp4194()
device.write('reset')
device.write('meas_imp')

#create interface
sample,sample_dict = probendaten()
messparameter,messparameter_dict = messparameter()
sweep_parameter,sweep_parameter_dict = sweep_parameter()
control_panel,measure,view=control_panel()

#set up interface
out = widgets.Output()
hbox1 = widgets.HBox([sample,control_panel])
display(hbox1)
hbox2=widgets.HBox([messparameter,sweep_parameter])
display(hbox2,out)

#add widgets to a list for disabling them
all_widgets = [measure]
add_widgets_to_list(sample_dict,all_widgets)
add_widgets_to_list(sweep_parameter_dict,all_widgets)
add_widgets_to_list(messparameter_dict,all_widgets)

def on_measure_clicked(b):
    with out:
        clear_output()
        change_state(all_widgets)
        if check_values(messparameter_dict,sweep_parameter_dict) == False:
            information_box('Invalid Voltage Settings or Observation of non-existent data points!')
            change_state(all_widgets)
            return
            
        device.write('imp_G_B')
        device.write('bias_off')
    
        device.write('sweep_freq')
        device.set_parameter('set_start',sweep_parameter_dict['start'].value)
        device.set_parameter('set_stop',sweep_parameter_dict['stop'].value)
        device.set_parameter('set_nop',sweep_parameter_dict['nop'].value)
    
        if sweep_parameter_dict['type'].value=='Linear':
            device.write('sweep_lin')
        else:# log
            device.write('sweep_log')
        if sweep_parameter_dict['direction'].value=='Up':
            device.write('sweep_up')
        else:
            device.write('sweep_down')
    
        device.write('sweep_single')
        device.write('imp_monitor_I')
        device.set_parameter('set_osc',sweep_parameter_dict['osc'].value)
        device.set_parameter('set_delay_time',sweep_parameter_dict['d_time'].value)
        device.set_parameter('set_delay_apperture',sweep_parameter_dict['d_apperture'].value)
        device.set_parameter('aver_num',sweep_parameter_dict['averaging'].value)
        device.write((sweep_parameter_dict["integration"].value).lower()) # Set integration number
        
        # Now that we have set the frequency values ask user for calibration
        answer = ask_for_calibration()
        if answer == True : #perform an open calibration
            device.write('start_open_cal')
            device.write('open_cal_on') #data saved in registers OG and OB

        
        # open the file dialog
        default_filename = f"{sample_dict['wafer'].value}_{sample_dict['sample'].value}_{sample_dict['field'].value}_CV.txt"
        file = save_file(default_filename)
    
        #now perform the measurement
        device.write('autoscale_A') # Autoscale A
        device.write('autoscale_B') # Autoscale B
    
        # create the numpy list with the biases
        # create the arrays with everything that needs to be saved into the array
        f_values = []
        G_values = []
        B_values = []
        log_f_values = []
        omega_values = []
        log_omega_values = [] 
        Z_values = []
        phi_values = []
        D_values = []
        Cs_values =[]
        Cp_values =[]
        Cp_area_values =[]
        Cs_area_values = []
        ReZ_values = []
        ImZ_values = []
        G_area_omega_values = []
        U_values = []
        area_values = []
        
    
        num_of_points =int(abs(messparameter_dict["stop"].value-messparameter_dict["start"].value)/abs(messparameter_dict["step"].value) + 1)
        radius = messparameter_dict["radius"].value
        area = np.pi * radius**2 # in um2
        area = area * 10**(-8) # in cm2  
        biases= np.round(np.linspace(messparameter_dict["start"].value,messparameter_dict["stop"].value,num_of_points,endpoint = True),3)

        # frequency index 
        f_index = messparameter_dict['f_point'].value-1

        # create the figure
        fig = plt.figure(figsize=(12,10),layout = "constrained")
        spec = fig.add_gridspec(ncols=3, nrows=2)

        ax1 = fig.add_subplot(spec[0,0])
        ax1_twin = ax1.twinx()
        ax1.set_title("Cp/D(f) live")
        ax1.set_xlabel("Frequency (Hz)")
        ax1.set_ylabel("Cp (F)")
        ax1_twin.set_ylabel("D")
        
        ax2 = fig.add_subplot(spec[0,1])
        ax2_twin = ax2.twinx()
        ax2.set_title("Z/Phi(f) live")
        ax2.set_xlabel("Frequency (Hz)")
        ax2.set_ylabel("Z (Ohm)")
        ax2_twin.set_ylabel("Phi (°)")

        ax3 = fig.add_subplot(spec[0,2])
        ax3.set_title("ImZ(ReZ(f)) live")
        ax3.set_xlabel("Re Z (Ohm)")
        ax3.set_ylabel("Im Z (Ohm)")

        ax4 = fig.add_subplot(spec[1,0])
        ax4_twin = ax4.twinx()
        ax4.set_title("Cp/D(U)")
        ax4.set_xlabel("Voltage U (V)")
        ax4.set_ylabel("Cp (F)")
        ax4_twin.set_ylabel("D")
        
        ax5 = fig.add_subplot(spec[1,1])
        ax5_twin = ax5.twinx()
        ax5.set_title("Z/Phi(U)")
        ax5.set_xlabel("Voltage U (V)")
        ax5.set_ylabel("Z (Ohm)")
        ax5_twin.set_ylabel("Phi (°)")

        ax6 = fig.add_subplot(spec[1,2])
        ax6_twin = ax6.twinx()
        ax6.set_title("Cp/D(f)")
        ax6.set_xlabel("Frequency (Hz)")
        ax6.set_ylabel("Cp (F)")
        ax6_twin.set_ylabel("D")

        """figManager = plt.get_current_fig_manager()
        figManager.window.state('zoomed') 
        win = plt.gcf().canvas.manager.window
        win.overrideredirect(1)"""

        view["v-point"].value =str(biases[messparameter_dict["v_point"].value-1])
        for i,bias in enumerate(biases):
            view["v-value"].value = str(bias) 
            
            # voltage index
            v_index = messparameter_dict['v_point'].value-1
            
            U_values.extend([bias]*sweep_parameter_dict['nop'].value)
            area_values.extend([area]*sweep_parameter_dict['nop'].value)
            device.set_parameter('set_bias', bias) #set the bias
            device.write('start_sweep') #start the measurement
            device.wait() #wait for completition
            
            # read the registers
            
            freq = np.array(device.read_register('reg_sweep'))
            f_values.extend(freq)
            G = np.array(device.read_register('reg_A'))
            G_values.extend(G)
            B = np.array(device.read_register('reg_B'))
            B_values.extend(B)

            view["f-point"].value = str(freq[messparameter_dict["f_point"].value-1])
            time.sleep(messparameter_dict["sleep"].value)

            #do the calculations
            log_f = np.log10(freq)
            log_f_values.extend(log_f)
            
            omega = 2*np.pi*freq
            omega_values.extend(omega)

            log_omega = np.log10(omega)
            log_omega_values.extend(omega)

            Y_complex = G + 1j*B
            Z_complex = 1/Y_complex

            Z = np.absolute(Z_complex)
            Z_values.extend(Z)
            phi = np.angle(Z_complex, deg = True)
            phi_values.extend(phi)

            D = G/B
            D_values.extend(D)
                
            Cp = B/omega
            Cp_values.extend(Cp)

            Cs = Cp*(1+D**(2))
            Cs_values.extend(Cs)

            Cp_area = Cp/area
            Cp_area_values.extend(Cp_area)

            Cs_area = Cs/area
            Cs_area_values.extend(Cs_area)

            ReZ = np.real(Z_complex)
            ImZ = np.imag(Z_complex)
            ReZ_values.extend(ReZ)
            ImZ_values.extend(ImZ)

            G_area_omega= G/area/omega
            G_area_omega_values.extend(G_area_omega)

            # do the plots 
            # first the live ones
            if i>0:
                curve1.pop(0).remove()
                curve1_twin.pop(0).remove()
                curve2.pop(0).remove()
                curve2_twin.pop(0).remove()
                curve3.pop(0).remove()
    
        

            # frequency Cp/D plot
            curve1 = ax1.plot(freq,Cp,color = 'b')
            ax1.tick_params(axis = 'y',labelcolor = 'b')

            curve1_twin=ax1_twin.plot(freq,D, color = 'y')
            ax1_twin.tick_params(axis='y',labelcolor = 'y')

            # frequecy Z-phi
            curve2=ax2.plot(freq,Z,color = 'b')
            ax2.tick_params(axis = 'y',labelcolor = 'b')

            curve2_twin = ax2_twin.plot(freq,phi, color = 'y')
            ax2_twin.tick_params(axis='y',labelcolor = 'y')

            # Rez - Imz
            curve3=ax3.plot(ReZ,ImZ,color = 'b')
            ax3.tick_params(axis = 'y',labelcolor = 'b')
    
            # now the rest of the plots
            # Voltage vs Cp D for the user specified frequency 
            # This is updated for every iteration
            ax4.scatter(bias, Cp[f_index],color ='b')
            ax4.tick_params(axis = 'y',labelcolor = 'b')

            ax4_twin.scatter(bias, D[f_index],color ='y')
            ax4_twin.tick_params(axis = 'y',labelcolor = 'y')
        
            # Voltage, Z-phi
            # This is also updated every iteration
            ax5.scatter(bias, Z[f_index],color ='b')
            ax4.tick_params(axis = 'y',labelcolor = 'b')

            ax5_twin.scatter(bias, phi[f_index],color ='y')
            ax5_twin.tick_params(axis = 'y',labelcolor = 'y')

            # Final Plot 
            # This if for all the frequencies for the calculated voltage

            if biases[v_index]== bias:
                ax6.scatter(freq,Cp,color = 'b')
                ax6.tick_params(axis = 'y',labelcolor = 'b')

                ax6_twin.scatter(freq,D, color = 'y')
                ax6_twin.tick_params(axis='y',labelcolor = 'y')

            wm = plt.get_current_fig_manager()
            wm.window.state('zoomed')
            win = plt.gcf().canvas.manager.window
            win.overrideredirect(1) # draws a completely frameless window

            plt.pause(0.1)

        
        if messparameter_dict["hysterisis"].value == True:
            reversed_biases = reversed_array(biases)
            for bias in reversed_biases:
                clear_output(wait = True)
                view["v-value"].value = str(bias)
                
                # voltage index
                v_index = -messparameter_dict['v_point'].value # in reverse
                U_values.extend([bias]*sweep_parameter_dict['nop'].value)
                area_values.extend([area]*sweep_parameter_dict['nop'].value)
                device.set_parameter('set_bias', bias) #set the bias
                device.write('start_sweep') #start the measurement
                device.wait() #wait for completition
                
                # read the registers
            
                freq = np.array(device.read_register('reg_sweep'))
                f_values.extend(freq)
                G = np.array(device.read_register('reg_A'))
                G_values.extend(G)
                B = np.array(device.read_register('reg_B'))
                B_values.extend(B)
    
        
                time.sleep(messparameter_dict["sleep"].value)
    
                #do the calculations
                log_f = np.log10(freq)
                log_f_values.extend(log_f)
                
                omega = 2*np.pi*freq
                omega_values.extend(omega)
    
                log_omega = np.log10(omega)
                log_omega_values.extend(omega)
    
                Y_complex = G + 1j*B
                Z_complex = 1/Y_complex
    
                Z = np.absolute(Z_complex)
                Z_values.extend(Z)
                phi = np.angle(Z_complex, deg = True)
                phi_values.extend(phi)
    
                D = G/B
                D_values.extend(D)
                    
                Cp = B/omega
                Cp_values.extend(Cp)
    
                Cs = Cp*(1+D**(2))
                Cs_values.extend(Cs)
    
                Cp_area = Cp/area
                Cp_area_values.extend(Cp_area)
    
                Cs_area = Cs/area
                Cs_area_values.extend(Cs_area)
    
                ReZ = np.real(Z_complex)
                ImZ = np.imag(Z_complex)
                ReZ_values.extend(ReZ)
                ImZ_values.extend(ImZ)
    
                G_area_omega= G/area/omega
                G_area_omega_values.extend(G_area_omega)

                # do the plots 
                # first the live ones

                curve1.pop(0).remove()
                curve1_twin.pop(0).remove()
                curve2.pop(0).remove()
                curve2_twin.pop(0).remove()
                curve3.pop(0).remove()
    
                # frequency Cp/D plot
                curve1=ax1.plot(freq,Cp,color = 'b')
                ax1.tick_params(axis = 'y',labelcolor = 'b')
    
                curve1_twin = ax1_twin.plot(freq,D, color = 'y')
                ax1_twin.tick_params(axis='y',labelcolor = 'y')
    
                # frequecy Z-phi
                curve2=ax2.plot(freq,Z,color = 'b')
                ax2.tick_params(axis = 'y',labelcolor = 'b')
    
                curve2_twin=ax2_twin.plot(freq,phi, color = 'y')
                ax2_twin.tick_params(axis='y',labelcolor = 'y')
    
                # Rez - Imz
                curve3=ax3.plot(ReZ,ImZ,color = 'b')
                ax3.tick_params(axis = 'y',labelcolor = 'b')
        
                # now the rest of the plots
                # Voltage vs Cp D for the user specified frequency 
                # This is updated for every iteration
                ax4.scatter(bias, Cp[f_index],color ='b')
                ax4.tick_params(axis = 'y',labelcolor = 'b')
    
                ax4_twin.scatter(bias, D[f_index],color ='y')
                ax4_twin.tick_params(axis = 'y',labelcolor = 'y')
            
                # Voltage, Z-phi
                # This is also updated every iteration
                ax5.scatter(bias, Z[f_index],color ='b')
                ax4.tick_params(axis = 'y',labelcolor = 'b')
    
                ax5_twin.scatter(bias, phi[f_index],color ='y')
                ax5_twin.tick_params(axis = 'y',labelcolor = 'y')
    
                # Final Plot 
                # This if for all the frequencies for the calculated voltage
    
                if biases[v_index]== bias:
                    ax6.scatter(freq,Cp,color = 'b')
                    ax6.tick_params(axis = 'y',labelcolor = 'b')
    
                    ax6_twin.scatter(freq,D, color = 'y')
                    ax6_twin.tick_params(axis='y',labelcolor = 'y')
                wm = plt.get_current_fig_manager()
                wm.window.state('zoomed')

                win = plt.gcf().canvas.manager.window
                win.overrideredirect(1) # draws a completely frameless window

                plt.pause(0.1)
        
        win.overrideredirect(0)

        device.write('bias_off')

        # save to file
        with open(file,'w') as f:
            dt = datetime.datetime.now().replace(second=0, microsecond=0)
            f.write(f'# Measurement started at {dt}'+"\n")
            f.write(f"# Wafer {sample_dict['wafer'].value} Sample {sample_dict['sample'].value} Comment {sample_dict['comment'].value} Temperature {sample_dict['temperature'].value}" +"\n")
            f.write(f"# V_start {messparameter_dict['start'].value} V V_stop {messparameter_dict['stop'].value} V V_step {messparameter_dict['step'].value} V"+"\n")
            f.write(f"# f_start {sweep_parameter_dict['stop'].value} Hz f_stop {sweep_parameter_dict['stop'].value} Hz no_steps {sweep_parameter_dict['nop'].value} SweepType {sweep_parameter_dict['type'].value}"+"\n")
            f.write(f"# OSC {sweep_parameter_dict['osc'].value} V"+"\n")
            f.write(f"# integration time {sweep_parameter_dict['integration'].value} averaging {sweep_parameter_dict['averaging'].value}"+"\n")
            f.write(f"# area {area} cm^2"+"\n")
            f.write("\n")

        # create the dataframe
        dictionary = {"U(V)": U_values, "A(cm^2)":area_values,"f(Hz)":f_values ,"log_f(Hz)":log_f_values,"omega(1/s)":omega_values,"log_omega(1/s)":log_omega_values,"Z(Ohm)":Z_values,"Phi(°)":phi_values, "G(S)":G_values,"B(S)":B_values,"D":D_values,"Cs(F)":Cs_values, "Cs_area(F/cm^2)":Cs_area_values,"Cp(F)":Cp_values,"Cp_area(F/cm^2)":Cp_area_values,"ReZ(Ohm)":ReZ_values,"ImZ(Ohm)":ImZ_values,"G_area/omega(S*s/cm^2)":G_area_omega_values}


        df = pd.DataFrame(dictionary)

        #save the dictionary
        df.to_csv(file,sep=" ",mode='a')

        information_box("measurement finished")
        change_state(all_widgets)

measure.on_click(on_measure_clicked)
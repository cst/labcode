import ipywidgets as widgets
from ipywidgets import GridspecLayout,Layout, AppLayout,TwoByTwoLayout
import sys
from IPython.display import clear_output


def probendaten():
    width = 'auto'
    probendaten = GridspecLayout(6,2)

    probendaten[0,0] = widgets.Label('Wafer',layout=Layout(height='auto', width = width))
    probendaten[0,1] = widgets.Label('Comment',layout=Layout(height='auto', width = width))
    probendaten[1,0] = widgets.Text(layout=Layout(height='auto', width = width)) # wafer value
    probendaten[1,1] = widgets.Text(layout=Layout(height='auto', width = width)) # Bemerkung value
    probendaten[2,0] = widgets.Label('Sample',layout=Layout(height='auto', width = width))
    probendaten[3,0] = widgets.Text(layout=Layout(height='auto', width = width)) #probe value
    probendaten[4,0] = widgets.Label('Field',layout=Layout(height='auto', width = width)) 
    probendaten[4,1] = widgets.Label('Temeperature (K)',layout=Layout(height='auto', width = width)) 
    probendaten[5,0] = widgets.Text(layout=Layout(height='auto', width = width)) # Feld value
    probendaten[5,1] = widgets.BoundedFloatText(value = 295,min = 0 ,max = sys.float_info.max,step = 1,layout=Layout(height='auto', width = width)) #Temperatur value

    dict = {
        'wafer': probendaten[1,0],
        'comment':probendaten[1,1],
        'sample':probendaten[3,0],
        'field':probendaten[5,0],
        'temperature':probendaten[5,1]
    }
    
    tab = widgets.Tab()
    tab.children = [probendaten]
    tab.set_title(0,'Sample Information')

    return tab,dict


def messparameter():
    width = 'auto'
    messparameter = GridspecLayout(15,1)

    messparameter[0,0] = widgets.Label('V_start (V)',layout=Layout(height='auto', width = width))
    messparameter[1,0] = widgets.BoundedFloatText(value = 1,min =-40 ,max = 40,step = 1,layout=Layout(height='auto', width = width)) # Start value
    messparameter[2,0] = widgets.Label('V_stop (V)',layout=Layout(height='auto', width = width)) 
    messparameter[3,0] = widgets.BoundedFloatText(value = -3,min =-40 ,max = 40,step = 1,layout=Layout(height='auto', width = width)) #stop value
    messparameter[4,0] = widgets.Label('V_step (V)',layout=Layout(height='auto', width = width))
    messparameter[5,0] = widgets.BoundedFloatText(value = 0.1,min = -80 ,max = 80,step = 0.01,layout=Layout(height='auto', width = width)) #step value
    messparameter[6,0] = widgets.Label('Wait in Voltage Point (s)',layout=Layout(height='auto', width = width))
    messparameter[7,0] = widgets.BoundedFloatText(value = 0,min = 0 ,max = sys.float_info.max,step = 1,layout=Layout(height='auto', width = width)) #sleep value
    messparameter[8,0] = widgets.Label('Radius of The Test Stucture(um)',layout=Layout(height='auto', width = width))
    messparameter[9,0] = widgets.BoundedIntText(value = 50,min = 1 ,max = sys.float_info.max,step = 1,layout=Layout(height='auto', width = width)) #radius value
    messparameter[10,0] = widgets.Label('Observed Frequency Point',layout=Layout(height='auto', width = width))
    messparameter[11,0] = widgets.BoundedIntText(value = 2,min = 1 ,max = sys.maxsize,step = 1,layout=Layout(height='auto', width = width)) #Frequenzpunkt value
    messparameter[12,0] = widgets.Label('Observed Voltage Point',layout=Layout(height='auto', width = width))
    messparameter[13,0] = widgets.BoundedIntText(value = 20,min = 1 ,max = sys.maxsize,step = 1,layout=Layout(height='auto', width = width)) #Spannungspunkt value
    messparameter[14,0] = widgets.Checkbox(description = 'Hysterisis',value = True, indent = True, style = {'description_width': 'initial'},layout=Layout(height='auto', width = width)) #hysterisis value

    dict={
        'start':messparameter[1,0],
        'stop':messparameter[3,0],
        'step':messparameter[5,0],
        'sleep':messparameter[7,0],
        'radius':messparameter[9,0],
        'f_point':messparameter[11,0],
        'v_point':messparameter[13,0],
        'hysterisis':messparameter[14,0]
    }
    
    tab = widgets.Tab()
    tab.children = [messparameter]
    tab.set_title(0,'Voltage Parameters')
    

    return tab,dict
    
def sweep_parameter():
    width = 'auto'

    sweep_parameter= GridspecLayout(12,2)

    #title

    #first column
    sweep_parameter[0,0] = widgets.Label('Number of Frequency Points',layout=Layout(height='auto', width = width))
    sweep_parameter[1,0] = widgets.BoundedIntText(value = 4,min =1 ,max = 401,step = 1,layout=Layout(height='auto', width = width)) # Anzahl Frequenzpunkte 
    sweep_parameter[2,0] = widgets.Label('f_start (Hz)',layout=Layout(height='auto', width = width))
    sweep_parameter[3,0] = widgets.BoundedFloatText(value=100000,min = 100, max = 40e6,step = 100,layout=Layout(height='auto', width = width)) #start frequency
    sweep_parameter[4,0] = widgets.Label('f_stop (Hz)',layout=Layout(height='auto', width = width))
    sweep_parameter[5,0] = widgets.BoundedFloatText(value=1000000,min = 100, max = 40e6,step = 100,layout=Layout(height='auto', width = width)) #stop frequency
    sweep_parameter[6,0] = widgets.Label('OSC Level (V)',layout=Layout(height='auto', width = width))
    sweep_parameter[7,0] = widgets.BoundedFloatText(value=0.05,min = 10e-3, max = 1,step = 1e-3,layout=Layout(height='auto', width = width)) # Osc level
    sweep_parameter[8,0] = widgets.Label('Sweep Type',layout=Layout(height='auto', width = width))
    sweep_parameter[9,0] = widgets.RadioButtons(options = ['Logarithmic','Linear'],value = 'Linear',layout = Layout(height='auto', width = width)) #sweep type
    sweep_parameter[10,0] = widgets.Label('Integration Time',layout=Layout(height='auto', width = width)) 
    sweep_parameter[11,0] = widgets.Dropdown(options = ['Short','Medium','Long'],value = 'Medium',layout = Layout(height='auto', width = width)) # Integration time

    #second column 
    sweep_parameter[0,1] = widgets.Label('Delay Time (ms)',layout=Layout(height='auto', width = width))
    sweep_parameter[1,1] = widgets.BoundedFloatText(value = 0,min =0 ,max = 3.6e6,step = 1,layout=Layout(height='auto', width = width)) # delay time 
    sweep_parameter[2,1] = widgets.Label('Delay Aperture (%)',layout=Layout(height='auto', width = width))
    sweep_parameter[3,1] = widgets.BoundedFloatText(value=0.5,min = 0.5, max = 100,step = 0.1,layout=Layout(height='auto', width = width)) #delay aperture

    # Now continue next to sweep type
    sweep_parameter[8,1] = widgets.Label('Sweep Direction',layout=Layout(height='auto', width = width))
    sweep_parameter[9,1] = widgets.RadioButtons(options = ['Up','Down'],value = 'Up',layout = Layout(height='auto', width = width)) #sweep direction
    sweep_parameter[10,1] = widgets.Label('Averaging Number',layout=Layout(height='auto', width = width))
    sweep_parameter[11,1] = widgets.FloatLogSlider(value=4,base=2,min=0,max=8, step=1,layout = Layout(height='auto', width = width))# Averaging

    dict = {
        'nop': sweep_parameter[1,0],
        'start': sweep_parameter[3,0],
        'stop': sweep_parameter[5,0],
        'osc': sweep_parameter[7,0],
        'type': sweep_parameter[9,0],
        'integration': sweep_parameter[11,0],
        'd_time': sweep_parameter[1,1],
        'd_apperture': sweep_parameter[3,1],
        'direction': sweep_parameter[9,1],
        'averaging': sweep_parameter[11,1]
    }
    tab = widgets.Tab()
    tab.children = [sweep_parameter]
    tab.set_title(0,'Sweep\nParameters')

    return tab,dict
    

def control_panel():
    width =  'auto'
    control_panel = GridspecLayout(6,2)

    control_panel[0,:] = widgets.Button(description = "Start Measurement",layout=Layout(height='auto', width = width))
    control_panel[1,0] = widgets.Label("V-Point",layout=Layout(height='auto', width = width))
    control_panel[1,1] = widgets.Label("f-Point",layout=Layout(height='auto', width = width))

    for j in range(2):
        control_panel[2,j]= widgets.Text(disabled = True,layout=Layout(height='auto', width = width))

    control_panel[3,0] = widgets.Label("Actual Voltage",layout=Layout(height='auto', width = width))

    control_panel[4,0] = widgets.Text(disabled = True,layout=Layout(height='auto', width = width))

    measure = control_panel[0,:]

    view = {
        'v-point': control_panel[2,0],
        'f-point': control_panel[2,1],
        'v-value':control_panel[4,0]
    }

    return control_panel,measure,view


    

    

    
    
    
    
    


    
    
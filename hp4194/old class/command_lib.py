import pyvisa
import enum

class hp4194(object):

    def __init__(self,address="GPIB3::17::INSTR"):
        self.adress = address
        self.rm = pyvisa.ResourceManager()
        self.inst = self.rm.open_resource(address)
        self.inst.control_ren(5) #local lockout
        self.inst.timeout = None

    def __del__(self):
        self.inst.control_ren(0)
        self.inst.close()

    def reset(self):
        self.inst.write("RST")
        
    
    def measurement_mode(self,code):
        """
        Parameter 
        code for measurement (1-3)
        
        1:Impedance measurement('IMPEDANCE') (default)
        2:Gain-Phase measurement
        3:Impedance measurement('IMP with Z PROBE')
        """
        if code in range(1,4):
            self.inst.write(f"FNC{code}")

    def impedance_function(self,code):
        """
        Measuerement Function for impedance

        Parameter
        code: 1-20

        1: /Z/-theta (default)
        2: R-X
        3: Ls-Rs
        4: Ls-Q
        5: Cs-Rs
        6: Cs-Q
        7: Cs-D
        8: /Y/ - theta
        9: G-B
        10: Lp-G
        11: Lp-Q
        12: Cp-G
        13: Cp-Q
        14: Cp-D
        15: /Z/-Ls
        16: /Z/-Cs
        17: /Z/-Lp
        18: /Z/-Cp
        19: Lp-Rp
        20: Cp-Rp
        """
        if code in range(1,21):
            self.inst.write(f"IMP{code}")
    
    def gain_phase_function(self,code):
        """
        Measurement function for gain phase

        Parameter 
        code: 1-6

        1: Tch/Rch(dB)-theta (default)
        2: Tch/Rch-theta
        3. Tch/Rch(dB)-tau
        4: Rch-Tch(V)
        5: Rch-Tch(dBm)
        6: Rch-Tch(dBV)
        """
        if code in range(1,7):
            self.inst.write(f"GPP{code}")

    def monitor_impedance(self,code):
        """
        monitor function for impedance

        Parameter 
        code : 0-2

        0:Off (default)
        1:V(AC)
        2:I(AC)
        """
        if code in range(3):
            self.inst.write(f"IVM{code}")

    def monitor_gain_phase(self,code):
        """
        monitor function for gain-phase

        Parameter 
        Code: 0-6

        0:off (default)
        1:Rch(V)
        2:Rch(dBm)
        3:Rch(dBV)
        4:Tch(V)
        5:Tch(dBm)
        6:Tch(dBV)
        """
        if code in range(7):
            self.inst.write(f"GNM{code}")

    def sweep_parameter(self,code):
        """
        Choose parameter to sweep

        Parameter
        Code: 1-5

        1: Frequency (default)
        2: DC Bias (Impedance Measurement Only)
        3: Osc level(V)
        4: Osc level(dBm) (Linear sweep only)
        5: Osc level(dBV) (Linear sweep only)
        """
        if code in range(1,6):
            self.inst.write(f"SWP{code}")

    def sweep_type(self,code):
        """
        Sweep Type

        Parameter
        Code 1 or 2

        1: Linear (default)
        2: Log
        """
        if code in range(1,3):
            self.inst.write(f"SWT{code}")

    def sweep_direction(self,code):
        """
        Sweep Direction

        Parameter 
        code : 1 or 2

        1: Up (default)
        2: Down 
        """
        if code in range(1,3):
            self.inst.write(f"SWD{code}")

    def programmed_point_meas(self,code):
        """
        Programmed point measurement

        Parameter 
        code 0 or 1 

        0: off (default)
        1: on
        """
        if code in range(2):
            self.inst.write(f"PPM{code}")

    def sweep_markers(self):
        """
        Execute sweep between markers
        """
        self.inst.write("MKEXP")
    
    def compensation(self,code):
        """
        Compensation for impedance Measurement

        Parameter 
        code 1 or 2

        1 : Interpolation mode (default)
        2: All points mode 
        """
        if code in range(1,3):
            self.inst.write(f"CMPN{code}")

    def open_calibration(self):
        """
        Start open calibration (impedance measuremenent)
        """
        self.inst.write("ZOPEN")

    def short_calibration(self):
        """
        Start short Calibration (Impedance Measurement)
        """
        self.inst.write("ZSHRT")

    def open_calibration_status(self,code):
        """
        Code:
        0 for off (default) 
        1 for on
        """
        if code in range(1):
            self.inst.write(f"OPN{code}")

    def short_calibration_status(self,code):
        """
        Code:
        0 for off (default)
        1 for on
        """
        if code in range(1):
            self.inst.write(f"SHT{code}")


    def calibration(self,suffix):
        """
        Start different calibration and switch status (impedance measurement)

        Parameter
        Suffix:

        Send Y to start 0S calibration 
        Send Z to start 0Ohm calibration
        Send STD to start 50 Ohm (standard) Calibration
        Send 0 to deactivate Standard calibration (default)
        Send 1 to activate Standard Calibration
        """
        if suffix in ["Y","Z","STD",0,1]:
            self.inst.write(f"CAL{suffix}")

    def phase_scale_mode(self,code):
        """
        For impedance and Gain - Phase Measurements

        Parameter
        Code : 1 or 2
        
        1: Phase scale to normal mode (default)
        2: Phase scale to expansion mode 
        """
        if code in range(1,3):
            self.inst.write(f"PHS{code}")
    
    def store_offset_reference(self):
        """
        For Gain Phase Measurement
        """
        self.inst.write("OFSTR")

    def data_offset_status(self,axis,code):
        """
        Change Data offset for A and B

        Parameters
        Axis: A or B
        Code 1 (on) or 0 (off) default
        """
        if axis in ["A","B"] and code in [0,1]:
            self.inst.write(f"{axis}OF{code}")

    def display_mode(self,code):
        """
        Parameter
        Code: 1-3

        1: X-A&B (default)
        2: A-B 
        3: Table
        """
        if code in range(1,4):
            self.inst.write(f"DSP{code}")

    def autoscale(self,axis):
        """
        For X-A&B mode an axis have to be specified
        For A-B mode not (Send "")
        """
        if axis in ["","A","B"]:
            self.inst.write(f"AUTO{axis}")

    def display_data_status(self,axis,code):
        """
        Send axis A,B (X-A&B mode) or AB for (A-B mode) 
        and code 0(off) or 1(on) (default)
        """
        if axis in ['A','B',"AB"] and code in [0,1]:
            self.inst.write(f"DP{axis}{code}")

    def axis_scale(self,axis,code):
        """
        Axis A or B
        Code 1(linear)(default) or 2 (log) 
        """
        if axis in ["A","B"] and code in [1,2]:
            self.inst.write(f"{axis}SC{code}")
    
    def set_parameter_axis(self,axis,type,value):
        """
        Set Parameter for axis 

        Parameters
        Axis A or B
        Type: MIN,MAX,DIV(for scale division in linear scale only)
        value: Value of parameter
        """
        if axis in ["A","B"] and type in ["MIN","MAX","DIV"]:
            self.inst.write(f"{axis}{type}={value}")

    def graticule_status(self,code):
        """
        Code:

        0: Graticule off
        1: Graticule on
        """
        if code in range(2):
            self.inst.write(f"GRT{code}")

    def unit_display(self,code):
        """
        Code:

        0: Unit display off
        1: Unit display on (default)
        """
        if code in range(2):
            self.inst.write(f"UNIT{code}")

    def storage_mode(self,code):
        """
        Code:

        0: Storage Mode off (default)
        1: Storage Mode on
        """
        if code in range(2):
            self.inst.write(f"STRG{code}")

    def single_sweep(self):
        """
        Execute Single sweep
        """
        self.inst.write("SWM2;SWTRG")
    
     def integration_time(self,code):
         """
         Code:

         1: for short 500us (default)
         2: for medium 5ms 
         3 : for long 100ms
         """
    
    def averaging(self,number):
        if number in [1,2,4,81,16,32,64,128,256]:
            self.inst.write(f"NOA={number}")

    def set_parameter(self,parameter,value):
        """
        Accepted parameters

        START
        STOP
        STEP
        CENTER
        NOP
        
        FREQ
        BIAS
        OSC

        DTIME
        DFREQ
        """
        self.inst.write(f"{parameter}={value}")

    def return_data(self,register):
        """
        Return data from the specified register
        """
        return self.inst.query(f"{register}?")
        


        
        


    

    


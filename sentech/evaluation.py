import ipywidgets as widgets
import matplotlib.pyplot as plt 
import pandas as pd
import os
import numpy as np

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox

def information_box(information):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.attributes("-topmost", 1)

    root.update()

    #display meaagebox
    tkinter.messagebox.showinfo(message=information,parent=root)
    root.attributes("-topmost", 0)
    root.destroy()

def choose_folder():
    root = tk.Tk()
    root.withdraw()

    root.attributes('-topmost', 1)#window stays above all other applications
    root.update()

    #choose nonemty folder
    folder = tk.filedialog.askdirectory(parent = root)
    
    while folder == '':
        folder = tk.filedialog.askdirectory(parent = root)        
        
    root.destroy()
    return folder

def change_state(widgets_list):
    for widget in widgets_list:
        widget.disabled = not widget.disabled


def get_device(file):
    with open(file) as f:
        lines = f.readlines()

    if lines[1] == "System: SI ALD RWTH [ALD 007]\n":
        return 'ALD'
    elif lines[1] == "System: SI 500 RWTH Aachen [500-211]\n":
        return 'ICP-CL'
    elif lines[1] == "Anlage: SI 500 RWTH Aachen\n":
        return 'ICP-FL'
    else:
        return "" #No device detected

def read_file(file):
    device = get_device(file)
    
    if device == 'ALD':
        with open(file) as f:
            lines = f.readlines()
        start_time = lines[4]
        df = pd.read_csv(files[0],sep ='\t',skiprows = 10,encoding = 'ANSI',index_col=False)
    elif device == 'ICP-CL':
        with open(file) as f:
            lines = f.readlines()
        start_time = lines[4]
        df = pd.read_csv(file,sep ='\t',skiprows = 10,encoding = 'ANSI',index_col=False)
    elif device =='ICP-FL':
        with open(file) as f:
            lines = f.readlines()
        start_time = lines[3]
        df = pd.read_csv(file,sep ='\t',skiprows = 9,encoding = 'ANSI',index_col=False)
    else: #device = ""
        start_time = None
        df = None
    return device,df,start_time
        
        
        
dir_button = widgets.Button(description = 'Choose Folder')
plot_button = widgets.Button(description = 'Plot')
next_file = widgets.Button(description = 'Next File')
previous_file = widgets.Button(description = 'Previous file')
current_file = widgets.Text(description = 'Current File',disabled = True,value = 'No File Selected')
x_axis = widgets.Dropdown(description = 'X-Axis',disabled = True)
y_axis = widgets.Dropdown(description = 'Y-Axis')
x_scale = widgets.Dropdown(description ='X-Scale',options = ['linear','log'],value = 'linear')
y_scale = widgets.Dropdown(description ='Y-Scale',options = ['linear','log'],value = 'linear')
output = widgets.Output()

counter = 0
folder = None
files = []
df = None
start_time = None
device = None

buttons = widgets.HBox([dir_button,plot_button,previous_file,next_file])
config = widgets.HBox([x_axis,y_axis,current_file])
scale = widgets.HBox([x_scale,y_scale])

display(buttons)
display(config)
display(scale)
display(output)

all_widgets = [dir_button,plot_button,previous_file,next_file,y_axis,x_scale,y_scale]
def on_choose_folder_clicked(b):
    global counter,folder,files,df,start_time,device
    with output:
        change_state(all_widgets)
        current_file.value = 'No File Selected'
        folder = choose_folder()
        os.chdir(folder) #change current working directory 
        files = os.listdir(os.getcwd()) #get the files from the chosen directory

        # get all the valid files 
        valid_files = []
        for file in files:
            if file.endswith('.log') == True:
                device = get_device(file)
                if device != "":
                    valid_files.append(file)

        files = valid_files.copy()

        if len(files) == 0:
            information_box("No Valid Files Detected! Please Choose a New Folder!")
        else:
            counter = 0
            current_file.value = files[counter]
            device,df,start_time = read_file(current_file.value)
            options = list(df.columns.values)
            x_axis.options = [options[0]] #only the first column
            y_axis.options = options[1:] #the rest columns
            x_axis.value = options[0]
            y_axis.value = options[1]
        change_state(all_widgets)
    
dir_button.on_click(on_choose_folder_clicked)

def on_previous_file_clicked(b):
    global counter,folder,files,df,start_time,device
    with output:
        change_state(all_widgets)
        if counter>0:
            counter = counter -1
            current_file.value = files[counter]
            device,df,start_time = read_file(current_file.value)
            options = list(df.columns.values)
            x_axis.options = [options[0]] #only the first column
            y_axis.options = options[1:] #the rest columns
            x_axis.value = options[0]
            y_axis.value = options[1]
        else:
            information_box("There is no previous file!")
        change_state(all_widgets)

previous_file.on_click(on_previous_file_clicked)

def on_next_file_clicked(b):
    global counter,folder,files,df,start_time,device
    with output:
        change_state(all_widgets)
        if counter<len(files)-1:
            counter = counter +1
            current_file.value = files[counter]
            device,df,start_time = read_file(current_file.value)
            options = list(df.columns.values)
            x_axis.options = [options[0]] #only the first column
            y_axis.options = options[1:] #the rest columns
            x_axis.value = options[0]
            y_axis.value = options[1]
        else:
            information_box("There is no next file!")
        change_state(all_widgets)

next_file.on_click(on_next_file_clicked)

def on_plot_clicked(b):
    global counter,folder,files,df,start_time,device
    with output:
        change_state(all_widgets)
        if current_file.value != 'No File Selected': #there is a file to plot
            
            fig,ax = plt.subplots()
            fig.suptitle(device +" "+ start_time)
            
            if x_scale.value == 'log':
                x = np.absolute(df[x_axis.value])
                ax.set_xscale('log')
            else:
                x = df[x_axis.value]
                ax.set_xscale('linear')
             
            if y_scale.value == 'log':
                y = np.absolute(df[y_axis.value])
                ax.set_yscale('log')
            else:
                y = df[y_axis.value]
                ax.set_yscale('linear')
    
            ax.set_xlabel(x_axis.value)
            ax.set_ylabel(y_axis.value)
    
            ax.plot(x,y,color = 'b')
    
            mng = plt.get_current_fig_manager()
            mng.window.state('zoomed')
            mng.window.attributes('-topmost', 1)
            plt.show(block= True)
            
        else:
            information_box("No file to plot!")
        change_state(all_widgets)

plot_button.on_click(on_plot_clicked)
        
        
        
        

    
    
            
        
        
            
        
            
            
        
        
    
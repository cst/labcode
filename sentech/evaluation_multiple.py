import ipywidgets as widgets
import matplotlib.pyplot as plt 
import pandas as pd
import os
import numpy as np

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox

def information_box(information):
    #open dialog and hide the main window
    root = tk.Tk()
    root.withdraw()
    root.attributes("-topmost", 1)

    root.update()

    #display meaagebox
    tkinter.messagebox.showinfo(message=information,parent=root)
    root.attributes("-topmost", 0)
    root.destroy()

def choose_folder():
    root = tk.Tk()
    root.withdraw()

    root.attributes('-topmost', 1)#window stays above all other applications
    root.update()

    #choose nonemty folder
    folder = tk.filedialog.askdirectory(parent = root)
    
    while folder == '':
        folder = tk.filedialog.askdirectory(parent = root)        
        
    root.destroy()
    return folder

def change_state(widgets_list):
    for widget in widgets_list:
        widget.disabled = not widget.disabled


def get_device(file):
    with open(file) as f:
        lines = f.readlines()

    if lines[1] == "System: SI ALD RWTH [ALD 007]\n":
        return 'ALD'
    elif lines[1] == "System: SI 500 RWTH Aachen [500-211]\n":
        return 'ICP-CL'
    elif lines[1] == "Anlage: SI 500 RWTH Aachen\n":
        return 'ICP-FL'
    else:
        return "" #No device detected

def read_file(file):
    device = get_device(file)
    
    if device == 'ALD':
        with open(file) as f:
            lines = f.readlines()
        start_time = lines[4]
        df = pd.read_csv(files[0],sep ='\t',skiprows = 10,encoding = 'ANSI',index_col=False)
    elif device == 'ICP-CL':
        with open(file) as f:
            lines = f.readlines()
        start_time = lines[4]
        df = pd.read_csv(file,sep ='\t',skiprows = 10,encoding = 'ANSI',index_col=False)
    elif device =='ICP-FL':
        with open(file) as f:
            lines = f.readlines()
        start_time = lines[3]
        df = pd.read_csv(file,sep ='\t',skiprows = 9,encoding = 'ANSI',index_col=False)
    else: #device = ""
        start_time = None
        df = None
    return device,df,start_time
        
        
        
dir_button = widgets.Button(description = 'Choose Folder')
plot_button = widgets.Button(description = 'Plot')
instrument = widgets.Dropdown(description = 'Device', options =['ALD','ICP-CL','ICP-FL'],value = 'ALD')
selected_files = widgets.SelectMultiple(description = "Selected files",layout=widgets.Layout(width='1000px', height='300px'))
x_axis = widgets.Dropdown(description = 'X-Axis',disabled = True)
y_axis = widgets.Dropdown(description = 'Y-Axis')
x_scale = widgets.Dropdown(description ='X-Scale',options = ['linear','log'],value = 'linear')
y_scale = widgets.Dropdown(description ='Y-Scale',options = ['linear','log'],value = 'linear')
output = widgets.Output()

counter = 0
folder = None
files = []
df = None
start_time = None
device = None

buttons = widgets.HBox([dir_button,plot_button,instrument,selected_files])
config = widgets.HBox([x_axis,y_axis])
scale = widgets.HBox([x_scale,y_scale])

display(buttons)
display(config)
display(scale)
display(output)

all_widgets = [dir_button,plot_button,instrument,y_axis,x_scale,y_scale,selected_files]
def on_choose_folder_clicked(b):
    global folder,files,df,start_time,device
    with output:
        change_state(all_widgets)
        folder = choose_folder()
        os.chdir(folder)
        files = os.listdir(os.getcwd()) #get the files from the chosen directory
        

        # get all the valid files 
        valid_files = []
        for file in files:
            if file.endswith('.log') == True:
                device = get_device(file)
                if device == instrument.value:
                    valid_files.append(file)

        files = valid_files.copy()

        if len(files) == 0:
            information_box("No Valid Files Detected! Please Choose a New Folder!")
        else:
            device,df,start_time = read_file(files[0]) #read the first file for the columns
            options = list(df.columns.values)
            x_axis.options = [options[0]] #only the first column
            y_axis.options = options[1:] #the rest columns
            x_axis.value = options[0]
            y_axis.value = options[1]
            selected_files.options = files
            selected_files.value = [files[0]]
        change_state(all_widgets)
    
dir_button.on_click(on_choose_folder_clicked)


def on_plot_clicked(b):
    global folder,files,df,start_time,device
    with output:
        change_state(all_widgets)
        if len(selected_files.value) != 0:
            
            fig,ax = plt.subplots()
            fig.suptitle(device)
            
            if x_scale.value == 'log':
                ax.set_xscale('log')
            else:
                ax.set_xscale('linear')
             
            if y_scale.value == 'log':
                ax.set_yscale('log')
            else:
                ax.set_yscale('linear')
    
            ax.set_xlabel(x_axis.value)
            ax.set_ylabel(y_axis.value)
    
            for file in selected_files.value:
                device,df,start_time = read_file(file)         
                if x_scale.value == 'log':
                    x = np.absolute(df[x_axis.value])
                else:
                    x = df[x_axis.value]
                if y_scale.value == 'log':
                    y = np.absolute(df[y_axis.value])
                else:
                    y = df[y_axis.value]
                 
                ax.plot(x,y,label=start_time.split(":",1)[1])
                
            fig.legend()
            mng = plt.get_current_fig_manager()
            mng.window.state('zoomed')
            mng.window.attributes('-topmost', 1)
            plt.show(block= True)
            
        else:
            information_box("No file to plot!")
        change_state(all_widgets)

plot_button.on_click(on_plot_clicked)